---
title: Presentations Requirements
subtitle: MATH 202 Linear Algebra
colorlinks: true
urlcolor: Maroon
geometry: margin=1in
...

# The Presentation

As part of the evaluation process of this course,
you will give a 10 minutes presentation
on a topic related to linear algebra.

The presentations will be held on Fridays of week 5-7.
See details in the [schedule](https://prodduke-my.sharepoint.com/:x:/r/personal/xc171_duke_edu/Documents/MATH202-2023-03%20Presentation.xlsx?d=wc0faf55adc31436fb2aae894976f8d21&csf=1&web=1&e=rtbtnk).

Except for the session when you present,
you do *not* need to attend presentation meetings.

You can also choose to do a slide-show or give a whiteboard presentation.

The presentations will be recorded for grading purpose.

# Topic

You should choose a topic which

1. has not been covered in the class at the time of your presentation,
2. and is suitable for the audience of your fellow students.

Here are some suggested topics

1. Applications listed in the slides of Lecture 1.
2. Singular value decomposition. See Section 7.4 of the textbook.
3. [In Mysterious Pattern, Math and Nature Converge](https://www.quantamagazine.org/in-mysterious-pattern-math-and-nature-converge-20130205/)
4. [AI Reveals New Possibilities in Matrix Multiplication](https://www.quantamagazine.org/ai-reveals-new-possibilities-in-matrix-multiplication-20221123/)
5. [New Algorithm Breaks Speed Limit for Solving Linear Equations](https://www.quantamagazine.org/new-algorithm-breaks-speed-limit-for-solving-linear-equations-20210308/)
6. [Matrix Multiplication Inches Closer to Mythic Goal](https://www.quantamagazine.org/mathematicians-inch-closer-to-matrix-multiplication-goal-20210323/)
7. [Symmetric Polynomails](https://en.wikipedia.org/wiki/Symmetric_polynomial)
8. [When Life is Linear From Computer Graphics to Bracketology](https://www.jstor.org/stable/10.4169/j.ctt19b9kd8?turn_away=true)
9. [D-Optimal designs](https://www.itl.nist.gov/div898/handbook/pri/section5/pri521.htm)
10. [Symmetric Polynomails](https://en.wikipedia.org/wiki/Symmetric_polynomial)

:bomb: Please submit your topic in the [schedule](https://prodduke-my.sharepoint.com/:x:/r/personal/xc171_duke_edu/Documents/MATH202-2023-03%20Presentation.xlsx?d=wc0faf55adc31436fb2aae894976f8d21&csf=1&web=1&e=rtbtnk)
before week-4. Otherwise you will be given a topic.

# Exemplary talks

Here are [links](https://duke.box.com/s/lousc9o14adugoin2yu1jgjc2kye8wk6) to 
some excellent student presentations from another course.

:bulb: Some tips for preparing the talk ---

* Choose a topic that keeps your audience awake.
* Think about what the audience can get from your talk if they only listen for 3
  minutes.
* Rehearse the talk once with a friend. (See how long it take for them to start
  checking their phone.)
* Time yourself to see how long it takes.
* Expect to be interrupted during the talk.
* Prepare more than you plan to speak, just in case you go faster than expected,
  or someone you know asks a tough question. 
* Prepare to cut your talk short in case you go slower than expected.

\pagebreak{}

# Rubrics

Your presentation will graded based on which of the following category describe
it best.

### Complete (2-5pt)

\begin{tabular}{|p{2cm}|p{3cm}|p{3cm}|p{3cm}|p{3cm}|}
\hline
Criteria                 & Excellent (5pt)                                               & Good (4pt)                                      & Acceptable (3pt)                               & Needs Improvement (2pt)               \\ \hline
Topic (20\%)              & Highly relevant, deep understanding                           & Appropriate, slightly challenging               & Suitable, too simple/advanced                & Poorly suited for audience             \\ \hline
Slides (20\%)             & Well-organized, visually appealing, error-free, clear formulas & Well-organized, clear formulas, minor errors    & Generally organized, some errors             & Disorganized, numerous errors          \\ \hline
Explanation (20\%)        & Precise, easy-to-understand, strong motivation                & Mostly clear, strong motivation                 & Clear, may lack depth/motivation            & Difficult to follow, weak motivation    \\ \hline
Creativity (20\%)         & Engaging, creative, appropriate humor                         & Some creativity and humor                       & Lacks creativity or humor                   & Unengaging, devoid of creativity/humor  \\ \hline
Response (10\%)           & Confidently handles feedback, strong understanding            & Responds effectively to most questions          & Addresses some questions, may struggle       & Difficulty responding to questions      \\ \hline
Time Control (5\%)        & Within 10-minute time frame                                   & Slightly exceeds allotted time                  & Shorter than allotted time                  & Significantly shorter than allotted time\\ \hline
English Fluency (5\%)     & Excellent articulation and fluency                            & Effective articulation and fluency              & Basic proficiency in English                & Struggles with fluency and articulation \\ \hline
\end{tabular}

###  Incomplete (1pt) 

Presentation not given, but prior communication with the instructor occurred. 

###  Did Not Meet Expectations (0pt) 

Presentation not given without prior communication with the instructor. 

\pagebreak{}

## Talk ---

What I Like:

\vfill

What I suggest:

\vfill

What I want to ask  (if time permits):

\vfill

\hrule

## Talk ---

What I Like:

\vfill

What I suggest:

\vfill

What I want to ask  (if time permits):

\vfill

\hrule

## Talk ---

What I Like:

\vfill

What I suggest:

\vfill

What I want to ask  (if time permits):

\vfill
