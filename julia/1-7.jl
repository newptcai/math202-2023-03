# # Section 1.7
using LinearAlgebra, RowEchelon

## For making slides
using Latexify
copy_to_clipboard(true)

## Example 1
A = [1 4 2
    2 5 1
    3 6 0]
rref(A)

## Example 2
A = [0 1 4
    1 2 -1
    5 8 0]
rref(A)

# ## Example 1

## a.
A = [1 -3
    3 5
    -1 7]
u = [2
    -1]
A*u

## b
b = [3
    2
    -5]
rref(hcat(A, b))

## c
A \ b

## d
c = [3
    2
    5]
rref(hcat(A, c))
