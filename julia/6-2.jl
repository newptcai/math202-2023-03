# # 6.2 Orthogonal Sets
using LinearAlgebra, RowEchelon, SymPy, Plots, LaTeXStrings

## For making slides
using Latexify
copy_to_clipboard(true)

## Orthogonal sets
pyplot()
u1 = [3; 1; 1]
u2 = [-1; 2; 1]
u3 = [-1/2; -2; 7/2]
A = hcat(u1, u2, u3)
(x, y, z) = eachrow(A)
plot([0, x[1]], [0, y[1]], [0, z[1]], label=L"$u_1$", size=(500, 500), linewidth=5);
plot!([0, x[2]], [0, y[2]], [0, z[2]], label=L"$u_2$", linewidth=5);
plot!([0, x[3]], [0, y[3]], [0, z[3]], label=L"$u_3$", linewidth=5)

# ## Orthonormal columns

## U has orthonormal columns
a = sqrt(Sym(2))
x = [a; 3]
u1 = [1/a;
     1/a;
     0]
u2 = Sym.([2//3;
     -2//3;
     1//3])
U = hcat(u1, u2)

## Verify this
norm(u1)
norm(u2)
dot(u1, u2)

## Verify again
U'* U

## Verify Theorem 7
norm(U * x)
norm(x)

## Verify Theorem 7 with random vectors
x = Sym.(rand(-10:10, 2))
simplify(norm(U * x))
norm(x)

# ## Projections to $R^n$

## Example 2
u1 = [2; 5; -1]
u2 = [-2; 1; 1]
y = Sym.([1; 2; 3])

## Projection of y 
a1 = dot(y, u1)/dot(u1, u1)
a2 = dot(y, u2)/dot(u2, u2)
yhat = a1*u1 + a2*u2
z = y - yhat

## Verify the projection
dot(z, u1)
dot(z, u2)
# ## Orthonormal columns

## U has orthonormal columns
a = sqrt(Sym(2))
x = [a; 3]
u1 = [1/a;
     1/a;
     0]
u2 = [2//3;
     -2//3;
     1//3]
U = hcat(u1, u2)

## Verify this
norm(u1)
norm(u2)
dot(u1, u2)

## Verify again
U'* U

## Verify Theorem 7
norm(U * x)
norm(x)

## Verify Theorem 7 with random vectors
x = Sym.(rand(-10:10, 2))
simplify(norm(U * x))
norm(x)

# ## Projections to $R^n$

## Example 2
u1 = [2; 5; -1]
u2 = [-2; 1; 1]
y = Sym.([1; 2; 3])

## Projection of y 
a1 = dot(y, u1)/dot(u1, u1)
a2 = dot(y, u2)/dot(u2, u2)
yhat = a1*u1 + a2*u2
z = y - yhat

## Verify the projection
dot(z, u1)
dot(z, u2)
