# # Lecture 17
using LinearAlgebra, RowEchelon, SymPy

## Row space
A = [-2 -5 8 0 -17;
    1 3 -5 1 5;
    3 11 -19 7 1;
    1 7 -13 5 -3]
B = round.(Integer, rref(A))

## Visualize
A = [3 0 -1; 3 0 -1; 4 0 5]
B = round.(Integer, rref(A))