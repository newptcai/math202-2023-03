# ## 5.2 Characteristic equations
using LinearAlgebra, RowEchelon, SymPy, Random

## For making slides
using Latexify
copy_to_clipboard(true)
set_default(env=:raw)

## Example 1
A = [2 3;
3 -6]
eigvals(A)

## Example 3
@syms λ
A = [
    5 -2 6 -1;
    0 3 -8 0;
    0 0 5 4;
    0 0 0 1;
    ]
B = A - λ*I
charEq = det(B)

## TPS
@syms λ
A = [
    1 0 -1
    2 3 -1
    0 6 0
   ]
B = A - λ*I
charEq = det(B)

## Eigenvalues
@syms λ
charEq = λ^6 - 4* λ^5 - 12* λ^4
charEq1 = factor(charEq)

# ## City-suburb problem

## Find eigenvalues 
A = [.95 .03;
.05 .97]
vals = round.(eigvals(A), digits=3)

## Find and rescale the eigenvectors
vecs = eigvecs(A)
vecs = hcat([v/minimum(v) for v in eachcol(vecs)]...)

## Make x0 a linear combination of eigenvectors
x0 = [0.6; 0.4]
c = round.(vecs \ x0, digits=3)

## The solution
x(k) = vec*((vals).^k .* c)
@syms k
x(k)
