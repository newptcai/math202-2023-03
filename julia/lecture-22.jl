# # Lecture 22
using LinearAlgebra, RowEchelon, SymPy

## For making slides
using Latexify
copy_to_clipboard(true)

# ## Dynamic Systems

## eigenvalues
A = [.5 .4; -.104 1.1]
vals = eigvals(A)

## eigenvectors
vecs = eigvecs(A)
vecs = vecs ./ minimum(abs.(vecs), dims=1)

# ## Differential equations

## eigenvalue
A = [-1.5 .5; 1 -1]
vals = eigvals(A)

## eigenvectors
vecs = eigvecs(A)
vecs = vecs ./ minimum(abs.(vecs), dims=1)
