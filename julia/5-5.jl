# ## 5.2 Characteristic equations
using LinearAlgebra, RowEchelon, SymPy, Random

## For making slides
using Latexify
copy_to_clipboard(true)
set_default(env=:raw)

## Example 1
@syms λ
A = [0 -1;
1 0]
evals = eigvals(Sym.(A))
evecs = eigvecs(Sym.(A))
B = A - λ*I
charEq = det(B)
@syms xs[1:2]
B.subs(λ, evals[1]) * xs
B.subs(λ, evals[2]) * xs