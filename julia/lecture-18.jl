# # Lecture 18
using LinearAlgebra, RowEchelon, SymPy

## For making slides
using Latexify
copy_to_clipboard(true)

# ## Change of basis

## Exercise
CtoE = [1 3; -4 -5]
BtoE=[-9 -5; 1 -1]
BtoC = inv(CtoE)*BtoE

# ## Difference equations

# ### Example of Casorati matrix

## The Casorati matrix
signals(k) = [1^k (-2)^k 3^k]
casorati(k) = vcat(signals(k), signals(k+1), signals(k+2))
casorati(Sym("k"))

## Try different k
c0 = casorati(0)
rref(c0)
c1 = casorati(1)
rref(c1)

# ### Exercise of Casorati matrix

## Casorati matrix does not give any information

## The Casorati matrix
signals(k) = [k^2 2*k*abs(k)]
casorati(k) = vcat(signals(k), signals(k+1))
casorati(Sym("k"))

## try different k
c = casorati(0)
rref(c)
c = casorati(1)
rref(c)
c = casorati(-1)
rref(c)

## But the signals are independent
c = vcat(signals(-1), signals(1))
rref(c)

# ## homogeneous equations

## Example
@syms r
sol = factor(r^3-2*r^2-5*r+6)

## Fibonacci sequence
roots = sympy.real_roots(r^2-r-1)
