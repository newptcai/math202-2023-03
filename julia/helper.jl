function rationalorint(a)
    ra = rationalize(a)
    if denominator(ra) == 1
        return numerator(ra)
    else
        return ra
    end
end
