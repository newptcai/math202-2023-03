# # Lecture 25
using LinearAlgebra, RowEchelon, SymPy, Plots, LaTeXStrings

## For making slides
using Latexify
copy_to_clipboard(true)

# ## 6.3 

# ### Example 2

## Some helper functions for making plots
xs_ys(vs) = Tuple(eltype(vs[1])[vs[i][j] for i in 1:length(vs)] for j in eachindex(first(vs)))
xs_ys(v,vs...) = xs_ys([v, vs...])
xs_ys(r::Function, a, b, n=100) = xs_ys(r.(range(a, stop=b, length=n)))

## Plot
plotlyjs()
u1 = [2; 5; -1]
u2 = [-2; 1; 1]
y = [1; 2; 3]
o = [0; 0; 0]
hy1 = dot(u1, y)/norm(u1)^2*u1
hy2 = dot(u2, y)/norm(u2)^2*u2
hy = hy1 + hy2
plot(xs_ys(o, u1), 
    legend=:topleft,
    label=L"u_1", 
    size=(500, 500), linewidth=1, 
    xlabel=L"x_1", ylabel=L"x_2", zlabel=L"x_3",
    extra_plot_kwargs = 
    KW(:include_mathjax => "cdn"))
plot!(xs_ys(o, u2), label=L"u_2", linewidth=1)
plot!(xs_ys(o, y), label=L"y", linewidth=5)
plot!(xs_ys(o, hy1), label=L"proj_{u_1} y", linewidth=5)
plot!(xs_ys(o, hy2), label=L"proj_{u_1} y", linewidth=5)
plot!(xs_ys(o, hy), label=L"proj_{W} y", linewidth=5)
plot!(xs_ys(y, hy1), color=:gray, primary = false,
    linewidth=1, line=:dash)
plot!(xs_ys(y, hy2), color=:gray, primary = false,
    linewidth=1, line=:dash)
plot!(xs_ys(y, hy), color=:gray, primary = false,
    linewidth=1, line=:dash)
plot!(xs_ys(hy1, hy), color=:gray, primary = false,
    linewidth=1, line=:dash)
plot!(xs_ys(hy2, hy), color=:gray, primary = false,
    linewidth=1, line=:dash)
l = 11
xs = range(-2, 2, length=l)
ys = range(0, 5, length=l)
A = hcat(u1, u2)
A1 = A[1:2, :]
zs = fill(BigFloat(0.0), (l, l))
for i in 1:l
    for j in 1:l
        c = A1 \ [xs[i], ys[j]]
        zs[j, i] = c[1] * u1[3] + c[2] * u2[3]
    end
end
wireframe!(xs, ys, zs, label=L"\mathrm{Span}(u_1,u_2)")

##

# ## 6.4 

# ### Example 4

## 
A = [1 0 0;
    1 1 0;
    1 1 1;
    1 1 1]
F = qr(A)
Q = Matrix(F.Q)
R = Matrix(F.R)
A1 = round.(Q*R)
A == A1