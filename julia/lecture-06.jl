# # Lecture 06

using LinearAlgebra, RowEchelon

## The last exercise
## The two columns are linearly independent, so it is onto
## There are only two columns and three rows.
## We need 3 rows to have 3 pivotal variables 
## so that the system is conssistent.
A = [36 51 13 33;
    52 34 74 45;
    0 7 1.1 3]
A[:, 1:3] \ A[:, 4]

# ## Matrix multiplication
A = [5 1;
    3 -2]
B = [2 0;
    4 3]
A * B