# # 2.5 Matrix Factorization
using LinearAlgebra, RowEchelon, Random, Latexify

# Setup Latexify
copy_to_clipboard(true)
set_default(env = :raw)

# help function
include("./helper.jl")

# ## LU Partition

# ### An example

## LU Factorize
A = [3 -7 -2 2; -3 5 1 0;6 -4 0 -5; -9 5 -5 12];
F = lu(A, NoPivot())

## Check
A == F.L * F.U

## Solve equations -- method 1
b = [-9; 5; 7; 11];
x1 = round.(A \ b)

## Solve questions -- LU way
y = F.L \ b;
x2 = F.U \ y

# ### The idea

## Reduce A to echelon form
A = reshape(Vector(1:9), (3,3))
E1 = [1 0 0; -2 1 0; -3 0 1]
A1 = E1 * A
E2 = [1 0 0; 0 1 0; 0 -2 1]
A2 = E2 * A1

# ### An example of the algorithm

## Factorize
A = [2 4 -1 5 -2;
     -4 -5 3 -8 1;
     2 -5 -4 1 8;
     -6 0 7 -3 1]

## Step 1
E1 = [1 0 0 0;
      2 1 0 0;
      -1 0 1 0;
      3 0 0 1];
A1 = E1 * A

## Step 2
E2 = [1 0 0 0;
      0 1 0 0;
      0 3 1 0;
      0 -4 0 1];
A2 = E2 * A1

## Step 3
E3 = [1 0 0 0;
      0 1 0 0;
      0 0 1 0;
      0 0 -2 1];
A3 = E3 * A2

# ### Think-Pair-Share
A = [6 9;
     4 5]
F = lu(A)
L = rationalize.(inv(F.P) * F.L)
U = rationalize.(F.U)
A == L * U

# ### When you need permutation
Random.seed!(1234)
A = rand(-3:3, (3,3))
F = lu(A)
L = rationalize.(inv(F.P) * F.L)
U = rationalize.(F.U)
A == L * U

