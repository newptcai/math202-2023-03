# # 2.7 Applications to computer graphics
using LinearAlgebra, RowEchelon, Random, Latexify

# Setup Latexify
copy_to_clipboard(true)
set_default(env = :raw)

# help function
include("./helper.jl")


## Example 3
A = [1 .25; 0 1]
S = [0.75 0; 0 1]
A * S
S * A
