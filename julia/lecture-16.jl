# # Lecture 15
using LinearAlgebra, RowEchelon, SymPy

## Basis of P_3
A = [1 1 2 6;
    0 -1 -4 -18;
    0 0 1 9;
    0 0 0 -1]
A1 = inv(A)
A * A1