# # Section 1.2
using LinearAlgebra, RowEchelon, SymPy

## For making slides
using Latexify
copy_to_clipboard(true)

## Get a random matrix
using Random
Random.seed!(1111)
A = rand(0:4, (4, 6))

## Example 4 --- Solving linear equation
A = [1 6 2 -5 -2 4;
     0 0 2 -8 -1 3;
     0 0 0 0 1 7]
rref(A)
