# ## 5.3 Diagonalization
using LinearAlgebra, RowEchelon, SymPy, Random

## For making slides
using Latexify
copy_to_clipboard(true)
set_default(env=:raw)

## Power
D = [5 0; 0 3]
D^2
D^3
D^4
@syms k
D^k

## Diagonalize
A = Sym.([1 3 3;
    -3 -5 -3;
    3 3 1])
D = diagm([-2; -2; 1])
P = eigvecs(Sym.(A))
A1 = P*D*inv(P)
A == A1

## Diagnolize
A = [2 4 3;
    -4 -6 -3;
    3 3 1]
D = diagm(real.(eigvals(A)))
P = real.(eigvecs(A)) # Not invertible

## Diagnolizable?
A = [5 -8 1;
    0 0 7;
    0 0 -2]
eigvals(A)

## Less than n eigenvalues
A = [5 0 0 0;
    0 5 0 0;
    1 4 -3 0;
    -1 -2 0 -3]
D = diagm(eigvals(A))
P = eigvecs(A)
A1 = round.(P*D*(P^-1))
A == A1

## Less than n eigenvalues
Random.seed!(1234)
A = rand(1:100, (7,7))
A = LowerTriangular(A)
D = diagm(eigvals(A))
P = eigvecs(A)
A1 = round.(P*D*(P^-1))
A == A1

## Exercise
D = [5 0; 0 3]
P = [1 1; -1 -2]
A = P*D*P^-1
D1 = [3 0; 0 5]
P1= [1 1; -2 -1]
A1 = P1*D1*P1^-1
A == A1
