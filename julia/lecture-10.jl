# # Lecture 10
using LinearAlgebra, RowEchelon

## For making slides
using Latexify
copy_to_clipboard(true)

# ## Defintion of determinants

## Example 1
A = [1 5 0; 2 4 -1; 0 2 0]
det(A)

# ## Triangular matrices

## Brute force
A = [3 -7 8 9 -6; 
    0 2 -5 7 3; 
    0 0 1 5 0;
    0 0 0 4 -1;
    0 0 0 0 -2
    ]
det(A)

## Use the diagnol
prod(diag(A))

# ## Properties

## Pick a random 5×5 matrix A
using Random
Random.seed!(1232)
A = rand(-1:2, (5,5))

## Compute its determinant
det(A)

## Row replacement
A[1,:] .+= -1*A[3,:]; A
det(A)

## Row replacement
A[5,:] .+= 2*A[1,:]; A
det(A)

## Row replacement
A[4,:] .+= 3*A[2,:]; A
det(A)

## Interchange rows
A = A[[2,1,3,4,5], :]
det(A)

## Interchange rows
A = A[[1,5,3,4,2], :]
det(A)

## Interchange rows
A = A[[4,2,3,1,5], :]
det(A)

## Scale a row
A[2,:] =  3 * A[2,:]; A
det(A)

## Application
A = [1 -4 2; -2 8 -9; -1 7 0]
det(A)

## Transpose
A = rand(-5:5, (4,4))
det(A)
det(A')

## Multiplication
A = rand(-5:5, (4,4))
B = rand(-5:5, (4,4))
det(A)
det(B)
det(A)*det(B)
det(A*B) 