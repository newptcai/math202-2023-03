# # Section 1.6
using LinearAlgebra, RowEchelon, SymPy, Latexify

include("./helper.jl")

## For making slides
using Latexify
copy_to_clipboard(true)

# # Propane gas

## Reduce to echelon form
@syms x[1:4]
A = [3 0 -1 0
    8 0 0 -2
    0 2 -2 -1]
A1 = rref(A)

## The equations
xs = sympy.Matrix([x[i] for i in 1:4])
@syms xs[1:4]
A2 = A1 * xs

## Solve the equations
sympy.solve(A2, xs[1:3])
