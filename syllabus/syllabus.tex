%! TEX program = lualatex
\documentclass[11pt]{article}

% Misc packages
\usepackage{ifthen}
\usepackage{xspace}
\usepackage{ifpdf}
\usepackage{framed}
\usepackage{amsmath}
\usepackage{amssymb}

% Colours
\PassOptionsToPackage{dvipsnames,svgnames,x11names,table}{xcolor}
\usepackage{xcolor}
\definecolor{dku-blue}{HTML}{003A81}
\definecolor{dku-green}{HTML}{006F3F}

% Fonts
\usepackage[no-math]{fontspec}
%\usepackage{xunicode}
\usepackage{xltxtra}
\defaultfontfeatures{Mapping=tex-text,Numbers=Lining}
% \newfontfamily\cnfont{Hiragino Sans GB}
\AtBeginDocument{%
    \setmainfont{Carlito}%
    \setsansfont[
        BoldFont={Carlito}]{Carlito}%
    \raggedbottom%
}
% \usepackage[sf]{noto}

% Multi-language support
\usepackage{polyglossia}
\setmainlanguage{english}

% Links
\usepackage{url}
\renewcommand{\UrlBreaks}{\do\.\do\@\do\\\do\/\do\!\do\_\do\|\do\;\do\>\do\]%
    \do\)\do\,\do\?\do\'\do+\do\=\do\#\do\-}
\renewcommand{\UrlFont}{\normalfont}
\usepackage{hyperref} % Required for adding links	and customizing them
\hypersetup{colorlinks, breaklinks, urlcolor=Maroon, linkcolor=Maroon,
    citecolor=Green} % Set link colors
\hypersetup{%
    pdfauthor=Xing Shi Cai,%
    pdftitle=MATH 202 Linear Algebra Syllabus}

% Geometry
\usepackage[a4paper,
    includehead,tmargin=2cm,nohead,
    hmargin=2cm,
    includefoot,foot=1.5cm,bmargin=2cm]{geometry}

% Tabular
\usepackage{tabularx}
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{array}
\newcommand\gvrule{\color{lightgray}\vrule width 0.5pt}

% From pandoc
\renewcommand{\tabularxcolumn}[1]{m{#1}}
\usepackage{calc} % for calculating minipage widths
% Correct order of tables after \paragraph or \subparagraph
\usepackage{etoolbox}
\makeatletter
\patchcmd\longtable{\par}{\if@noskipsec\mbox{}\fi\par}{}{}
\makeatother
% Allow footnotes in longtable head/foot
\IfFileExists{footnotehyper.sty}{\usepackage{footnotehyper}}{\usepackage{footnote}}
\makesavenoteenv{longtable}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
    \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
    \usepackage{selnolig}  % disable illegal ligatures
\fi

% For including pictures from current directory
\graphicspath{.}

% Misc
\usepackage{enumitem}
\usepackage{parskip}
\usepackage{hanging}

\newcommand{\push}{\hangpara{2em}{1}}

% Section titles
\usepackage{titlesec}
\titleformat{\section}{\raggedright\large\bfseries\color{dku-blue}}{}{0em}{}[{\titlerule[\heavyrulewidth]}]
\titleformat{\subsection}{\raggedright\itshape\color{dku-blue}}{}{0em}{}

% Emoji
\usepackage{emoji}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{tabularx}{\textwidth}{Xm{6cm}}
    \arrayrulecolor{dku-blue}\toprule\vspace{6pt}
    {\bfseries\color{dku-blue}
        {\Large MATH 202} \newline\newline
        {\huge Linear Algebra} \newline\newline
        {\Large Spring 2023, Session 4}}
     & \includegraphics[width=6cm]{dku-logo.pdf} \\
    \bottomrule
\end{tabularx}

\vspace{1em}

\textcolor{dku-blue}{\rule{\textwidth}{1pt}}

{
    \setlength{\parskip}{0.2em}
    \textbf{Class meeting time:}
    See DKU Hub.

    \textbf{Academic credit:} 4 DKU credits.

    \textbf{Course format:}
    Lectures.

    \textbf{Asynchronous study:}
    For students who cannot attend the lectures in person or remotely, slides and video
    recordings will be provided.

    \textbf{Office hour:}
    Two hours per week. In person and on Zoom.

    \textbf{Last update:} \today{}
}

\vspace{-1em}\textcolor{dku-blue}{\rule{\textwidth}{1pt}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Instructor's information}

Dr.\ \href{https://newptcai.gitlab.io/}{Xing Shi Cai}, Assistant Professor of
Mathematics, Duke Kunshan University

Email: \href{mailto:xingshi.cai@dukekunshan.edu.cn}{xingshi.cai@dukekunshan.edu.cn}

Dr.\ Cai received his PhD in computer science at McGill University in Canada.
After that he worked as postdoc researcher at Uppsala University in Sweden.
His main research interest is in applying probability theory in the analysis of
algorithms and complex networks.

\section*{What is this course about?}

This is an introductory college linear algebra course.
Linear algebra is the study of systems of linear equations, vector spaces, and
linear transformations.
Solving systems of linear equations is a basic tool in mathematics used for solving
problems in science, engineering, business, and many other fields.
Main topics of this course include systems of linear equations and elementary row
operations, Euclidean $n$-space and subspaces, linear transformations and matrix
representations, Gram-Schmidt orthogonalization process, determinants, eigenvectors
and eigenvalues, and applications.
The content of this course is essential to almost all areas of mathematics,
engineering, computer science and other data-focused sciences and research.

\section*{What background knowledge do I need before taking this course?}

Prerequisite: Math 101 or Math 105.

\section*{What will I learn in this course?}

Upon successful completion of the course, students will be able to

\begin{itemize} 
    \item Perform matrix algebra, apply Gaussian elimination and
          interpret the resulting matrix and describe the solution set to a system of linear
          equations.
    \item
          State, interpret, and apply key definitions and theorems, including:
          vector spaces, subspaces, linear independence, basis, dimension,
          linear transformations and corresponding matrix representations, the
          Invertible Matrix Theorem, the Rank and Nullity Theorem, etc.
    \item
          Understand definition and properties of determinants, and compute the
          determinant of a given matrix.
          Use Cramer's Rule to solve certain systems of linear equations, based on the
          calculations of determinants.
    \item
          Find eigenvalues and eigenvectors, and diagonalize matrices.
    \item
          Apply orthogonality and projections to solve geometric or algebraic
          problems, including Gram-Schmidt orthogonalization and least squares
          solutions.
    \item
          Use properties and results of matrix algebra, vector spaces, linear
          transformations, etc., to construct short proofs of statements in
          abstract settings.
\end{itemize}

\section*{What will I do in this course?}

\subsection*{Lectures}

You will attend four lectures each week.

\subsection*{Recitation}

There will be an one-hour recitation each week lead by a TA.
Attendance is optional but highly recommended.

\subsection*{WeBWork}
After the last class of each week,
you will be given one computerized assignment on WeBWork (automatically graded).

\subsection*{Assignment}
There are going to be one assignment set given for each lecture,
but your assignment will not be collected.

\subsection*{Quizzes}
Except for week 1 and 4,
there will be an in-class quiz at the beginning of each Thursday's lecture.

\subsection*{Exam}

There are going to be two exams ---
one midterm exam in week 4,
and one final exam in week 8.

\subsection*{Presentation}
You will select a problem related to linear algebra and present it to the
instructor in 15 minutes.
This will be a group project if there are more than 20 students in class.

\section*{How can I prepare for the class sessions to be successful?}

To succeed, you should be prepared to devote 4 hours to this
course on a daily basis. You are strongly encouraged to use the tutoring
resources of ARC, to work with classmates, to seek information online,
and to contact instructors in a timely manner for additional help as
needed.

\section*{What required texts, materials, and equipment will I need?}

\textbf{\emph{Linear Algebra and Its Applications}~6th Edition,}
David C.
Lay, Steven R.
Lay, Judi J.
McDonald, (E-book will be purchased through the university)

Library textbook contact: Xue QIU,
\href{mailto:xue.qiu@dukekunshan.edu.cn}{\nolinkurl{xue.qiu@dukekunshan.edu.cn}} , Office: AB 2112

The access method of the textbook will be sent to your by our library staffs in the
first week of the course.

\section*{How will my grade be determined?}

Course grades will be assigned according to a standard 10-pt scale:

\begin{longtable}[h]{@{}llll@{}}
\toprule
Grades & Percentage & C+ & [77\%, 80\%) \\
\midrule
\endhead
A+ & [98\%, 100\%] & C & [73\%, 77\%) \\
A & [93\%, 98\%) & C- & [70\%, 73\%) \\
A- & [90\%, 93\%) & D+ & [67\%, 70\%) \\
B+ & [87\%, 90\%) & D & [63\%, 67\%) \\
B & [83\%, 87\%) & D- & [60\%, 63\%) \\
B- & [80\%, 83\%) & F & [0\%, 60\%) \\
\bottomrule
\end{longtable}

\emoji{bomb}\emoji{bomb}\emoji{bomb} Note that---

\begin{enumerate}
    \item Grades are rounded downwards, e.g., 92.99 will become to 92 (A-).
    \item Petitions for increasing grades will \emph{never} get replies.
    \item Regrade requests will be \emph{rejected} unless the grader misread an answer.
    \item Grades will \emph{not} be curved at the end of the course.
\end{enumerate}

The course grade will be based on:

\begin{itemize}
    \item Quizzes: 25\%
    \item WeBWork: 10\%
    \item Midterm exam: 25\%
    \item Final exam: 30\%
    \item Presentation: 10\%
\end{itemize}

A maximum of 1\% bonus points will be given to students who actively participate in
online discussions.

\section*{What are the course policies?}

\subsection*{Q \& A}

Questions \emph{must} be posted on
\href{https://edstem.org}{Ed-Discussion}.
Emails will \emph{not} get replied.

\subsection*{Remote learning}

If you are not able to come to classes or exams in-person, you will need
to send the instructor the permission from the Dean of Undergraduate Studies,
Failing to do so will result in falling the course.


\subsection*{WeBWork}

We will use \href{http://webwork.dukekunshan.edu.cn/webwork2/}{WeBWork}
for computerized assignments. Login with your NetID at

\hspace{2em}\href{http://webwork.dukekunshan.edu.cn/webwork2}{\texttt{http://webwork.dukekunshan.edu.cn/webwork2}}

To access it, you \textbf{must} be on campus or use DKU VPN.
Please make sure your VPN is working.

You can learn how to use WeBWork by checking

\begin{itemize}
\tightlist
\item
  \href{https://www.youtube.com/watch?v=LgmLcnJS-qI}{A tutorial on
  YouTube}
\item
  \href{https://webwork.maa.org/wiki/Completing_homework_online}{How to
  complete homework online}
\item
  \href{https://webwork.maa.org/wiki/Mathematical_notation_recognized_by_WeBWorK}{Mathematical
  notation recognized by WeBWorK}
\end{itemize}

\subsection*{Assignments}

You solutions to assignments given in each lecture will not be collected.
But the problems will appear in the next week's quiz.

\subsection*{Quizzes}

To avoid that one single quiz being too hard,
a quiz with median grade lower than 65\% will have one retake quiz.
The final grade of this quiz will be taken as 
the max of the original grade and the retake grade.

\subsection*{Exams}

All exams will be closed book.
Books, notes, collaborations, calculators, the Internet, 
and other aids are \emph{not} allowed during exams.
Cheating for the first time will result in getting zero points in an exam.
Repeated infractions will cause failing the course.

We do not give make-up exams for any reason if you miss a midterm exam.
Thus, missing a midterm exam is a very serious matter.
An unexcused absence from any exam will be counted as a zero.
Excuses may be accepted, at the discretion of the instructor 
(for example, absence due to pre-planned academic field trips, illness on the day of the exam), 
and supporting documentation must be submitted to the instructor in advance if possible.
If an excuse is approved, then 
\begin{equation*} 
\text{the percentage you get on final exam} \times \text{midterm exam total possible points} 
\end{equation*} 
will be counted as a midterm grade.

Final exam must be taken during the assigned time.
Make-up final exam (if approved) will take place at a certain time in the following
semester/session arranged by Dean's office and the instructor.

Please refer to \href{https://undergrad.dukekunshan.edu.cn/en/undergraduate-bulletin}{Undergraduate Bulletin} and visit the
\href{https://dukekunshan.edu.cn/en/academics/advising}{Office of Undergraduate Advising website} for DKU course
policies and guidelines.

\subsection*{Academic Integrity}
As a student, you should abide by the academic honesty standard of the Duke Kunshan
University.
Its Community Standard states: ``Duke Kunshan University is a community comprised of
individuals from diverse cultures and backgrounds.
We are dedicated to scholarship, leadership, and service and to the principles of
honesty, fairness, respect, and accountability.
Members of this community commit to reflecting upon and upholding these principles
in all academic and non-academic endeavors, and to protecting and promoting a
culture of integrity and trust.''
For all graded work, students should pledge that they have neither given nor
received any unacknowledged aid.

\subsection*{Academic Policy \& Procedures}
You are responsible for knowing and adhering to academic policy and procedures as
published in University Bulletin and Student Handbook.
Please note, an incident of behavioral infraction or academic dishonesty (cheating
on a test, plagiarizing, etc.) will result in immediate action from me, in
consultation with university administration (e.g., Dean of Undergraduate Studies,
Student Conduct, Academic Advising).
Please visit the Undergraduate Studies website for additional guidance related to
academic policy and procedures.
Academic integrity is everyone's responsibility.

\subsection*{Academic Disruptive Behavior and Community Standard}
Please avoid all forms of disruptive behavior, including but not limited
to: verbal or physical threats, repeated obscenities, unreasonable
interference with class discussion, making/receiving personal phone
calls, text messages or pages during class, excessive tardiness, leaving
and entering class frequently without notice of illness or other
extenuating circumstances, and persisting in disruptive personal
conversations with other class members. Please turn off phones, pagers,
etc.\ during class unless instructed otherwise. Laptop computers may be
used for class activities allowed by the instructor during synchronous
sessions. If you choose not to adhere to these standards, I will take
action in consultation with university administration (e.g., Dean of
Undergraduate Studies, Student Conduct, Academic Advising).

\subsection*{Academic Accommodations}
If you need to request accommodation for a disability, you need a signed
accommodation plan from Campus Health Services, and you need to provide
a copy of that plan to me. Visit the Office of Student Affairs website
for additional information and instruction related to accommodations.

\section*{What campus resources can help me during this course?}

\subsection*{Academic Advising and Student Support}
Please consult with me about appropriate course preparation and
readiness strategies, as needed. Consult your academic advisors on
course performance (i.e., poor grades) and academic decisions (e.g.,
course changes, incompletes, withdrawals) to ensure you stay on track
with degree and graduation requirements. In addition to advisors, staff
in the Academic Resource Center can provide recommendations on academic
success strategies (e.g., tutoring, coaching, student learning
preferences). All ARC services will continue to be provided online.
Please visit the
\href{https://dukekunshan.edu.cn/en/academics/advising}{Office
of Undergraduate Advising website} for additional information related
to academic advising and student support services.

\subsection*{Writing and Language Studio}
For additional help with academic writing---and more generally with
language learning---you are welcome to make an appointment with the
Writing and Language Studio (WLS). To accommodate students who are
learning remotely as well as those who are on campus, writing and
language coaching appointments are available in person and online. You
can register for an account, make an appointment, and learn more about
WLS services, policies, and events on the
\href{https://dukekunshan.edu.cn/en/academics/language-and-culture-center/writing-and-language-studio}{WLS
website}. You can also find writing and language learning resources on
the \href{https://sakai.duke.edu/x/mQ6xqG}{Writing \&
Language Studio Sakai site}.

\subsection*{Online resources}

For math related questions, the follows websites may be helpful:
\begin{itemize}
\item
  \href{https://math.stackexchange.com/}{Math Stack
  Exchange} --- Ask questions about mathematics and get answers from
  other users.
\item
  \href{https://mathworld.wolfram.com/}{Wolfram Mathworld}
  --- A good place to look up mathematical definitions.
\end{itemize}

\subsection*{What is the expected course schedule?}

\emoji{bomb} Exact topics covered in each week may subject to change.

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.20}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.80}}@{}}
\toprule
\endhead
Week 1 
& 
1.1 Systems of Linear Equations

1.2 Row Reduction and Echelon Forms 

1.3 Vector Equations

1.4 The Matrix Equation $A x=b$.

1.5 Solutions Sets of Linear Systems

1.7 Linear Independence
\\
\midrule
Week 2 
&
1.8 Introduction to Linear Transformations

1.9 The Matrix of a Linear Transformation

1.10 Applications

2.1 Matrix Operations

2.2 The Inverse of a Matrix

2.3 Characterizations of Invertible Matrices

2.4 Partitioned Matrices
\\
\midrule
Week 3 
& 
2.5 Matrix Factorizations

2.6 The Leontief Input-Output Model

2.7 Application to Computer Graphics

3.1 Introduction to Determinants

3.2 Properties of Determinants

3.3 Cramer's Rule
\\
\midrule
Week 4 
& 
4.1 Vector Spaces and Subspaces

4.2 Null Spaces, Column Spaces, and Linear Transformations

4.3 Linearly Independent Sets; Bases

4.4 Coordinate Systems

\emoji{bomb} Midterm Exam (Week 1-3)
\\
\midrule
Week 5 
& 

4.5 The Dimension of a Vector Space

4.6 Change of Basis

4.7-4.8 Applications
\\
\midrule
Week 6 
& 
5.1 Eigenvectors and Eigenvalues

5.2 The Characteristic Equation

5.3 Diagonalization

5.4 Eigenvectors and Linear Transformations

5.6 Discrete Dynamical Systems

5.7 Applications \emoji{thinking-face}
\\
\midrule
Week 7 
& 
6.1 Inner Product, Length, and Orthogonality

6.2 Orthogonal Sets

6.3 Orthogonal Projections

6.4 The Gram-Schmidt Process

6.5 Least-Squares Problems

6.6 Applications
\\
\midrule
Week 8 
& \emoji{bomb} Final exam (Week 4-7)
\\
\bottomrule
\end{longtable}
\end{document}
