# MATH202 -- Linear Algebra

This repository contains materials for the course MATH202 (Linear Algebra)
taught at [Duke Kunshan University](https://dukekunshan.edu.cn/) 
by [Xing Shi Cai](https://newptcai.gitlab.io)
starting from March 2023.

See [`pdf`](./pdf) folder for the syllabus, slides, quizzes etc.

You are free to use any material here. But this is *not* the official course website.
