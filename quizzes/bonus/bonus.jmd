---
title: MATH 202 Linear Algebra
subtitle: Final Exam Bonus Questions
author: "Instructor Xing Shi Cai"
date: 2023-05-15
fontsize: 12pt
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using LinearAlgebra
#using Plots
using Random
#using Distributions
#using DataFrames
using SymPy
using RowEchelon
# Set default for Latexify
set_default(starred = true)

function print_matrix(A)
    print(sympy.latex(Sym.(A)))
end

function print_empty_matrix(m, n; w=3)
    row = join(fill("\\underline{\\hspace{$(w)cm}}", n), " & ")
    matrix = join(fill(row, m), " \\\\\n")
    print("""
        \\begin{bmatrix}
            $matrix
        \\end{bmatrix}"""
    )
end

function print_latex(ex)
    print(sympy.latex(ex))
end

# Generate a random m by n integer matrix with elements from -5 to 5
function random_matrix(m=3, n=3)
    return rand(-5:5, m, n)
end

function random_matrix_with_rank(matrix_rank, m=3, n=3)
    while true
        global A = random_matrix(m, n)
        if rank(A) == matrix_rank
            break
        end
    end
    return A
end

function column_space_basis(A::Matrix{Int})
    # Compute the RREF of A
    R = rref(A)

    # Find the rank of A
    r = rank(A)

    # Find the pivot columns
    pivot_cols = Int64[]
    for i in 1:r
        col = findfirst(==(1), R[i, :])
        if col !== nothing
            push!(pivot_cols, col)
        end
    end

    # Extract the basis of the column space
    basis = A[:, pivot_cols]

    return basis
end

function gram_schmidt(vectors)
    result = []
    for v in vectors
        for u in result
            v -= dot(v, u) / norm(u) * u
        end
        push!(result, v/norm(v))
    end
    return result
end
```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks in the toilet.
\end{itemize}

\smallskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

\begin{tcolorbox}
\large{}
\emoji{bomb} To write a proper proof, it is necessary to provide both equations and explanatory statements.
Make sure to reference relevant theorems from the textbook in your proof, and avoid any grammar or spelling errors as they may result in point deductions.
\end{tcolorbox}

\pagebreak{}

# \emoji{slightly-smiling-face}

```julia; results="hidden"

@syms ϕ
function makeA1(phi)
    [
    1//2 0 0
    -tan(phi/2)//2 1//2 0
    0 0 1
    ]
end
function makeA2(phi)
    [
    1 sin(phi) 0
    0 1 0
    0 0 1
    ]
end
function makeA3(phi)
    [
    1 0 0
    -tan(phi/2) 1 0
    0 0 1
    ]
end

```
Consider three linear transformations of homogeneous coordinates

\begin{equation*}
    A_1 =
    `j print(sympy.latex(makeA1(ϕ)))`
    ,
    \qquad
    A_2 =
    `j print(sympy.latex(makeA2(ϕ)))`
    ,
    \qquad
    A_3 =
    `j print(sympy.latex(makeA3(ϕ)))`
    .
\end{equation*}

## a

If $\phi = \pi/2$, 
how does applying $A_1$, $A_2$ and $A_3$ 
sequentially
transform
the \emoji{slightly-smiling-face} below on the left-hand-side?
Please draw your answer below on the right-hand-side.

\begin{figure}[htpb]
    \centering
\begin{tikzpicture}[scale=1]
    \filldraw[white] (-3.5,-3.5) rectangle (3.5,3.5);
    % Draw the grid
    \draw[step=1cm,gray,very thin] (-3,-3) grid (3,3);
    % Draw the x and y axes
    \draw[thick,->] (-3.5,0) -- (3.5,0) node[right] {$x$};
    \draw[thick,->] (0,-3.5) -- (0,3.5) node[above] {$y$};
    % Draw the face
    \fill[yellow] (0,0) circle (2);
    \fill[white] (-0.7,0.7) circle (0.5);
    \fill[white] (0.7,0.7) circle (0.5);
    \draw (-1,-1) .. controls (-1.2,-1.6) and (1.2,-1.6) .. (1,-1);
\end{tikzpicture}
\begin{tikzpicture}[scale=1]
    \filldraw[white] (-3.5,-3.5) rectangle (3.5,3.5);
    % Draw the grid
    \draw[step=1cm,gray,very thin] (-3,-3) grid (3,3);
    % Draw the x and y axes
    \draw[thick,->] (-3.5,0) -- (3.5,0) node[right] {$x$};
    \draw[thick,->] (0,-3.5) -- (0,3.5) node[above] {$y$};
    % Draw the face
    \begin{scope}[rotate around={-90:(0,0)}, scale=1/2]
        \fill[yellow] (0,0) circle (2);
        \fill[white] (-0.7,0.7) circle (0.5);
        \fill[white] (0.7,0.7) circle (0.5);
        \draw (-1,-1) .. controls (-1.2,-1.6) and (1.2,-1.6) .. (1,-1);
    \end{scope}
\end{tikzpicture}
\end{figure}

```julia; results="hidden"

A1 = makeA1(sympy.pi/2)
A2 = makeA2(sympy.pi/2)
A3 = makeA3(sympy.pi/2)
A4 = A3*A2*A1

```

:bulb: When $\phi = \pi$, we have
\begin{equation*}
    A_1 = `j print(sympy.latex(A1))`,
    \qquad
    A_2 = `j print(sympy.latex(A2))`,
    \qquad
    A_3 = `j print(sympy.latex(A3))`.
\end{equation*}
Thus
\begin{equation*}
    A_3 \times A_2 \times A_1 = `j print(sympy.latex(A4))`
\end{equation*}

## b

What does applying the linear transformation $A_1$, $A_2$ and $A_3$ sequentially
do in general?

Answer: 
<!---
\underline{\hspace{8cm}}
--->
\underline{Rotate clockwise by $\phi$ radians and scale by a factor of $1/2$.}

## c

Justify you answer in part **b**.

Answer:

```julia; results="hidden"

A1 = makeA1(ϕ)
A2 = makeA2(ϕ)
A3 = makeA3(ϕ)
A4 = sympy.simplify(A3*A2*A1)
A4 = A4.rewrite(sympy.exp).simplify()
A4 = convert(Matrix{Sym}, A4)
e1 = [1; 0; 1]
e2 = [0; 1; 1]
e1rotate = convert(Matrix{Sym}, sympy.simplify(A4 * e1))
e2rotate = convert(Matrix{Sym}, sympy.simplify(A4 * e2))

```

We have
\begin{equation*}
    A_4 
    = A_3 \times A_2 \times A_1
    = 
    `j print(sympy.latex(A4))`
\end{equation*}
Since
\begin{equation*}
    A_4
    `j print(sympy.latex(sympy.Matrix(e1)))`
    =
    `j print(sympy.latex(e1rotate))`
    ,
\end{equation*}
and
\begin{equation*}
    A_4
    `j print(sympy.latex(sympy.Matrix(e2)))`
    =
    `j print(sympy.latex(e2rotate))`
    ,
\end{equation*}
$A_4$ rotates both $\mathbf{e}_1$ and $\mathbf{e}_2$ by $\phi$ radians
and scale them by a factor of $1/2$,
as shown in the picture below.

\begin{figure}[htpb]
\centering

\begin{tikzpicture}[scale=3]
% Grid
\draw[step=1, gray, very thin] (-1,-1) grid (2,2);

% Coordinate axes
\draw[->, thick] (-1,0) -- (2,0) node[right] {$x$};
\draw[->, thick] (0,-1) -- (0,2) node[above] {$y$};

% Green arrows
\draw[->, very thick, green] (0,0) -- (0,1) node[above left] {$\mathbf{e}_2$};
\draw[->, very thick, green] (0,0) -- (1,0) node[below right] {$\mathbf{e}_1$};

% Red arrows (rotated)
\draw[->, very thick, red, rotate around={-60:(0,0)}] (0,0) -- (1/2,0) node[above right] {$A_4 \mathbf{e}_1$};
\draw[->, very thick, red, rotate around={-60:(0,0)}] (0,0) -- (0,1/2) node[right] {$A_4 \mathbf{e}_2$};

% Angle arc
\draw[->, blue] (0.3,0) arc (0:-60:0.3);
\node[blue] at (0.4, -0.2) {$\phi$};

\end{tikzpicture}
\caption{The effect of $A_4$}
\end{figure}

\pagebreak{}

# :pencil: (1pt)

```julia; results="hidden"

Random.seed!(236)
w=3
A = Sym.(rand(-3:3, (w,w)))
B = Sym.(rand(-3:3, (w,w)))
Z = zeros(Int, (w,w))
C = vcat(hcat(A', Z), hcat(Z, convert.(Int, I(w))))
D = vcat(hcat(A', Z), hcat(Z, B))
@assert det(A) != 0
@assert det(B) != 0

```

Let
\begin{equation*}
    A =
    `j print_matrix(A)`,
    \qquad
    B =
    `j print_matrix(B)`.
\end{equation*}

## (0.1pt)

Find the determinants of $A$ and $B$.

Answer:
<!---
$\det(A), \det(B) = \underline{\hspace{2cm}}, \underline{\hspace{2cm}}$.
--->
$\det(A), \det(B) = `j det(A)`, \det(B) = `j det(B)`$.

## (0.1pt)

Let
\begin{equation*}
    C =
    `j print_matrix(C)`.
\end{equation*}
Find $\det(C)$.

Answer: 
<!---
$\det(C) = \underline{\hspace{3cm}}$.
--->
$\det(C) = `j det(C)`$

## (0.3pt)

Justify your answer to the previous question.

Answer:

Applying cofactor expansions repeatedly, 
we have
\begin{equation*}
    \det(C) 
    = 
    \det\left(
    `j print_matrix(C[1:5, 1:5])`
    \right)
    = 
    \det\left(
    `j print_matrix(C[1:4, 1:4])`
    \right)
    = 
    \det\left(
    A^T
    \right)
    =
    \det(A)
    ,
\end{equation*}
where the last step uses the fact that transposing a matrix does not change its determinant.

\pagebreak{}

## (0.1pt)

Let
\begin{equation*}
    D =
    `j print_matrix(D)`.
\end{equation*}
Find $\det(D)$.

<!---
Answer: $\det(D) = \underline{\hspace{3cm}}$.
--->
$\det(D) = `j det(D)`$

## (0.4pt)

Justify your answer to the previous question.

Answer:

We can write $D$ as a block matrix by
\begin{equation*}
    D
    =
    \begin{bmatrix}
        A^T & 0 \\
        0 & B \\
    \end{bmatrix}
    =
    \begin{bmatrix}
        A^T & 0 \\
        0 & I \\
    \end{bmatrix}
    \begin{bmatrix}
        I & 0 \\
        0 & B \\
    \end{bmatrix}
\end{equation*}
where $0$ denote the $3 \times 3$ zero matrix.
Thus
\begin{equation*}
    \det(D)
    =
    \det\left(
    \begin{bmatrix}
        A^T & 0 \\
        0 & I \\
    \end{bmatrix}
    \right)
    \det\left(
    \begin{bmatrix}
        I & 0 \\
        0 & B \\
    \end{bmatrix}
    \right)
    =
    \det(A)
    \det(B)
\end{equation*}
where the last step follows from the same argument as 2.3.

\pagebreak{}

# :car: (1pt)

The flow in a network is shown in the picture.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\textwidth]{flow.png}
\end{figure}

```julia; results="hidden"

xs = [Sym("x$i") for i in 1:4]
vars = tuple(xs...)
eqs = tuple([
    sympy.Eq(xs[1]+xs[3], 20)
    sympy.Eq(80, xs[1] + xs[2])
    sympy.Eq(xs[2], xs[3] + xs[4])
]...)
answer = sympy.solve(eqs, vars)

```

## a (0.5pt)

Find the general flow pattern in the network.

Answer:

<!---
\begin{equation*}
    \begin{cases}
        x_1 = \underline{\hspace{3cm}} \\
        x_2 = \underline{\hspace{3cm}} \\
        x_3 \text{ is free } \\
        x_4 = \underline{\hspace{3cm}} \\
    \end{cases}
\end{equation*}
--->


\begin{equation*}
    \begin{cases}
        x_1 = \underline{`j print_latex(answer[xs[1]])`} \\
        x_2 = \underline{`j print_latex(answer[xs[2]])`} \\
        x_3 \text{ is free } \\
        x_4 = \underline{`j print_latex(answer[xs[4]])`} \\
    \end{cases}
\end{equation*}

## b (0.5pt)

If all flow must be non-negative, what is the largest possible value of $x_3$?

Answer: 
<!---
$\underline{\hspace{3cm}}$
--->
$\underline{20}$

\pagebreak{}

\begin{center}
--- The Real End ---
\end{center}

![This picture is made with linear algebra](./bunny.jpg){width=400px}
