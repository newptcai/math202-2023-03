---
title: MATH 202 Linear Algebra
subtitle: Quiz 04
author: "Instructor Xing Shi Cai"
date: 2023-04-27
fontsize: 12pt
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using LinearAlgebra
#using Plots
using Random
#using Distributions
#using DataFrames
using SymPy
using RowEchelon
# Set default for Latexify
set_default(starred = true)

function print_matrix(A)
    print(sympy.latex(Sym.(A)))
end

function print_empty_matrix(m, n)
    row = join(fill("\\underline{\\hspace{3cm}}", n), " & ")
    matrix = join(fill(row, m), " \\\\\n")
    print("""
        \\begin{bmatrix}
            $matrix
        \\end{bmatrix}"""
    )
end

function print_latex(ex)
    print(sympy.latex(ex))
end

# Generate a random m by n integer matrix with elements from -5 to 5
function random_matrix(m=3, n=3)
    return rand(-5:5, m, n)
end

function random_matrix_with_rank(matrix_rank, m=3, n=3)
    while true
        global A = random_matrix(m, n)
        if rank(A) == matrix_rank
            break
        end
    end
    return A
end

function column_space_basis(A::Matrix{Int})
    # Compute the RREF of A
    R = rref(A)

    # Find the rank of A
    r = rank(A)

    # Find the pivot columns
    pivot_cols = Int64[]
    for i in 1:r
        col = findfirst(==(1), R[i, :])
        if col !== nothing
            push!(pivot_cols, col)
        end
    end

    # Extract the basis of the column space
    basis = A[:, pivot_cols]

    return basis
end

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks in the toilet.
\end{itemize}

\smallskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

\begin{tcolorbox}
\large{}
\emoji{bomb} To write a proper proof, it is necessary to provide both equations and explanatory statements.
Make sure to reference relevant theorems from the textbook in your proof, and avoid any grammar or spelling errors as they may result in point deductions.
\end{tcolorbox}

\pagebreak{}

# :multiply: (1pt)

## a

```julia; echo=false, results="hidden"

Random.seed!(22683)
A = random_matrix(4, 5)
A[2:end, 1] .=0
A[3:4, 2:3] .=0
A = A[randperm(4), :]
A = A[:, randperm(5)]
A

```

Let
\begin{equation*}
    A
    =
    `j print_matrix(A)`.
\end{equation*}

What are 

<!--
* $\dim \operatorname{Col} A = \underline{\hspace{3cm}}$
* $\dim \operatorname{Row} A = \underline{\hspace{3cm}}$
* $\dim \operatorname{Nul} A = \underline{\hspace{3cm}}$
* $\operatorname{rank} A = \underline{\hspace{3cm}}$
* $\dim \operatorname{Col} A^T = \underline{\hspace{3cm}}$
* $\dim \operatorname{Row} A^T = \underline{\hspace{3cm}}$
* $\dim \operatorname{Nul} A^T = \underline{\hspace{3cm}}$
* $\operatorname{rank} A^T = \underline{\hspace{3cm}}$
--->

* $\dim \operatorname{Col} A = \underline{`j print(rank(A))`}$
* $\dim \operatorname{Row} A = \underline{`j print(rank(A))`}$
* $\dim \operatorname{Nul} A = \underline{`j print(5-rank(A))`}$
* $\operatorname{rank} A = \underline{`j print(rank(A))`}$
* $\dim \operatorname{Col} A^T = \underline{`j print(rank(A'))`}$
* $\dim \operatorname{Row} A^T = \underline{`j print(rank(A'))`}$
* $\dim \operatorname{Nul} A^T = \underline{`j print(4-rank(A'))`}$
* $\operatorname{rank} A^T = \underline{`j print(rank(A'))`}$

:bulb: Note that
\begin{equation*}
    A \sim `j print_matrix(rref(Sym.(A)))`
\end{equation*}

## b

```julia; echo=false, results="hidden"

Random.seed!(22689)
A = random_matrix_with_rank(2, 4, 2)
basis = column_space_basis(A)
xs = rand(-3:3, 2)
A = hcat(A, xs[1]*A[:, 1] + xs[2]*A[:, 2])
@syms a, b, c
vs = [a, b, c]
h = sum([vs[i]*A[:, i] for i in 1:3])

```

What is the dimension of the set
\begin{equation*}
    H = 
    \left\{
    `j print_matrix(h)`
    :
    a, b, c \in \mathbb{R}
    \right\}
\end{equation*}

<!---
Answer: $\underline{\hspace{5cm}}$
--->

Answer: $\underline{2}$

:bulb: This is the same as asking for the dimension of the column space of
\begin{equation*}
    `j print_matrix(A)`
    \sim
    `j print_matrix(rref(Sym.(A)))`
\end{equation*}

## c

```julia; echo=false, results="hidden"

Random.seed!(22690)
A = random_matrix_with_rank(2, 4, 2)
basis = column_space_basis(A)
xs = rand(-3:3, 2)
A = hcat(A, xs[1]*A[:, 1] + xs[2]*A[:, 2])
B = hcat(A, [0; 0; 0; -1])
@syms a, b, c
vs = [a, b, c]
h = sum([vs[i]*A[:, i] for i in 1:3])
h[4, :] .+= 1
h

```
What is the dimension of the set
\begin{equation*}
    H = 
    \left\{
    `j print_matrix(h)`
    :
    a, b, c \in \mathbb{R}
    \right\}
\end{equation*}

\bigskip{}

<!--
Answer: $\underline{\hspace{5cm}}$
--->

Answer: \underline{$H$ has not dimension since $H$ is not a vector space.}

:bulb: We can rewrite $H$ as
\begin{equation*}
    H = 
    \left\{
    a
    `j print_matrix(A[:, 1])`
    +
    b
    `j print_matrix(A[:, 2])`
    +
    c
    `j print_matrix(A[:, 3])`
    +
    `j print_matrix([0; 0; 0; 1])`
    :
    a, b, c \in \mathbb{R}
    \right\}
\end{equation*}
This is not a vector space since $\mathbf{0} \notin H$.
To see this, note that
\begin{equation*}
    a
    `j print_matrix(A[:, 1])`
    +
    b
    `j print_matrix(A[:, 2])`
    +
    c
    `j print_matrix(A[:, 3])`
    +
    `j print_matrix([0; 0; 0; 1])`
    =
    \mathbf{0}
\end{equation*}
has no solution since
\begin{equation*}
    `j print_matrix(B)`
    \sim
    `j print_matrix(rref(Sym.(B)))`
\end{equation*}

\pagebreak{}

# :robot:

Let $\mathbb{P}_{2}$ be the vector space of polynomials of degree $2$ or less.
Consider the following two bases of $\mathbb{P}_{2}$:
\begin{equation*}
\begin{array}{lcl}
\mathcal{B} & = & \lbrace t-t^{2},\ -t^{2},\ 1-t \rbrace, \\
\mathcal{C} & = & \lbrace -2-t+t^{2},\ -2+t^{2},\ 1-t \rbrace.
\end{array}
\end{equation*}

```julia; echo=false, results="hidden"

@syms t
basis = [1; t; t^2]
PB = [0 0 1; 1 0 -1; -1 -1 0]
[dot(PB[:, i], basis) for i in 1:3]
PB_inverse = inv(Sym.(PB))
PC = [-2 -2 1; -1 0 -1; 1 1 0]
[dot(PC[:, i], basis) for i in 1:3]
PC_inverse = inv(Sym.(PC))

```

## a

Find the change of basis matrix from the basis $\mathcal{B}$ to the standard basis $\mathcal{E} = \{1, t, t^2\}$.

Answer:

<!---
\begin{equation*}
    P_{\mathcal{E} \leftarrow \mathcal{B}} 
    = 
    `j print_empty_matrix(3,3)`
\end{equation*}
--->

\begin{equation*}
P_{\mathcal{E} \leftarrow \mathcal{B}} 
=
`j print_matrix(PB)`
\end{equation*}

:bulb: To find the change of basis matrix from $\mathcal{B}$ to $\mathcal{E}$, we first
express each vector in $\mathcal{B}$=$\{t-t^2, -t^2, 1-t\}$ as a linear
combination of the vectors in $\mathcal{E}=\{1,t,t^2\}$, and then we collect the
coefficients into a matrix. 

We have
$$t-t^2=(0) \times 1+(1) \times t+(-1) \times t^2$$
$$-t^2=(0) \times 1+(0) \times t+(-1) \times t^2$$
$$1-t=(1) \times 1+(-1) \times t+(0) \times t^2$$
Therefore, with the coefficients arranged as columns,
the change of basis matrix from $\mathcal{B}$ to $\mathcal{E}$ is:
$\left[\begin{array}{ccc}0 & 0 & 1 \\1 & 0 & -1\\-1 & -1 & 0\end{array}\right]$

## b

Find the change of basis matrix from the standard basis $\mathcal{E}$ to the basis $\mathcal{C}$.

Answer:

<!---
\begin{equation*}
    P_{\mathcal{C} \leftarrow \mathcal{E}} 
    = 
    `j print_empty_matrix(3,3)`
\end{equation*}
--->

\begin{equation*}
P_{\mathcal{C} \leftarrow \mathcal{E}} 
=
`j print_matrix(PC_inverse)`
\end{equation*}

To find the change of basis matrix from the standard basis
$\mathcal{E}=\{1,t,t^2\}$ to the basis $\mathcal{C}$=$\{-2-t+t^2, -2+t^2, 1-t\}$,
we have 
$$1=(-1) \times (-2-t+t^2)+(1) \times (-2+t^2)+(1) \times (1-t)$$
$$t=(-1) \times (-2-t+t^2)+(1) \times (-2+t^2)+(0) \times (1-t)$$
$$t^2=(-2) \times (-2-t+t^2)+(3) \times (-2+t^2)+(2) \times (1-t)$$
Therefore, with the coefficients arranged as columns,
the change of basis matrix from $\mathcal{E}$ to $\mathcal{C}$ is:
$\left[\begin{array}{ccc}-1 & -1 & -2 \\ 1 & 1 & 3 \\ 1 & 0 & 2\end{array}\right]$

Note:  An alternate way is to find the change of basis from $\mathcal{C}$ to
$\mathcal{E}$, and take its inverse matrix. As the change of basis matrix has
independent columns, by Invertible Matrix Theorem and the Change of Basis
Theorem, we have
$P_{\mathcal{C}\leftarrow\mathcal{E}}=(P_{\mathcal{E}\leftarrow\mathcal{C}})^{-1}$.

## c

Find the change of basis matrix from the basis $\mathcal{B}$ to the basis $\mathcal{C}$.

Answer:

<!---
\begin{equation*}
    P_{\mathcal{C} \leftarrow \mathcal{B}} 
    = 
    `j print_empty_matrix(3,3)`
\end{equation*}
--->

\begin{equation*}
P_{\mathcal{C} \leftarrow \mathcal{B}} 
=
`j print_matrix(PC_inverse*PB)`
\end{equation*}


We are performing change of basis in $\mathbb{P}_2$ with the standard basis $\mathcal{E}=\{1,t,t^2\}$, and thus, we have an easier way to obtain the change of basis from $\mathcal{B}$ to $\mathcal{C}$: 
$$
P_{\mathcal{C}\leftarrow\mathcal{B}}=P_{\mathcal{C}\leftarrow\mathcal{E}} \times P_{\mathcal{E}\leftarrow\mathcal{B}}=(P_{\mathcal{E}\leftarrow\mathcal{C}})^{-1} \times P_{\mathcal{E}\leftarrow\mathcal{B}}=(P_C)^{-1}P_B
$$
Note that in sub-question (1) and (2) we have already obtained $P_{\mathcal{C}\leftarrow\mathcal{E}}$ and $P_{\mathcal{E}\leftarrow\mathcal{B}}$. Therefore, we have
$$
P_{\mathcal{C}\leftarrow\mathcal{B}}=    \left[\begin{array}{ccc}-1 & -1 & -2 \\1 & 1 & 3\\1 & 0 & 2\end{array}\right]  \times     
\left[\begin{array}{ccc}0 & 0 & 1 \\1 & 0 & -1\\-1 & -1 & 0\end{array}\right] =\left[\begin{array}{ccc}1 & 2 & 0 \\-2 & -3 & 0\\-2 & -2 & 1\end{array}\right]
$$

## d

Find the change of basis matrix from the basis $\mathcal{C}$ to the basis $\mathcal{B}$.

Answer:

<!---
\begin{equation*}
    P_{\mathcal{B} \leftarrow \mathcal{C}} = 
    `j print_empty_matrix(3,3)`
\end{equation*}
--->

\begin{equation*}
    P_{\mathcal{B} \leftarrow \mathcal{C}} = 
\left[\let\quad=\relax\begin{array}{ccc}-3&-2&0\cr 2&1&0\cr -2&-2&1\end{array}\right]
\end{equation*}

:bulb: Problem **d** has been dropped since the answer was included accidentally.

<!---
\begin{equation*}
P_{\mathcal{C} \leftarrow \mathcal{B}} 
=
`j print_matrix(PB_inverse*PC)`
\end{equation*}
--->

\pagebreak{}

# :money_bag:

You have won a lottery and
will receive yearly payments of $a_n$ :dollar:, 
where 
\begin{equation}
    \label{eq:nonhomo}
    a_{n+2} = 8a_{n+1} - 7a_n + 2,
    \qquad (n \in \{0, 1, \dots\}).
\end{equation}

```julia; echo=false, results="hidden"

function q3()
    # Define the symbols and function
    @vars n integer=true
    a = sympy.Function("a")

    # Define the recurrence relation
    recurrence_relation_nonhomo = a(n+2) - 8*a(n+1) + 7*a(n) - 2

    # Solve the recurrence relation
    solution_nonhomo = rsolve(recurrence_relation_nonhomo, a(n))

    # Solve the recurrence relation with initail valudes
    solution_nonhomo_init = rsolve(recurrence_relation_nonhomo, 
        a(n), 
        Dict(a(0)=>Sym(3//10), a(1)=>5))

    # Define the recurrence relation
    recurrence_relation_homo = a(n+2) - 8*a(n+1) + 7*a(n)

    # Solve the recurrence relation
    solution_homo = rsolve(recurrence_relation_homo, a(n))
    return (solution_homo, solution_nonhomo, solution_nonhomo_init)
end
(solution_homo, solution_nonhomo, solution_nonhomo_init) = q3()
solution_particular = (solution_nonhomo-solution_homo).subs(sympy.Symbol("C0"), 0)

```

## a

Find two linearly independent solutions to the homogeneous linear difference
equation
\begin{equation*}
    a_{n+2} = 8a_{n+1} - 7 a_n,
    \qquad (n \in \{0, 1, \dots\}).
\end{equation*}

Answer: $\underline{(7)^n, (1)^n}$

## b

Verify $a_n = `j print_latex(solution_particular)`$ is a solution of \eqref{eq:nonhomo}

Answer:

To verify $a_n=-n/3$ is a solution of $a_{n+2}=8a_{n+1}-7a_{n}+2$, we can replace the term $a_n$ with $-n/3$: 
$$
\text{Left hand side (LHS)} = a_{n+2} = -\frac{n+2}{3}
$$
$$
\text{Right hand side (RHS)} = 8  \times  (-\frac{n+1}{3}) - 7  \times  (-\frac{n}{3})+2=-\frac{n+2}{3}
$$
We verified LHS = RHS. 
Therefore, $a_n=-n/3$ is a (particular) solution of $a_{n+2}=8a_{n+1}-7a_{n}+2$


## c

What is the solution set of \eqref{eq:nonhomo}?

Answer: The solution set is
\begin{equation*}
    H = \left\{
    C_0 \underline{7^n}
    +
    C_1 \underline{1^n}
    +
    \underline{-\frac{n}{3}}
    : C_0, C_1 \in \mathbb{R}\right\}
\end{equation*}

<!---
\begin{equation*}
    H = \left\{`j print_latex(solution_nonhomo)`: C_0, C_1 \in \mathbb{R}\right\}
\end{equation*}
--->

## d

Among the solutions in $H$, which one satisfies that $a_0 = \frac{3}{10}$, $a_1 = 5$?

Answer:

<!---
\begin{equation*}
    a_n = \underline{\hspace{8cm}}
\end{equation*}
--->
\begin{equation*}
    a_n = \underline{`j print_latex(solution_nonhomo_init)`}
\end{equation*}
for all $n \ge 0$.

:bulb: Given the initial conditions $a_0=3/10$, $a_1=5$ we have the following linear system:
$$C_0 \times 7^0+C_1 \times 1^0+(-0/3)=3/10$$
$$C_0 \times 7^1+C_1 \times 1^1+(-1/3)=5$$
Solving the linear system we got $C_0=151/180,C_1=-97/180$
$$a_n=\underline{\frac{151}{180} \times 7^n-\frac{97}{180} \times 1^n-\frac{n}{3}}
\text{ for all } n\geq0$$

\pagebreak{}

# :white_check_mark:

Which of the following statements are always true?

<!---
\begin{enumerate}
  \item The number of pivot columns of a matrix equals the dimension of its column space.
  \item A plane in $\mathbb{R}^3$ is a two-dimensional subspace of $\mathbb{R}^3$.
  \item The dimension of the vector space $\mathbb{P}_4$ is 4.
  \item If $\dim V = n$ and $S$ is a linearly independent set in $V$, then $S$ is a basis for $V$.
  \item If a set $\{\mathbf{v}_1, \dots, \mathbf{v}_p\}$ spans a finite-dimensional 
        vector space $V$ and if $T$ is a set of more than $p$ vectors in $V$, then $T$ is linearly dependent.
  \item $\mathbb{R}^2$ is a two-dimensional subspace of $\mathbb{R}^3$.
  \item The number of free variables in the equation $Ax = 0$ equals the dimension of $\operatorname{Nul} A$.
  \item A vector space is infinite-dimensional if it is spanned by an infinite set.
  \item If $\dim V = n$ and if $S$ spans $V$, then $S$ is a basis of $V$.
  \item The only three-dimensional subspace of $\mathbb{R}^3$ is $\mathbb{R}^3$ itself.
  \item If there exists a set $\{\mathbf{v}_1, \dots, \mathbf{v}_p\}$ that spans $V$, then $\dim V \leq p$.
  \item If there exists a linearly independent set $\{v_1, \dots, v_p\}$ in $V$, then $\dim V \geq p$.
  \item If $\dim V = p$, then there exists a spanning set of $p + 1$ vectors in $V$.
  \item The columns of the change-of-coordinates matrix $P_{\mathcal{C} \leftarrow \mathcal{B}}$ are $\mathcal{B}$-coordinate vectors of the vectors in $\mathcal{C}$.
  \item If $V = \mathbb{R}^n$ and $\mathcal{C}$ is the standard basis for $V$, then $P_{\mathcal{C} \leftarrow \mathcal{B}}$ is the same as the change-of-coordinates matrix $P_{\mathcal{B}}$ introduced in Section 4.4.
  \item The columns of $P_{\mathcal{C} \leftarrow \mathcal{B}}$ are linearly independent.
  \item If $V = \mathbb{R}^2$, $\mathcal{B} = \{b_1, b_2\}$, and $\mathcal{C} = \{c_1, c_2\}$, then row reduction of $\begin{bmatrix} c_1 & c_2 & b_1 & b_2 \end{bmatrix}$ to $\begin{bmatrix} I & P \end{bmatrix}$ produces a matrix $P$ that satisfies $[x]_{\mathcal{B}} = P [x]_{\mathcal{C}}$ for all $x$ in $V$.
\end{enumerate}
--->

\begin{enumerate}
  \item A plane in $\mathbb{R}^3$ is a two-dimensional subspace of $\mathbb{R}^3$.
  \item The dimension of the vector space $\mathbb{P}_4$ is 5.
  \item If $\dim V = n$ and $S$ is a linearly dependent set of $n$ vectors in $V$, then $S$ is a basis for $V$.
  \item If a set $\{\mathbf{v}_1, \dots, \mathbf{v}_p\}$ spans a finite-dimensional vector space $V$ and if $T$ is a set of exactly $p$ vectors in $V$, then $T$ is linearly dependent.
  \item The number of free variables in the equation $A \mathbf{x} = 0$ equals the dimension of $\operatorname{Nul} A$ minus one.
  \item A vector space is infinite-dimensional if it is spanned by a finite set.
  \item If $\dim V = p$, then there exists a spanning set of $p - 1$ vectors in $V$.
  \item The columns of the change-of-coordinates matrix $P_{\mathcal{C} \leftarrow \mathcal{B}}$ are $\mathcal{B}$-coordinate vectors of the vectors in $\mathcal{C}$.
  \item If $V = \mathbb{R}^n$ and $\mathcal{E}$ is the standard basis for $V$, then $P_{\mathcal{E} \leftarrow \mathcal{B}}$ is \emph{not} the same as the change-of-coordinates matrix $P_{\mathcal{B}}$ introduced in Section 4.4.
  \item If $V = \mathbb{R}^2$, $\mathcal{B} = \{b_1, b_2\}$ and $\mathcal{C} = \{c_1, c_2\}$ are two bases of $V$, then row reduction of $\begin{bmatrix} c_1 & c_2 & b_1 & b_2 \end{bmatrix}$ to $\begin{bmatrix} I & P \end{bmatrix}$ does \emph{not} produce a matrix $P$ that satisfies $[x]_{\mathcal{B}} = P [x]_{\mathcal{C}}$ for all $x$ in $V$.
\end{enumerate}

\bigskip{}

Answer: $\underline{2, 10}$

\begin{enumerate}
  \item \textbf{False.} A plane in $\mathbb{R}^3$ must pass through the origin to be a two-dimensional subspace of $\mathbb{R}^3$.
  \item \textbf{True.} The dimension of the vector space $\mathbf{P}_4$ is 5, since there are 5 basis elements: $1, t, t^2, t^3, t^4$.
  \item \textbf{False.} If $S$ is a linearly dependent set in $V$, it cannot be a basis for $V$.
  \item \textbf{False.} If there exists a set $\{\mathbf{v}_1, \dots, \mathbf{v}_p\}$ that spans $V$, then $\dim V \leq p$. However, this does not imply anything about an arbitrary set of $p$ vectors.
  \item \textbf{False.} The number of variables in the equation $Ax = 0$ equals the dimension of $\operatorname{Nul} A$, not the dimension minus one.
  \item \textbf{False.} A vector space is infinite-dimensional if it is spanned by an infinite set, not a finite set.
  \item \textbf{False.} If $\dim V = p$, then there exists a spanning set of $p$ vectors in $V$. It is not possible to have a spanning set of $p - 1$ vectors in $V$, as the dimension of the vector space determines the size of a basis, which is the smallest possible spanning set.
  \item \textbf{False.} The columns of the change-of-coordinates matrix $P_{\mathcal{C} \leftarrow \mathcal{B}}$ are $\mathcal{C}$-coordinate vectors of the vectors in $\mathcal{B}$.
  \item \textbf{False.} If $V = \mathbb{R}^n$ and $\mathcal{E}$ is the standard basis for $V$, then $P_{\mathcal{E} \leftarrow \mathcal{B}}$ is the same as the change-of-coordinates matrix $P_{\mathcal{B}}$ introduced in Section 4.4.
  \item \textbf{True.} If $V = \mathbb{R}^2$, $\mathcal{B} = \{b_1, b_2\}$, and $\mathcal{C} = \{c_1, c_2\}$, then row reduction of $\begin{bmatrix} c_1 & c_2 & b_1 & b_2 \end{bmatrix}$ to $\begin{bmatrix} I & P \end{bmatrix}$ produces a matrix $P$ that satisfies $[x]_{\mathcal{C}} = P [x]_{\mathcal{B}}$ for all $x$ in $V$.
\end{enumerate}

# :mega: (0.1pt)

The percentage improvement is determined by comparing you best quiz score after
the midterm to you midterm score.

Then the bonus point is calculated as
```
Bonus Points = Min(Percentage Improvement * 0.2, 2)
```

If you got 72% in midterm, and your best quiz grade is 85% afterwards, then your bonus point will be

\bigskip{}

Answer: \underline{\hspace{5cm}}

\pagebreak{}

![A rabbit taking an quiz with cameras watching](./bunny.png){width=400px}
