---
title: MATH 202 Linear Algebra
subtitle: Quiz 02
author: "Instructor Xing Shi Cai"
date: 2023-04-07
fontsize: 12pt
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
using LinearAlgebra
#using Plots
using Random
#using Distributions
#using DataFrames
using SymPy
using RowEchelon

# Set default for Latexify
set_default(starred = true)

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] \emph{touch} my phone or use the internet.
\end{itemize}

\smallskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

![A rabbit taking an quiz](./bunny.png){width=330px}

\pagebreak{}

# :zap: (1pt)

Consider the following circuit

![An electrical circuit](./circuit.png){width=200px}

## a

```julia; echo=false, results="hidden"

is = [Sym("i$i") for i in 1:3]
vs = [10, 20, 40]
vars = tuple(is...)
# Try find esier problem
@syms a, b
eqs = tuple([
    sympy.Eq((a)*is[1]-a*is[2], vs[1]),
    sympy.Eq((1+a+b)*is[2]-b*is[3]-a*is[1], vs[2]),
    sympy.Eq((3+b)*is[3]-b*is[2], vs[3]),
]...)
mat = sympy.linear_eq_to_matrix(eqs, vars)
ex = det(mat[1])
for a1 in 1:5, b1 in 1:5
    println("a = $a1, b=$b1")
    println(ex.subs(Dict(a=>a1, b=>b1)))
end
ab_sol = Dict(a=>1, b=>1)
eqs1 = [eq.subs(ab_sol) for eq in eqs]
augmat = hcat(mat[1].subs(ab_sol), mat[2].subs(ab_sol))
answer = rref(augmat)[:, end]
echelonmat = rref(augmat)
matinv = inv(mat[1].subs(ab_sol))

```

Write down the matrix equation that determins the loop currents.

Answer:

\begin{equation*}
`j println(sympy.latex(mat[1].subs(ab_sol)))`
\begin{bmatrix}
    I_1 \\ I_2 \\ I_3
\end{bmatrix}
=
`j println(sympy.latex(mat[2].subs(ab_sol)))`
\end{equation*}

<!---
\begin{equation*}
\begin{bmatrix}
  \underline{\hspace{2cm}} & \underline{\hspace{2cm}} & \underline{\hspace{2cm}} \\
  \underline{\hspace{2cm}} & \underline{\hspace{2cm}} & \underline{\hspace{2cm}} \\
  \underline{\hspace{2cm}} & \underline{\hspace{2cm}} & \underline{\hspace{2cm}} \\
\end{bmatrix}
\begin{bmatrix}
    I_1 \\ I_2 \\ I_3
\end{bmatrix}
=
\begin{bmatrix}
  \underline{\hspace{2cm}} \\
  \underline{\hspace{2cm}} \\
  \underline{\hspace{2cm}} \\
\end{bmatrix}
\end{equation*}
--->

## b

Find the inverse of the coefficient matrix

Answer:

<!---
\begin{equation*}
\begin{bmatrix}
  \underline{\hspace{2cm}} & \underline{\hspace{2cm}} & \underline{\hspace{2cm}} \\
  \underline{\hspace{2cm}} & \underline{\hspace{2cm}} & \underline{\hspace{2cm}} \\
  \underline{\hspace{2cm}} & \underline{\hspace{2cm}} & \underline{\hspace{2cm}} \\
\end{bmatrix}
\end{equation*}
--->

\begin{equation*}
`j print(sympy.latex(matinv))`
\end{equation*}

## c

Determine the loop currents.

Answer:

\begin{equation*}
\begin{bmatrix}
    I_1 \\ I_2 \\ I_3
\end{bmatrix}
=
`j print(sympy.latex(answer))`
%\begin{bmatrix}
%  \underline{\hspace{2cm}} \\
%  \underline{\hspace{2cm}} \\
%  \underline{\hspace{2cm}} \\
%\end{bmatrix}
\end{equation*}

# :black_square_button: (1pt)

Check the :black_square_button: (like :check_mark_button:) before each true statement.

:bulb: Consider only square matrices.

1. :black_square_button:  If the equation $A\mathbf{x} = \mathbf{0}$ has the trivial solution, then $A$ is row equivalent to the $n \times n$ identity matrix.
1. :check_mark_button:    If the columns of $A$ span $\mathbb{R}^n$, then the columns are linearly independent.
1. :check_mark_button:    If $A$ is an $n \times n$ matrix, then the equation $A\mathbf{x} = \mathbf{b}$ has at least one solution for at least one $\mathbf{b}$ in $\mathbb{R}^n$.
1. :check_mark_button:    If the equation $A\mathbf{x} = \mathbf{0}$ has a nontrivial solution, then $A$ has fewer than $n$ pivot positions.
1. :check_mark_button:    If $A^T$ is not invertible, then $A$ is not invertible.
1. :check_mark_button:    In order to prove that $B$ is the inverse of $A$, it is enough to show that $AB = I$.
1. :black_square_button:  The inverse of $AB$ is always $B^{-1} A^{-1}$.
1. :check_mark_button:    If $A = \begin{bmatrix} a & b \\ 3 a & 3 b \end{bmatrix}$, then $A$ is not invertible.
1. :black_square_button:  If $A$ and $B$ are invertible, then $(A+B)^{-1} = A^{-1} + B^{-1}$.
1. :check_mark_button:  If $A$ is an invertible matrix, then $A^{-2} = (A^{-1})^2$ is also invertible.

## Solution


\begin{enumerate}
\item
The equation $Ax = 0$ always has the trivial solution. This does not tell us
anything about $A$.
\item
By the Invertible Matrix Theorem (IMT).
\item
If $\mathbf{b} = \mathbf{0}$, the homogeneous equation $A\mathbf{x} = \mathbf{0}$ always has the trivial solution, regardless of whether $A$ is invertible or not.
\item
By the Invertible Matrix Theorem (IMT).
\item
By the Invertible Matrix Theorem (IMT).
\item
By the Invertible Matrix Theorem (IMT).
Alternative proof --- Assume $AB = I$. Then $Bx = 0$ $\Leftrightarrow$ $A(Bx) = A0$ $\Leftrightarrow$ $(AB)x = 0$ $\Leftrightarrow$  $x = 0$. By the theorem that [A square matrix A is invertible if and only if $x = 0$ is the only solution of the matrix equation $Ax = 0$], we can conclude that B is also invertible. Then AB = I $\Leftrightarrow$ $B(AB)B^{-1} = BI B^{-1} $ $\Leftrightarrow$ $BA = I$.
\item
This is only true if both $A$ and $B$ are invertible.
\item
Note that the two columns are linearly dependent.
Alternative proof ---$\det(A) = 3ab - 3ab = 0$ $\Leftrightarrow$ A is not invertible.
\item
Counterexample: Let A=$\left[\begin{array}{cc}1 & 0 \\0 & 1 \end{array}\right]$, B = $\left[\begin{array}{cc}-1 & 0 \\0 & -1 \end{array}\right]$
Then, A and B are invertible matrices since their determinants are non-zero. However, A+B = 0, which is not invertible and the stated equation will not hold true in that situation.
\item
Given A is invertible, we have $A^{-1}$ exists and $(A^{-1})^2 (A^2) = I$.
\end{enumerate}

# :orange_square: (1pt)

## a  

Suppose that \ 
$E_1 \left[\begin{array}{cc}
-1 &3\\
-4 &2
\end{array}\right] = \left[\begin{array}{cc}
-6 &18\\
-4 &2
\end{array}\right]$.  Find $E_1$ and $E_1^{-1}$.

Answer:

<!---
\begin{equation*}
    E_1 = 
    \begin{bmatrix}
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}\\ 
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}
    \end{bmatrix}
    ,
    \qquad
    E_1^{-1}
    =
    \begin{bmatrix} 
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}\\
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}
    \end{bmatrix} 
\end{equation*}
--->

\begin{equation*}
    E_1 = 
    \begin{bmatrix}
    6&0\\ 
    0&1
    \end{bmatrix}
    ,
    \qquad
    E_1^{-1}
    =
    \begin{bmatrix} 
        1/6&0\\
        0&1
    \end{bmatrix} 
\end{equation*}

## b

Suppose that \ 
$E_2 \left[\begin{array}{cc}
-1 &3\\
-4 &2
\end{array}\right] = \left[\begin{array}{cc}
-4 &2\\
-1 &3
\end{array}\right]$.  Find $E_2$ and $E_2^{-1}$.

Answer:

<!---
\begin{equation*}
    E_2 = 
    \begin{bmatrix}
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}\\ 
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}
    \end{bmatrix}
    ,
    \qquad
    E_2^{-1}
    =
    \begin{bmatrix} 
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}\\
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}
    \end{bmatrix} 
\end{equation*}
--->

\begin{equation*}
    E_2 =
    \begin{bmatrix}
        0 & 1 \\
        1 & 0
    \end{bmatrix}
    ,
    \qquad
    E_2^{-1} =
    \begin{bmatrix}
        0 & 1 \\
        1 & 0
    \end{bmatrix}
\end{equation*}

## c  

Suppose that \ 
$E_3 \left[\begin{array}{ccc}
2 &5 &-4\\
-3 &-5 &-1\\
-3 &4 &-1
\end{array}\right] = \left[\begin{array}{ccc}
2 &5 &-4\\
-12 &-20 &-4\\
-3 &4 &-1
\end{array}\right]$. 
Find $E_3$ and $E_3^{-1}$.

Answer:


<!---
\begin{equation*}
    E_3 = 
    \begin{bmatrix}
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}&\underline{\hspace{2cm}}\\ 
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}&\underline{\hspace{2cm}}\\ 
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}&\underline{\hspace{2cm}}
    \end{bmatrix}
    ,
    \qquad
    E_3^{-1}
    =
    \begin{bmatrix} 
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}&\underline{\hspace{2cm}}\\
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}&\underline{\hspace{2cm}}\\
        \underline{\hspace{2cm}}&\underline{\hspace{2cm}}&\underline{\hspace{2cm}}
    \end{bmatrix} 
\end{equation*}
--->

\begin{equation*}
E_3 =
\begin{bmatrix}
    1&0&0\\
    0&4&0\\
    0&0&1
\end{bmatrix}
,
\qquad
E_3^{-1} =
\begin{bmatrix}
    1&0&0\\
    0&1/4&0\\
    0&0&1
\end{bmatrix}
\end{equation*}


# :pencil: (1pt)

\begin{tcolorbox}
\emoji{bomb} To write a proper proof, it is necessary to provide both equations and explanatory statements.
Make sure to reference relevant theorems from the textbook in your proof, and avoid any grammar or spelling errors as they may result in point deductions.
\end{tcolorbox}

\medskip{}

## a


```julia; echo=false, results="hidden"

Random.seed!(1234)
A = tril(rand(-5:5, (8,8)))
A[5, 5] = 0

```

Decide if the following matrix is invertible and prove your conclusion
**without** using determinants.

`j print(latexify(A))`

Answer:

Let $A$ denote this $8 \times 8$ matrix.
Note that $A^T$ is in echelon form and has only $7$ pivot positions.
So by the Invertible Matrix Theorem (IMT),
$A^T$ is not invertible.
Thus, by IMT again, $A$ is not invertible.

## b


```julia; echo=false, results="hidden"

Random.seed!(123)
A = rand(-3:3, (8,8))
det(A)
A1 = Sym.(A)
B = inv(A1)

```

Consider the following two matrices
\begin{equation*}
    A
    =
    `j print(latexify(A, env=:raw))`
\end{equation*}
and
\begin{equation*}
    B
    =
    `j print(latexify(B, env=:raw))`
    ,
\end{equation*}
which satisfy that $A B = I_8$.
Prove that the rows of $B$, when seen as vectors, are linearly independent.

Answer:

:bulb: The matrices look really computation-heavy but we don't need to care much
about their elements.

Given square matrices A, B and their product $AB=I_8$, 
by the IMT (Inverse Matrix Theorem),
we know that both $B$ and thus $B^T$ are invertible.

It again follows from the invertible matrix theorems that the columns of $B^T$,
i.e, the rows of $B$ seen as vectors,
are linearly independent.

# :mega: (0.1pt)

What is the random emoji in this week's (week 2) announcement?

:bulb: No need to draw the emoji.

Answer: \underline{\hspace{5cm}}

\begin{center}
    \Large
    \textbf{A maze made by ChatGPT}

    \begin{verbatim}
+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
|                 |                       |     |
+--+--+--+--+--+  +  +--+--+--+--+--+  +  +  +  +
|                 |  |     |           |  |  |  |
+  +  +  +--+--+--+--+  +  +--+--+--+  +  +--+  +
|     |     |        |  |        |     |        |
+  +--+--+--+--+  +  +  +  +--+--+--+--+--+--+  +
|     |           |  |     |              |     |
+--+--+--+--+  +--+  +--+  +  +--+--+--+  +  +  +
|                 |     |     |              |  |
+--+--+--+  +--+  +  +--+--+  +--+--+--+--+--+  +
|           |     |     |        |        |     |
+  +--+--+--+  +  +  +--+--+--+  +--+--+  +  +  +
|     |        |  |              |     |     |  |
+--+--+--+--+  +--+--+--+--+--+--+--+--+--+--+--+
    \end{verbatim}
\end{center}
