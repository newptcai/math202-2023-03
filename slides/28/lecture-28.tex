\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Least-squares Problems, Machine Learning and Linear Models}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{6.5 Least-squares Problems}

\begin{frame}
    \frametitle{Can we solve this?}
    
    The equation
    \begin{equation*}
    \begin{bmatrix} 2 & -2 \\ -3 & 3 \end{bmatrix}\bfx =  \begin{bmatrix} 7 \\ 25 \end{bmatrix}
    \end{equation*}
    has no solution.

    \think{} But can we find the \emph{best} approximate solution?
\end{frame}

\subsection{Solution of the General Least-Squares Problem}

\begin{frame}
    \frametitle{Least-squares solutions}

    A \alert{least-squares solution} of $A \bfx = \bfb$ is a vector $\hat{\bfx}$ such
    that
    \begin{equation*}
        \norm{\bfb - A \hat{\bfx}}
        \le
        \norm{\bfb - A \bfx}
        ,
        \qquad
        \text{for all } \bfx.
    \end{equation*}
    \only<1>{\cake{} What is $\hat{\bfx}$ when $A$ is invertible?}%
    \only<2->{This impels that $\hat{\bfx}$ is the \emph{least-squares} solution
        if and only if
        \begin{equation*}
            A \hat{\bfx} =  \proj_{\colspace A} \bfb
        \end{equation*}
    }%
    \only<2>{
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{6.5-fig-1.png}
    \end{figure}
    }
    \only<3>{\cake{} We know $\proj_{\colspace A} \bfb$ is unique.
    But is $\hat{\bfx}$ necessarily unique?}
\end{frame}

\begin{frame}
    \frametitle{Normal equations}

    Suppose that $\hat{\bfx}$ satisfies $A \hat{\bfx} = \hat{\bfb}$.

    Then $\hat{\bfx}$ is also the \emph{(exact) solution} of
    \begin{equation*}
        A^{T} A \bfx = A^{T} \bfb
    \end{equation*}
    This is call the \alert{normal equation} of $A \bfx = \bfb$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{6.5-fig-2.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 13 --- Find least-squares solutions}

    The set of least-squares solutions of $A \bfx = \bfb$
    coincides with the nonempty set of solutions of
    $A^T A \bfx =A^T \bfb$.

\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Find the least-squares solutions for
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            4 & 0 \\
            0 & 2 \\
            1 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfb
        =
        \begin{bmatrix}
            2 \\ 0 \\ 11
        \end{bmatrix}
    \end{equation*}

    Solution: The normal equation $A^{T} A \bfx = A^{T} \bfb$ is
    \begin{equation*}
        \begin{bmatrix} 
            17 & 1\\
            1 & 5\\
        \end{bmatrix} 
        \begin{bmatrix}
            x_1 \\ x_2
        \end{bmatrix}
        =
        \begin{bmatrix}
            19 \\ 11
        \end{bmatrix}
    \end{equation*}
    Since $A^{T} A$ is invertible,
    the least-squares solution is
    \begin{equation*}
        \hat{\bfx}
        =
        (A^{T} A)^{-1} A^{T} \bfb
        =
        \dots
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Find the least-squares solutions for
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            1 & 1 & 0 & 0 \\
            1 & 1 & 0 & 0 \\
            1 & 0 & 1 & 0 \\
            1 & 0 & 1 & 0 \\
            1 & 0 & 0 & 1 \\
            1 & 0 & 0 & 1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfb
        =
        \begin{bmatrix}
            -3 \\ -1 \\ 0 \\ 2 \\ 5 \\ 1
        \end{bmatrix}
    \end{equation*}
    Solution: The augmented matrix for $A A^T \bfx = A^T \bfb$ has echelon form
    \begin{equation*}
        \begin{bmatrix}
            1 & 0 & 0 & 1 & 3 \\
            0 & 1 & 0 & -1 & -5 \\
            0 & 0 & 1 & -1 & -2 \\
            0 & 0 & 0 & 0 & 0 \\
        \end{bmatrix}
    \end{equation*}
    \cake{} What is $\hat{\bfx}$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 13 --- Unique least-squares solutions}

    Let $A$ be a $m \times n$ matrix. The following statements are equivalent:
    \begin{enumerate}[a.]
        \item The equation $A \bfx = \bfb$ has a unique least-squares solution for each
            $\bfb \in \dsR^m$.
        \item The columns of $A$ are linearly independent.
        \item The matrix $A^T A$ is invertible.
    \end{enumerate}
    When these statements are true, then the least-squares solution for $A$ is
    \begin{equation*}
        \hat{\bfx} = (A^{T} A)^{-1} A^{T} \bfb.
    \end{equation*}

    Proof: Exercise 27-29.
\end{frame}

\begin{frame}
    \frametitle{Least-squares errors}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The distance from $\bfb$ to $A \hat{\bfx}$ is called the
            \alert{least-squares error}.

            In Example 1, the least-squares error is
            \begin{equation*}
                \norm{\bfb - A \hat{\bfx}}
                =
                \sqrt{84}
            \end{equation*}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.5-fig-3.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Alternative Calculations of Least-Squares Solutions}

\begin{frame}
    \frametitle{Example 4}

    What are the least-squares solutions for
    \begin{equation*}
        A =
        \begin{bmatrix}
            1 & -6 \\
            1 & -2 \\
            1 & 1 \\
            1 & 7 \\
        \end{bmatrix}
        ,
        \qquad
        \bfb =
        \begin{bmatrix}
            -1 \\ 2 \\ 1 \\ 6
        \end{bmatrix}
    \end{equation*}
    \hint{} The columns of $A$ are orthogonal.
\end{frame}

\begin{frame}
    \frametitle{Theorem 15 --- Using QR factorization}

    Let $A$ be a $m \times n$ matrix with linearly independent columns and let $A = QR$ be a
    QR factorization as in Theorem 12.

    Then for each $\bfb \in \dsR^{m}$ there is a unique least-squares solution
    \begin{equation*}
        \hat{\bfx} = R^{-1} Q^T \bfb
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 15 --- What the proof needs}
    \begin{block}{Theorem 12 --- QR Factorization}

        If $A$ has linearly independent columns,
        then $A$ can be factored as $A = QR$,
        where $Q$ is a matrix whose columns form an
        orthonormal basis for $\colspace A$.
    \end{block}
    \begin{block}{Theorem 10 --- Orthonormal bases}

        If the columns of $Q$ is an \emph{orthonormal basis} of $W$, then
        \begin{equation*}
            \proj_{W} \bfy
            =
            Q Q^{T} \bfy
            .
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    {\small
    \begin{block}{Theorem 13}
    Let $A$ be $m \times n$. The following statements are equivalent:
    \begin{enumerate}
        \item[b.] The columns of $A$ are linearly independent.
        \item[c.] The matrix $A^T A$ is invertible.
    \end{enumerate}
    \end{block}
    }

    \begin{block}{Proof of $\text{c} \imp \text{b}$ }
        Assume that the columns of $A$ are not linearly independent.
        Then there exist $\bfx \ne \bfzero$ such that
        \begin{equation*}
            A \bfx
            =
            \blankshort{}
        \end{equation*}
        This implies that $A^{T}A \bfx = \blankveryshort{}$.
        This a contradiction to c because c implies that \blankshort{}.
    \end{block}
\end{frame}

\section{6.6 Machine Learning and Linear Models}

\begin{frame}
    \frametitle{Because of Machine Learning}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{machine_learning_2x.png}
        \caption*{\url{https://xkcd.com/1838/}}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Fitting a line to experimental data}

    Give experimental data $(x_{1}, y_{1}), \dots (x_{n}, y_{n})$,
    how to find a line which \emph{best}
    fit the data?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{linear-regression.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Least-squares lines}

    The \alert{least-squares line} is the line $y = \beta_0 + \beta_1 x$ which
    minimizes \alert{the sum of the squares of the residues}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{6.6-fig-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Finding the least-squares line}

    Let
    \begin{equation*}
        X =
        \begin{bmatrix}
            1 & x_{1} \\
            1 & x_{2} \\
            \vdots & \vdots \\
            1 & x_{n} \\
        \end{bmatrix}
        ,
        \quad
        \symbf{\beta}
        =
        \begin{bmatrix}
            \beta_0 \\ \beta_1
        \end{bmatrix}
        ,
        \quad
        \bfy =
        \begin{bmatrix}
            y_{1} \\
            y_{2} \\
            \vdots \\
            y_{n} \\
        \end{bmatrix}
    \end{equation*}
    Then
    \begin{equation*}
        X \symbf{\beta} - \bfy
        =
            \begin{bmatrix}
            \beta_0 + \beta_1 x_{1} - y_{1} \\
            \beta_0 + \beta_1 x_{2} - y_{2} \\
            \vdots \\
            \beta_0 + \beta_1 x_{n} - y_{n} \\
        \end{bmatrix}
    \end{equation*}

    Thus \alert{the sum of the squares of the residues} is $\norm{X
    \symbf{\beta} - \bfy}^{2}$.

    So finding the \emph{least-squares solutions} $\hat{\symbf{\beta}}$ of $X \symbf{\beta} = \bfy$
    is the same as finding the \emph{least-squares line}.
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Finding the least-square line for the data points $(2,1), (5,2), (7,3), (8,3)$.

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{6.6-example-1.png}
    \end{figure}
\end{frame}

\subsection{The General Linear Model}

\begin{frame}
    \frametitle{The General Linear Model}

    In general, a linear model is of the form $X \symbf{\beta} = \bfy$,
    where $X$ can have various forms.

    The aim is to find $\symbf{\beta}$ which minimizes the \emph{norm/length} of the residue vector $\symbf{\epsilon}$ which
    satisfies
    \begin{equation*}
        X \symbf{\beta} = \bfy + \symbf{\epsilon}.
    \end{equation*}

    This amounts to find the \emph{least-squares solutions} for $X \symbf{\beta} = \bfy$ by
    finding $\hat{\symbf{\beta}}$, the exact solution of
    \begin{equation*}
        X^{T} X \symbf{\beta} = X^{T} \bfy.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Given data $(x_{1}, y_{1}), \dots, (x_{n}, y_{n})$, how to find the
    \emph{least-squares fit} of the data by
    \begin{equation*}
        y = \beta_{0} + \beta_{1} x + \beta_{2} x^{2}
    \end{equation*}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.6-fig-4.png}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}

        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Given data $(x_{1}, y_{1}), \dots, (x_{n}, y_{n})$, how to find the
    \emph{least-squares fit} of the data by
    \begin{equation*}
        y = \beta_{0} + \beta_{1} x + \beta_{2} x^{2} + \beta_{3} x^{3}
    \end{equation*}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.6-fig-5.png}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}

        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{Multiple Regression}
%
%    Given data $(u_{1}, v_{1}, y_{1}), \dots, (u_{n}, v_{n}, y_{n})$, how to find the
%    \emph{least-squares fit} of the data by
%    \begin{equation*}
%        y = \beta_{0} + \beta_{1} u + \beta_{2} v
%    \end{equation*}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%            \begin{figure}[htpb]
%                \centering
%                \includegraphics[width=\linewidth]{6.6-fig-6.png}
%            \end{figure}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%
%        \end{column}
%    \end{columns}
%\end{frame}

%\begin{frame}
%    \frametitle{One Last Exercise}
%
%    \think{} Given the data $(1, 7.9), (2, 5.4), (3, -0.9)$,
%    write down the system of linear equations whose solution will give the
%    least-squares fit of the data in the form of
%    \begin{equation*}
%        y = \beta_{1} \cos(x) + \beta_{2} \sin(x)
%    \end{equation*}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%            \begin{figure}[htpb]
%                \centering
%                \includegraphics[width=\linewidth]{6.6-exercise.png}
%            \end{figure}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%
%        \end{column}
%    \end{columns}
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{6.5}{27, 29, 31}
            \sectionhomework{6.6}{21, 23, 25}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[standout]
    \Huge{}
    The End
    \begin{figure}[htpb]
        \includegraphics[width=0.6\textwidth]{bunny-fun.jpg}
    \end{figure}
\end{frame}

\end{document}
