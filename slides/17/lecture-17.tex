\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- The Dimension of a Vector Space}

\begin{document}

\maketitle{}

%\section*{Review of Last Week}
%
%\begin{frame}
%    \frametitle{What are vectors}
%    
%    \begin{figure}[htpb]
%        \centering
%        \href{https://youtu.be/fNk_zzaMoSs}{\includegraphics[width=\textwidth]{YouTube-vector.png}}
%    \end{figure}
%\end{frame}

\lectureoutline{}

\begin{frame}
    \frametitle{What we have learned last week?}
    
    \begin{enumerate}[<+->]
        \item What are vector spaces?
        \item What are column spaces and null spaces?
        \item What are linear independence?
        \item What are bases?
    \end{enumerate}
\end{frame}

\section{4.5 The Dimension of a Vector Space}

\begin{frame}
    \frametitle{Introduction}

    What do we mean when say $1$ or $2$ or $3$-dimensional space?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{r3-subspace.png}
    \end{figure}
    %\emoji{clapper} See this \href{https://youtu.be/fo9wmJ0lzh8}{video} by Dr.~Trefor Bazett.
\end{frame}

\begin{frame}
    \frametitle{More than $2$?}

    \cake{} In $\dsR^{2}$, can you find more than $2$ vectors which are linearly
    independent?
\end{frame}

\begin{frame}
    \frametitle{Theorem 10 --- Maximal size of linearly independent set}

    If $V$ has a basis $\scB = \{\bfb_1, \dots, \bfb_{n}\}$,
    then any set with more than $n$ vectors in $V$ is linearly dependent.

    In other words, a linearly independent set in $V$ has no more than $n$ vectors.
\end{frame}

\begin{frame}
    \frametitle{Theorem 11 --- Exact size of a basis}

    If $V$ has a basis of $n$ vectors,
    then every basis of $V$ must consist of exactly $n$ vectors.
\end{frame}

\begin{frame}
    \frametitle{Dimensions}

    If $V$ is spanned by a finite set, then $V$ is \alert{finite-dimensional},
    otherwise it is \alert{infinite-dimensional}.

    The \alert{dimension} of $V$, $\dim V$, is the number of vectors in a basis of
    $V$.

    \pause{}

    \begin{block}{\dizzy{} A strange case}
    The \alert{dimension} of $\{\bfzero\}$ is defined to be $0$.
    See details \href{https://math.stackexchange.com/q/1909645/1618}{here}.

    \cake{} Can $\{\bfzero\}$ be a basis of $\{\bfzero\}$?
    \vspace{3cm}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    \cake{} What are the dimensions of $\dsR^{n}$ and $\dsP_{n}$?

\end{frame}

\begin{frame}
    \frametitle{Example 2}

    What is the dimension of
    \begin{equation*}
        H
        =
        \Span
        {
            \begin{bmatrix}
                3 \\ 6 \\ 2
            \end{bmatrix}
            ,
            \begin{bmatrix}
                -1 \\ 0 \\ 1
            \end{bmatrix}
        }
        .
    \end{equation*}
    \cake{} Are these two vectors linearly independent?
\end{frame}

\begin{frame}
    \frametitle{Example 3}
    
    What is the dimension of 
    \begin{equation*}
        H
        =
        \left\{
            \begin{bmatrix} 
                a - 3b + 6c \\
                5 a + 4d \\
                b - 2 c - d\\
                5d
            \end{bmatrix} 
            :
            a, b, c, d \in \dsR
        \right\}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    The subspaces of $\dsR^3$ can be classified by dimension.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{r3-subspace.png}
    \end{figure}

    \only<1>{\cake{} Do $\dsR^{3}$ has subspace of $4$ more or dimensions?}
    \only<2>{\think{} Can we have a $3$-dimensional subspace $\dsR^{3}$ other than
    $\dsR^{3}$?}
\end{frame}

\subsection{Subspaces of a Finite-Dimensional Space}

\begin{frame}
    \frametitle{Theorem 12 --- Dimensions of subspaces}

    Let $H$ be a subspace of a finite-dimensional vector space $V$.
    Any linearly independent set in $H$ can be expanded,
    if \emph{necessary}, to a basis for $H$.
    Also, $H$ is finite-dimensional and
    \begin{equation*}
        \dim H \le \dim V
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 13 --- The basis theorem}

    Let $V$ be a $p$-dimensional vector space with $p \ge 1$.

    Any linearly independent set of $p$ vectors is a basis of $V$.

    \pause{}

    Any set of $p$ vectors spanning $V$ is a basis of $V$.
    \begin{block}{Theorem 5 (4.3) --- The Spanning Set Theorem}

        Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$
        and $H = \Span{\bfv_{1}, \dots, \bfv_{p}}$.
        \begin{enumerate}
            \item[b.] If $H \ne \{\bfzero\}$, then some subsets of $S$ is a basis of $H$.
        \end{enumerate}
    \end{block}
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Show that
%    \begin{equation*}
%        \scB = \{1, 1-t, 2 -4 t + t^2, 6-18t+9t^2-t^3\}
%    \end{equation*}
%    forms a basis of $\dsP_{3}$.
%
%    \begin{block}{Proof}
%        \small{}
%        First we can to show that these vectors are \blankshort{}
%        by showing that their coordinate vectors are \blankshort{}.
%
%        Then by Theorem \blankshort{} in this section, 
%        they form a basis of $\dsP_{3}$ because \blankshort{}.
%    \end{block}
%\end{frame}

%\subsection*{\zany{} None-integer ``dimensions''}
%
%\begin{frame}
%    \frametitle{Infinite:dimensional vector space}
%    
%    \cake{} Can you guess what is the dimension of the vector space
%    $\dsS$ be the set of doubly infinite sequences like
%    \begin{equation*}
%        \{y_{k}\} = (\cdots, y_{-3}, y_{-2}, y_{-1}, y_{0}, y_{1}, y_{2}, \cdots)
%    \end{equation*}
%
%    \pause{}
%
%    \cake{} What about
%    Let $C([0,1])$ be to the set of continuous function on the interval $[0,1]$?
%
%    \dizzy{} Do they have the same dimension?
%\end{frame}
%
%\begin{frame}
%    \frametitle{Fractal dimensions}
%    The word \emph{dimension} in mathematics in general means \emph{complexity}.
%
%    The following \alert{fractal} has a \alert{fractal dimension} of
%    $1.585$.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.4\linewidth]{Sierpinski_triangle.png}
%        \caption*{Sierpinski Triangle}
%    \end{figure}
%
%    \emoji{clapper} See this \href{https://youtu.be/gB9n2gHsHN4}{video} by  
%    3Blue1Brown.
%\end{frame}

\subsection{Row Space (Section 4.3)}

\begin{frame}
    \frametitle{Row space}

    The \alert{row space} of $A$, denote by $\rowspace A$,  is $\colspace A^{T}$.

    Let
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            -2 & -5 & 8 & 0 & -17 \\
            1 & 3 & -5 & 1 & 5 \\
            3 & 11 & -19 & 7 & 1 \\
            1 & 7 & -13 & 5 & -3 \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            \bfr_1 \\ \bfr_2 \\ \bfr_3 \\ \bfr_4
        \end{bmatrix}
    \end{equation*}
    Then
    \begin{equation*}
        \rowspace A = \colspace A^{T}
        = \Span{\bfr_{1}, \dots, \bfr_{4}}.
    \end{equation*}

    \cake{} What $\rowspace A^{T}$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 7 (4.3) --- Row space and row equivalence}

    If $A \sim B$, then $\rowspace A = \rowspace B$.

    If $B$ is in echelon form, the nonzero rows of $B$ form a basis of both row
    space.
\end{frame}

\begin{frame}
    \frametitle{Example --- A row space}

    \cake{} Find the bases of $\rowspace A$, $\colspace A$ and $\nullspace A$
    where
    \begin{equation*}
        A=
        \left[
            \begin{array}{ccccc}
                -2 & -5 & 8 & 0 & -17 \\
                1 & 3 & -5 & 1 & 5 \\
                3 & 11 & -19 & 7 & 1 \\
                1 & 7 & -13 & 5 & -3 \\
            \end{array}
        \right]
        \sim
        \left[
            \begin{array}{ccccc}
                1 & 0 & 1 & 0 & 1 \\
                0 & 1 & -2 & 0 & 3 \\
                0 & 0 & 0 & 1 & -5 \\
                0 & 0 & 0 & 0 & 0 \\
            \end{array}
        \right]
    \end{equation*}

    \pause{}

    \begin{block}{The dimension of a row space}
        The dimensions of $\rowspace A$ and $\colspace A$ are the same, i.e.,
        \begin{equation*}
            \dim \rowspace A = \dim \colspace A
        \end{equation*}
    \end{block}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.5}{27, 31, 35, 39, 41, 51}
        \end{column}
    \end{columns}
\end{frame}
\end{document}
