\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Characterizations of Invertible Matrices, Partitioned Matrices}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{2.3 Characterizations of Invertible Matrices}

\subsection{The Invertible Matrix Theorem}

\begin{frame}
    \frametitle{Theorem 8 --- The Invertible Matrix Theorem}

    Let $A$ be $n \times n$.
    The following are equivalent.
    \begin{enumerate}
        \footnotesize{}
        \item[a.] $A$ is an invertible matrix.
        \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
        \item[c.] $A$ has $n$ pivot positions.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[e.] The columns of $A$ form a linearly independent set.
        \item[f.] The linear transformation $\bfx \mapsto A \bfx$ is one-to-one.
        \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[h.] The columns of $A$ spans $\dsR^n$.
        \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
        \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
        \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
        \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}
\end{frame}

\begin{frame}[c]
    \frametitle{A road map of proof}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\linewidth]{invertible-matrix-01.png}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \includegraphics[width=0.9\linewidth]{invertible-matrix-02.png}
            \end{figure}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{Theorem 8 --- Proof of (j) \rightarrow{} (d)}

    \begin{enumerate}
        \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
    \end{enumerate}

    \think{} How to prove (j) \rightarrow{} (d)?

    \hint{} Proof by contrapositive, i.e., $\neg (d) \imp \neg (j)$.
\end{frame}

\begin{frame}
    \label{frame:ajdcb}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[c.] $A$ has $n$ pivot positions.
        \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
    \end{enumerate}

    \think{} How to prove ---
    \begin{itemize}
        \item (a) \rightarrow{} (j) \cake{}
        \item (j) \rightarrow{} (d) --- See the previous slide.
        \item (d) \rightarrow{} (c) \cake{}
        \item (c) \rightarrow{} (b) --- $A$ is of size $n \times n$.
        \item (b) \rightarrow{} (a) --- Theorem 7 (2.2).
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}
    \begin{enumerate}
        \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
        \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
    \end{enumerate}
    
    \think{} How to prove (k) \rightarrow{} (g)?
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
        \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[c.] $A$ has $n$ pivot positions.
    \end{enumerate}

    \think{} How to prove ---
    \begin{itemize}
        \item (a) \rightarrow{} (k) \cake{}
        \item (k) \rightarrow{} (g) --- See the previous slide.
        \item (g) \rightarrow{} (c) --- By Theorem 4 (1.4).
        \item (c) \rightarrow{} (a) --- See page~\pageref{frame:ajdcb}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[h.] The columns of $A$ spans $\dsR^n$.
        \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
    \end{enumerate}

    \think{} How to prove ---
    \begin{itemize}
        \item (g) $\leftrightarrow$ (h) --- By Theorem 4 (1.4).
        \item (g) $\leftrightarrow$ (i) \cake{}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[e.] The columns of $A$ form a linearly independent set.
        \item[f.] The linear transformation $\bfx \mapsto A \bfx$ is one-to-one.
    \end{enumerate}

    \think{} How to prove ---
    \begin{itemize}
        \item (d) $\leftrightarrow$ (e) \cake{}
        \item (e) $\leftrightarrow$ (f) --- By Theorem 12 (1.9)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8}

    \begin{enumerate}
        \item[a.] $A$ is an invertible matrix.
        \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}

    \think{} How to prove ---
    \begin{itemize}
        \item (a) \rightarrow{} (l) --- By Theorem 6 (2.2)
        \item (l) \rightarrow{} (a) \cake{}
    \end{itemize}

    \begin{block}{Theorem 6}
        \begin{enumerate}
            \item[c.] If $A$ is invertible, then so is $A^{T}$.
        \end{enumerate}
    \end{block}
\end{frame}

\subsection{Invertible Linear Transformations}%

\begin{frame}
    \frametitle{Invertible}

    A linear transform
    $T: \dsR^{n} \mapsto \dsR^{n}$
    is \alert{invertible} if there exists
    $S: \dsR^{n} \mapsto \dsR^{n}$
    such that
    \begin{equation*}
        \begin{aligned}
            & S(T(\bfx)) = \bfx, \qquad (\text{for all } \bfx \in \dsR^{n}) \\
            & T(S(\bfx)) = \bfx, \qquad (\text{for all } \bfx \in \dsR^{n}) \\
        \end{aligned}
    \end{equation*}

    $S$ is called the \alert{inverse} of $T$.

    \cake{} Prove that if $T$ is invertible, 
    then it must be a \emph{onto} mapping.
\end{frame}

\begin{frame}
    \frametitle{Theorem 9 --- Relation to invertible matrices}

    Let $T: \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation and
    let $A$ be its standard matrix.
    Then $T$ is \emph{invertible} if and only if $A$ is \emph{invertible}.
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Prove that if a linear mapping $T:\dsR^{n} \mapsto \dsR^{n}$ is one-to-one,
    then $T$ is invertible.

    \hint{} Use the Invertible Matrix Theorem and the following
    %\begin{block}{Theorem 9}
    %    Let $T: \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation and
    %    let $A$ be its standard matrix.
    %    Then $T$ is \emph{invertible} if and only if $A$ is \emph{invertible}.
    %\end{block}
    \begin{block}{Theorem 12 (1.9)}
        Let $T: \dsR^{m} \mapsto\dsR^{n}$ be a linear transformation,
        and let $A$ be its standard matrix.

        Then $T$ is one-to-one if and only if the columns of $A$ are \emph{linearly
        independent}.
    \end{block}
\end{frame}

\section{2.4 Partitioned Matrices}%

\begin{frame}
    \frametitle{Example 1 --- Partition a matrix into blocks}

    How to write this matrix
    \begin{equation*}
        A
        =
        \left[
            \begin{array}{cccccc}
                a_{11} & a_{12} & a_{13} & a_{14} & a_{15} & a_{16} \\ 
                a_{21} & a_{22} & a_{23} & a_{24} & a_{25} & a_{26} \\
                a_{31} & a_{32} & a_{33} & a_{34} & a_{35} & a_{36} \\ 
            \end{array}
        \right]
    \end{equation*}
    as a $2 \times 3$ \alert{partitioned/block matrix}?

    \pause{}

    \emoji{fire}
    Monition:
    \begin{itemize}
        \item Useful when the matrices are too large for the memory of computers.
        \item Speed up computations.
    \end{itemize}
\end{frame}

%\begin{frame}
%    \frametitle{}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{partition-example-01.png}
%    \end{figure}
%\end{frame}

\subsection{Multiplication of Partitioned Matrices}

\begin{frame}
    \frametitle{Example 3 --- Multiplication}

    Let
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccc|cc}
                4 & 1 & 8 & 3 & -2 \\
                0 & -5 & 2 & 7 & 9 \\ \hline
                -1 & 6 & -3 & 5 & 2 \\
            \end{array}
        \right]
        ,
        \qquad
        B
        =
        \left[
            \begin{array}{cc}
                -8 & 9 \\
                2 & -5 \\
                7 & 4 \\ \hline
                0 & -6 \\
                -9 & 1 \\
            \end{array}
        \right]
    \end{equation*}
    How to compute $A B$ using matrix partitions?
\end{frame}

%\begin{frame}
%    \frametitle{Multiplication}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{partition-example-02.png}
%        \includegraphics[width=\linewidth]{partition-example-03.png}
%    \end{figure}
%
%\end{frame}

\begin{frame}
    \frametitle{Example 4}
    
    How to compute
    \begin{equation*}
        \begin{bmatrix}
            -3 & 1 & 2 \\
            1 & -4 & 5 \\
        \end{bmatrix}
        \begin{bmatrix}
            a & b \\
            c & d \\
            e & f \\
        \end{bmatrix}
    \end{equation*}
    using matrix partitions?
\end{frame}

\begin{frame}
    \frametitle{Theorem 10 --- Column-Row Expansion}

    If $A$ is $m \times n$ and $B$ is $n \times p$, then
    \begin{equation*}
        \begin{aligned}
            A B
            &
            =
            \begin{bmatrix}
                \mcol_1(A) & \mcol_2(A) & \cdots & \mcol_n(A)
            \end{bmatrix}
            \begin{bmatrix}
                \mrow_1(B) \\ \mrow_2(B) \\ \cdots \\ \mrow_n(B)
            \end{bmatrix}
            \\
            &
            =
            \mcol_1(A)
            \mrow_1(B)
            +
            \mcol_2(A)
            \mrow_2(B)
            +
            \cdots
            +
            \mcol_n(A)
            \mrow_n(B)
            .
        \end{aligned}
    \end{equation*}
\end{frame}

\subsection{Inverses of Partitioned Matrices}

\begin{frame}
    \frametitle{Example 5 --- Inverses of Partitioned Matrices}

    The matrix
    \begin{equation*}
        A =
        \begin{bmatrix}
            A_{11} & A_{12} \\
            0 & A_{22} \\
        \end{bmatrix}
    \end{equation*}
    is said to be \alert{block upper triangular}.

    Assume that $A_{11}$ is of size $p \times p$ and $A_{22}$ is of size $q \times q$,
    and $A$ is invertible. 
    Then what is $A^{-1}$?
    %\begin{equation*}
    %    A^{-1}
    %    =
    %    \begin{bmatrix}
    %        A_{11}^{-1} & -A_{11}^{-1} A_{12} A_{22}^{-1} \\
    %        0 & A_{22}^{-1} \\
    %    \end{bmatrix}
    %\end{equation*}

    %Proof ---
\end{frame}

\begin{frame}
    \frametitle{Block Diagonal Matrix}

    A \alert{block diagonal matrix} is a \emph{partitioned matrix} with zero blocks off the main
    diagonal. 

    Such a matrix is invertible if and only if each block on the diagonal
    is invertible. 

    \emoji{eye} See Exercises 13 and 14.

    \cake{} When is the following matrix invertible?
    \begin{equation*}
        A =
        \begin{bmatrix}
            A_{11} & A_{12} \\
            0 & A_{22} \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{2.3}{21, 25, 43, 45, 47}
            \sectionhomework{2.4}{15, 17, 23, 25, 27}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
