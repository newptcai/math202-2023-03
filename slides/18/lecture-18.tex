\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Change of Basis}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{4.5 The Dimension of a Vector Space}

\subsection{The Dimensions of \texorpdfstring{$\nullspace A$}{\mathrm{Nul} A}, \texorpdfstring{$\colspace A$}{\mathrm{Col} A}, and \texorpdfstring{$\rowspace A$}{\mathrm{Row} A}}

\begin{frame}[c]
    \frametitle{What are ranks according to YouTube}

    \begin{figure}[htpb]
        \centering
        \href{https://youtu.be/uQhTuRlWMxw?t=400}{\includegraphics[width=\textwidth]{YouTube-rank.png}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The rank of a matrix}

    The \alert{rank} of an $m \times n$ matrix $A$ is the dimension of
    $\colspace A$, which is the number of pivot columns by Theorem 6 (4.3).

    \pause{}

    \begin{block}{Example 9 (4.3)}
        Since
        \begin{equation*}
            A =
            \begin{bmatrix}
                1 & 4 & 0 &  2 & -1\\
                3 & 12 & 1 & 5 & 5 \\
                2 & 8 & 1 &  3 & 2 \\
                5 & 20 & 2 &  8 & 8 \\
            \end{bmatrix}
            \sim
            \begin{bmatrix}
                1 & 4 & 0 &  2 & 0\\
                0 & 0 & 1 & -1 & 0 \\
                0 & 0 & 0 &  0 & 1 \\
                0 & 0 & 0 &  0 & 0 \\
            \end{bmatrix}
            =
            B
            ,
        \end{equation*}
        the $\{\bfa_1, \bfa_3, \bfa_5\}$ forms a basis of $\nullspace
        A$,
        just as $\{\bfb_1, \bfb_3, \bfb_5\}$ forms a basis of $\colspace B$.
    \end{block}

    \hint{} $\rank A = \dim \colspace A = \dim \rowspace A$.
\end{frame}

\begin{frame}
    \frametitle{The nullity of a matrix}
    The \alert{nullity} of an $m \times n$ matrix $A$ is the dimension of
    $\nullspace A$, which is the number of free variables in $A \bfx =
    \bfzero$ (see 4.2).

    \pause{}

    \begin{block}{Example 3 (4.2)}
        \small{}
        Let
        \begin{equation*}
            A =
            \left[
                \begin{array}{ccccc}
                    -3 & 6 & -1 & 1 & -7 \\
                    1 & -2 & 2 & 3 & -1 \\
                    2 & -4 & 5 & 8 & -4 \\
                \end{array}
            \right]
            .
        \end{equation*}
        The solution of $A \bfx = \bfzero$ is
        \begin{equation*}
            \bfx
            =
            \begin{bmatrix}
                2 x_2 + x_4 - 3 x_5 \\
                x_2 \\
                -2 x_4 + 2 x_5 \\
                x_{4} \\
                x_{5} \\
            \end{bmatrix}
            =
            x_{2}
            \begin{bmatrix}
                2 \\ 1 \\ 0 \\ 0 \\ 0
            \end{bmatrix}
            +
            x_{4}
            \begin{bmatrix}
                1 \\ 0 \\ -2 \\ 1 \\ 0
            \end{bmatrix}
            +
            x_{5}
            \begin{bmatrix}
                -3 \\ 0 \\ 2 \\ 0 \\ 1
            \end{bmatrix}
            =
            x_{2} \bfu + x_{4} \bfv + x_{5} \bfw.
        \end{equation*}
        Thus $\{\bfu, \bfv, \bfw\}$ is a basis of $\nullspace A$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    \cake{} Consider
    \begin{equation*}
        A =
        \begin{bmatrix}
            -3 & 6 & -1 & 1 & -7 \\
            1 & -2 & 2 & 3 & -1 \\
            2 & -4 & 5 & 8 & -4 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & -2 & 2 & 3 & -3 \\
            0 & 0 & 1 & 2 & -2 \\
            0 & 0 & 0 & 0 & 0 \\
        \end{bmatrix}
    \end{equation*}
    \cake{} What are the dimensions of $\nullspace A$ and $\colspace A$?

    \cake{} What are the nullity and rank of $A$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 14 --- The Rank Theorem}
    
    Let $A$ be a $m \times n$ matrix. We have
    \begin{equation*}
        \rank A + \nullity A = m
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 6}
    \cake{} If A is a $7 \times 9$ matrix with a two-dimensional null space, what is the rank of A?

    \pause{}

    \cake{} Could a $6 \times 9$ matrix have a two-dimensional null space?
\end{frame}

\begin{frame}
    \frametitle{Example 7 --- The big picture of linear algebra}

    Let 
    $A = 
    \begin{bmatrix} 
        3 & 0 & -1 \\
        3 & 0 & -1 \\
        4 & 0 & 5
    \end{bmatrix}
    $
    .
    Note that
    $A 
    \sim
    \begin{bmatrix} 
        1 & 0 & 0\\
        0 & 0 & 1 \\
        0 & 0 & 0 \\
    \end{bmatrix}
    $
    .

    \cake{} What are $\rowspace A$, $\nullspace A$, $\colspace A = \rowspace
    A^{T}$ and $\nullspace A^T$?
    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{4.5-fig-2.png}
    \end{figure}

    \emoji{eye} See this wonderful \href{https://youtu.be/ggWYkes-n6E}{YouTube
    video} for more details.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Consider 
    $
    A=
    \begin{bmatrix} 
        1 & 2 & 3 \\
        4 & 5 & 6 \\
    \end{bmatrix} 
    $
    .

    \cake{} What are the dimensions of $\rowspace A$, 
    $\nullspace A$, 
    $\colspace A = \rowspace A^{T}$ and $\nullspace A^T$?
\end{frame}
\subsection{Rank and the Invertible Matrix Theorem}

\begin{frame}
    \frametitle{Theorem 8 (2.3) --- The Invertible Matrix Theorem}

    Let $A$ be $n \times n$.
    The following are equivalent.
    \begin{enumerate}
        \footnotesize{}
        \item[a.] $A$ is an invertible matrix.
        \item[b.] $A$ is row equivalent to the $n \times n$ identity matrix.
        \item[c.] $A$ has $n$ pivot positions.
        \item[d.] The equation $A \bfx = 0$ has only the trivial solution.
        \item[e.] The columns of $A$ form a linearly independent set.
        \item[f.] The linear transformation $\bfx \mapsto A \bfx$ is one-to-one.
        \item[g.] The equation $A \bfx = \bfb$ has at least one solution for each $\bfb$ in $\dsR^n$.
        \item[h.] The columns of $A$ spans $\dsR^n$.
        \item[i.] The linear transformation $\bfx \mapsto A \bfx$ maps $\dsR^n$ onto $\dsR^n$.
        \item[j.] There is an $n \times n$ matrix $C$ such that $CA = I_n$.
        \item[k.] There is an $n \times n$ matrix $D$ such that $AD = I_n$.
        \item[l.] $A^T$ is an invertible matrix.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{The Invertible Matrix Theorem (cont.)}

    \dizzy{} Let's add a few more.

    \begin{enumerate}
        \item[m.] The columns of $A$ is a basis of $\dsR^{n}$
        \item[n.] $\colspace A = \dsR^{n}$
        \item[o.] $\dim \colspace A = n$
        \item[p.] $\rank A = n$
        \item[q.] $\nullspace A = \{\bfzero\}$
        \item[r.] $\dim \nullspace A = 0$
    \end{enumerate}
\end{frame}

%\begin{frame}
%    \frametitle{Practice Problem 1}
%    
%    Let $V$ be  a nonzero finite-dimensional vector space.
%    Are the following two statements true?
%
%    \begin{enumerate}[<+->]
%        \item If $\dim V = p$ and if $S$ is a linearly dependent subset of $V$, then $S$ contains more than $p$ vectors.
%        \item If $S$ spans $V$ and if $T$ is a subset of $V$ that contains more vectors than $S$, then $T$ is linearly dependent.
%    \end{enumerate}
%\end{frame}

\section{4.6 Change of Basis}

\begin{frame}[c]
    \frametitle{An introduction by YouTube}
    
    \begin{figure}[htpb]
        \centering
        \href{https://youtu.be/Qp96zg5YZ_8}{\includegraphics[width=\textwidth]{YouTube-change-of-basis.png}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Two bases}

    Consider two bases 
    $\scB=\{\bfb_{1}, \bfb_{2}\}$ 
    and
    $\scC=\{\bfc_{1}, \bfc_{2}\}$.

    \cake{} What the coordinates $[\bfx]_{\scB}$
    and $[\bfx]_{\scC}$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{change-of-basis-01.png}
        \caption*{Two bases and the same vector}%
    \end{figure}

    \think{} Given $[\bfx]_{\scB}$, how do we compute $[\bfx]_{\scC}$
    and vice versa.
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Consider two bases
    $\scB = \{\bfb_1, \bfb_2\}$
    and
    $\scC = \{\bfc_1, \bfc_2\}$, such that
    \begin{equation*}
        \bfb_1 = 4 \bfc_1 + \bfc_2,
        \qquad
        \bfb_2 = -6 \bfc_1 + \bfc_2
        .
    \end{equation*}
    Suppose that we are given
    \begin{equation*}
        \bfx = 3 \bfb_1 + \bfb_2.
    \end{equation*}
    \think{} What is $[\bfx]_\scC$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 15 --- Change of bases}

    Let
    $\scB = \{\bfb_1, \bfb_2, \dots, \bfb_n\}$
    and
    $\scC = \{\bfc_1, \bfc_2, \dots, \bfc_n\}$
    be two bases of $V$.
    Then
    \begin{equation*}
        [\bfx]_{\scC}
        =
        \underset{\scC\leftarrow\scB}{P}
        \cdot
        [\bfx]_{\scB}
        ,
    \end{equation*}
    \pause{}
    where
    \begin{equation*}
        \underset{\scC\leftarrow\scB}{P}
        =
        \begin{bmatrix}
            [\bfb_1]_{\scC} &
            [\bfb_2]_{\scC} &
            \cdots &
            [\bfb_n]_{\scC}
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 15 in picture}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{change-of-basis-02.png}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{A digression}
%
%    \cake{} Show that for any basis $\scB$ of $V$,
%    \begin{equation*}
%        [\bfzero]_{\scB}
%        =
%        \begin{bmatrix}
%            0 \\
%            0 \\
%            \vdots \\
%            0
%        \end{bmatrix}
%    \end{equation*}
%
%    \pause{}
%
%    \think{}  Show that a
%    $\{\bfu_{1}, \dots, \bfu_{p}\}$
%    in $V$ is linearly independent if and only if the set of coordinate vectors
%    $\{[\bfu_{1}]_{\scB}, \dots, [\bfu_{p}]_{\scB}\}$
%    is linearly independent in $\dsR^n$.
%\end{frame}

\begin{frame}
    \frametitle{The other way}

    The matrix
    \begin{equation*}
        \underset{\scC\leftarrow\scB}{P}
        =
        \begin{bmatrix}
            [\bfb_1]_{\scC} &
            [\bfb_2]_{\scC} &
            \cdots &
            [\bfb_n]_{\scC}
        \end{bmatrix}
        .
    \end{equation*}
    is invertible because its columns are linearly independent.

    \cake{} Why are they linearly independent?

    \pause{}

    Thus
    \begin{equation*}
        [\bfx]_{\scC}
        =
        \underset{\scC\leftarrow\scB}{P}
        \cdot
        [\bfx]_{\scB}
        ,
    \end{equation*}
    implies
    \begin{equation*}
        \left(\underset{\scC\leftarrow\scB}{P}\right)^{-1}
        \cdot
        [\bfx]_{\scC}
        =
        [\bfx]_{\scB}
        .
    \end{equation*}
    In other words,
    \begin{equation*}
        \underset{\scB\leftarrow \scC}{P}
        =
        \left(\underset{\scC\leftarrow\scB}{P}\right)^{-1}
        .
    \end{equation*}
\end{frame}

\subsection{Change of Basis in \texorpdfstring{$\dsR^{n}$}{R^n}}

\begin{frame}
    \frametitle{Example -- Change of Basis in $\dsR^{2}$}

    Let
    $
    \scB =
    \left\{
        \begin{bmatrix}
            2 \\ 1
        \end{bmatrix}
        ,
        \begin{bmatrix}
            -1 \\ 1
        \end{bmatrix}
    \right\}
    $
    ,
    $
    \scC =
    \left\{
        \begin{bmatrix}
            1 \\ -2
        \end{bmatrix}
        ,
        \begin{bmatrix}
            -1 \\ 1
        \end{bmatrix}
    \right\}
    $
    .

    Let
    $
    \scE = 
    \left\{
        \begin{bmatrix}
            1 \\ 0
        \end{bmatrix}
        ,
        \begin{bmatrix}
            0 \\ 1
        \end{bmatrix}
    \right\}
    $
    be the standard basis.

    \cake{} What are $[\bfb_1]_\scE$ and $[\bfb_2]_\scE$?

    \pause{}

    Then
    \begin{equation*}
        \begin{aligned}
            \underset{\scE \leftarrow \scB}{P}
            &
            =
            \begin{bmatrix}
                [\bfb_1]_{\scE},[\bfb_2]_{\scE}
            \end{bmatrix}
            =
            \begin{bmatrix}
                \underline{\hspace{2cm}}
            \end{bmatrix}
            =
            P_{\scB}
            ,
            \\
            \underset{\scE \leftarrow \scC}{P}
            &
            =
            \begin{bmatrix}
                [\bfc_1]_{\scE},[\bfc_2]_{\scE}
            \end{bmatrix}
            =
            \begin{bmatrix}
                \underline{\hspace{2cm}}
            \end{bmatrix}
            =
            P_{\scC}
            .
        \end{aligned}
    \end{equation*}

    \cake{} Given 
    $\underset{\scE \leftarrow \scC}{P}$,
    what is
    $\underset{\scC \leftarrow \scE}{P}$?

    \pause{}

    Thus
    \begin{equation*}
        \underset{\scC \leftarrow \scB}{P}
        =
        \underset{\scC \leftarrow \scE}{P}
        \cdot
        \underset{\scE \leftarrow \scB}{P}
        =
        \left(\underset{\scE \leftarrow \scC}{P}\right)^{-1}
        \cdot
        \underset{\scE \leftarrow \scB}{P}
        =
        P_\scC^{-1} P_\scB
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Change of Basis in $\dsR^{n}$}

    If
    $\scB = \{\bfb_1,\dots, \bfb_n\}$
    and
    $\scC = \{\bfc_1,\dots, \bfc_n\}$
    are bases of $\dsR^{n}$,
    and
    $\scE = \{\bfe_1,\dots, \bfe_n\}$
    is the standard basis,
    then
    \begin{equation*}
        \begin{aligned}
            \underset{\scE \leftarrow \scB}{P}
            &
            =
            \begin{bmatrix}
                [\bfb_1]_{\scE},\dots, [\bfb_n]_{\scE}
            \end{bmatrix}
            =
            \begin{bmatrix}
                \underline{\hspace{2cm}}
            \end{bmatrix}
            =
            P_{\scB}
            ,
            \\
            \underset{\scE \leftarrow \scC}{P}
            &
            =
            \begin{bmatrix}
                [\bfc_1]_{\scE},\dots, [\bfc_n]_{\scE}
            \end{bmatrix}
            =
            \begin{bmatrix}
                \underline{\hspace{2cm}}
            \end{bmatrix}
            =
            P_{\scC}
            .
        \end{aligned}
    \end{equation*}

    Thus
    \begin{equation*}
        \underset{\scC \leftarrow \scB}{P}
        =
        \underset{\scC \leftarrow \scE}{P}
        \cdot
        \underset{\scE \leftarrow \scB}{P}
        =
        \left(\underset{\scE \leftarrow \scC}{P}\right)^{-1}
        \cdot
        \underset{\scE \leftarrow \scB}{P}
        =
        P_\scC^{-1} P_\scB
        .
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Consider two bases of $\dsR^{2}$ ---
    \begin{equation*}
        \scB =
        \left\{
            \begin{bmatrix}
                -9 \\ 1
            \end{bmatrix}
            ,
            \begin{bmatrix}
                -5 \\ -1
            \end{bmatrix}
        \right\}
        ,
        \qquad
        \scC =
        \left\{
            \begin{bmatrix}
                1 \\ -4
            \end{bmatrix}
            ,
            \begin{bmatrix}
                3 \\ -5
            \end{bmatrix}
        \right\}
        .
    \end{equation*}

    \cake{} What are
    \begin{equation*}
        \underset{\scE \leftarrow \scB}{P}
        \,
        ,
        \quad
        \underset{\scE \leftarrow \scC}{P}
        \,
        ,
        \quad
        \underset{\scC \leftarrow \scB}{P}
        \,
        .
    \end{equation*}

    %\pause{}

    %\think{} Compute
    %$
    %\underset{\scC \leftarrow \scB}{P}
    %$
    %using
    %\begin{equation*}
    %    \begin{bmatrix}
    %        \underset{\scE \leftarrow \scC}{P}
    %        &
    %        \underset{\scE \leftarrow \scB}{P}
    %    \end{bmatrix}
    %    \sim
    %    \begin{bmatrix}
    %        I
    %        &
    %        \underset{\scC \leftarrow \scB}{P}
    %    \end{bmatrix}
    %\end{equation*}
\end{frame}

\begin{frame}
    \frametitle{A trick in computation}

    To compute
    $
    \underset{\scC \leftarrow \scB}{P}
    =
    \left(\underset{\scE \leftarrow \scC}{P}\right)^{-1}
    \cdot
    \underset{\scE \leftarrow \scB}{P}
    =
    P_\scC^{-1} P_\scB
    $
    by \emoji{hand}, use
    \begin{equation*}
        \begin{bmatrix}
            P_\scC
            &
            P_\scB
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            I
            &
            \underset{\scC \leftarrow \scB}{P}
        \end{bmatrix}
            .
    \end{equation*}
    In other words, row reduce the left half of the land-hand-side to $I$,
    the right half of result will be $\underset{\scC \leftarrow \scB}{P}$
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let
    $\bfb_{1} = \begin{bmatrix} 1 \\ -3 \end{bmatrix}$,
    $\bfb_{2} = \begin{bmatrix} -2 \\ 4 \end{bmatrix}$,
    $\bfc_{1} = \begin{bmatrix} -7 \\ 9 \end{bmatrix}$
    and $\bfc_{2} = \begin{bmatrix} -5 \\ 7 \end{bmatrix}$,
    and consider the two bases of $\dsR^{2}$
    $\scB = \{\bfb_1, \bfb_2\}$
    and
    $\scC = \{\bfc_1, \bfc_2\}$.
    Compute $\underset{\scB \leftarrow \scC}{P}$.
\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%
%    Let $\scF = \{f_1, f_2\}$ and $\scG = \{g_1, g_2\}$ be bases for a vector space $V$, and let $P$ be a matrix
%    whose columns are $[f_1]_G$ and $[f_2]_G$. Which of the following equations is satisfied
%    by $P$ for all $v \in V$?
%    \begin{enumerate}
%        \item $[v]_\scF = P [v]_\scG$
%        \item $[v]_\scG = P [v]_\scF$
%    \end{enumerate}
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.6}{15, 17, 19}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
