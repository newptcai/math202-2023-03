\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Eigenvectors and Linear Transformations, Discrete Dynamical System}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{5.4 Eigenvectors and Linear Transformations}

\subsection{Eigenvectors of Linear Transformations}

\begin{frame}
    \frametitle{Review --- Eigenvectors of Matrices}

    Let $A$ be an $n \times n$ matrix.

    An \alert{eigenvector}  of the $A$ is a non-zero
    vector $\bfv \in \dsR^{n}$ that satisfies
    $$
    A \bfv = \lambda \bfv
    $$
    for some $\lambda \in \dsR$.

    Here, $\lambda$ is  called the \alert{eigenvalue} 
    associated with the eigenvector $\bfv$.
\end{frame}

\begin{frame}
    \frametitle{Eigenvectors of Linear Transformations}
    Let $T: V \rightarrow V$ be a linear transformation,
    where $V$ is a vector space. 

    An \alert{eigenvector}  of $T$ is a non-zero
    vector $\bfv \in V$ that satisfies
    $$
    T(\bfv) = \blankshort{}
    $$
    for some $\lambda \in \dsR$.

    Here, $\lambda$ is  called an \alert{eigenvalue} 
    associated with the eigenvector $v$.

    \hint{} An $n \times n$ matrix can be identified with $T(\bfx) = A \bfx$.
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    Let $\{s_{n}\} = \cos(k \pi/2)$.
    Show that $\{s_{n}\}$ is an eigenvector of $D : \dsS \mapsto \dsS$ defined by
    $D(\{x_{k}\}) = \{x_{k+2}\}$.

    \pause{}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[scale=0.8]
            \begin{axis}[
                xlabel={$k$},
                ylabel={},
                grid=both,
                legend pos=outer north east,
                ]
                \addplot+[only marks, mark options={blue}, blue]
                    coordinates {
                        (-5, 0)
                        (-4, -1)
                        (-3, 0)
                        (-2, 1)
                        (-1, 0)
                        (0, 1)
                        (1, 0)
                        (2, -1)
                        (3, 0)
                        (4, 1)
                        (5, 0)
                    };
                \addlegendentry{$\{s_{n}\}$}
                \addplot+[only marks, mark options={red}, red]
                    coordinates {
                        (-5, 0)
                        (-4, 1)
                        (-3, 0)
                        (-2, -1)
                        (-1, 0)
                        (0, -1)
                        (1, 0)
                        (2, 1)
                        (3, 0)
                        (4, -1)
                        (5, 0)
                    };
                \addlegendentry{$D(\{s_{n}\})$}
                \addplot+[only marks, mark=square*, mark options={green}, green]
                    coordinates {
                        (-5, 0)
                        (-3, 0)
                        (-1, 0)
                        (1, 0)
                        (3, 0)
                        (5, 0)
                    };
                \end{axis}
            \end{tikzpicture}
        \end{figure}
\end{frame}

\subsection{The Matrix of a Linear Transformation}

\begin{frame}
    \frametitle{Review --- Theorem 10 (1.8) --- Standard matrix}

    Let $T : \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation.

    Let $\scE = \{\veclist[n]{e}\}$ be the standard basis of $\dsR^{n}$.

    Then
    \begin{equation*}
        T(\bfx) = A \bfx,
        \qquad
        \text{for all } \bfx \in \dsR^{n},
    \end{equation*}
    where
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            T(\bfe_1) &
            T(\bfe_2) &
            \cdots &
            T(\bfe_n)
        \end{bmatrix}
    \end{equation*}

    The matrix $A$ is called the \alert{standard matrix} of $T$.

    \think{} How to generalize to $T: V \mapsto V$?
\end{frame}

\begin{frame}
    \frametitle{What about $T:V \mapsto V$?}
    Let $T : V \to V$ be a linear transformation where $V$ is a vector space.

    Let $\scB = \{\bfb_1, \dots, \bfb_n\}$ be a basis of $V$.
    
    \think{} Can we find a matrix $[T]_{\scB}$ such that
    \begin{equation*}
        [T(\bfx)]_{\scB} = [T]_{\scB} [\bfx]_{\scB},
        \qquad
        \text{for all } \bfx \in V.
    \end{equation*}

    \only<1>{
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.6\linewidth]{linear-transformation-n-to-m-02.png}
        \end{figure}
    }
    \only<2>{Yes! We can take
    \begin{equation*}
        [T]_{\scB}
        =
        \begin{bmatrix}
            [T(\bfb_1)]_\scB &
            [T(\bfb_2)]_\scB &
            \dots &
            [T(\bfb_n)]_\scB
        \end{bmatrix}
        .
    \end{equation*}
    This called
    the \alert{matrix for $T$ relative to $\scB$}
    or the \alert{$\scB$-matrix for $T$}.}
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    Let $\scB = \{\bfb_{1}, \bfb_{2}\}$ be a basis of $V$.

    Let $T:V \mapsto V$ be a linear transformation which satisfies
    \begin{equation*}
        T(\bfb_{1}) 
        = 
        3 \bfb_{1} - 2 \bfb_{2}
        ,
        \qquad
        T(\bfb_{2}) 
        = 
        4 \bfb_{1} + 7 \bfb_{2}
    \end{equation*}
    What is $[T]_{\scB}$?
\end{frame}

\begin{frame}
    \frametitle{Example 3}
    Let $\scB = \{1, t, t^2\}$ be the standard basis of $\dsP^{2}$.

    Let $T: \dsP_2 \mapsto \dsP_2$ the linear mapping defined by
    \begin{equation*}
        T(\bfp(t)) = \dv{t} \bfp(t).
    \end{equation*}
    What is $[T]_{\scB}$?
\end{frame}

\begin{frame}
    \frametitle{Example 3 in a picture}
    Let's check we get the right answer.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{linear-transformation-n-to-n.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let $\scB = \{1+t, 1-t\}$ be a basis of $\dsP_{1}$.

    Let $T: \dsP_1 \mapsto \dsP_1$ the linear mapping defined by
    \begin{equation*}
        T(a_0 + a_1 t)
        = 
        3 a_0 + (5 a_0 - 2 a_1) t
    \end{equation*}
    What is $[T]_{\scB}$?
\end{frame}

\subsection{Linear Transformations on \texorpdfstring{$\dsR^{n}$}{R^n}}

\begin{frame}
    \frametitle{Theorem 8 --- Diagonal Matrix Representation}

    Let $T(\bfx) = A \bfx$ where $A = P D P^{-1}$ and $D$ is diagonal matrix.

    If $\scB$ is the basis for $\dsR^n$ formed from the columns of $P$,
    then $[T]_{\bfB} = D$.

    \astonished{} Diagonalization of $A$ amounts to finding a diagonal
    matrix representation of $\bfx \mapsto A \bfx$.
\end{frame}

\begin{frame}
    \frametitle{Example 4}
    Let $T(\bfx) = A \bfx$ where
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            7 & 2 \\
            -4 & 1 \\
        \end{bmatrix}
    \end{equation*}
    We can diagonalize $A$ by $A = P D P^{-1}$ where
    \begin{equation*}
        P
        =
        \begin{bmatrix}
            1 & 1 \\
            -1 & -2 \\
        \end{bmatrix}
        ,
        \qquad
        D
        =
        \begin{bmatrix}
            5 & 0 \\
            0 & 3 \\
        \end{bmatrix}
    \end{equation*}

    Then $[T]_{\scB} = D$ and
    \begin{equation*}
        [T(\bfx)]_\scB
        =
        [T]_\scB [\bfx]_\scB
        =
        D [\bfx]_\scB
    \end{equation*}
    where
    \begin{equation*}
        \scB
        =
        \blankshort{}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let $T(\bfx) = A \bfx$ where $A = \begin{bmatrix} 5 & -3 \\ -7 & 1
    \end{bmatrix}$

    Find a basis $\scB = \{\bfb_1, \bfb_2\}$ of $\dsR^{2}$ such that
    $[T]_{\scB}$.
\end{frame}

\subsection{Similarity of Matrix Representations}


\begin{frame}
    \frametitle{\tps{}}

    Recall that $A$ is \alert{similar}  to $B$ if $A = P B P^{-1}$.

    Prove that if $A$ is similar to $B$, then $A^{2}$ is similar to $B^{2}$.
\end{frame}

\begin{frame}
    \frametitle{Similar matrices}
    The proof of theorem 8 does not use $D$ is diagonal.

    If $A = P C P^{-1}$, i.e., $A$ and $C$ are similar, the theorem holds.
    
    The set of all matrices similar to $A$ are the same set of all
    matrix representations of $\bfx \mapsto A \bfx$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-n-to-m-03.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Same $T$, different bases}
    
    \astonished{} All matrices similar to $A$ is the matrix for $T(\bfx) = A
    \bfx$ relative to some basis $\scB$.

    This applies to $A$ itself.

    \cake{} Why is $A$ similar to itself?

    \cake{} What is the basis $\scB$ such that $[T]_{\scB} = A$?
\end{frame}

\begin{frame}
    \frametitle{Example 5}
    
    Let $A = \begin{bmatrix} 4 & -9 \\ 4 & -8 \end{bmatrix}$.
    Let $P = \begin{bmatrix} \bfb_1, \bfb_2 \end{bmatrix} = \begin{bmatrix} 3 & 2 \\ 2 & 1 \end{bmatrix}$.

    Note that $\scB = \{\bfb_{1}, \bfb_{2}\}$ is a basis of $\dsR^{2}$.

    Find $C$, the $\scB$-matrix for $\bfx \mapsto A \bfx$.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-n-to-m-03.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    The \alert{trace}  of a square matrix $A$ is the sum of the diagonal
    entries in $A$ and is denoted by $\operatorname{tr} A$. 

    For example
    \begin{flalign*}
        \operatorname{tr}
        \begin{bmatrix}
            1 & 2 \\
            3 & 4
        \end{bmatrix}
        =
        \blankshort{}
    \end{flalign*}

    \pause{}

    It can be verified that
    \begin{equation*}
        \operatorname{tr}(FG) = \operatorname{tr}(GF)
    \end{equation*}
    for
    any two $n\times n$ matrices $F$ and $G$.

    \pause{}

    Show that if $A$ and $B$ are similar, then $\operatorname{tr} A =
    \operatorname{tr} B$.
\end{frame}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{5.4}{27, 31}
        \end{column}
    \end{columns}
\end{frame}
\end{document}
