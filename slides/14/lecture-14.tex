\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Null Spaces, Column Spaces}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{4.2 Null Spaces, Column Spaces, and Linear Transformations}

\begin{frame}
    \frametitle{Introduction}

    \emoji{telescope} What are in this section ---
    \begin{itemize}
        \item Subspaces of $\mathbb{R}^n$ can be described as solutions to homogeneous linear equations or linear combinations of specified vectors.
        \item More generally, 
            the \alert{kernel} and \alert{range} of a linear transformation
            from a vector space to another are also subspaces.
    \end{itemize}
\end{frame}

\subsection{The Null Space of a Matrix}

\begin{frame}
    \frametitle{Null spaces}

    Let $A$ be a $m \times n$ matrix.

    The \alert{null space} of $A$ is the solution
    set of $A \bfx = \bfzero$, denoted by $\nullspace A$.

    In other words, letting $T(\bfx) = A \bfx$, then
    \begin{equation*}
        \nullspace A = \{\bfx: \bfx \in \dsR^{n}, T(\bfx) = \bfzero\}
    \end{equation*}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{null-space.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    Let
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccc}
                1 & -3 & -2 \\
                -5 & 9 & 1 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfu =
        \left[
            \begin{array}{c}
                5 \\
                3 \\
                -2 \\
            \end{array}
        \right]
        .
    \end{equation*}
    \cake{} Is $\bfu$ in $\nullspace A$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 2 --- Why a null space called a space?}

    The null space of $A$ is a subspace of $\dsR^{n}$.

    \cake{} Why does this imply that it is a vector space?

    \vspace{-1.5em}

    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Properties of subspaces]
                \footnotesize{}
                \vspace{-0.5em}
                For all $\bfv, \bfu \in H$ and $c \in \dsR$
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$,
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
        \begin{column}{0.49\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Is the following set a vector space?
    \begin{equation*}
        H
        =
        \left\{
            \left[
                \begin{array}{c}
                    a \\
                    b \\
                    c \\
                    d \\
                \end{array}
            \right]
            :
            a - 2b + 5c = d,
            c -a = b
        \right\}
        .
    \end{equation*}
\end{frame}

\subsection{An Explicit Description of \texorpdfstring{$\nullspace A$}{Nul A}}

\begin{frame}
    \frametitle{Example 3}

    Finding a spanning set for the null space of the matrix
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccccc}
                -3 & 6 & -1 & 1 & -7 \\
                1 & -2 & 2 & 3 & -1 \\
                2 & -4 & 5 & 8 & -4 \\
            \end{array}
        \right]
        \sim
        \left[
            \begin{array}{cccccc}
                1 & -2 & 0 & -1 & 3 \\
                0 & 0 & 1 & 2 & -2 \\
                0 & 0 & 0 & 0 & 0 \\
            \end{array}
        \right]
        =
        U
    \end{equation*}
    \cake{} Why do $A$ and $U$ have the same \emph{null space}?
\end{frame}

\begin{frame}
    \frametitle{Example 3 --- Two facts}

    \begin{block}{Fact 1}
        The vectors $\bfu, \bfv, \bfw$ are linearly independent.
    \end{block}

    \pause{}

    \begin{block}{Fact 2}
        When $\nullspace A$ contains nonzero vectors, 
        the number of vectors in the spanning set for $\nullspace A$
        equals the number of free variables in the equation $A \bfx = \bfzero$.
    \end{block}

    \dizzy{} When $\nullspace A = \{\bfzero\}$, what its spanning set?
    How many free variables are there in $A \bfx = \bfzero$?
\end{frame}

\subsection{The Column Space of a Matrix}

\begin{frame}
    \frametitle{Column spaces}

    Let $A$ be a $m \times n$ matrix. 
    The \alert{column space} of $A$ is the span
    of its columns, denoted by $\colspace A$.

    If $A = [\bfa_{1}\, \dots \bfa_{n}]$, then
    \begin{equation*}
        \colspace A = \Span{\bfa_{1}\, \dots \bfa_{n}}
    \end{equation*}

    In other words, letting $T(\bfx) = A \bfx$, then
    \begin{equation*}
        \colspace A = \{\bfb: \bfb = T (\bfx) \text{ for some } \bfx \in \dsR^{n}\}.
    \end{equation*}
    \cake{} So we can call $\colspace A$ the \blankshort{} of $T$.
\end{frame}


\begin{frame}
    \frametitle{Theorem 3 --- Why a column space called a space?}

    The column space of $A$ is a subspace of $\dsR^{m}$.

    \cake{} Why?
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Find $A$ such that
    \begin{equation*}
        \colspace A =
        \left\{
            \begin{bmatrix}
                6a - b \\
                a + b \\
                -7 a \\
            \end{bmatrix}
            :
            a, b \in \dsR
        \right\}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Column spaces and solution sets}
    
    \begin{block}{Fact}
        The column space of an $m \times n$ matrix $A$ is all of $\mathbb{R}^m$
        if and only if the equation $A \bfx = \bfb$ has a (at least one) solution for each $\bfb$ in
        $\mathbb{R}^m$.
    \end{block}

    \cake{} This follows from a theorem in Chapter 1. Which one?
\end{frame}

\subsection{The Contrast Between \texorpdfstring{$\nullspace A$}{Nul A} and \texorpdfstring{$\colspace A$}{Col A}}

\begin{frame}[c]
    \frametitle{Example 5, 6, 7}
    \begin{columns}
        \begin{column}{0.4\textwidth}
            Let
            \begin{equation*}
                A =
                \left[
                    \begin{array}{cccc}
                        2 & 4 & -2 & 1 \\
                        -2 & -5 & 7 & 3 \\
                        3 & 7 & -8 & 6 \\
                    \end{array}
                \right]
            \end{equation*}
            The solution of $A \bfx = \bfzero$ is
            \begin{equation*}
                \begin{aligned}
                    x{_1} & = -9 \cdot x{_3} \\
                    x{_2} & = 5 \cdot x{_3} \\
                    x{_3} & = x{_3} \\
                    x{_4} & = 0 \\
                \end{aligned}
            \end{equation*}
        \end{column}
        \begin{column}{0.6\textwidth}
            \begin{enumerate}[<+->]
                \item $\colspace A$ is  subspace of $\dsR^\txtq$
                \item $\nullspace A$ is  subspace of  $\dsR^\txtq$
                \item Find a non-zero vector in $\colspace $A.
                \item Find a non-zero vector in $\nullspace $A.
                \item Is $\bfu = \begin{bmatrix}3 \\ -2 \\ -1 \\ 0 \end{bmatrix}
                    \in \nullspace A$? $\colspace A$?
                \item Is $ \bfv = \begin{bmatrix} 3 \\ -1 \\3 \end{bmatrix} \in \colspace A$?
                    $\nullspace A$?
            \end{enumerate}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let $A = \begin{bmatrix} -6 & 12 \\ 3 & 6 \end{bmatrix}$ 
    and $\mathbf{w} = \begin{bmatrix} 2 \\ 1 \end{bmatrix}$.

    Is $\mathbf{w}$ is in $\mathrm{Col}(A)$.

    Is $\mathbf{w}$ in $\mathrm{Nul}(A)$?
\end{frame}

\subsection{Kernel and Range of a Linear Transformation}

\begin{frame}
    \frametitle{\dizzy{} Why some many definitions?}
    
    Subspaces of vector spaces other than $\mathbb{R}^n$ are often described in
    terms of a linear transformation instead of a matrix. 

    To make this precise,
    we will generalize the definition linear transformation given in Section 1.8.
\end{frame}

\begin{frame}
    \frametitle{Linear transformation of vector spaces}

    Recall that a transformation $T:\dsR^{m} \mapsto \dsR^{n}$ is \alert{linear} if
    for all vectors $\bfu, \bfv \in \dsR^{m}$ and all scalars $c \in \dsR$
    \begin{enumerate}[(i)]
        \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
        \item  $T(c \bfu) = c T(\bfu)$.
    \end{enumerate}

    \pause{}

    Let $V$ and $W$ be vector spaces.
    A transformation $T:V \mapsto W$ is \alert{linear} if
    for all vectors $\bfu, \bfv \in V$ and all scalars $c \in \dsR$
    \begin{enumerate}[(i)]
        \item  \underline{\hspace{5cm}}
        \item  \underline{\hspace{5cm}}
    \end{enumerate}
    \cake{} How should we finish this definition?
\end{frame}

\begin{frame}
    \frametitle{Kernels and ranges}

    Let $T: V \mapsto W$ be a linear transformations.

    \only<1>{
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\linewidth]{kernel-range.png}
        \end{figure}
    }
    \only<2>{
        The \alert{kernel} of $T$ is the solution
        \begin{equation*}
            \{\bfv \in V: T(\bfv) = \bfzero\}.
        \end{equation*}

        The \alert{range/image} of $T$ is the subset of $W$
        \begin{equation*}
            \{\bfw \in W: T(\bfv) = \bfw \text{ for some } \bfv \in V\}.
        \end{equation*}
        \cake{} If $T(\bfx) = A \bfx$ where $A$ is a $m \times n$ matrix.
        What is the kernel of $T$ for $A$?
        What is the range of $T$ for $A$?
    }
\end{frame}

\begin{frame}
    \frametitle{Kernels and ranges are subspaces}

    Let $T: V \mapsto W$ be a \emph{linear transformation} from a vector space $V$ to
    another vector space $W$.

    \only<1>{Then the \emph{kernel} of $T$ is a subspace of $V$.}
    \only<2>{Then the \emph{range} of $T$ is a subspace of $W$.}

    \vspace{-2em}

    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Properties of subspaces]
                \footnotesize{}
                \vspace{-0.5em}
                For all $\bfv, \bfu \in H$ and $c \in \dsR$
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$,
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Properties of linear transformation]
                \footnotesize{}
                \vspace{-0.5em}
                A mapping $T:V \mapsto W$ is \alert{linear} if
                for all $\bfu, \bfv \in V$
                \begin{enumerate}[(i)]
                    \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
                    \item  $T(c \bfu) = c T(\bfu)$.
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 8}

    Let $V = C^{1}([a,b])$, i.e., the set of continuously differentiable functions on $[a,b]$.

    Let $W = C^{0}([a,b])$, i.e., the set of continuous functions on $[a,b]$.

    For $f \in V$, let $D:V \mapsto W$ be the transformation which maps $f$ to
    its derivative.

    \only<1>{
        \think{} Is $D$ a linear transformation?
        \vspace{-1.5em}
        \begin{columns}[totalwidth=\linewidth]
            \begin{column}{0.49\textwidth}
                \begin{tcolorbox}[title=Properties of linear transformation]
                    \footnotesize{}
                    \vspace{-0.5em}
                    A mapping $T:V \mapsto W$ is \alert{linear} if
                    for all $\bfu, \bfv \in V$
                    \begin{enumerate}[(i)]
                        \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
                        \item  $T(c \bfu) = c T(\bfu)$.
                    \end{enumerate}
                \end{tcolorbox}
            \end{column}
            \begin{column}{0.49\textwidth}
            \end{column}
        \end{columns}
    }
    \only<2>{
        \think{} What are the \emph{kernel} and \emph{range} of $D$?
    }
\end{frame}

\begin{frame}
    \frametitle{Example 9}

    Consider
    \begin{equation*}
        y''(t) + \omega^{2} y(t) = 0
    \end{equation*}
    where $\omega$ is a constant.

    \think{} Which linear transformation's \emph{kernel} is the solution set this
    equation?
\end{frame}
%
%\begin{frame}
%    \frametitle{Exercise}
%
%    Let $V$ and $W$ be vector spaces, and let $T: V \mapsto W$
%    be a linear transformation.
%
%    Given a subspace $U$ of $V$,
%    let $T(U)$ denote the set of all images of the form $T(x)$,
%    where $x \in U$.
%
%    \think{} Show that $T(U)$ is a subspace of $W$.
%\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Let $A$ be an $m \times n$ matrix.

    Which of the following are true?
    \begin{enumerate}
        \item The null space of $A$ is a vector space.
        \item The null space of an $m \times n$ matrix is in $\mathbb{R}^m$.
        \item The column space of $A$ is the range of the mapping $x \mapsto Ax$.
        \item If the equation $Ax = b$ is consistent, then $\operatorname{Col} A = \mathbb{R}^m$.
    \end{enumerate}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.2}{39, 41, 43, 45, 47}
        \end{column}
    \end{columns}
\end{frame}
\end{document}
