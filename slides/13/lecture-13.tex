\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Vector Space and Subspace}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{4 Vector Spaces}

\begin{frame}
    \frametitle{Digital Signal Processing}
    
    \alert{Digital signal processing} (DSP) refers to the manipulation, analysis, and
    modification of digital signals such as sound, images, and data.

    DSP (and AI) allows us to control smart speakers by speaking.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{Amazon-Alexa.jpg}
        \caption*{Amazon's smart speaker Alexa}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Discrete-time signal}
    
    For DSP to work with voice, analog signals must be convert to discrete-time
    signal.

    A discrete-time signal $\{y_{n}\}_{n \in \dsZ}$ can be seen as \alert{vector} in a
    \alert{vector space}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{Sampled.signal.pdf}
        \caption*{Discrete Time Signal}
    \end{figure}
\end{frame}

\section{4.1 Vector Space and Subspace}

\subsection*{Vector Spaces}

\begin{frame}
    \frametitle{Review of ${\color{orange}\dsR^{n}}$}

    The set ${\color{orange}\dsR^{n}}$
    together with vector addition and scalar multiplication have these 
    \emph{axioms} (properties) ---

    For all $\bfv, \bfw, \bfu \in {\color{orange}\dsR^{n}}$ and all $r, s \in \dsR$
    \vspace{-1em}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{enumerate}
                \item  $\bfv + \bfw   \in {\color{orange}\dsR^{n}}$
                \item  $\bfv + \bfw  = \bfw  +  \bfv$
                \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                \item  there exists $\bfzero \in {\color{orange}\dsR^{n}}$ such that  $\bfv +  \bfzero =  \bfv$
                \item  there exists $-\bfv  \in {\color{orange}\dsR^{n}}$ such that $-\bfv  +  \bfv =  \bfzero$
            \end{enumerate}
        \end{column}
        \begin{column}{0.5\textwidth}
            \pause{}
            \begin{enumerate}
                \setcounter{enumi}{5}
                \item  $r \cdot  \bfv \in {\color{orange}\dsR^{n}}$
                \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                \item  $1 \cdot  \bfv =  \bfv$
            \end{enumerate}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Vector spaces}

    A \alert{vector space}  consists of a set ${\color{red}V}$ along with two
    operations `$+$' and `$\cdot$' such that the following
    \emph{axioms} (properties) hold ---

    For all $\bfv, \bfw, \bfu \in {\color{red}V}$
    (\emph{vectors}) and all $r, s \in \dsR$ (\emph{scalars})
    \vspace{-1em}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{enumerate}
                \item  $\bfv + \bfw   \in {\color{red}V}$
                \item  $\bfv + \bfw  = \bfw  +  \bfv$
                \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                \item  there exists $\bfzero \in {\color{red}V}$ such that  $\bfv +  \bfzero =  \bfv$
                \item  there exists $-\bfv  \in {\color{red}V}$ such that $-\bfv  +  \bfv =  \bfzero$
            \end{enumerate}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{enumerate}
                \setcounter{enumi}{5}
                \item  $r \cdot  \bfv \in {\color{red}V}$
                \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                \item  $1 \cdot  \bfv =  \bfv$
            \end{enumerate}
        \end{column}
    \end{columns}

    \cake{} So is $\dsR^{n}$ (with `$+$` and `$\cdot$`) a vector space?
\end{frame}

\begin{frame}
    \frametitle{Three properties}

    It follows from the axioms of vector spaces that
    \begin{equation*}
        \begin{aligned}
            0 \bfu & = \bfzero, 
            \\
            c \bfzero & = \bfzero,
            \\
            -1 \cdot \bfu & = -\bfu,
        \end{aligned}
    \end{equation*}

    \think{} They look easy. But can you really derive them?
\end{frame}

\begin{frame}
    \frametitle{Proof of $0 \bfu = \bfzero$}
    \vspace{-2em}

    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Axioms of Vector Spaces]
                \footnotesize{}
                \vspace{-0.5em}
                For all $\bfv, \bfw \in V$ and $c \in \dsR$
                \begin{enumerate}
                    \item  $\bfv + \bfw   \in {\color{red}V}$
                    \item  $\bfv + \bfw  = \bfw  +  \bfv$
                    \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                    \item  there exists $\bfzero \in {\color{red}V}$ s.t.\ $\bfv +  \bfzero =  \bfv$
                    \item  there exists $-\bfv  \in {\color{red}V}$ s.t.\ $-\bfv  +  \bfv =  \bfzero$
                    \item  $r \cdot  \bfv \in {\color{red}V}$
                    \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                    \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                    \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                    \item  $1 \cdot  \bfv =  \bfv$
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title={Proof of $0 \bfu = \bfzero$}]
                \footnotesize{}
                By axiom \blankveryshort{},
                \begin{equation*}
                    0 \bfu = (0+0) \bfu = 0 \bfu + 0 \bfu
                \end{equation*}
                Add $-0\bfu$ to both sides ---
                \vspace{5.2cm}
            \end{tcolorbox}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \vspace{-2em}

    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Axioms of Vector Spaces]
                \footnotesize{}
                \vspace{-0.5em}
                For all $\bfv, \bfw \in V$ and $c \in \dsR$
                \begin{enumerate}
                    \item  $\bfv + \bfw   \in {\color{red}V}$
                    \item  $\bfv + \bfw  = \bfw  +  \bfv$
                    \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                    \item  there exists $\bfzero \in {\color{red}V}$ s.t.\ $\bfv +  \bfzero =  \bfv$
                    \item  there exists $-\bfv  \in {\color{red}V}$ s.t.\ $-\bfv  +  \bfv =  \bfzero$
                    \item  $r \cdot  \bfv \in {\color{red}V}$
                    \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                    \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                    \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                    \item  $1 \cdot  \bfv =  \bfv$
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title={Proof of $r \bfzero = \bfzero$}]
                \footnotesize{}
                By \blankveryshort{},
                \begin{equation*}
                    r \mathbf{0} + r \mathbf{0}= r(\mathbf{0} + \mathbf{0})
                \end{equation*}
                Thus, by \blankveryshort{},
                \begin{equation}
                    \label{eq:1}
                    r \mathbf{0} + r \mathbf{0}= r\mathbf{0}
                \end{equation}
                By \blankveryshort{}, there exists a vector $-r \bfzero$ such
                that $r \bfzero + (-r \bfzero) = \bfzero$.
                Adding $(-r \bfzero)$ to both sides of \eqref{eq:1}
                \begin{equation}
                    \label{eq:2}
                    (r \mathbf{0} + r \mathbf{0}) + (- r\mathbf{0})
                    =\bfzero
                \end{equation}
                By \blankveryshort{},
                the LHS of \eqref{eq:2} equals
                \begin{equation*}
                    r \mathbf{0} + (r \mathbf{0} + (- r\mathbf{0}))
                    =
                    r \mathbf{0}
                \end{equation*}
            \end{tcolorbox}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 3: Signals}

    Let $\dsS$ be the set of doubly infinite sequences like
    \begin{equation*}
        \{y_{k}\} = (\cdots, y_{-3}, y_{-2}, y_{-1}, y_{0}, y_{1}, y_{2}, \cdots)
    \end{equation*}

    \think{} How can we define `$+$', `$\cdot$' so that $\dsS$ forms a vector space?

    \only<2>{\begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{vector-space-signal.png}
        \caption*{Elements of $\dsS$ can represent a signal measured at discrete
        times}%
    \end{figure}}
\end{frame}

\begin{frame}
    \frametitle{Example 4: Polynomials}

    Let $\dsP_n$ be the set of polynomials of degree $n$, i.e.,
    \begin{equation*}
        \bfp(t) = a_{0} + a_{1} t + a_{2} t^{2} + \cdots + a_{n} t^{n}.
    \end{equation*}

    \think{} How can we define `$+$', `$\cdot$' so that $\dsP_{n}$ is a vector space?
\end{frame}

\begin{frame}
    \frametitle{Example 5: Real-valued functions}

    Let $\dsD$ be the set of real-valued functions on $\dsR$.

    Some examples of elements in $\dsD$ include
    \begin{equation*}
        \sin(t), 2 t + 1, \exp(t), \dots.
    \end{equation*}

    \think{} How can we define `$+$', `$\cdot$' so that $\dsD$ is a vector space?
\end{frame}

\begin{frame}
    \frametitle{Example --- Differentiable functions}
    
    Let $C^3([0,1])$ be to the set of functions that are at least three times
    continuously differentiable on the interval $[0,1]$

    Some examples of elements in $C^3([0,1])$ include
    \begin{equation*}
        \sin(t), 2 t + 1, \exp(t), \dots.
    \end{equation*}

    \pause{}

    \cake{} Can you think of function not in $C^3([0,1])$?

    \think{} How can we define `$+$', `$\cdot$' so that $C^3([0,1])$ is a vector space?
\end{frame}

\begin{frame}
    \frametitle{Example --- Matrices}
    
    Let $M_{n}(\dsR)$ (also can be written as $\dsR^{n \times n}$) be 
    the set of $n \times n$ matrices with real entries.

    \think{} How can we define `$+$', `$\cdot$' so that $M_{n}(\dsR)$ is a vector space?
\end{frame}

\begin{frame}[standout]
    Vectors are \alert{elements of a vector space}, not just column matrices like
    \begin{equation*}
        \begin{bmatrix}
            1 \\ 2 \\ 3
        \end{bmatrix}
    \end{equation*}
\end{frame}

\subsection*{Subspaces}

\begin{frame}
    \frametitle{Subspaces}

    Let $V$ be a vector space.
    If a subset $H$ of $V$ satisfies the following
    \begin{enumerate}
        \item[a.] $\bfzero \in H$,
        \item[b.] for each $\bfu, \bfv \in H$, $\bfu + \bfv \in H$,
        \item[c.] each $\bfu \in H$ and each scalar $c$, $c \bfu \in H$,
    \end{enumerate}
    we say $H$ is a \alert{subspace} of $V$.

    \dizzy{} Is $\dsR^{2}$ a subspace of $\dsR^{3}$?
\end{frame}

\begin{frame}
    \frametitle{Subspaces are vector spaces}
    \vspace{-2em}
    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Axioms of Vector Spaces]
                \footnotesize{}
                \vspace{-0.5em}
                For all $\bfv, \bfw \in V$ and $c \in \dsR$
                \begin{enumerate}
                    \item  $\bfv + \bfw   \in {\color{red}V}$
                    \item  $\bfv + \bfw  = \bfw  +  \bfv$
                    \item  $( \bfv + \bfw  ) + \bfu  =  \bfv + ( \bfw + \bfu)$
                    \item  there exists $\bfzero \in {\color{red}V}$ s.t.\ $\bfv +  \bfzero =  \bfv$
                    \item  there exists $-\bfv  \in {\color{red}V}$ s.t.\ $-\bfv  +  \bfv =  \bfzero$
                    \item  $r \cdot  \bfv \in {\color{red}V}$
                    \item  $(r + s) \cdot \bfv = r \cdot \bfv + s \cdot \bfv$
                    \item  $r\cdot( \bfv + \bfw  ) = r\cdot \bfv +r\cdot \bfw $
                    \item  $(rs) \cdot  \bfv = r \cdot (s \cdot  \bfv)$
                    \item  $1 \cdot  \bfv =  \bfv$
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Properties of subspaces]
                \footnotesize{}
                \vspace{-0.5em}
                For all $\bfv, \bfu \in H$ and $c \in \dsR$
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$,
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 7}

    \think{} Is $\dsP_{2}$ a subspace of $\dsP_{3}$?

    \vspace{-1.5em}
    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Properties of subspaces]
                \footnotesize{}
                \vspace{-0.5em}
                For all $\bfv, \bfu \in H$ and $c \in \dsR$
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$,
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
        \begin{column}{0.49\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 8}

    Consider the following subset of $\dsR^{3}$
    \begin{equation*}
        H =
        \left\{
        \begin{bmatrix}
            s \\ t \\ 0
        \end{bmatrix}
        :
        s \in \dsR,
        t \in \dsR
        \right\}
    \end{equation*}

    \cake{} Is $H$ is a \alert{subspace} of $\dsR^{3}$?
    \vspace{-1.5em}
    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Properties of subspaces]
                \footnotesize{}
                \vspace{-0.5em}
                For all $\bfv, \bfu \in H$ and $c \in \dsR$
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$,
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
        \begin{column}{0.49\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\subsection*{A Subspace Spanned by a Set}

\begin{frame}
    \frametitle{Example 10}

    Show that for any two vectors $\bfv_{1}, \bfv_{2} \in V$,
    the set $H = \Span{\bfv_{1}, \bfv_{2}}$
    is a subspace.

    \vspace{-1.5em}
    \begin{columns}[totalwidth=\linewidth]
        \begin{column}{0.49\textwidth}
            \begin{tcolorbox}[title=Properties of subspaces]
                \footnotesize{}
                \vspace{-0.5em}
                For all $\bfv, \bfu \in H$ and $c \in \dsR$
                \begin{enumerate}
                    \item[a.] $\bfzero \in H$,
                    \item[b.] $\bfu + \bfv \in H$,
                    \item[c.] $c \bfu \in H$,
                \end{enumerate}
            \end{tcolorbox}
        \end{column}
        \begin{column}{0.49\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Theorem 1 --- The span of vectors}

    If $\bfv_1, \dots, \bfv_p$ are in a vector space $V$, then $\Span{\bfv_1, \dots,
    \bfv_p}$ is a subspace of $V$.

    We call $\Span{\bfv_1, \dots, \bfv_p}$ the subspace \alert{spanned/generated} by
    $\bfv_1, \dots, \bfv_p$.
\end{frame}

\begin{frame}
    \frametitle{Example 11}

    Show that the following is a subspace of $\dsR^{4}$ ---
    \begin{equation*}
        H
        =
        \left\{
            \begin{bmatrix}
                a - 3b \\ b - a \\ a \\ b
            \end{bmatrix}
            :
            a, b \in \dsR
        \right\}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Which of the following two sets of vectors are vector spaces?
    \begin{equation*}
        S
        =
        \left\{
            \begin{bmatrix}
                x \\ y
            \end{bmatrix}
            :
            x \ge 0, y \ge 0
        \right\}
    \end{equation*}
    and
    \begin{equation*}
        H
        =
        \left\{
            \begin{bmatrix}
                2 t \\ 0 \\ -t + 1
            \end{bmatrix}
            :
            t \in \dsR
        \right\}
    \end{equation*}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.1}{19, 21, 35, 37, 39, 41}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
