\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

% Pie chart drawing library 
\input{../tikz.tex}
\usepackage{pgf-pie}  

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Systems of Linear Equations, Echelon Forms}

\begin{document}

\maketitle

\lectureoutline{}

\section{The Syllabus in 3 Minutes}

\begin{frame}[c]
    \frametitle{What you definitely need to known}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \huge
            \bomb{} Grades will \emph{not} be curved. 
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{crying.png}
                \caption*{A student who forgot this after the final exam}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{What you need to known}

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=0.8, transform shape]
                \pie{
                    10/\cake{} 7 \emoji{laptop} assignments,
                    10/\laughing{} 1 presentation,
                    25/\smiling{} 5 quizzes,
                    25/\sweat{} 1 midterm exam,
                    30/\zany{} 1 final exam
                }
            \end{tikzpicture}
        \end{center}
        \caption{Distribution of points}%
        \label{fig:}
    \end{figure}

    \emoji{money-mouth-face} You can at most 2\% bonus points.

    \bomb{} Bonus points cannot use for getting A+.
\end{frame}

\section{Why Linear Algebra?}

\begin{frame}
    \frametitle{Because of Machine Learning \dots}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{robot.png}
        \caption*{Humans --- We thought creating AI will let us do the fun jobs
        \emoji{cry}}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{and many other applications}

    Linear Algebra provides a \emph{concise language} for many technology
    problems, such as ---

    \begin{itemize}
        \item \emoji{crossed-swords} maximize war effort
            (\href{https://en.wikipedia.org/wiki/Linear\_programming\#History}{linear
            programming}),
        \item \emoji{lock} build ``unbreakable'' cipher code
            (\href{https://www.math.utah.edu/~gustafso/s2016/2270/published-projects-2016/adamsTyler-moodyDavid-choiHaysun-CryptographyTheEnigmaMachine.pdf}{cryptography}),
        \item
            \emoji{robot} make computers ``smarter''
            (\href{https://www.analyticsvidhya.com/blog/2019/07/10-applications-linear-algebra-data-science/}{artificial intelligence}),
        \item \emoji{framed-picture} create funny photos
            (\href{https://observablehq.com/@yurivish/example-of-2d-linear-transforms}{linear
            transformation}),
        \item \emoji{spider-web} process complex network
            (\href{https://en.wikipedia.org/wiki/GraphBLAS}{GraphBLAS}),
        \item \emoji{alien} control the Mars rover
            (\href{https://en.wikipedia.org/wiki/Linear\_network\_coding}{linear
            network coding}),
        \item \emoji{notes} recommend music
            (\href{https://en.wikipedia.org/wiki/Singular\_value\_decomposition}{SVD}),
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{An example --- 2D convolution}

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.9\linewidth]{image-process1.png}
        \includegraphics<2>[width=0.8\linewidth]{image-process2.png}
        \caption*{Pictures by \href{https://www.analyticsvidhya.com/blog/2019/07/10-applications-linear-algebra-data-science/}{Khyati Mahendru}}%
    \end{figure}

    See this
    \href{https://magamig.github.io/posts/real-time-video-convolution-using-webgl/}{live
        demonstration}.
\end{frame}

\section{1.1 Systems of Linear Equations}%

\subsection{Linear Systems}%

\begin{frame}
    \frametitle{A puzzle}

    A \emoji{cricket-game} and \emoji{baseball} cost \$1.10 together.

    The \emoji{cricket-game} costs one dollar more than the \emoji{baseball}.

    \cake{} How much does the \emoji{baseball} cost?
\end{frame}

\begin{frame}[c]
    \frametitle{Daniel Kahneman}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Daniel Kahneman is an \emoji{israel} \emoji{us}
            psychologist and economist.

            Nobel \emoji{trophy} winner.

            \advance \leftmargini -1em

            \begin{quote}
                \small{}

                More than 50\% of students at Harvard, MIT, and Princeton gave the 
                intuitive-incorrect-answer to the \emoji{baseball} puzzle.

                \begin{flushright}
                    \emph{Thinking, Fast and Slow} by Daniel Kahneman
                \end{flushright}
            \end{quote}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Kahneman.jpg}
                \caption*{Daniel Kahneman (1934--)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Linear equations}

    A \alert{linear equation} in the variables $x_{1}, \ldots, x_{n}$ is an equation like
    \begin{equation*}
        a_{1} x_{1} + a_{2} x_{2} + \dots + a_{n} x_{n} = b.
    \end{equation*}

    \cake{} Which of the followings are \emph{not} linear equations?
    \begin{center}
        \begin{minipage}{0.4\textwidth}
            \begin{enumerate}
                \item $4 x_1 - 5 x_2 + 2 = x_1$
                \item $x_2 = 2(\sqrt{6} - x_{1})+x_3$
                \item $x_2 = 2 (\sqrt{x_1} - 6)$
                \item $2 x_1 + x_2 - x_3 = 2 \sqrt{6}$
                \item $4 x_1 - 5 x_2 = x_1 x_2$
            \end{enumerate}
        \end{minipage}
    \end{center}
\end{frame}

\begin{frame}[t]
    \frametitle{Linear Systems}

    A \alert{system of linear equations (linear system)} is group of linear equations
    using the same variables, e.g.,
    \begin{equation}
        \label{eq:sys}
        \systeme{2 x_1 - x_2 + 1.5 x_3 = 8,
                   x_1        -  4 x_3   = -7}
    \end{equation}

    A \alert{solution} of a linear system is a list of numbers $(s_1, s_{2}, \dots,
    s_{n})$ such that replacing $x_i$ by $s_i$ makes all the equations true.

    One solution of \eqref{eq:sys} is $(5, 6.5, 3)$.
\end{frame}

\begin{frame}
    \frametitle{Solution sets}
    The set of all solutions of a linear system is the \alert{solution set}.

    Two systems are \alert{equivalent} if their solution sets are the same.

    Consider
    \begin{equation*}
        \systeme{2 x_1 - x_2 + 1.5 x_3 = 8,
        x_1 - 4 x_3= -7}
    \end{equation*}
    and
    \begin{equation*}
       \systeme{4 x_1 - 2 x_2 + 3 x_3 = 16,
         - 3 x_1 + 12 x_3  = 21}
    \end{equation*}

    \cake{} Are these two systems equivalent?
\end{frame}

\begin{frame}[c]
    \frametitle{Wassily Leontief}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Wassily Leontief is an \emoji{ru}-\emoji{us}
            \emoji{money-mouth-face}.
            Winner of Nobel \emoji{trophy}.

            One of first to use \emoji{robot} to solve linear systems.

            At the time, \emoji{robot} couldn't handle $500$ unknowns.

            Today, people are still making \href{https://www.quantamagazine.org/new-algorithm-breaks-speed-limit-for-solving-linear-equations-20210308/}{process}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Leontief.jpg}
                \caption*{Wassily Leontief (1905-1999)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Three Types of Solution Sets}

\begin{frame}[c]
    \frametitle{Three cases}

    The \emph{fundamental question} about a linear systems is that if it is
    \begin{itemize}
        \item[\inconsistent] \alert{inconsistent},  i.e., having no solutions, or
        \item[\consistent] \alert{consistent}, i.e., having
            \begin{itemize}
                \item[\emoji{unicorn}] one \alert{unique solution}, or
                \item[\emoji{infinity}] \alert{infinitely many solutions}.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Unique solutions}

    The following linear system is consistent and has a unique solution.

    \begin{equation*}
        \systeme{x_1 - 2 x_2 = -1,
        - x_1 + 3 x_2 = 3}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{3-case-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Non-unique solutions}

    System (a) is inconsistent and has no solutions.

    System (b) is consistent and has infinitely many solutions.

    \begin{equation*}
        (a) \systeme{x_1 - 2 x_2 = -1,
        - x_1 + 2 x_2 = 3}
        \quad
        (b) \systeme{x_1 - 2 x_2 = -1,
        - x_1 + 2 x_2 =  1}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=1.0\linewidth]{3-case-2.png}
    \end{figure}
\end{frame}

\subsection{Matrix Notations}%

\begin{frame}
    \frametitle{Matrix the film}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{matrix.jpg}
        \caption*{The Matrix (1999) is a Sci-Fi thriller \emoji{film-frames}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{What is a matrix?}

    A \alert{matrix} is rectangular array of numbers like this
    \begin{equation*}
        \begin{bmatrix}
            1 & -2 & 1 \\
            0 &  2 & -8 \\
            5 &  0 & -5 \\
        \end{bmatrix}
        \qquad
        \begin{bmatrix}
            1 & -2 & 1 \\
            0 &  2 & -8 \\
        \end{bmatrix}
    \end{equation*}
    It can contain just one row or one column ---
    \begin{equation*}
        \begin{bmatrix}
            1 & -2 & 1 \\
        \end{bmatrix}
        \qquad
        \begin{bmatrix}
            1 \\
            0 \\
            5 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Matrix notations for linear systems}

    Given a system
    \begin{equation*}
        \systeme{x_1 - 2 x_2 + x_3 = 0,
            2 x_2 - 8 x_3 = 8,
            5 x_1 - 5 x_3 = 10}
    \end{equation*}
    the \alert{coefficient matrix}
    and
    the \alert{augmented matrix} are
    \begin{equation*}
            \begin{bmatrix}
                1 & - 2 &    1 \\
                0 &   2 &   -8 \\
                5 &   0 &   -5
            \end{bmatrix}
        \hspace{2cm}
            \begin{bmatrix}
                1 & - 2 &    1  & 0 \\
                0 &   2 &   -8  & 8 \\
                5 &   0 &   -5  & 10
            \end{bmatrix}
    \end{equation*}
\end{frame}

\subsection{Solving Linear Systems}%

\begin{frame}[c]
    \frametitle{Ways to solve linear system}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            To solve
            \begin{equation}
                \label{eq:sys:1}
                \systeme{x_1 - 2 x_2 +   x_3 = 0,
                2 x_2 - 8 x_3 = 8,
                5 x_1 - 5 x_3 = 10}
            \end{equation}
            you can
            \begin{itemize}
                \item \emoji{cry} and give up,
                \item \emoji{phone} you mum \emoji{old-woman} for help, or
                \item draw some pictures \emoji{artist}, or
                \item use a computer \emoji{robot}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{3-plane.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{How the textbook does it}

    Alternatively, you can show that the following are equivalent
    \begin{equation*}
        \systeme{x_1 - 2 x_2 +   x_3 = 0,
        2 x_2 - 8 x_3 = 8,
        5 x_1 - 5 x_3 = 10}
        \hspace{2cm}
        \systeme{x_1 = 1,
        x_2 = 0,
        x_3 = -1}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Row operations}

    The three types of \alert{row operations} on a matrix are
    \begin{itemize}
        \item \alert{Replace} one row by the sum of itself and a multiple of another row.
        \item \alert{Interchange} two rows.
        \item \alert{Scale} a row by multiplying it with a \emph{non-zero} constant.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example}
    
    We solved \eqref{eq:sys:1} by applying row operations to the left (the augmented
    matrix)
    to get the right.
    \begin{equation*}
        \begin{bmatrix}
            1 & - 2 &    1  & 0 \\
            0 &   2 &   -8  & 8 \\
            5 &   0 &   -5  & 10
        \end{bmatrix}
        \hspace{2cm}
        \begin{bmatrix}
            1 & 0 & 0  & 1 \\
            0 & 1 & 0  & 0 \\
            0 & 0 & 1  & -1
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{}}

    Can you transfer the left to the right with four row operations?
    \begin{equation*}
        \begin{bmatrix}
            1 & 2 & -5 & 0 \\
            0 & 1 & -3 & -2 \\
            0 & -3 & 9 & 5 \\
        \end{bmatrix}
        \hspace{2cm}
        \begin{bmatrix}
            2 & 4 & -10 & 0 \\
            0 & 0 & 0 & 1 \\
            0 & 1 & -3 & -2 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Section Summary}
    
    In this section, we have learned

    \begin{itemize}
        \item[\emoji{baseball}] \alert{Linear equations}
        \item[\sweat{}] \alert{Linear systems} and their \alert{solution sets}
        \item[\emoji{kitchen-knife}] \emph{Three} types of solution sets
        \item[\emoji{black-square-button}] \alert{Matrices} and \alert{augmented matrices} 
        \item[\emoji{roll-of-paper}] Solving linear systems with \alert{row operations}
        \item[\emoji{wink}] And don't trust your intuition!
    \end{itemize}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{1.1}{5, 19, 27, 29, 31, 33, 35, 43}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{How to solve linear systems in Julia}
    
    Most programming languages can solve linear systems.

    My favourite is \href{https://julialang.org/}{Julia}. It is
    \begin{itemize}
        \item[\emoji{zap}] Fast
        \item[\emoji{dancers}] Dynamic
        \item[\cake{}] Like Python
        \item[\emoji{heart}] Open source
    \end{itemize}
    See our pCloud folder for some examples.

    \emoji{bomb} Programming is not required in the course and quizzes and exams are closed book.
\end{frame}

\end{document}
