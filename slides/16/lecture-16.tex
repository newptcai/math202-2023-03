\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Coordinate Systems}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{4.4 Coordinate Systems}

\begin{frame}
    \frametitle{Introduction}
    
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=0.5, every node/.style={black}]
                % Draw white background
                \fill[white] (-1,-1) rectangle (6,6);
                % Draw grid
                \draw[step=1cm, gray, very thin] (0, 0) grid (5,5);
                % Set seed for random number generator
                \pgfmathsetseed{48}
                % Draw x and y axes
                \draw[thick] (0,0) -- (5,0) node[anchor=west] {$x_1$};
                \draw[thick] (0,0) -- (0,5) node[anchor=south] {$x_2$};
                % Draw vector to (4,3)
                \drawvector{4,3}{};
                \node at (4,3) {\emoji{carrot}};
                \node at (0,0) {\emoji{rabbit-face}};
            \end{tikzpicture}
            \caption*{What is the coordinate of the \emoji{carrot}?}
        \end{center}
    \end{figure}

    \pause{}

    Colloquially, we call $(x_{1}, \dots, x_{n})$ the \emph{coordinate} of
    $\begin{bmatrix} x_{1} \\ \dots \\ x_{n} \end{bmatrix}$.

    \think{} How can we make this precise and extend it to \emph{vector spaces}?
\end{frame}

\subsection{Coordinates}

\begin{frame}
    \frametitle{Theorem 7 --- The Unique Representation Theorem}

    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a \emph{basis} of $V$.
    Then for each $\bfx \in V$, there exists a \emph{unique} set of scalars
    $c_1, \dots, c_n$
    such that
    \begin{equation*}
        \bfx = c_1 \bfb_1 + \dots c_n \bfb_n.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Coordinates}

    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $V$.

    The \alert{coordinates of $\bfx$ relative to $\scB$}
    are the scalars $c_1, \dots, c_n$ such that
    \begin{equation*}
        \bfx = c_1 \bfb_1 + \dots c_n \bfb_n.
    \end{equation*}

    \pause{}

    We denote this by
    \begin{equation*}
        [\bfx]_{\scB}
        =
        \begin{bmatrix}
            c_1 \\ \vdots \\ c_{n}
        \end{bmatrix}
        .
    \end{equation*}

    The mapping $\bfx \mapsto [\bfx]_{\scB}$ is the \alert{coordinate mapping}.

    \cake{} What is the domain and codomain of this mapping?
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Consider a basis for $\dsR^{2}$
    \begin{equation*}
        \scB =
        \left\{
            \begin{bmatrix}
                1 \\ 0
            \end{bmatrix}
            ,
            \begin{bmatrix}
                1 \\ 2
            \end{bmatrix}
        \right\}
        .
    \end{equation*}
    What is $\bfx$ if
    \begin{equation*}
        [\bfx]_{\scB}
        =
        \begin{bmatrix}
            -2 \\ 3
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \cake{} What is the coordinate of $\bfx = \begin{bmatrix} 1 \\ 6 \end{bmatrix}$
    relative to the \emph{standard basis} $\scE = \{\bfe_1, \bfe_2\}$?
\end{frame}

\begin{frame}
    \frametitle{A graphical interpretation}

    The same vector has different coordinates for different bases.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{coordinate-graphic.png}
    \end{figure}

    \cake{} 
    Given $[\bfx]_{\scB}$, how do we change it to $[\bfx]_{\scE}$ and
    vice versa?
\end{frame}

\subsection{Coordinates in \texorpdfstring{$\mathbb{R}^{n}$}{R^n}}

\begin{frame}
    \frametitle{Change of coordinates matrix}

    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $\dsR^{n}$.
    Let
    \begin{equation*}
        P_{\scB} =
        \begin{bmatrix}
            \bfb_{1} & \dots & \bfb_{n}
        \end{bmatrix}
        .
    \end{equation*}
    We call $P_{\scB}$ the \alert{change of coordinates matrix} from $\scB$ to
    $\scE$.

    \cake{} What is $P_{\scE}$ where $\scE$ is the standard basis of $\dsR^{n}$?
\end{frame}

\begin{frame}
    \frametitle{Properties of the change of coordinates matrix}

    \cake{} Why is that for $\bfx \in \dsR^{n}$
    \begin{equation*}
        \bfx = P_{\scB} \cdot [\bfx]_{\scB},
    \end{equation*}
    \pause{}So we have
    \begin{equation*}
        [\bfx]_{\scB} = (P_{\scB})^{-1} \cdot \bfx.
    \end{equation*}

    \cake{} Why is $P_{\scB}$ invertible?
    
    \pause{}
    \hint{} Note that $\bfx \mapsto [\bfx]_{\scB}$ is a linear transformation.
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Let
    \begin{equation*}
        \scB =
        \left\{
            \begin{bmatrix}
                2 \\ 1
            \end{bmatrix}
            ,
            \begin{bmatrix}
                -1 \\ 1
            \end{bmatrix}
        \right\}
        ,
        \qquad
        \bfx =
        \begin{bmatrix}
            4 \\ 5
        \end{bmatrix}
        .
    \end{equation*}

    Then what is
    \begin{flalign*}
        [\bfx]_{\scB}
        =
        &&
    \end{flalign*}

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{coordinate-change.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Is the mapping $\bfx \mapsto [\bfx]_{\scB} = (P_{\scB})^{-1} \bfx$ always one-to-one?

    Is it always onto?

    What is your justification?
\end{frame}

\subsection{The Coordinate Mapping}

\begin{frame}
    \frametitle{One-to-one and onto?}

    Now consider a vector space $V$, not necessarily $\dsR^{n}$.

    For $\bfx \in V$, is the mapping $\bfx \mapsto [\bfx]_{\scB}$ 
    one-to-one?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{coordinate-mapping.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8 --- Coordinate mapping is linear}

    Let $\scB = \{\bfb_{1}, \dots, \bfb_{n}\}$ be a basis of $V$.
    The mapping $\bfx \mapsto [\bfx]_{\scB}$ is a one-to-one \emph{linear
    mapping} from
    $V$ onto $\dsR^{n}$.

    \pause{}

    \begin{block}{Isomorphism}
        A one-to-one and onto linear mapping 
        from a vector space $V$ onto a vector space $W$
        is called an \alert{isomorphism}.

        In other words, Theorem 8 says $V$ and $\dsR^{n}$ are indistinguishable as vector spaces. 
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Coordinates of linear combinations}
    \cake{}
    Since $\bfx \mapsto [\bfx]_{\scB}$ is a linear mapping,
    we have
    \begin{flalign*}
        [
        c_{1} \bfu_{1}
        +
        c_{2} \bfu_{2}
        +
        \cdots
        c_{p} \bfu_{p}
        ] _{\scB}
        =
        &&
    \end{flalign*}

    \hint{}
    In words, the $\scB$-coordinate vector of a linear combination of
    $\veclist[p]{u}$ is the same linear combination of their coordinate vectors.
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    Let $\scB = \{1, t, t^2, t^3\}$ be the standard basis of $\dsP_{3}$.
    Then the polynomial (vector)
    \begin{equation*}
        \bfp(t) = a_0 + a_1 t + a_2 t^2 + a_3 t^3
    \end{equation*}
    has the coordinates
    \begin{equation*}
        [\bfp]_{\scB}
        =
        \begin{bmatrix}
            \phantom{a_0} \\ \phantom{a_1} \\ \phantom{a_2} \\ \phantom{a_3}
        \end{bmatrix}
        .
    \end{equation*}
    \cake{} Thus the mapping $\bfp \mapsto [\bfp]_{\scB}$ is an isomorphism from
    \blankshort{} to \blankshort{}.
\end{frame}

\begin{frame}
    \frametitle{Example 6}

    Use coordinates vectors to show that
    \begin{equation*}
        1 + 2 t^2,
        \qquad
        4 + t + 5t^2,
        \qquad
        3 + 2t
    \end{equation*}
    are linearly independent.

    \begin{block}{Fact}
        A set of vectors are linearly independent
        if and only if their coordinate vectors are linearly independent.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 7}

    Let
    \begin{equation*}
        \bfv_{1} =
        \begin{bmatrix}
            3 \\ 6 \\ 2
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2} =
        \begin{bmatrix}
            -1 \\ 0 \\ 1
        \end{bmatrix}
        ,
        \qquad
        \bfx =
        \begin{bmatrix}
            3 \\ 12 \\ 7
        \end{bmatrix}
        .
    \end{equation*}

    \cake{} Is $\bfx \in \Span{\bfv_1, \bfv_2}$?
    If so what is its coordinate $[\bfx]_{\{\bfv_{1}, \bfv_{2}\}}$?
\end{frame}

\begin{frame}
    \frametitle{Example 7 in picture}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{coordinate-r3.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Show that two vectors $\bfv_{1}$ and $\bfv_{2}$ 
    are linearly independent
    if and only if their coordinate vectors 
    $[\bfv_{1}]_{\scB}$
    and
    $[\bfv_{2}]_{\scB}$
    for some basis $\scB$ are linearly independent.

    \begin{block}{Proof of ``if''}
        %Assume $\scB = \{\bfb_1 \dots \bfb_{p}\}$. Write
        %\begin{equation*}
        %    [\bfv_{1}]_{\scB}
        %    =
        %    \begin{bmatrix}
        %        v_{11} \\
        %        \vdots \\
        %        v_{1p}
        %    \end{bmatrix}
        %    ,
        %    \qquad
        %    [\bfv_{2}]_{\scB}
        %    =
        %    \begin{bmatrix}
        %        v_{21} \\
        %        \vdots \\
        %        v_{2p}
        %    \end{bmatrix}
        %\end{equation*}
        %Thus
        %\begin{equation*}
        %    \bfv_1 = \underline{\hspace{3cm}},
        %    \qquad
        %    \bfv_2 = \underline{\hspace{3cm}}
        %\end{equation*}
        If $c_{1} \bfv_{1} + c_{2} \bfv_{2} = \bfzero$, then
        \begin{equation*}
            c_{1}[\bfv_{1}]_{\scB}
            +
            c_{2}[\bfv_{2}]_{\scB}
            =
            \underline{\hspace{2cm}}
            =
            \bfzero
        \end{equation*}
        because $\bfx \mapsto [\bfx]_{\scB}$ is \blankshort{}.

        Since 
        $[\bfv_{1}]_{\scB}$
        and
        $[\bfv_{2}]_{\scB}$
        are \blankshort{},
        we must have
        $c_{1} = c_{2} = \underline{\hspace{1cm}}$.

        This implies $\bfv_1$ and $\bfv_2$ are \blankshort{}.
    \end{block}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \think{}
%    Use coordinate vectors to show that the following is a basis of $\dsP_{2}$.
%    \begin{equation*}
%        \scB =
%        \{1+t^2, t-3t^2, 1+t-3t^2\}.
%    \end{equation*}
%
%    \think{}
%    Find $\bfq \in \dsP_{2}$ such that
%    \begin{equation*}
%        [\bfq]_{\scB} =
%        \begin{bmatrix}
%            -1 \\ 1 \\2
%        \end{bmatrix}
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Exercise}
%
%    \sweat{}
%    Let $S$ be a finite set in a vector space $V$ with the property
%    that every $x \in V$ has a unique representation as a linear
%    combination of elements of $S$. Show that $S$ is a basis of $V$.
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.4}{23, 25, 27, 29, 35, 41}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
