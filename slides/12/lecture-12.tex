\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}
\title{Lecture 11 --- Cramer's Rule}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}

    \tableofcontents{}
\end{frame}

\section{3.3 Cramer's Rule, Volume and Linear Transformations}

\subsection{Cramer's Rule}

\begin{frame}
    \frametitle{\testtube{} An experiment}
    
    Let $\bfe_{1}, \dots, \bfe_{3}$ be the role columns of $I_3$.
    Let
    \begin{equation*}
        A =
        \begin{bmatrix}
            2 & 1 & 2 \\
            2 & 0 & 2 \\
            0 & 0 & 2
        \end{bmatrix}
    \end{equation*}
    \think{} What are 
    $A \bfe_{1}$,
    $A \bfe_{2}$,
    $A \bfe_{3}$?

    \pause{}

    \cake{} What are 
    $\bfe_{1} A$,
    $\bfe_{2} A$,
    $\bfe_{3} A$.
\end{frame}

\begin{frame}
    \frametitle{Rows and columns}
    Let $A$ be an $n \times n$ matrix.

    Let $\bfe_{1}, \dots, \bfe_{n}$ be the role columns of $I_n$.

    \cake{} What are $\bfe_{j} A$ and $A \bfe_{j}$?
\end{frame}

\begin{frame}
    \frametitle{A notation}

    Given $A = \begin{bmatrix} \bfa_1 & \bfa_2 & \dots & \bfa_n \end{bmatrix}$,
    let
    \begin{equation*}
        A_i(\bfb) =
        \begin{bmatrix}
            \bfa_1 & \dots & \bfa_{i-1} & \bfb & \bfa_{i+1} & \dots & \bfa_{n}
        \end{bmatrix}
        .
    \end{equation*}

    \pause{}

    \begin{example}
    Let
    \begin{equation*}
        A=
        \left[
            \begin{array}{ccc}
                0 & 0 & -1 \\
                0 & 3 & -1 \\
                3 & 3 & 0 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfb =
        \left[
            \begin{array}{c}
                0 \\
                3 \\
                2 \\
            \end{array}
        \right]
        .
    \end{equation*}
    \cake{} What is
    \begin{equation*}
        A_{3}(\bfb) =
        \left[
            \phantom{
            \begin{array}{ccc}
                0 & 0 & 0 \\
                3 & 3 & 3 \\
                2 & 2 & 2 \\
            \end{array}
            }
        \right]
    \end{equation*}
    %\cake{} What is $A_{1}(\bfb), A_{2}(\bfb)$?
    \end{example}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Let 
    \begin{equation*}
        I = \begin{bmatrix} 1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix},
        \qquad
        \bfx = \begin{bmatrix}7 \\ 13 \\ 5 \end{bmatrix}.
    \end{equation*}

    What are $\det(I_{1}(\bfx)), \det(I_2(\bfx)), \det(I_{3}(\bfx))$?
\end{frame}

\begin{frame}
    \frametitle{What is $\det(I_{i}(\bfx))$?}
    Let $I$ be the $n \times n$ identity matrix.
    Let $\bfx = \begin{bmatrix}x_1 \\ \vdots \\ x_n \end{bmatrix}$.

    \cake{} What is the determinant of
    \begin{equation*}
        I_{i}(\bfx)
        =
        \begin{bmatrix}
            1      & \cdots & 0      & x_1    & 0      & \cdots & 0      \\
            \vdots & \ddots & \vdots & \vdots & \vdots & \ddots & \vdots \\
            0      & \cdots & 1      & x_{i-1}& 0      & \cdots & 0      \\
            0      & \cdots & 0      & x_{i}  & 0      & \cdots & 0      \\
            0      & \cdots & 0      & x_{i+1}& 1      & \cdots & 0      \\
            \vdots & \ddots & \vdots & \vdots & \vdots & \ddots & \vdots \\
            0      & \cdots & 0      & x_n    & 0      & \cdots & 1
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 7 --- Cramer's rule}

    If $A$ is invertible, then for any $\bfb$, the solution of $A \bfx = \bfb$ is
    \begin{equation*}
        \bfx =
        \begin{bmatrix}
            \displaystyle\frac{\det A_{1}(\bfb)}{\det A}
            \\
            \vdots
            \\
            \displaystyle\frac{\det A_{n}(\bfb)}{\det A}
        \end{bmatrix}
    \end{equation*}

    \hint{} Don't use this to solve the equation. Too \emoji{snail}!
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    \think{} Let
    \begin{equation*}
        A =
        \begin{bmatrix}
            3 & -2 \\
            -5 & 4 \\
        \end{bmatrix}
        ,
        \qquad
        \bfb
        =
        \begin{bmatrix}
            6 \\ 8
        \end{bmatrix}
        .
    \end{equation*}
    Use Cramer's Rule to solve $A \bfx = \bfb$.
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Use Cramer's rule to describe the solution sets.
    \begin{equation*}
        \systeme[x_1x_2]{
            3 s x_1 -2 x_2 = 4,
           -6 x_1 +s x_2 = 1
        }
    \end{equation*}
\end{frame}

\subsection{A formula for $A^{-1}$}

\begin{frame}
    \frametitle{Compute $A^{-1}$ with Cramer's rule}
    
    Let $A$ be an invertible matrix.
    Show that
    \begin{equation*}
        (A^{-1})_{i j} = \frac{\det A_{i}(\bfe_{j})}{\det A},
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{Compute the inverse with Cramer's rule}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%            \begin{block}{Theorem 7}
%                If $A$ is invertible, then for any $\bfb$, the solution of $A \bfx = \bfb$ is
%                \begin{equation*}
%                    \bfx =
%                    \begin{bmatrix}
%                        \frac{\det A_{1}(\bfb)}{\det A}
%                        \\
%                        \vdots
%                        \\
%                        \frac{\det A_{n}(\bfb)}{\det A}
%                    \end{bmatrix}
%                \end{equation*}
%            \end{block}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            Let $A$ be an invertible matrix.
%            Show that
%            \begin{equation*}
%                (A^{-1})_{i j} = \frac{\det A_{i}(\bfe_{j})}{\det A},
%            \end{equation*}
%
%            \pause{}
%
%            \hint{} Hints:
%            \begin{enumerate}[<+->]
%                \item What is $A^{-1} \bfe_{j}$?
%                \item What is the solution of $A \bfx = \bfe_{j}$?
%                \item How to use Cramer's rule to compute this solution $\bfx$?
%            \end{enumerate}
%        \end{column}
%    \end{columns}
%\end{frame}

\begin{frame}
    \frametitle{Theorem 8 --- A formula for $A^{-1}$}

    Recall that the $(i, j)$ cofactor of $A$ is the number
    \begin{equation*}
        C_{ij} = (-1)^{i+j} \det A_{ij}.
    \end{equation*}
    If $A$ is invertible, then $A^{-1} = \frac{1}{\det A} C$ where
    \begin{equation*}
        C =
        \begin{bmatrix}
            C_{1,1} & C_{2,1} & \cdots & C_{n,1} \\
            C_{1,2} & C_{2,2} & \cdots & C_{n,2} \\
            \vdots  & \vdots  & \ddots & \vdots  \\
            C_{1,n} & C_{2,n} & \cdots & C_{n,n}
        \end{bmatrix}
    \end{equation*}
    The matrix $C$ is called the \alert{adjugate/classical adjoint} of $A$ and
    is denoted by $\adj A$.
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Recall that
    \begin{equation*}
        C_{ij} = (-1)^{i+j} \det A_{ji}.
    \end{equation*}
    Let
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccc}
                2 & 1 & 3 \\
                1 & -1 & 1 \\
                1 & 4 & -2 \\
            \end{array}
        \right]
        .
    \end{equation*}
    Then $\det A = 14$.
    What is $C_{32}$ and what is $(A^{-1})_{23}$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let $A$ be a matrix containing only integers.

    What are some sufficient conditions for $A^{-1}$ to contain only
    integers?

    \hint{} Recall that
    \begin{equation*}
        A^{-1} = \frac{1}{\det A} \adj A
    \end{equation*}
\end{frame}

\subsection{Determinants as Area or Volume}

\begin{frame}
    \frametitle{Theorem 9 --- Determinants as Area}

    If $A$ is a $2 \times 2$ matrix,
    the area of the parallelogram determined by the columns of $A$ is $\abs{\det A}$.

    \begin{columns}
        \begin{column}{0.2\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{determinant-2d-1.png}
            \end{figure}
        \end{column}
        \begin{column}{0.8\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{determinant-2d-2.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Theorem 9 --- Determinants as Volume}

    If $A$ is a $3 \times 3$ matrix,
    the volume of the parallelepiped determined by the columns of $A$ is
    $\abs{\det A}$.

    \begin{columns}
        \begin{column}{0.3\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{determinant-2d-3.png}
            \end{figure}
        \end{column}
        \begin{column}{0.7\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{determinant-2d-4.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Linear Transformation}

\begin{frame}
    \frametitle{Theorem 10 --- Linear Transformation in $\dsR^{2}$}

    Let $T: \dsR^{2} \mapsto \dsR^{2}$ be a linear transform defined by $T(\bfx)
    = A\bfx$.
    Then for any $S \in \dsR^{2}$
    \begin{equation*}
        \area (T(S)) = \abs{\det(A)} \area S.
    \end{equation*}

    Proof: 
    \only<1>{First just consider when $S$ is the unit square.}
    \only<2>{Then note that any area can be approximated by small squares.}

    \begin{figure}[htpb]
        \centering
        \only<1>{
            \begin{tikzpicture}
                % Draw the first square and fill it with blue
                \fill[mayablue] (0,0) rectangle (1,1);

                % Draw the second parallelogram, fill it with blue, and make it twice as large
                \fill[mayablue] (2,0) -- (4,0) -- (5,2) -- (3,2) -- cycle;

                % Add an arrow from the square to the parallelogram
                \draw[->,ultra thick] (1.3,0.5) -- (2,0.5);

                % Add a node for S in the center of the square
                \node at (0.5,0.5) {$S$};

                % Add a node for T(S) in the center of the parallelogram
                \node at (3.5,1) {$T(S)$};
            \end{tikzpicture}
        }
        \only<2>{
            \includegraphics[width=0.8\linewidth]{determinant-2d-transform-1.png}
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 5 --- The area of an eclipse}

    What is the area of the eclipse in the picture?
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{determinant-2d-transform-2.png}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}

        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Theorem 10 --- Linear Transformation in $\dsR^{3}$}

    Let $T: \dsR^{3} \mapsto \dsR^{3}$ be a linear transform defined by $T(\bfx)
    = A \bfx$.
    Then for any $V \in \dsR^{3}$
    \begin{equation*}
        \vol (T(V)) = \det(A) \vol V.
    \end{equation*}
\end{frame}


\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}
    The \emph{tetrahedron} $S'$ is a linear transformation of $S$.
    What is $\vol(S')$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{determinant-3d-transform-1.png}
    \end{figure}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{3.3}{17, 25, 29, 31, 33}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
