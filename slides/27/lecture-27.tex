\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Orthogonal Projections, The Gram-Schmidt Process}

\begin{document}

\maketitle{}

\section{6.3 Orthogonal Projections}

\begin{frame}
    \frametitle{Review --- Projections to a one-dimensional subspace}

    \begin{figure}
        \centering
        \includegraphics[width=0.8\linewidth]{projection-subspace.png}
        \caption*{Projections of vectors in $\dsR^{2}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Projections to a subspace in general}

    Given $\bfy \in \dsR^{n}$ and subspace $W$ of $\dsR^{n}$, we want to find
    $\bfy = \bfz + \hat{\bfy}$, such that $\bfz \in W^{\perp}$ and $\hat{\bfy} \in W$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{projection.png}
        \caption*{Projections to a two-dimensional subspace}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Let $\{\bfu_{1}, \dots, \bfu_{5}\}$ be an \emph{orthogonal basis} for $\dsR^{5}$.
    Let
    \begin{equation*}
        \bfy =
        c_{1} \bfu_1
        +
        c_{2} \bfu_2
        +
        c_{3} \bfu_3
        +
        c_{4} \bfu_4
        +
        c_{5} \bfu_5
        .
    \end{equation*}
    Let $W = \Span{\bfu_1, \bfu_2}$.

    What are $\bfz$ and $\hat{\bfy}$ such that 
    \begin{itemize}
        \item $\bfy = \bfz + \hat{\bfy}$,
        \item $\bfz \in W^{\perp}$,
        \item $\hat{\bfy} \in W$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8 --- The Orthogonal Decomposition Theorem}

    The projection of $\bfy$ to $W$ is unique.

    If $\{\bfu_{1}, \dots, \bfu_{p}\}$ is an \emph{orthogonal basis} of $W$,
    then
    \begin{equation*}
        \hat{\bfy} =
        \frac{\bfy \cdot \bfu_1}{\bfu_1 \cdot \bfu_1} \bfu_1
        +
        \frac{\bfy \cdot \bfu_2}{\bfu_2 \cdot \bfu_2} \bfu_2
        +
        \cdots
        +
        \frac{\bfy \cdot \bfu_p}{\bfu_p \cdot \bfu_p} \bfu_p
        ,
    \end{equation*}
    and
    \begin{equation*}
        \bfz = \bfy - \hat{\bfy}
    \end{equation*}
    is orthogonal to $W$.

    \pause{}

    \hint{} To find $\hat{\bfy}$, we don't need an orthogonal basis for $\dsR^{n}$.
    We only need an orthogonal basis for $W$.

    The vector $\hat{\bfy}$ is called the \alert{orthogonal projection of
    $\bfy$ onto $W$}.

    The vector $\bfz$ is called the \alert{component of $\bfy$ orthogonal to $W$}.
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Let
    \begin{equation*}
        \bfu_1
        =
        \left[
            \begin{array}{c}
                2 \\
                5 \\
                -1 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfu_2
        =
        \left[
            \begin{array}{c}
                -2 \\
                1 \\
                1 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfy
        =
        \left[
            \begin{array}{c}
                1 \\
                2 \\
                3 \\
            \end{array}
        \right]
        .
    \end{equation*}
    Let $W = \Span{\bfu_1, \bfu_2}$.

    What are $\hat{\bfy}$ and $\bfz$?
\end{frame}

\subsection{A Geometric Interpretation of the Orthogonal Projection}

\begin{frame}
    \frametitle{A Geometric Interpretation}

    The orthogonal projection of $\bfy$ to $W$ is the
    sum of its projections onto one-dimensional
    subspaces that are mutually orthogonal.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{projection-r3.png}
        \caption*{$\hat{\bfy}$ projected to $\Span{\bfu_1, \bfu_2}$}
    \end{figure}
\end{frame}

\subsection{Properties of Orthogonal Projections}

\begin{frame}{Review}
    \begin{block}{Theorem 5 (6.2)}
        Let $\{\bfu_1, \dots, \bfu_p\}$ be an \emph{orthogonal basis} of $W$,
        then for all $\bfy \in W$,
        \begin{equation*}
            \bfy 
            = 
            \frac{\bfy \cdot \bfu_{1}}{\bfu_{1}\cdot \bfu_1} \bfu_1 
            +
            \dots 
            +
            \frac{\bfy \cdot \bfu_{p}}{\bfu_{p}\cdot \bfu_p} \bfu_p
            .
        \end{equation*}
    \end{block}
    \begin{block}{Theorem 8 (6.3)}
    If $\{\bfu_{1}, \dots, \bfu_{p}\}$ is an \emph{orthogonal basis} of $W$,
    then
    \begin{equation*}
        \hat{\bfy} =
        \frac{\bfy \cdot \bfu_1}{\bfu_1 \cdot \bfu_1} \bfu_1
        +
        \cdots
        +
        \frac{\bfy \cdot \bfu_p}{\bfu_p \cdot \bfu_p} \bfu_p
        ,
    \end{equation*}
    and $\bfz = \bfy - \hat{\bfy}$.
    \end{block}
    \cake{} What is $\hat{\bfy} = \proj_W \bfy$ if $\bfy \in W$?
\end{frame}


\begin{frame}
    \frametitle{Theorem 9 --- The Best Approximation Theorem}

    Let $\hat{\bfy} = \proj_W \bfy$.
    Then for all $\bfv \in W$ with $\bfv \ne \hat{\bfy}$,
    \begin{equation*}
        \norm{\bfy - \hat{\bfy}}
        \alert{<}
        \norm{\bfy - \bfv}
        .
    \end{equation*}

    \hint{} It also follows from this theorem that if $\bfy \in W$, then $\hat{\bfy}
    = \bfy$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 9 in a picture}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{projection-best-approximation.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 10 --- Orthonormal bases}

    If $\{\bfu_{1},\dots, \bfu_{p}\}$ is an \emph{orthonormal basis} of $W$, then
    \begin{equation*}
        \proj_{W} \bfy
        =
        (\bfy \cdot \bfu_{1})
        \bfu_{1}
        +
        (\bfy \cdot \bfu_{2})
        \bfu_{2}
        +
        \cdots
        (\bfy \cdot \bfu_{p})
        \bfu_{p}
        .
    \end{equation*}
    \pause{}
    Thus letting $U = \begin{bmatrix}\bfu_{1} & \cdots & \bfu_{p}
    \end{bmatrix}$, we have
    \begin{equation*}
        \proj_{W} \bfy
        =
        U U^{T} \bfy
        .
    \end{equation*}

    \cake{} What is $U^{T} U$?
\end{frame}

\section{6.4 The Gram-Schmidt Process}

\begin{frame}
    \frametitle{Example 1}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            Let $W = \Span{\bfx_1, \bfx_2}$ where
            \begin{equation*}
                \bfx_1
                =
                \begin{bmatrix}
                    3 \\ 6 \\ 0
                \end{bmatrix}
                ,
                \quad
                \bfx_2
                =
                \begin{bmatrix}
                    1 \\ 2 \\ 2
                \end{bmatrix}
            \end{equation*}

            Find an orthogonal basis of $W$.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.4-fig-1.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            Let $W = \Span{\bfx_1, \bfx_2, \bfx_3}$ where
            \begin{equation*}
                \bfx_1
                =
                \begin{bmatrix}
                    1 \\ 1 \\ 1 \\ 1
                \end{bmatrix}
                ,
                \quad
                \bfx_2
                =
                \begin{bmatrix}
                    0 \\ 1 \\ 1 \\ 1
                \end{bmatrix}
                ,
                \quad
                \bfx_3
                =
                \begin{bmatrix}
                    0 \\ 0 \\ 1 \\ 1
                \end{bmatrix}
                .
            \end{equation*}
            Find an orthogonal basis of $W$.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{6.4-fig-2.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Theorem 11 --- The Gram-Schmidt Process}
    Given a basis $\{\mathbf{x}_1, \dots, \mathbf{x}_p\}$ for a non-zero vector
    subspace $W$ of $\mathbb{R}^n$, define $\mathbf{v}_1, \dots, \mathbf{v}_p$ by
    \begin{align*}
        \mathbf{v}_1 & = \mathbf{x}_1, \\
        \mathbf{v}_2 & = \mathbf{x}_2 - \frac{\mathbf{x}_2 \cdot \mathbf{v}_1}{\mathbf{v}_1 \cdot \mathbf{v}_1} \mathbf{v}_1, \\
        \mathbf{v}_3 & = \mathbf{x}_3 - \frac{\mathbf{x}_3 \cdot \mathbf{v}_1}{\mathbf{v}_1 \cdot \mathbf{v}_1} \mathbf{v}_1 - \frac{\mathbf{x}_3 \cdot \mathbf{v}_2}{\mathbf{v}_2 \cdot \mathbf{v}_2} \mathbf{v}_2, \\
                     & \vdots \\
        \mathbf{v}_p & = \mathbf{x}_p - \sum_{i=1}^{p-1} \frac{\mathbf{x}_p \cdot \mathbf{v}_i}{\mathbf{v}_i \cdot \mathbf{v}_i} \mathbf{v}_i.
    \end{align*}
    The set of vectors $\{\mathbf{v}_1, \dots, \mathbf{v}_p\}$ is an orthogonal basis for $W$. 

    In addition,
    \begin{equation*}
        \Span{\mathbf{x}_1, \dots, \mathbf{x}_k} = \Span{\mathbf{v}_1, \dots,
        \mathbf{v}_k}, \qquad  (1 \leq k \leq p).
    \end{equation*}
\end{frame}

\subsection{Orthonormal bases}

\begin{frame}
    \frametitle{Example 3 -- Orthonormal bases}

    Construct an orthonormal basis form the orthogonal basis
    \begin{equation*}
        \bfv_{1}
        =
        \begin{bmatrix}
            3 \\ 6 \\ 0
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2}
        =
        \begin{bmatrix}
            0 \\ 0 \\ 2
        \end{bmatrix}
    \end{equation*}
\end{frame}

\subsection{QR Factorization}

\begin{frame}
    \frametitle{Example 4}
    Let
    \begin{equation*}
        A
        =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                1 & 1 & 0 \\
                1 & 1 & 1 \\
                1 & 1 & 1 \\
            \end{array}
        \right]
    \end{equation*}

    Find a $Q$ and $R$ such that $A = QR$, where 
    \begin{itemize}
        \item $Q$ is $4 \times 3$ matrix with orthonormal columns, 
        \item $R$ is $3 \times 3$ upper triangular matrix with non-zero columns
            on the diagonal.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 12 --- QR Factorization}

    If $A$ is an $m \times n$ matrix with linearly independent columns,
    then $A$ can be factored as $A = QR$,
    \begin{itemize}
        \item where $Q$ is an $m \times n$ matrix whose columns form an orthonormal basis for $\colspace A$
        \item and $R$ is an $n \times n$ upper triangular invertible matrix with positive entries on its diagonal.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Suppose $A = QR$, where $Q$ is $m \times n$ and $R$ is $n \times n$.
    Show that if the columns of $A$ are linearly independent,
    then $R$ must be invertible.

    \bomb{} This is not necessary a QR factorization.

    \hint{} Why does equation $R \bfx = 0$ have only the trivial solution?
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{6.3}{13, 23, 25, 27}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
