\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Linearly Independent Sets; Bases}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{4.3 Linearly Independent Sets; Bases}

\begin{frame}
    \frametitle{Introduction}
    
    This section studies the subsets that span a vector space
    $V$ or a subspace $H$ as ``efficiently'' as possible. 

    The \emoji{key} idea is that of \emph{linear independence}.
\end{frame}

\begin{frame}
    \frametitle{Review --- Linear independence in $\dsR^{n}$}
    A set of vectors $\{\bfv_{1}, \dots, \bfv_p\}$ in $\dsR^{n}$
    is \alert{linearly independent}
    if
    \begin{equation*}
        c_1 \bfv_1 + c_2 \bfv_2 + \dots + c_p \bfv_v = \bfzero
    \end{equation*}
    has only the trivial solution $c_1 = c_2 = \dots = c_p = 0$.

    Otherwise it is \alert{linearly dependent}.
\end{frame}

\begin{frame}
    \frametitle{Linear independence in $\dsR^{n}$ in vector spaces}
    A set of vectors $\{\bfv_{1}, \dots, \bfv_p\}$ in a \emph{vector space} $V$
    is \alert{linearly independent} if
    \begin{equation*}
        \underline{\hspace{5cm} = \hspace{1cm}}
    \end{equation*}
    has only the trivial solution $c_1 = c_2 = \dots = c_p = 0$.

    Otherwise it is \alert{linearly dependent}.

    \cake{} Is $\{\bfzero\}$ linearly independent?

    \cake{} If $\bfv \ne \bfzero$, is $\{\bfv\}$ linearly independent?
\end{frame}


\begin{frame}
    \frametitle{Theorem 7 (1.7) --- Linear independence of two or more vectors}
    Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$ 
    be a set of vectors in \emph{$\dsR^{n}$}
    with $p \ge 2$.

    Then $S$ is \emph{linearly dependent} if and only if
    \emph{at least one vector} in $S$ is a \emph{linear combination}
    of the others.

    If $S$ is linearly dependent and $\bfv_1 \ne \bfzero$,
    then some $\bfv_j$ with ($j > 1$) is a linear combination of
    the preceding vectors, $\bfv_1, \dots, \bfv_{j-1}$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 4 (4.3) --- Linearly independent?}

    Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$ 
    be a set of vectors in a \emph{vector space $V$}
    with $p \ge 2$.

    Then $S$ is \emph{linearly dependent} if and only if
    \emph{at least one vector} in $S$ is a \emph{linear combination}
    of the others.

    If $S$ is linearly dependent and $\bfv_1 \ne \bfzero$,
    then some $\bfv_j$ with ($j > 1$) is a linear combination of
    the preceding vectors, $\bfv_1, \dots, \bfv_{j-1}$.

    \cake{} What is the sufficient and necessary
    condition for $\{\bfv_{1}, \bfv_{2}\}$ to be \emph{linearly dependent}?
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Consider three vectors
    $\bfp_{1}(t) = 1$, $\bfp_{2}(t) = t$
    and $\bfp_{3}(t) = 4 - t$
    in the vector space $\dsP_{1}$.

    \cake{} Are they linearly independent?
\end{frame}

%\begin{frame}
%    \frametitle{Example 2}
%
%    Consider $\{\sin t, \cos t\}$ in $C[0,1]$.
%    Are they linearly independent?
%    What about $\{\sin 2t, \sin t \cos t\}$?
%\end{frame}

\begin{frame}
    \frametitle{\sweat{} Think-Pair-Share}

    Let $T:V \mapsto W$ be a linear transform.
    Further assume that $T$ is \emph{one-to-one}.
    Show that if
    $\{T(\bfv_{1}), \ldots, T(\bfv_{p})\}$
    is linearly dependent,
    then so is
    $\{\bfv_{1}, \ldots, \bfv_{p}\}$.

    \begin{block}{Proof}
        \footnotesize{}
        Since $\{T(\bfv_{1}), \ldots, T(\bfv_{p})\}$ are linearly dependent,
        there exist $c_{1}, \dots, c_{p}$, which are not all $0$,  such that
        \begin{equation*}
            c_1 T(\bfv_{1}) + \dots c_{p} T(\bfv_{p})
            =
            \bfzero
        \end{equation*}
        Thus, by the linearity of $T$,
        \begin{equation*}
            T(\underline{\hspace{3cm}})
            =
            \bfzero
        \end{equation*}
        Again, by the linearity of $T$, $T(\underline{\hspace{1cm}}) = \bfzero$.

        Therefore, since $T$ is one-to-one, we have
        \begin{equation*}
            \underline{\hspace{3cm}}
            =
            \bfzero
        \end{equation*}
    \end{block}
\end{frame}

\subsection{Bases}

\begin{frame}
    \frametitle{Bases (plural)}

    Let $H$ be a subspace of $V$.
    A set of vectors $\scB = \{\bfv_{1}, \dots, \bfv_{p}\}$ is a \alert{basis}
    (singular) of $H$ if
    \begin{enumerate}[(i)]
        \item $\scB$ is \emph{linearly independent},
        \item and $\Span{\scB} = H$.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let $A = [\bfa_{1} \, \ldots \, \bfa_{n}]$ be an invertible $n \times n$ matrix.

    Show that the columns of $A$ a basis for $\dsR^{n}$.

    \cake{} What are the two things we need to check?

    \pause{}

    \vspace{-0.5em}
    \begin{tcolorbox}[
        title={The Invertible Matrix Theorem (2.3)}
        ]
        Let $A$ be $n \times n$.
        The following are equivalent.
        \begin{enumerate}
            \item[a.] $A$ is an invertible matrix.
            \item[e.] The columns of $A$ form a linearly independent set.
            \item[h.] The columns of $A$ spans $\dsR^n$.
        \end{enumerate}
    \end{tcolorbox}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            The set $\{\bfe_{1}, \dots, \bfe_{n}\}$
            is called the \alert{standard basis} of $\dsR^{n}$

            \cake{}
            Given a vector $\begin{bmatrix} 5 \\ 1 \\ 3 \end{bmatrix}$,
            how can we write it as a linear combination of the standard basis of
            $\dsR^{3}$?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{basis-standard.png}
                \caption*{The standard basis of $\dsR^{3}$}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    \think{} How to check if the following a basis of $\dsR^{3}$?
    \begin{equation*}
        \bfv_{1} =
        \begin{bmatrix}
            3 \\ 0 \\ -6
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2} =
        \begin{bmatrix}
            -4 \\ 1 \\ 7
        \end{bmatrix}
        ,
        \qquad
        \bfv_{3} =
        \begin{bmatrix}
            -2 \\ 1 \\ 5
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 6}
    The set $S = \{1, t, t^{2}, \dots, t^{n}\}$
        is called the \alert{standard basis} of $\dsP_{n}$.

    \cake{}
    Given a vector $\bfp = 3 + t^2$,
    how can we write it as a linear combination of the standard basis of
    $\dsP_{3}$?
\end{frame}

\subsection{The Spanning Set Theorem}

\begin{frame}
    \frametitle{Example 7}

    Let
    \begin{equation*}
        \bfv_{1} =
        \begin{bmatrix}
            0 \\ 2 \\ -1
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2} =
        \begin{bmatrix}
            2 \\ 2 \\ 0
        \end{bmatrix}
        ,
        \qquad
        \bfv_{3} =
        \begin{bmatrix}
            6 \\ 16 \\ -5
        \end{bmatrix}
        ,
    \end{equation*}
    and $H = \Span{\bfv_1, \bfv_2, \bfv_3}$.
    Show that $\{\bfv_{1}, \bfv_{2}\}$ is a basis of $H$.

    \hint{} $\bfv_{3}  = 5 \bfv_{1} + \bfv_{3}$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 5 --- The Spanning Set Theorem}

    Let $S = \{\bfv_{1}, \dots, \bfv_{p}\}$
    and $H = \Span{\bfv_{1}, \dots, \bfv_{p}}$.
    \begin{enumerate}[<+->]
        \item[a.] If a vector $\bfv_{k}$ in $S$ is the linear combination of other vectors
            in $S$, then $\bfv_{k}$ can be removed so that the remaining vectors still span $H$.
        \item[b.] If $H \ne \{\bfzero\}$, then some subsets of $S$ is a basis of $H$.
    \end{enumerate}
\end{frame}

\subsection{Bases for $\nullspace A$ and $\colspace A$}

\begin{frame}
    \frametitle{Example 3 (4.2) --- Bases of null spaces}

    Finding a \emph{basis} for the null space of the matrix
    \begin{equation*}
        A =
        \left[
            \begin{array}{ccccc}
                -3 & 6 & -1 & 1 & -7 \\
                1 & -2 & 2 & 3 & -1 \\
                2 & -4 & 5 & 8 & -4 \\
            \end{array}
        \right]
    \end{equation*}
    amounts to solve $A \bfx = \bfzero$, which gives
    \begin{equation*}
        \bfx
        =
        \begin{bmatrix}
            2 x_2 + x_4 - 3 x_5 \\
            x_2 \\
            -2 x_4 + 2 x_5 \\
            x_{4} \\
            x_{5} \\
        \end{bmatrix}
        =
        x_{2}
        \begin{bmatrix}
            2 \\ 1 \\ 0 \\ 0 \\ 0
        \end{bmatrix}
        +
        x_{4}
        \begin{bmatrix}
            1 \\ 0 \\ -2 \\ 1 \\ 0
        \end{bmatrix}
        +
        x_{5}
        \begin{bmatrix}
            -3 \\ 0 \\ 2 \\ 0 \\ 1
        \end{bmatrix}
        =
        x_{2} \bfu + x_{4} \bfv + x_{5} \bfw,
    \end{equation*}
    and $\nullspace A = \Span{\bfu, \bfv, \bfw}$.

    \astonished{} Since $\{\bfu, \bfv, \bfw\}$ is linearly independent,
    it is a basis of $\nullspace A$.
\end{frame}

\begin{frame}
    \frametitle{Example 8}

    Let
    \begin{equation*}
        B =
        \begin{bmatrix}
            1 & 4 & 0 &  2 & 0\\
            0 & 0 & 1 & -1 & 0 \\
            0 & 0 & 0 &  0 & 1 \\
            0 & 0 & 0 &  0 & 0 \\
        \end{bmatrix}
    \end{equation*}
    The pivot columns of $B$, $\{\bfb_1, \bfb_3, \bfb_5\}$ form a basis of its column space.
\end{frame}

\begin{frame}
    \frametitle{Example 9}

    Since
    \begin{equation*}
        A =
        \begin{bmatrix}
            1 & 4 & 0 &  2 & -1\\
            3 & 12 & 1 & 5 & 5 \\
            2 & 8 & 1 &  3 & 2 \\
            5 & 20 & 2 &  8 & 8 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & 4 & 0 &  2 & 0\\
            0 & 0 & 1 & -1 & 0 \\
            0 & 0 & 0 &  0 & 1 \\
            0 & 0 & 0 &  0 & 0 \\
        \end{bmatrix}
        =
        B
        ,
    \end{equation*}
    the $\{\bfa_1, \bfa_3, \bfa_5\}$ forms a basis of $\nullspace
    A$,
    just as $\{\bfb_1, \bfb_3, \bfb_5\}$ forms a basis of $\colspace B$.

    \hint{} Note that $A \bfx = \bfzero$ if and only if $B \bfx = \bfzero$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 6 --- Bases of column spaces}

    The pivot columns of a matrix $A$ form a basis for $\colspace A$.
\end{frame}

\subsection{Two views of a basis}

\begin{frame}
    \frametitle{Two views of a basis}

    If $S$ is a basis of $H$,  then it is

    \begin{itemize}[<+->]
        \item the \emph{smallest} spanning set
            --- deleting any vectors in $S$ will make it no longer \emph{span $H$};
        \item the \emph{largest} independent set
            --- adding any vector of $H$ to $S$
            will make it no longer \emph{linearly independent}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example 10}

    \cake{} Which of the following are bases of $\dsR^{3}$?
    \begin{equation*}
        \scA
        =
        \left\{
            \begin{bmatrix}
                1  \\
                0  \\
                0 
            \end{bmatrix}
            ,
            \begin{bmatrix}
                2  \\
                3  \\
                0 
            \end{bmatrix}
        \right\}
        \quad
        \scB
        =
        \left\{
            \begin{bmatrix}
                1  \\
                0  \\
                0 
            \end{bmatrix}
            ,
            \begin{bmatrix}
                2  \\
                3  \\
                0 
            \end{bmatrix}
            ,
            \begin{bmatrix}
                4  \\
                5  \\
                6 
            \end{bmatrix}
        \right\}
        \quad
        \scC
        =
        \left\{
            \begin{bmatrix}
                1  \\
                0  \\
                0 
            \end{bmatrix}
            ,
            \begin{bmatrix}
                2  \\
                3  \\
                0 
            \end{bmatrix}
            ,
            \begin{bmatrix}
                4  \\
                5  \\
                6 
            \end{bmatrix}
            ,
            \begin{bmatrix}
                7  \\
                8  \\
                9 
            \end{bmatrix}
        \right\}
    \end{equation*}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.3}{33, 35, 37, 39, 41, 43}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
