\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Review 01 --- Vector Spaces}

\begin{document}

\begin{frame}
    \frametitle{Let's Play Some Games}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-game.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Rule of the Game}
    
    \begin{itemize}
        \item \emoji{smiley} Form teams of 3 people max.
        \item \emoji{game-die} A random student picks a problem from the list.
        \item \emoji{busts-in-silhouette} Work together with your teammate on the problem.
        \item \emoji{alarm-clock} Stay focused: There's a time limit for each problem.
        \item \emoji{mega} When time's up, a random student shares their team's answer.
        \item \emoji{star} Earn bonus points for your team with correct answers.
        \item \emoji{bulb} After each round, we'll discuss the solution as a class.
    \end{itemize}
\end{frame}

\lectureoutline{}

\section{4.1 Vector Space}

\begin{frame}
    \frametitle{The Problem}

    Which of the followings are vector spaces? Why

    \begin{enumerate}
        \item All polynomials in $\mathbb{P}_{n}$ with $p(0) = 0$.
        \item All polynomials of the form $p(t) = 3 + t^{2}$.
        \item The set of all $2 \times 2$ matrices with determinant equal to 2.
        \item The set of all continuous functions $f: \mathbb{R} \rightarrow \mathbb{R}$.
        \item The set of all even functions $g: \mathbb{R} \rightarrow \mathbb{R}$.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{The Answer}
    
    \begin{enumerate}
        \item Yes
        \item No
        \item No
        \item Yes
        \item Yes
    \end{enumerate}
\end{frame}

\section{4.2 Null Spaces, Column Spaces, and Linear Transformations}

\begin{frame}
    \frametitle{The Question}
    
    It can be shown that
    \begin{equation*}
        \systeme{
            5 x_1 + x_2 - 3 x_3 = 0,
            -9 x_1 + 2 x_2 + 5 x_3 = 1,
            4 x_1 + 1 x_2 - 6 x_{3} = 9
        }
    \end{equation*}
    has a solution.

    Why does this imply that
    \begin{equation*}
        \systeme{
            5 x_1 + x_2 - 3 x_3 = 0,
            -9 x_1 + 2 x_2 + 5 x_3 = -5,
            4 x_1 + 1 x_2 - 6 x_{3} = -45
        }
    \end{equation*}
    also have a solution?

    \hint{} Do not do row operations or try to solve the system.
\end{frame}

\begin{frame}
    \frametitle{The Answer}

    Recall that column spaces are vector spaces.
    
    Thus, if $\begin{bmatrix} 0 \\ 1 \\ 9 \end{bmatrix}$ is a column space,
    then $-5 \begin{bmatrix} 0 \\ 1 \\ 9 \end{bmatrix}$ is in the same column
    space.
\end{frame}

\section{4.3 Linearly Independent Sets; Bases}

\begin{frame}
    \frametitle{The Problem}
    Let
    \begin{equation*}
        A
        =
        \left(
            \begin{array}{ccccc}
                1 & 3 & -5 & 4 & -7 \\
                4 & 1 & -7 & 5 & 0 \\
                0 & 5 & -3 & -7 & -4 \\
                -1 & 0 & 0 & 4 & -5 \\
                3 & 3 & -5 & -6 & 3 \\
            \end{array}
        \right)
    \end{equation*}
    and
    \begin{equation*}
        B
        =
        \left(
            \begin{array}{ccccc}
                1 & 0 & 0 & 0 & 5 \\
                0 & 1 & 0 & 0 & 1 \\
                0 & 0 & 1 & 0 & 3 \\
                0 & 0 & 0 & 1 & 0 \\
                0 & 0 & 0 & 0 & 0 \\
            \end{array}
        \right)
    \end{equation*}
    It can be checked that $A \sim B$. 

    Find are bases of $\colspace A, \nullspace A, \rowspace A$.
\end{frame}

\begin{frame}
    \frametitle{The Answer}
    
    A basis for $\nullspace A$ is
    \begin{equation*}
        \left(
            \begin{array}{c}
                -5 \\
                -1 \\
                -3 \\
                0 \\
                1 \\
            \end{array}
        \right)
    \end{equation*}
\end{frame}

\section{4.4 Coordinate Systems}

\begin{frame}
    \frametitle{The Problem}
    Consider this basis of $\dsR^{3}$ ---
    \begin{equation*}
        \scB
        =
        \left\{
            \begin{bmatrix}
                1  \\
                5  \\
                3  \\
            \end{bmatrix}
            ,
            \begin{bmatrix}
                2 \\
                2 \\
                5 \\
            \end{bmatrix}
            ,
            \begin{bmatrix}
                2 \\
                3 \\
                5 \\
            \end{bmatrix}
        \right\}
    \end{equation*}
    Let
    \begin{equation*}
        \bfx=
        \begin{bmatrix}
            1 \\ 2 \\ 3
        \end{bmatrix}
        .
    \end{equation*}
    What is $[\bfx]_\scB$?
\end{frame}

\begin{frame}
    \frametitle{The Answer}
    We have
    \begin{equation*}
        P_{\scB}
        =
        \left(
            \begin{array}{ccc}
                1 & 2 & 2 \\
                5 & 2 & 3 \\
                3 & 5 & 5 \\
            \end{array}
        \right)
    \end{equation*}
    So
    \begin{equation*}
        [\bfx]_\scB
        =
        (P_{\scB})^{-1} \bfx
        =
        \left(
            \begin{array}{ccc}
                -5 & 0 & 2 \\
                -16 & -1 & 7 \\
                19 & 1 & -8 \\
            \end{array}
        \right)
        \bfx
        =
        \begin{bmatrix}
            1 \\ 3 \\ -3
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The Problem}
    
    Consider the vector space of all polynomials $\dsP$.

    Prove that the dimension of $\dsP$ infinite.

    \hint{} Prove by contradiction.
\end{frame}

\begin{frame}
    \frametitle{The Answer}
    
    Assume that $\dsP$ is of dimension $n$.

    Consider $\dsP_{n}$. This is a subspace of $\dsP$ with dimension $n+1$.
    This contradicts the assumption that $\dim \dsP = n$ by Theorem 12.
    Thus the dimension of $\dsP$ cannot be any finite number.
\end{frame}

\section{4.6 Change of Basis}

\begin{frame}
    \frametitle{The Problem}
    Let
    \begin{equation*}
        \bfb_1
        =
        \left(
            \begin{array}{cc}
                1 \\
                4 \\
            \end{array}
        \right)
        ,
        \qquad
        \bfb_2
        =
        \left(
            \begin{array}{cc}
                -1 \\
                -3 \\
            \end{array}
        \right)
    \end{equation*}
    and
    \begin{equation*}
        \bfc_1
        =
        \left(
            \begin{array}{cc}
                1 \\
                2 \\
            \end{array}
        \right)
        ,
        \qquad
        \bfc_2
        =
        \left(
            \begin{array}{cc}
                1 \\
                3 \\
            \end{array}
        \right)
    \end{equation*}
    Let 
    $\scB = \{\bfb_1, \bfb_2\}$
    and
    $\scC = \{\bfc_1, \bfc_2\}$.

    If
    \begin{equation*}
        [\bfx]_{\scB} = 
        \begin{bmatrix}
            -1 \\ -1
        \end{bmatrix}
    \end{equation*}
    what is
    \begin{equation*}
        [\bfx]_{\scC} = 
        \phantom{
            \begin{bmatrix}
                -3 \\ -5
            \end{bmatrix}
        }
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The Answer}
    
    This is just
    \begin{equation*}
        (P_{\scC})^{-1}P_{\scB} [\bfx]_{\scB}
        =
        \begin{bmatrix} 
            3 \\ -1
        \end{bmatrix} 
    \end{equation*}
\end{frame}

\section{4.7 Digital Signal Processing}

\begin{frame}
    \frametitle{The Problem}

    Consider the set of signals
    \begin{equation*}
        W
        =
        \left\{
        \{
        x_{k}
        \}
        :
        x_{k}
        =
        \begin{cases}
            0 & \text{if } k < 0 \\
            r & \text{if } k \ge 0
        \end{cases}
        ,
        \text{where }
        r \in \dsR
        \right\}
    \end{equation*}
    Prove that $W$ is a subspace of $\dsS$.
\end{frame}

\section{The Big Prize}

\begin{frame}
    \frametitle{The Big Prize}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{wet-wipe.jpg}
        \caption*{The Prize For Winner This Problem}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Problem}

    Given a vector space $V$ and a vector $\mathbf{v} \in V$, 
    prove that there can only be one additive inverse (denoted as $-\mathbf{v}$)
    that satisfies the property $\mathbf{v} + (-\mathbf{v}) = \mathbf{0}$, where
    $\mathbf{0}$ is the zero vector in the vector space.

    \hint{} Use only the 10 axioms a vector space must satisfy.
\end{frame}

\end{document}
