\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Eigenvectors and Eigenvalues}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{5 Eigenvectors and Eigenvalues}

\begin{frame}
    \frametitle{Will \emoji{owl} become extinct?}
    
    An \emoji{owl}'s life has 3 stages --- juvenile, subadult and adult.

    Let $j_k, s_{k}, a_{k}$ denote the number of \emoji{owl} in a forest of each
    of these three stages in the year $k$.

    Data suggests
    \begin{equation}
        \label{eq:difference}
        \begin{bmatrix}
            j_{k+1} \\
            s_{k+1} \\
            a_{k+1} \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            0 & 0 & .33 \\
            .18 & 0 & 0 \\
            0 & .71 & .94 \\
        \end{bmatrix}
        \begin{bmatrix}
            j_{k} \\
            s_{k} \\
            a_{k} \\
        \end{bmatrix}
        \qquad
        \text{for all }
        k \in \{0, 1, \dots\}
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Difference Equations}
    
    Equation \eqref{eq:difference} can be written as $\bfx_{k+1} = A \bfx_{k}$.

    Such a system is called a \alert{difference equation/dynamic system}.

    \emoji{cry} Are the \emoji{owl} going to become extinct?

    To answer this question, we need find what is $\lim_{k\to \infty} \bfx_{k}$.

    \alert{Eigenvectors} and \alert{eigenvalues} will help us.
\end{frame}

\section{5.1 Eigenvectors and Eigenvalues}

\begin{frame}[c]
    \frametitle{What are eigenvectors and eigenvalues according to YouTube}

    \begin{figure}[htpb]
        \centering
        \href{https://youtu.be/Ip3X9LOh2dk}{\includegraphics[width=\textwidth]{YouTube-eigenvectors.png}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    Let
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            3 & -2 \\
            1 & 0
        \end{bmatrix}
        ,
        \quad
        \bfu =
        \begin{bmatrix}
            -1 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfv =
        \begin{bmatrix}
            2 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
    Then
    \begin{equation*}
        A \bfu =
        \begin{bmatrix}
            -5 \\ -1
        \end{bmatrix}
        ,
        \qquad
        A \bfv =
        \begin{bmatrix}
            4 \\ 2
        \end{bmatrix}
        =
        2 \bfv
        .
    \end{equation*}

    \think{} There is something special about $\bfv$!
\end{frame}

\begin{frame}
    \frametitle{Example 1 in a picture}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{eigen-vector-01.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Eigenvectors and eigenvalues}

    An \alert{eigenvector} of an $n \times n$ matrix $A$ 
    is a \emph{nonzero} vector $\bfx$ such that
    \begin{equation*}
        A \bfx = \lambda \bfx
    \end{equation*}
    for some scalar (real number) $\lambda$.

    \pause{}

    A scalar $\lambda$ is an \alert{eigenvalue} of $A$ if
    \begin{equation*}
        A \bfx = \lambda \bfx
    \end{equation*}
    has a non-trivial solution.
    Such an $x$ is an \emph{eigenvector} corresponding to $\lambda$.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    \begin{block}{Eigenvectors and eigenvalues}
        \footnotesize{}
        An \alert{eigenvector} of an $n \times n$ matrix $A$ 
        is a \emph{nonzero} vector $\bfx$ such that
        \begin{equation*}
            A \bfx = \lambda \bfx
        \end{equation*}
        for some scalar (real number) $\lambda$.

        A scalar $\lambda$ is an \alert{eigenvalue} of $A$ if
        \begin{equation*}
            A \bfx = \lambda \bfx
        \end{equation*}
        has a non-trivial solution.
        Such an $x$ is an \emph{eigenvector} corresponding to $\lambda$.
    \end{block}
    
    \bonus{} If $\bfx$ is an eigenvector of $A$, is $2 \bfx$ also an eigenvector of $A$?

    \pause{}

    \bonus{} If $\lambda$ is an eigenvalue of $A$, is $2 \lambda$ also an
    eigenvalue of $A$?

    \pause{}

    \bonus{} Can you think of an $A$ which has $0$ as an eigenvalue?
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Let
    \begin{equation*}
        A =
        \left[
            \begin{array}{cc}
                1 & 6 \\
                5 & 2 \\
            \end{array}
        \right]
        ,
        \qquad
        \bfu =
        \begin{bmatrix}
            6 \\ -5
        \end{bmatrix}
        ,
        \qquad
        \bfv =
        \begin{bmatrix}
            3 \\ -2
        \end{bmatrix}
        .
    \end{equation*}

    Are $\bfu$ and $\bfv$ eigenvectors of $A$?
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let
    \begin{equation*}
        A =
        \left[
            \begin{array}{cc}
                1 & 6 \\
                5 & 2 \\
            \end{array}
        \right]
        .
    \end{equation*}
    Is $7$ an eigenvalue?
\end{frame}

\begin{frame}
    \frametitle{Eigenspace}

    $\lambda$ is an eigenvalue of $A$ if and only if
    \begin{equation}
        \label{eq:eigen}
        (A - \lambda I) \bfx = \bfzero
    \end{equation}
    has a non-trivial (non-zero) solution.

    The solution set $H$ of \eqref{eq:eigen} is the \blankshort{} space of the
    matrix $A -\lambda I$.

    We call $H$ the \alert{eigenspace} of $A$ corresponding to $\lambda$.

    It contains the \blankshort{} vector and \emph{all} eigenvectors of $A$
    corresponding to $\lambda$.
\end{frame}

\begin{frame}
    \frametitle{Eigenspace in picture}

    Eigenspaces for $A$ in Example 3 and $\lambda=7$ and $\lambda=-4$%

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{eigenspace.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Given that $2$ is an eigenvalue of
    \begin{equation*}
        A =
        \begin{bmatrix}
            4 & -1 & 6 \\
            2 &  1 & 6 \\
            2 & -1 & 8 \\
        \end{bmatrix}
        ,
    \end{equation*}
    find a basis of its eigenspace.
\end{frame}

\begin{frame}
    \frametitle{Example 4 in a picture}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{eigenspace-3d.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Consider the following matrix $A$:
    $$
    A = \begin{bmatrix}
        1 & 1 \\
        1 & 1 \\
    \end{bmatrix}
    $$

    The eigenvalues of $A$ are the $\lambda$'s which satisfy
    $$
    \det(A - \lambda I) =0
    $$

    \bonus{} What are the eigenvalues of $A$?
\end{frame}

\begin{frame}
    \frametitle{When $0$ is an eigenvalue}

    $0$ is an eigenvalue of $A$ if and only if
    \begin{equation*}
        A \bfx = 0 \bfx = \bfzero
    \end{equation*}
    has a nontrivial solution.

    In other words, this happens when $A$ is \blankshort{}.

    \pause{}

    \begin{block}{The Invertible Matrix Theorem continued (5.2)}
        Let $A$ be an $n \times n$ matrix.
        Then $A$ is invertible if and only if:
        \begin{enumerate}
            \item[r.] The number $0$ is not an eigenvalue of $A$.
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Theorem 1 --- Eigenvalues of triangular matrices}

    The eigenvalues of a triangular matrix are on its main diagonal.
\end{frame}

%\begin{frame}
%    \frametitle{A little bit logic}
%    Given a statement $P$, we use $\neg P$ to denote its negation.
%
%    \pause{}
%
%    To show $P \imp Q$, we can instead show $\neg Q \imp \neg P$.
%
%    The latter is called the \alert{contrapositive} of the former.
%
%    The two statements are logically equivalent.
%
%    \pause{}
%
%    \begin{example}
%        To show that 
%        \begin{itemize}
%            \item If you are a \emoji{cat-face}, then you catch \emoji{mouse}
%        \end{itemize}
%        we can instead prove its \emph{contrapositive}
%        \begin{itemize}
%            \item If you do not \blankshort{}, then you are not \blankshort{}.
%        \end{itemize}
%    \end{example}
%\end{frame}

\begin{frame}
    \frametitle{Theorem 2 --- Linear independence}

    If $\bfv_1, \dots, \bfv_r$ are eigenvectors corresponding to
    \emph{distinct} eigenvalues $\lambda_1,\dots, \lambda_r$,
    then they are linearly independent.

    \only<1>{
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.7\linewidth]{eigenspace.png}
            \caption*{Example 3}
        \end{figure}
    }
    \only<2>{Proof by contradiction
    }
\end{frame}

\subsection{Difference Equations and Eigenvalues}

\begin{frame}
    \frametitle{Difference Equations and Eigenvalues}
    
    Consider the \alert{difference equation}
    \begin{equation*}
        \bfx_{x+1} = A \bfx_{k}, \qquad (k = 0, 1, \dots).
    \end{equation*}

    The simplest solution for this is to take an
    eigenvector $\bfx_{0}$ and its corresponding eigenvalue $\lambda$,
    and let
    \begin{equation*}
        \bfx_{k} = \lambda^{k} \bfx_{0}.
    \end{equation*}

    \pause{}

    \hint{} Any linear combinations of such solutions are also solutions.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \bonus{}\bonus{} If $A^2$ is the zero matrix, what are the eigenvalues of $A$?

    \hint{} Recall that $\lambda$ is an eigenvalue of $A$ if
    \begin{equation*}
        A \bfx = \lambda \bfx
    \end{equation*}
    for some $\bfx \ne \bfzero$.
\end{frame}

\begin{frame}
    \frametitle{Will \emoji{owl} become extinct?}
    
    Recall that in the \emoji{owl} population model
    \begin{equation}
        \label{eq:difference:1}
        \begin{bmatrix}
            j_{k+1} \\
            s_{k+1} \\
            a_{k+1} \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            0 & 0 & .33 \\
            .18 & 0 & 0 \\
            0 & .71 & .94 \\
        \end{bmatrix}
        \begin{bmatrix}
            j_{k} \\
            s_{k} \\
            a_{k} \\
        \end{bmatrix}
    \end{equation}
    One of the eigenvectors of the matrix in \eqref{eq:difference:1} is
    \begin{equation*}
        \bfx_{0} = 
        \left[
            \begin{array}{c}
                0.31754\\
                0.05811\\
                0.94646\\
            \end{array}
        \right]
    \end{equation*}
    corresponding to the eigenvalues $\lambda = 0.98359$

    \emoji{cake} What would happen to the \emoji{owl}
    their initial population is $1000 \bfx_{0}$?
\end{frame}
\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{5.1}{33, 35, 37, 39, 41, 43}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
