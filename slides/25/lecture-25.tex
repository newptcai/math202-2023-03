\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Inner Product, Length, and Orthogonality}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{6 Orthogonality and Least Squares}

\begin{frame}
    \frametitle{Grades and procrastination?}

    Can we find linear model relating a student's average grade
    and their numbers of delays of presentations?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.8\textwidth]{grade-procrastinateion-regression-1.pdf}
        \includegraphics<2>[width=0.8\textwidth]{grade-procrastinateion-regression-2.pdf}
    \end{figure}
\end{frame}

\section{6.1 Inner Product, Length, and Orthogonality}

\subsection{Inner Product}

\begin{frame}
    \frametitle{The inner product}

    If $\bfu, \bfv \in \dsR^{n}$, then the \alert{inner/dot product} of them is
    $\bfu^{T} \bfv$. 

    In other words
    \begin{equation*}
        \bfu \cdot \bfv
        =
        \begin{bmatrix}
            u_1 & \dots & u_n
        \end{bmatrix}
        \begin{bmatrix}
            v_1 \\ \vdots \\ v_n
        \end{bmatrix}
        =
        u_1 v_1 + u_2 v_2 + \cdots + u_n v_n
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    Let
    \begin{equation*}
        \bfu
        =
        \begin{bmatrix}
            2 \\ -5 \\ -1
        \end{bmatrix}
        ,
        \quad
        \bfv
        =
        \begin{bmatrix}
            3 \\ 2 \\ -3
        \end{bmatrix}
        .
    \end{equation*}
    \cake{} What are $\bfu \cdot \bfv$ and $\bfv \cdot \bfu$?
\end{frame}


\begin{frame}
    \frametitle{Theorem 1 --- Properties of inner product}

    Let $\bfu, \bfv, \bfw \in \dsR^{n}$ and $c \in \dsR$.
    Then
    \vspace{-.5em}
    \begin{enumerate}[a.]
        \item $\bfu \cdot \bfv = \bfv \cdot \bfu$.
        \item $(\bfu+\bfv) \cdot \bfw = \bfu \cdot \bfw + \bfv \cdot \bfw$.
        \item $(c \bfu) \cdot \bfv = c (\bfu \cdot \bfv) = \bfu \cdot (c \bfv)$.
        \item $\bfu \cdot \bfu \ge 0$, and $\bfu \cdot \bfu = 0$ if and only if
            $\bfu = \bfzero$.
    \end{enumerate}

    \emoji{eye} The proof follows from properties of the transpose operation in section 2.1.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    \begin{block}{Theorem 3 (2.1)}
        \begin{enumerate}[a.]
            \item[b.] $(A + B)^T = A^T + B^T$
            \item[c.] For any scalar $r$ , $r(A^T) = (r A)^T$
        \end{enumerate}
    \end{block}
    How to use the above theorem to show that
    \begin{enumerate}
        \item[b.] $(\bfu+\bfv) \cdot \bfw = \bfu \cdot \bfw + \bfv \cdot \bfw$.
        \item[c.] $(c \bfu) \cdot \bfv = c (\bfu \cdot \bfv)$.
    \end{enumerate}
\end{frame}

\subsection{The Length of a Vector}

\begin{frame}
    \frametitle{Length of vectors in $\dsR^{2}$}
    
    \cake{} If $\bfv = \begin{bmatrix} a \\ b \end{bmatrix}$, how should we define
    its length?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{length-in-r2.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Length of vectors in $\dsR^{n}$}

    The \alert{length/norm} of a vector $\bfv \in \dsR^{n}$ is defined by
    \begin{equation*}
        \norm{\bfv}
        =
        \sqrt{\bfv \cdot \bfv}
        .
    \end{equation*}

    \cake{} If we write $\bfv = \begin{bmatrix} v_1 \\ \vdots \\ v_n
    \end{bmatrix}$, then we have $\norm{\bfv} = $

    \pause{}

    \cake{} Why do we have
    \begin{equation*}
        \norm{c \bfv} = \abs{c} \, \norm{\bfv}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Unit vector}

    A vector whose length is $1$ is called a \alert{unit vector}.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=1.5, 
            every label/.style={black, fill=white, inner sep=1pt}
            ]
            \fill[white] (-1.5, -1.5) rectangle (1.5, 1.5);

            % Grid
            \draw[step=1, lightgray, very thin] (-1.5, -1.5) grid (1.5, 1.5);

            % Unit circle
            \draw[lightgray] (0, 0) circle (1);

            % Axes
            \draw[thick, ->] (-1.5, 0) -- (1.5, 0) node[right] {$x_1$};
            \draw[thick, ->] (0, -1.5) -- (0, 1.5) node[above] {$x_2$};

            % Random unit vectors
            \foreach \angle in {40, 160, 280}
            \drawvector{\angle:1}{$\mathbf{u}_{\angle}$};

            % Vectors (0, 1) and (1, 0)
            \drawvector{1, 0}{$e_1$};
            \drawvector{0, 1}{$e_2$};
        \end{tikzpicture}
        \caption*{Unit vectors in $\dsR^{2}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Normalizing}
    
    For any nonzero vector $\bfv$, the vector $\bfu = \norm{\bfv}^{-1} \bfv$ is
    a \emph{unit vector} in the same direction as $\bfv$.

    This computation is called \alert{normalizing} $\bfv$.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.8, 
            every node/.style={black, fill=white, inner sep=1pt},
            ]
            \fill[white] (-1.5, -1.5) rectangle (4.5, 4.5);

            % Grid
            \draw[step=1, lightgray, very thin] (-1.5, -1.5) grid (4.5, 4.5);

            % Unit circle
            \draw[lightgray] (0, 0) circle (1);

            % Axes
            \draw[thick, ->] (-1.5, 0) -- (4.5, 0) node[right] {$x_1$};
            \draw[thick, ->] (0, -1.5) -- (0, 4.5) node[above] {$x_2$};

            % Vector v
            \draw[vector] (0, 0) -- (3, 4) node[above] {$\mathbf{v}$};

            % Vector u
            \pgfmathsetmacro{\len}{sqrt(3*3 + 4*4)}
            \pgfmathsetmacro{\ux}{3/\len}
            \pgfmathsetmacro{\uy}{4/\len}
            \draw[vector] (0, 0) -- (\ux, \uy) node[anchor=south] {$\mathbf{u}$};
        \end{tikzpicture}
        \caption*{Normalizing $\bfv$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    \cake{} Let $\bfv = (1, -2, 2, 0)$. Find a unit vector $\bfu$ in the direction of
    $\bfv$.
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let $W$ be the subspace of $\dsR^2$ spanned by $\bfx = (2/3, 1)$.
    Find a unit vector $\bfz$ that is a basis for $W$.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.8, 
            every node/.style={black, fill=white, inner sep=1pt},
            ]
            \fill[white] (-1.5, -1.5) rectangle (4, 5);

            % Grid
            \draw[step=1, lightgray, very thin] (-1.5, -1.5) grid (3, 4);

            % Unit circle
            \draw[lightgray] (0, 0) circle (1);

            % Axes
            \draw[thick, ->] (-1.5, 0) -- (3.5, 0) node[right] {$x_1$};
            \draw[thick, ->] (0, -1.5) -- (0, 4.5) node[above] {$x_2$};

            % Vector W
            \pgfmathsetmacro{\wx}{3 * (2/3)}
            \pgfmathsetmacro{\wy}{3 * 1}
            \pgfmathsetmacro{\wxx}{-1 * (2/3)}
            \pgfmathsetmacro{\wyy}{-1 * 1}
            \draw[thin, red] (\wxx, \wyy) -- (\wx, \wy) node[anchor=west] {$\mathbf{W}$};

            % Vector x
            \draw[vector] (0, 0) -- (2/3, 1) node[anchor=south] {$\mathbf{x}$};
        \end{tikzpicture}
        \caption*{What is a basis of unit length for $W$?}
    \end{figure}
\end{frame}

\subsection{Distance in \texorpdfstring{$\dsR^{n}$}{R^n}}

\begin{frame}
    \frametitle{Distance in $\dsR^{1}$}

    For two real numbers $a$ and $b$, the distance between them is $\abs{a-b}$.

    \think{} Can we compute this using vectors?
\end{frame}

\begin{frame}
    \frametitle{Distance in $\dsR^{2}$}
    For $\bfu, \bfv \in \dsR^{2}$, how should we define \alert{distance between
    $\bfu$ and $\bfv$}?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.7, 
            every node/.style={black, fill=white, inner sep=1pt},
            ]
            \fill[white] (-1.5, -0.5) rectangle (6, 8);

            % Grid
            \draw[step=1, lightgray, very thin] (-1.5, -0.5) grid (5, 7);

            % Axes
            \draw[thick, ->] (-1.5, 0) -- (5, 0) node[right] {$x_1$};
            \draw[thick, ->] (0, -0.5) -- (0, 7) node[above] {$x_2$};

            % Vector v
            \draw[vector] (0, 0) -- (4, 6) node[anchor=west] {$\mathbf{v}$};

            % Vector u
            \draw[vector] (0, 0) -- (-1, 2) node[anchor=south] {$\mathbf{u}$};
        \end{tikzpicture}
        \caption*{What is the distance between $\bfu$ and $\bfv$?}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Distance in $\dsR^{n}$}
    For $\bfu, \bfv \in \dsR^{n}$, the \alert{distance between $\bfu$ and $\bfv$} is
    defined by
    \begin{equation*}
        \dist(\bfu, \bfv) = \norm{\bfu - \bfv}.
    \end{equation*}

    \cake{} If we write 
    $\bfu = \begin{bmatrix} u_1 \\ \vdots \\ u_n \end{bmatrix}$, 
    and
    $\bfv = \begin{bmatrix} v_1 \\ \vdots \\ v_n \end{bmatrix}$
    then we have
    \begin{flalign*}
        \dist(\bfu, \bfv) = &&
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    What is the distance between $\bfu = (7, 1)$ and $\bfv = (3,2)$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{distance-in-rn.png}
    \end{figure}
\end{frame}

\subsection{Orthogonal Vectors}

\begin{frame}
    \frametitle{Orthogonal vectors in $\dsR^{2}$}
    
    In $\dsR^{2}$, the lines determined by $\bfu$ and $\bfv$ are
    \alert{orthogonal} (perpendicular)
    if and only if
    \begin{equation*}
        \dist(\bfu, \bfv) = \dist(\bfu, -\bfv)
        \iff
        \bfu \cdot \bfv
        =
        0
        .
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{orthognal.png}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Orthogonal vectors in $\dsR^{n}$}

    In $\dsR^{n}$, we say two vectors $\bfu$ and $\bfv$ are \alert{orthogonal} 
    if $\bfu \cdot \bfv = 0$.

    \cake{} Which vector is orthogonal to all vectors?

    \cake{} Which vector is orthogonal to itself?
\end{frame}

\begin{frame}
    \frametitle{Theorem 2 --- The Pythagorean Theorem}

    Two vectors $\bfu$ and $\bfv$ are orthogonal if and only if
    \begin{equation*}
        \norm{\bfu+\bfv}^{2}
        =
        \norm{\bfu}^{2}
        +
        \norm{\bfv}^{2}
    \end{equation*}

    \begin{figure}
        \begin{center}
            \includegraphics[width=0.5\linewidth]{pythagoras.png}
        \end{center}
    \end{figure}
\end{frame}

\subsection{Orthogonal Complements}

\begin{frame}
    \frametitle{Orthogonal to a subspace}

    If $\bfz$ is orthogonal to \blankshort{} in subspace $W$ of $\dsR^{n}$,
    then we say that $\bfz$ is \alert{orthogonal} to $W$.

    \begin{figure}
    \begin{center}
        \includegraphics[width=0.5\linewidth]{orthognal-complement.png}
    \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Orthogonal complements}

    The sets of all vectors which are orthogonal to $W$ is called its
    \alert{orthogonal complement},
    denoted by $W^{\perp}$

    \cake{} What is $W^{\perp}$ in the picture?

    \begin{figure}
    \begin{center}
        \includegraphics[width=0.5\linewidth]{orthognal-complement.png}
    \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Show that if $\bfx$ is in both $W$ and $W^{\perp}$, then $\bfx = \bfzero$.
\end{frame}

\begin{frame}
    \frametitle{Properties of orthogonal complements}

    \begin{enumerate}
        \item A vector $\bfx$ is in $W^{\perp}$ if and only if $\bfx$ is orthogonal to every vector in a set that spans $W$.
        \item $W^{\perp}$ is a subspace of $\dsR^n$.
    \end{enumerate}

    \begin{figure}
    \begin{center}
        \includegraphics[width=0.4\linewidth]{orthognal-complement.png}
    \end{center}
    \end{figure}
    
    \emoji{weight-lifting} The proof is left as exercise.
\end{frame}

\begin{frame}
    \frametitle{Example 7 (4.5) --- The big picture of linear algebra}

    Let 
    $A = 
    \begin{bmatrix} 
        3 & 0 & -1 \\
        3 & 0 & -1 \\
        4 & 0 & 5
    \end{bmatrix}
    $
    .
    Note that
    $A 
    \sim
    \begin{bmatrix} 
        1 & 0 & 0\\
        0 & 0 & 1 \\
        0 & 0 & 0 \\
    \end{bmatrix}
    $
    .

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{4.5-fig-2.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 3 --- $\nullspace A$, $\rowspace A$, $\colspace A$}

    Let $A$ be an $m \times n$ matrix.
    Then
    \begin{equation*}
        (\rowspace A)^{\perp} = \nullspace A
        ,
        \qquad
        (\colspace A)^{\perp} = \nullspace A^{T}
        .
    \end{equation*}

    \pause{}

    \begin{figure}
        \begin{center}
            \includegraphics[width=0.7\linewidth]{row-col-null.png}
        \end{center}
    \end{figure}
\end{frame}

\section{Angles in \texorpdfstring{$\dsR^2$}{R^2} and \texorpdfstring{$\dsR^3$}{R^3}}

\begin{frame}
    \frametitle{Angles}

    If $\bfu$ and $\bfv$ are in either $\dsR^2$ or $\dsR^3$, then
    \begin{equation*}
        \bfu \cdot \bfv
        =
        \norm{\bfu} \norm{\bfv} \cos \theta
        ,
    \end{equation*}
    where $\theta$ is the angel between the two.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{6.1-09.png}
    \end{figure}

    Proof --- By \emph{the law of cosine}
    \begin{equation*}
        \norm{\bfu-\bfv}^2
        =
        \norm{\bfu}^2
        +
        \norm{\bfv}^2
        -
        2
        \norm{\bfu}
        \norm{\bfv}
        \cos(\theta)
    \end{equation*}
    Thus, \dots
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{6.1}{29, 33, 35, 37}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
