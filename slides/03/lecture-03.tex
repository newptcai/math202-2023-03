\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Matrix Equations, Solution Sets}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{1.4 The Matrix Equations $A \bfx = \bfb$}

\subsection{Matrix Equations}

\begin{frame}
    \frametitle{Example 1}

    \begin{equation*}
        \begin{bmatrix}
            1 & 2 & -1 \\
            0 & -5 & 3 \\
        \end{bmatrix}
        \begin{bmatrix}
            4 \\ 3 \\ 7
        \end{bmatrix}
        =
        \begin{bmatrix}
            3 \\ 6
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}{Matrix vector product}
    If $A$ is a $m \times n$ matrix, with columns
    $\veclist[n]{a}$, and $\bfx \in \dsR^n$, then
    the \alert{product of $A$ and $\bfx$} is defined by
    \begin{equation*}
        A \bfx =
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \cdots & \bfa_n
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ \vdots \\ x_n
        \end{bmatrix}
        =
        x_1 \bfa_1
        +
        x_2 \bfa_2
        +
        \dots
        +
        x_n \bfa_n
        .
    \end{equation*}

    \cake{} In other words, $A \bfx$ is the \blankshort{}
    of $\veclist[n]{a}$ with \blankveryshort{} 
    $x_1, \dots, x_n$.
\end{frame}

\begin{frame}
    \frametitle{A simple example}

    What is
    \begin{flalign*}
        &
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 1 & 0 \\
            0 & 0 & 1
        \end{bmatrix}
        \begin{bmatrix}
            r \\ s \\ t
        \end{bmatrix}
        &
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Identity matrix}

    A matrix of the following form is called the \alert{identity matrix}
    \begin{equation*}
        I_n = \begin{bmatrix}
            1 & 0 & \cdots & 0 \\
            0 & 1 & \cdots & 0 \\
            \vdots & \vdots & \ddots & \vdots \\
            0 & 0 & \cdots & 1 \\
        \end{bmatrix}
    \end{equation*}
    \cake{} Can you show that $I_n \bfx = \bfx$ for any $\bfx \in \dsR^{n}$?
\end{frame}

\begin{frame}
    \frametitle{Linear system as matrix equations}

    Consider the matrix equation
    \begin{equation*}
        \begin{bmatrix}
            1 & 2 & -1 \\
            0 & -5 & 3 \\
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ x_2 \\ x_3
        \end{bmatrix}
        =
        \begin{bmatrix}
            4 \\ 1
        \end{bmatrix}
    \end{equation*}
    What is the equivalent linear system?
\end{frame}

\begin{frame}
    \frametitle{Theorem 3 --- Three ways to write linear systems}

    If $A = [\bfa_1 \, \dots \, \bfa_n]$ is a $m \times n$ matrix, 
    and $\bfb \in \dsR^m$, 
    then
    the \emph{matrix equation}
    \begin{equation*}
        A \bfx = \bfb
    \end{equation*}
    has the same solution set as the \emph{vector equation}
    \begin{equation*}
        x_1 \bfa_1
        +
        x_2 \bfa_2
        +
        \dots
        +
        x_n \bfa_n
        =
        \bfb
    \end{equation*}
    and the \emph{linear system} with the \emph{augmented matrix}
    \begin{equation*}
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \cdots & \bfa_n & \bfb
        \end{bmatrix}
    \end{equation*}
\end{frame}

\subsection{Existence of Solutions}

\begin{frame}
    \frametitle{Example 3}

    Is the following equation \emph{consistent} for \emph{all} $b_1, b_2, b_3$?
    \begin{equation*}
        \begin{bmatrix}
            1 & 3 & 4 \\
            -4 & 2 & -6 \\
            -3 & -2 & -7 \\
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ x_2 \\ x_3
        \end{bmatrix}
        =
        \begin{bmatrix}
            b_1 \\ b_2 \\ b_3
        \end{bmatrix}
    \end{equation*}

    \cake{}
    This is asking if the following liner system is \emph{always} consistent.
    Is it?

    \begin{equation*}
        \begin{bmatrix}
            1 & 3 & 4 & b_1 \\
            -4 & 2 & -6 & b_2 \\
            -3 & -2 & -7 & b_3 \\
        \end{bmatrix}
        \sim
        \begin{bmatrix}
            1 & 3 & 4 & b_1 \\
            0 & 14 & 10 & 4 b_{1} + b_{2} \\
            0 & 0 & 0 & 14 \cdot b_1 - 7 \cdot b_2 + 14 \cdot b_3 \\
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 4 --- When is a linear system consistent}

    If $A = \begin{bmatrix} \bfa_1 & \dots & \bfa_n \end{bmatrix}$ 
    is a $m \times n$ matrix, 
    and $\bfx \in \dsR^n$, 
    then the followings are equivalent
    \begin{enumerate}[<+->]
        \item[a.] For each $\bfb \in \dsR^{m}$, the equation $A \bfx = \bfb$ has a solution, i.e., it is
            \emph{consistent}.
        \item[b.] For each $\bfb \in \dsR^{m}$, $\bfb$ is a \emph{linear combination} of $\bfa_1, \dots, \bfa_n$.
        \item[c.] For each $\bfb \in \dsR^{m}$, $\bfb$ is in $\Span{\bfa_1, \dots, \bfa_n}$.
        \item[d.] The \emph{echelon} form of $A$ has a pivot position in every row.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Theorem 5 --- Properties of matrix-vector product}

    If $A$ is $m \times n$ matrix, $\bfu, \bfv \in \dsR^n$, $c \in \dsR$, then
    \begin{enumerate}[a.]
        \item $A (\bfu + \bfv) = A \bfu + A \bfv$
        \item $A (c \bfu) = c A \bfu$
    \end{enumerate}

    \bomb{} You may be asked to prove such statements.
\end{frame}

%\begin{frame}[c]
%    \frametitle{Think, Pair and Share}
%
%    In the next few minutes \clock{}, you will be given an exercise.
%    Then you will
%
%    \pause{}
%
%    \begin{itemize}[<+->]
%        \item Think -- Work on the problem by yourself.
%        \item Pair -- Compare your solution with another student sitting besides you.
%        \item Share -- A student will be chosen to present the solution to the class.
%    \end{itemize}
%
%    \onslide<+->{\medal{} If you represent a correct answer, you get 0.1 bonus point for your
%    final grade.}
%\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Is it possible to find $\bfa_1, \bfa_2, \bfa_3 \in \dsR^{4}$
    such that every $[\bfa_1 \bfa_2 \bfa_3 ] \bfx = \bfb$ always have a
    solution?

    \hint{} What is the maximum number of pivot variables can we have in the
    matrix
    \begin{equation*}
        \begin{bmatrix}
            \bfa_1 & \bfa_2 & \bfa_3
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Section Summary}
    
    In this section, we have learned

    \begin{itemize}
        \item[\emoji{multiply}] 
            What is $A \bfx$?
        \item[\consistent] 
            When does $A \bfx = \bfb$ have a solution for every $\bfb \in \dsR^{n}$?
    \end{itemize}
\end{frame}

\section{1.5 Solution Sets of Linear Systems}

\subsection{Homogeneous Linear Systems}

\begin{frame}
    \frametitle{A trivial example}
    \cake{} Can you think of one solution of
    \begin{equation*}
        \systeme{3 x_1 + 5 x_2 - 4 x_3 = 0,
            -3 x_1 -2 x_2 + 4 x_3 = 0,
            6 x_1 + x_2 - 8 x_3 = 0}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Homogeneous linear systems}

    A linear system of the form $A \bfx = \bfzero$ is called \alert{homogeneous}.

    A linear system of the form $A \bfx = \bfb$  where $\bfb \ne \bfzero$ is
    called \alert{nonhomogeneous}.

    A \emph{homogeneous} system \emph{always} has a \emph{trivial} solution $\bfzero$.

    Thus, it is \emph{always} consistent.

    What about \emph{non-trivial (non-$\bfzero$)} solutions?
\end{frame}

\begin{frame}
    \frametitle{Non-trivial solutions}

    By the following,
    a \emph{homogeneous} linear system has nontrivial solutions
    if and only if \blankshort{}.

    \begin{block}{Theorem 2 (1.2)}
        A \emph{consistent} linear system has
        \begin{itemize}
            \item[\emoji{infinity}] infinite solutions if it has \emph{at least one} free variables, or
            \item[\emoji{unicorn}] a unique solution if it has \emph{no} free variables.
        \end{itemize}
    \end{block}

\end{frame}

%\begin{frame}
%    \frametitle{Example 1}
%    Solve the following homogeneous system
%    \begin{equation*}
%        \systeme{3 x_1 + 5 x_2 - 4 x_3 = 0,
%            -3 x_1 -2 x_2 + 4 x_3 = 0,
%            6 x_1 + x_2 - 8 x_3 = 0}
%    \end{equation*}
%    using
%    \begin{equation*}
%        \begin{bmatrix}
%            3 & 5 & -4 & 0 \\
%            -3 & -2 & 4 & 0 \\
%            6 & 1 & -8 & 0\\
%        \end{bmatrix}
%        \sim
%        \begin{bmatrix}
%            1 & 0 & -\frac{4}{3} & 0 \\
%            0 & 1 & 0 & 0 \\
%            0 & 0 & 0 & 0 \\
%        \end{bmatrix}
%    \end{equation*}
%\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Show that the homogeneous system
    \begin{equation*}
        10 x_1 - 3 x_2 - 2 x_3 = 0
    \end{equation*}
    has a solution set
    \begin{equation*}
        \bfx =
        x_{2}
        \begin{bmatrix}
            0.3 \\ 1 \\ 0
        \end{bmatrix}
        +
        x_{3}
        \begin{bmatrix}
            0.2 \\ 0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Parametric vector form}

    The solution set of Example 2 can be written as
    \begin{equation*}
        \bfx = s \bfu + t \bfv,
        \qquad
        \text{where }
        s, t \in \dsR,
        \quad
        \bfu =
        \begin{bmatrix}
            0.3 \\ 1 \\ 0
        \end{bmatrix}
        ,
        \quad
        \bfv =
        \begin{bmatrix}
            0.2 \\ 0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}
    This is called a solution in \alert{parametric vector form}.

    In other words, the solution set is 
    the \blankshort{} of $\bfu$ and $\bfv$.
\end{frame}

\begin{frame}
    \frametitle{Why parametric vector form?}
    
    \cake{} If $\bfu$ and $\bfv$ are both solutions of $A \bfx = \bfzero$,
    then why is any linear combination of the two also a solution of $A \bfx =
    \bfzero$?
\end{frame}

\begin{frame}
    \frametitle{Solution sets}

    The solution set of a homogeneous system is of the form
    $\Span{\bfv_{1}, \dots, \bfv_{p}}$.
    This is called the \emph{parametric vector form} of a solution set.

    \hint{} We will give a more formal explanation later.
\end{frame}

\subsection{Nonhomogeneous Linear Systems}

\begin{frame}
    \frametitle{Example 3}\label{example:3}
    Solving the \emph{nonhomogeneous} equation $A \bfx = \bfb$ with
    \begin{equation}
        A =
        \begin{bmatrix}
            3 & 5 & -4 \\
            -3 & -2 & 4 \\
            6 & 1 & -8 \\
        \end{bmatrix}
        ,
        \qquad
        b
        =
        \begin{bmatrix}
            7 \\ -1 \\ -4
        \end{bmatrix}
    \end{equation}
    using
    \begin{equation*}
        \begin{bmatrix}
            A & \bfb
        \end{bmatrix}
        =
        \begin{bmatrix}
            3 & 5 & -4 & 7\\
            -3 & -2 & 4 & -1  \\
            6 & 1 & -8  & -4 \\
        \end{bmatrix}
        \sim
        \left[
            \begin{array}{cccc}
                1 & 0 & \frac{-4}{3} & -1 \\
                0 & 1 & 0 & 2 \\
                0 & 0 & 0 & 0 \\
            \end{array}
        \right]
    \end{equation*}
    we get
    \begin{equation*}
        \bfx = 
        \begin{bmatrix} 
            -1 \\ 2 \\ 0
        \end{bmatrix} 
        +
        x_{3}
        \begin{bmatrix} 
            \frac{4}{3} \\ 0 \\ 1
        \end{bmatrix} 
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 3 --- parametric vector form}

    In other words, the solution set is
    \begin{equation*}
        \bfx = \bfp + t \bfv,
        \qquad
        \text{where }
        \bfp =
        \begin{bmatrix}
            -1  \\ 2 \\ 0
        \end{bmatrix}
        ,
        \quad
        \bfv =
        \begin{bmatrix}
            \frac{4}{3}  \\ 0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}

    \hint{} Note that
    \begin{enumerate}
        \item $t \bfv$ is the solution set of $A \bfx = \bfzero$;
        \item $A \bfp = \bfb$, i.e., $\bfp$ is \emph{one} solution of
            \hyperref[example:3]{Example 3}.
    \end{enumerate}
\end{frame}

%\begin{frame}
%    \frametitle{Geometric Interpretation}
%
%    The solution can be seen as \emph{the line through p parallel to v}.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{nonhomogeneous-solutions.png}
%        \caption*{The solution sets of $A \bfx = \bfzero$ and $A \bfx = \bfb$}%
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Theorem 6 -- Solution sets}
    The solution set of a \emph{nonhomogeneous} system $A \bfx = \bfb$
    is of the form $\bfp + t \bfv_{h}$,
    where $\bfp$ is one particular solution,
    and $\bfv_{h}$ is any solution of $A \bfx = \bfzero$.

    \smiling{} The proof is left as an exercise (Exercise 37).

    \cake{} If we cannot find $p$, what is the solution set?
\end{frame}

\begin{frame}
    \frametitle{Geometric perspective}

    Consider two eqautions
    \begin{equation*}
        \begin{bmatrix}
            1 & -2 \\
            0 & 0 \\
        \end{bmatrix}
        \begin{bmatrix} 
            x_1 \\
            x_2
        \end{bmatrix} 
        =
        \begin{bmatrix} 
            0 \\
            0
        \end{bmatrix} 
        \qquad
        \begin{bmatrix}
            1 & -2 \\
            0 & 0 \\
        \end{bmatrix}
       \begin{bmatrix} 
            x_1 \\
            x_2
        \end{bmatrix} 
        =
        \begin{bmatrix} 
            1 \\
            0
        \end{bmatrix} 
    \end{equation*}
    
    \begin{figure}[htpb]
        \begin{tikzpicture}[
            scale=1,
            every node/.style={black},
            ]
            % Set background color
            \fill[white] (-1.5,-1.5) rectangle (5,3);
            % Draw x and y axes
            \draw[thick,gray!40] (-1,-1) grid (4,2);
            \draw (-0.5,0) -- (4,0) node[right]{$x_1$};
            \draw (0,-0.5) -- (0,2) node[above]{$x_2$};
            \draw[domain=-0.5:3.5,smooth,variable=\x,black,ultra thick] plot ({\x},{\x/2});
            \draw[domain=-0.5:3.5,smooth,variable=\x,red,ultra thick] plot ({\x},{\x/2-1/2});
            \drawvector{1,0}{}
            \drawvector{0,-1/2}{}
            \drawvector{3,1}{}
        \end{tikzpicture}
    \end{figure}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{1.4}{19, 41, 45}
            \sectionhomework{1.5}{37, 39, 51}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
