%! TEX program = lualatex
%&luaheader

%-----------------------------------------------------------------------------------------
% Theme
%-----------------------------------------------------------------------------------------
\usetheme[progressbar=frametitle, block=fill, titleformat=smallcaps]{metropolis}
\metroset{subsectionpage=progressbar}

%-----------------------------------------------------------------------------------------
% Colours
%-----------------------------------------------------------------------------------------
% No need to define colour by yourself. All colours have been loaded by xcolor.
% https://tex.stackexchange.com/a/74807/8297
\definecolor{juliaorange}{HTML}{FFA500}
\definecolor{mayablue}{RGB}{82, 175, 230}
\definecolor{babyblue}{RGB}{183, 213, 226}

%-----------------------------------------------------------------------------------------
% Standard packages
%-----------------------------------------------------------------------------------------
% This can be omit -- https://tex.stackexchange.com/a/370279/8297
%\usepackage[utf8]{inputenc}
%\usepackage[T1]{fontenc}

\usepackage{amsmath,amsfonts,latexsym, xspace,amsthm,graphicx, amssymb}

% set path to images
\graphicspath{{../images/application}{../images/linear-algebra}{../images/people}{../images/misc}{../images/exercises}}

\usepackage{enumerate}

\usepackage{mathtools}

\usepackage{cleveref}

%-----------------------------------------------------------------------------------------
% Colour boxes
%-----------------------------------------------------------------------------------------
\usepackage{tcolorbox}
% Set default options for tcolorbox
\tcbset{
    colback=white,
    colframe=black,
    left=5pt,
    right=5pt,
    top=5pt,
    bottom=3pt
}

%-----------------------------------------------------------------------------------------
% Links and references
%-----------------------------------------------------------------------------------------
% No need to load hyperref because beamer loads it.
% https://tex.stackexchange.com/a/264001/8297

%-----------------------------------------------------------------------------------------
% Captions
%-----------------------------------------------------------------------------------------
\usepackage[font={scriptsize},justification=centering]{caption}
\usepackage{subcaption}

% reduce space after caption
\captionsetup{belowskip=0pt}

%-----------------------------------------------------------------------------------------
% Matrix
%-----------------------------------------------------------------------------------------
% Increase matrix vertical spacing
% \renewcommand*{\arraystretch}{1.3}

%-----------------------------------------------------------------------------------------
% Figure
%-----------------------------------------------------------------------------------------
\newcommand{\figchar}[1]{%
  \begingroup\normalfont
  \includegraphics[height=\fontcharht\font`\B]{#1}%
  \endgroup
}
\usepackage[export]{adjustbox} %https://tex.stackexchange.com/a/91580/8297 

%-----------------------------------------------------------------------------------------
% Beamer
%-----------------------------------------------------------------------------------------
% For easier writing
\setbeamertemplate{background}[grid]

\usepackage[normalem]{ulem}

% Fixes the frame numbering
\usepackage{appendixnumberbeamer}

% Fix a warning with \appendix
% https://tex.stackexchange.com/a/567706/8297
\pdfstringdefDisableCommands{%
    \def\translate#1{#1}%
}

% Colour emph
\setbeamercolor{emph}{fg=Purple}
\renewcommand<>{\emph}[1]{%
    {\usebeamercolor[fg]{emph}\only#2{\itshape}#1}%
}
%\setbeamercolor{alerted text}{fg=Maroon}

% For shrinking vertical space (Better to have space)
% https://tex.stackexchange.com/a/38407/8297
% \usepackage{etoolbox}% http://ctan.org/pkg/etoolbox
%\AtBeginEnvironment{figure}{\vspace{-1em}}\AtEndEnvironment{figure}{\vspace{-1em}}

% For svg picture
\usepackage{svg}

%-----------------------------------------------------------------------------------------
% Emoji
%-----------------------------------------------------------------------------------------
\usepackage{emoji}
\setemojifont{TwemojiMozilla}
\newcommand{\temoji}[1]{\text{\emoji{#1}}}

\newcommand{\cake}{\emoji{cake}}% A piece of cake (on the spot)
\newcommand{\smiling}{\emoji{slightly-smiling-face}}
\newcommand{\think}{\emoji{thinking}}
\newcommand{\sweat}{\emoji{sweat-smile}}
\newcommand{\zany}{\emoji{zany-face}}

\newcommand{\laughing}{\emoji{laughing}}
\newcommand{\astonished}{\emoji{astonished}}
\newcommand{\weary}{\emoji{weary}}
\newcommand{\cool}{\emoji{smiling-face-with-sunglasses}}
\newcommand{\dizzy}{\emoji{face-with-spiral-eyes}}
\newcommand{\mask}{\emoji{mask}}
\newcommand{\sleepy}{\emoji{sleepy}}

\newcommand{\question}{\emoji{question}}
\newcommand{\txtq}{\temoji{question}}

\newcommand{\hint}{\emoji{bulb}}
\newcommand{\bomb}{\emoji{bomb}}
\newcommand{\barrier}{\temoji{construction}}

\newcommand{\bonus}{\emoji{money-bag}}
\newcommand{\tree}{\emoji{deciduous-tree}}
\newcommand{\forest}{\tree{}\tree{}}
\newcommand{\medal}{\emoji{sports-medal}}
\newcommand{\clock}{\emoji{alarm-clock}}
\newcommand{\party}{\emoji{partying-face}}
\newcommand{\testtube}{\emoji{test-tube}}
\newcommand{\coin}{\emoji{coin}}
\newcommand{\coffee}{\emoji{coffee}}
\newcommand{\bunny}{\emoji{rabbit-face}}
\newcommand{\film}{\emoji{film-frames}}

\newcommand{\boy}{\emoji{boy-light-skin-tone}}
\newcommand{\girl}{\emoji{girl-light-skin-tone}}
\newcommand{\teacher}{\emoji{teacher-medium-skin-tone}}
\newcommand{\grandma}{\emoji{older-woman}}

\newcommand{\checkbutton}{\emoji{check-mark-button}}
\newcommand{\txtcheck}{\temoji{check-mark-button}}
\newcommand{\txtcross}{\temoji{cross-mark-button}}
\newcommand{\reject}{\emoji{cross-mark}}
\newcommand{\accept}{\emoji{check-mark-button}}
\newcommand{\triangular}{\emoji{triangular-ruler}}

% For linear algebra
\newcommand{\consistent}{\emoji{japanese-not-free-of-charge-button}}
\newcommand{\inconsistent}{\emoji{japanese-free-of-charge-button}}

% Animals
\newcommand{\cat}{\emoji{cat-face}}
\newcommand{\dog}{\emoji{dog-face}}
\newcommand{\bird}{\emoji{bird}}
\newcommand{\rabbit}{\emoji{rabbit-face}}

%-----------------------------------------------------------------------------------------
% Table
%-----------------------------------------------------------------------------------------
\usepackage{array} % For defining column types
\usepackage{booktabs} % For better-looking tables
\usepackage{siunitx} % For aligning numerical values
\usepackage{multirow}
\usepackage{booktabs}
\subtitle{\coursecode{} --- \coursename{}}

\author{Xing Shi Cai \url{https://newptcai.gitlab.io}}

\institute{Duke Kunshan University, Kunshan, China}

%-----------------------------------------------------------------------------------------
% Brackets
%-----------------------------------------------------------------------------------------
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

%-----------------------------------------------------------------------------------------
% Logic
%-----------------------------------------------------------------------------------------
\newcommand{\imp}{\rightarrow}
\renewcommand{\iff}{\leftrightarrow}
\renewcommand{\iff}{\Leftrightarrow}

%-----------------------------------------------------------------------------------------
% Linear Algebra
%-----------------------------------------------------------------------------------------
\usepackage{systeme}

\usepackage{etoolbox}
\newcommand\veclist[2][]{%
    \ifstrempty{#1}{%
        \ifstrempty{#2}{%
                \symbf{v}_{1}, \ldots, \symbf{v}_{p}% #1 are #2 are both empty
            }{%
                \symbf{#2}_{1}, \ldots, \symbf{#2}_{p}% #2 empty
            }%
    }{%
        \symbf{#2}_{1}, \ldots, \symbf{#2}_{#1}% #1 and #2 are not empty
    }%
}

\newcommand\Span[1]{\mathrm{Span}\left\{#1\right\}}

\newcommand{\bb}[1]{\cellcolor{babyblue}{#1}}
\newcommand{\bbs}{\bb{\blacksquare{}}}
\newcommand{\bast}{\bb{\ast{}}}

\usepackage{amsmath}

\DeclareMathOperator{\mrow}{row}
\DeclareMathOperator{\mcol}{col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
%\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\nullspace}{Nul}
\DeclareMathOperator{\colspace}{Col}
\DeclareMathOperator{\rowspace}{Row}
\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\proj}{proj}

%-----------------------------------------------------------------------------------------
% Chemistry
%-----------------------------------------------------------------------------------------
\newcommand*\chem[1]{\ensuremath{\mathrm{#1}}}

%-----------------------------------------------------------------------------------------
% Exercise
%-----------------------------------------------------------------------------------------
\newcommand{\practice}{%
    \vspace{0.5em}
    \begin{beamercolorbox}[wd=\textwidth,rounded=true]{block body example}%
        \scriptsize%
        \centering%
        \emoji{people-holding-hands}  Think, pair and share your solution.
    \end{beamercolorbox}%
}%

\newcommand{\poll}{%
    \vspace{1em}
    \begin{beamercolorbox}[wd=\textwidth,rounded=true]{block body example}%
        \centering%
        \emoji{hand} Submit your answer at \href{https://PollEv.com/xingshicai}{PollEv.com/xingshicai}
    \end{beamercolorbox}%
}%

\newenvironment{exerciseblock}[1]
{\begin{exampleblock}{\emoji{thinking} #1}}
{\end{exampleblock}}

\newcommand{\exercisequestion}[1]{%
    \smallskip
    \think{} #1%
}%

\newcommand{\tps}{\sweat{} Think-Pair-Share}

\newcommand{\exercise}{\think{} Exercise}

%-----------------------------------------------------------------------------------------
% Story
%-----------------------------------------------------------------------------------------
\newcommand{\story}{\section{Story Time (This will not appear in exams 😎️)}}

%-----------------------------------------------------------------------------------------
% Quote
%-----------------------------------------------------------------------------------------
\usepackage{etoolbox}
\AtBeginEnvironment{quote}{\vspace{1em}}

%-----------------------------------------------------------------------------------------
% Math fonts
%-----------------------------------------------------------------------------------------
% Allow unique letters in equations
\usefonttheme{professionalfonts}
\usepackage{unicode-math}
\setmathfont[Style=Alternate]{Asana-Math.otf}


%-----------------------------------------------------------------------------------------
% Math symbols
%-----------------------------------------------------------------------------------------
%\usepackage{MnSymbol}
\usepackage{stmaryrd}
\usepackage{bbold} % mathbb

\newcommand{\bE}{\mathbb{E}}
\newcommand{\bN}{\mathbb{N}}

\newcommand\bfa{{\symbf{a}}}
\newcommand\bfb{{\symbf{b}}}
\newcommand\bfc{{\symbf{c}}}
\newcommand\bfd{{\symbf{d}}}
\newcommand\bfe{{\symbf{e}}}
\newcommand\bff{{\symbf{f}}}
\newcommand\bfg{{\symbf{g}}}
\newcommand\bfh{{\symbf{h}}}
\newcommand\bfi{{\symbf{i}}}
\newcommand\bfj{{\symbf{j}}}
\newcommand\bfk{{\symbf{k}}}
\newcommand\bfl{{\symbf{l}}}
\newcommand\bfm{{\symbf{m}}}
\newcommand\bfn{{\symbf{n}}}
\newcommand\bfo{{\symbf{o}}}
\newcommand\bfp{{\symbf{p}}}
\newcommand\bfq{{\symbf{q}}}
\newcommand\bfr{{\symbf{r}}}
\newcommand\bfs{{\symbf{s}}}
\newcommand\bft{{\symbf{t}}}
\newcommand\bfu{{\symbf{u}}}
\newcommand\bfv{{\symbf{v}}}
\newcommand\bfw{{\symbf{w}}}
\newcommand\bfx{{\symbf{x}}}
\newcommand\bfy{{\symbf{y}}}
\newcommand\bfz{{\symbf{z}}}

\newcommand\bfA{{\symbf{A}}}
\newcommand\bfB{{\symbf{B}}}
\newcommand\bfC{{\symbf{C}}}
\newcommand\bfD{{\symbf{D}}}
\newcommand\bfE{{\symbf{E}}}
\newcommand\bfF{{\symbf{F}}}
\newcommand\bfG{{\symbf{G}}}
\newcommand\bfH{{\symbf{H}}}
\newcommand\bfI{{\symbf{I}}}
\newcommand\bfJ{{\symbf{J}}}
\newcommand\bfK{{\symbf{K}}}
\newcommand\bfL{{\symbf{L}}}
\newcommand\bfM{{\symbf{M}}}
\newcommand\bfN{{\symbf{N}}}
\newcommand\bfO{{\symbf{O}}}
\newcommand\bfP{{\symbf{P}}}
\newcommand\bfQ{{\symbf{Q}}}
\newcommand\bfR{{\symbf{R}}}
\newcommand\bfS{{\symbf{S}}}
\newcommand\bfT{{\symbf{T}}}
\newcommand\bfU{{\symbf{U}}}
\newcommand\bfV{{\symbf{V}}}
\newcommand\bfW{{\symbf{W}}}
\newcommand\bfX{{\symbf{X}}}
\newcommand\bfY{{\symbf{Y}}}
\newcommand\bfZ{{\symbf{Z}}}

\newcommand\vbfd{{\vec{\symbf{d}}}}

\newcommand\bfzero{\symbf{0}}
\newcommand\bfone{\symbf{1}}

\newcommand\dsA{{\mathbb{A}}}
\newcommand\dsB{{\mathbb{B}}}
\newcommand\dsC{{\mathbb{C}}}
\newcommand\dsD{{\mathbb{D}}}
\newcommand\dsE{{\mathbb{E}}}
\newcommand\dsF{{\mathbb{F}}}
\newcommand\dsG{{\mathbb{G}}}
\newcommand\dsH{{\mathbb{H}}}
\newcommand\dsI{{\mathbb{I}}}
\newcommand\dsJ{{\mathbb{J}}}
\newcommand\dsK{{\mathbb{K}}}
\newcommand\dsL{{\mathbb{L}}}
\newcommand\dsM{{\mathbb{M}}}
\newcommand\dsN{{\mathbb{N}}}
\newcommand\dsO{{\mathbb{O}}}
\newcommand\dsP{{\mathbb{P}}}
\newcommand\dsQ{{\mathbb{Q}}}
\newcommand\dsR{{\mathbb{R}}}
\newcommand\dsS{{\mathbb{S}}}
\newcommand\dsT{{\mathbb{T}}}
\newcommand\dsU{{\mathbb{U}}}
\newcommand\dsV{{\mathbb{V}}}
\newcommand\dsW{{\mathbb{W}}}
\newcommand\dsX{{\mathbb{X}}}
\newcommand\dsY{{\mathbb{Y}}}
\newcommand\dsZ{{\mathbb{Z}}}

\newcommand\scA{{\mathcal{A}}}
\newcommand\scB{{\mathcal{B}}}
\newcommand\scC{{\mathcal{C}}}
\newcommand\scD{{\mathcal{D}}}
\newcommand\scE{{\mathcal{E}}}
\newcommand\scF{{\mathcal{F}}}
\newcommand\scG{{\mathcal{G}}}
\newcommand\scH{{\mathcal{H}}}
\newcommand\scI{{\mathcal{I}}}
\newcommand\scJ{{\mathcal{J}}}
\newcommand\scK{{\mathcal{K}}}
\newcommand\scL{{\mathcal{L}}}
\newcommand\scM{{\mathcal{M}}}
\newcommand\scN{{\mathcal{N}}}
\newcommand\scO{{\mathcal{O}}}
\newcommand\scP{{\mathcal{P}}}
\newcommand\scQ{{\mathcal{Q}}}
\newcommand\scR{{\mathcal{R}}}
\newcommand\scS{{\mathcal{S}}}
\newcommand\scT{{\mathcal{T}}}
\newcommand\scU{{\mathcal{U}}}
\newcommand\scV{{\mathcal{V}}}
\newcommand\scW{{\mathcal{W}}}
\newcommand\scX{{\mathcal{X}}}
\newcommand\scY{{\mathcal{Y}}}
\newcommand\scZ{{\mathcal{Z}}}

%-----------------------------------------------------------------------------------------
% Brackets
%-----------------------------------------------------------------------------------------
\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

%-----------------------------------------------------------------------------------------
% Derivatives
%-----------------------------------------------------------------------------------------
\usepackage{physics}

%-----------------------------------------------------------------------------------------
% Probability
%-----------------------------------------------------------------------------------------
% Basic Probability
\newcommand{\E}[1]{{\mathbb E}\left[#1\right]}
\newcommand{\ee}[1]{{\mathbb E}[#1]}
\newcommand{\e}{{\mathbb E}}
\newcommand{\V}[1]{{\mathrm{Var}}\left(#1\right)}
\newcommand{\Vv}[1]{{\mathrm{Var}}(#1)}
%\newcommand{\var}{{\mathbb{Var}}}
\newcommand{\vv}{{\mathbb{Var}}}
\newcommand{\Cov}[1]{{\mathrm{Cov}}\left(#1\right)}
\newcommand{\bP}{{\mathbb P}}
\newcommand{\p}[1]{{\mathbb P}\left\{#1\right\}}
\newcommand{\pp}[1]{{\mathbb P}\{#1\}}
\newcommand{\psub}[2]{{\mathbb P}_{#1}\left\{#2\right\}}
\newcommand{\psup}[2]{{\mathbb P}^{#1}\left\{#2\right\}}
\newcommand{\po}[1]{{\mathbb P}_{\sigma}\left(#1\right)}
\newcommand{\pk}[1]{{\mathbb P}_{k}\left(#1\right)}
\newcommand{\I}[1]{{\mathbb 1}_{[#1]}}
\newcommand{\Cprob}[2]{{\mathbb P}\set{\left. #1 \; \right| \; #2}}
\newcommand{\probC}[2]{{\mathbb P}\set{#1 \; \left|  \; #2 \right. }}
\newcommand{\phat}[1]{\ensuremath{\hat{\mathbb P}}\left(#1\right)}
\newcommand{\Ehat}[1]{\ensuremath{\hat{\mathbb E}}\left[#1\right]}
\newcommand{\ehat}{\ensuremath{\hat{\mathbb E}}}
\newcommand{\Esup}[2]{{\mathbb E^{#1}}\left[#2\right]}
\newcommand{\esup}[1]{{\mathbb E^{#1}}}
\newcommand{\Esub}[2]{{\mathbb E_{#1}}\left[#2\right]}
\newcommand{\esub}[1]{{\mathbb E_{#1}}}

\newcommand\inprobLOW{\rightarrow_p}
\newcommand\inprobHIGH{\,{\buildrel p \over \rightarrow}\,}
\newcommand\inprob{{\inprobHIGH}}

\newcommand\inlawLOW{\rightarrow_d}
\newcommand\inlawHIGH{\,{\buildrel d \over \rightarrow}\,}
\newcommand\inlaw{{\inlawHIGH}}

\newcommand\inas{\,{\buildrel{a.s.} \over \rightarrow}\,}

\newcommand{\eql}{\,{\buildrel \scL \over =}\,}

\newcommand{\eqd}{\,{\buildrel{\mathrm{def}} \over =}\,}

\newcommand{\eqas}{\,{\buildrel{\mathrm{a.s.}} \over =}\,}

\newcommand{\convergeas}[1]{\,{\buildrel{{#1}} \over \longrightarrow}\,}

\newcommand\given[1][]{\;#1{\vert}\;}

\newcommand\cond{\;\middle|\;}

%-----------------------------------------------------------------------------------------
% Distributions
%-----------------------------------------------------------------------------------------
\DeclareMathOperator{\Bin}{Bin}
\DeclareMathOperator{\Ber}{Ber}
\DeclareMathOperator{\Poi}{Poi}
\DeclareMathOperator{\Geo}{Geo}
\DeclareMathOperator{\Unif}{Unif}
\DeclareMathOperator{\Exp}{Exp}
\DeclareMathOperator{\Be}{Beta}
\DeclareMathOperator{\Gam}{Gamma}
\DeclareMathOperator{\Gaussian}{\scN}

%-----------------------------------------------------------------------------------------
% Logic
%-----------------------------------------------------------------------------------------
\DeclareMathOperator{\Xor}{Xor}

%-----------------------------------------------------------------------------------------
% Acronyms
%-----------------------------------------------------------------------------------------
\usepackage{acro}

\DeclareAcronym{iid}{
    short = \scshape{iid},
    long = independent and identically distributed
}

\DeclareAcronym{rv}{
    short = \scshape{rv},
    long = random variable
}

\DeclareAcronym{lhs}{
    short = \scshape{lhs},
    long = left hand side
}

\DeclareAcronym{rhs}{
    short = \scshape{rhs},
    long = right hand side
}

\DeclareAcronym{gf}{
    short = \scshape{gf},
    long = generating function
}

\DeclareAcronym{egf}{
    short = \scshape{egf},
    long = exponential generating function
}

\DeclareAcronym{bai}{
    short = \scshape{bai},
    long = {B}asic {A}nalysis {I}
}

\DeclareAcronym{dm}{
    short = \scshape{dm},
    long = {D}iscrete {M}athematics
}

\DeclareAcronym{ac}{
    short = \scshape{ac},
    long = {A}pplied {C}ombinatorics
}

%-----------------------------------------------------------------------------------------
% Use Lua to extract lecture number
%-----------------------------------------------------------------------------------------
\usepackage{luacode}

\begin{luacode}
function lecturenum(jobname)
    local n = string.match(jobname, "%d+")
    tex.print(n)
end
\end{luacode}

\newcommand\lecturenum
{%
    \directlua{lecturenum("\jobname")}%
}%

\newcommand{\sectionhomework}[2]{
    \emoji{book} Section #1 ---
    \begin{itemize}
        \item[\emoji{question}] All T/F questions.
        \item[\emoji{weight-lifting}] Practice problems.
        \item[\emoji{pencil}] Ex.~#2.
    \end{itemize}
}

%-----------------------------------------------------------------------------------------
% For this course
%-----------------------------------------------------------------------------------------
\newcommand{\blankveryshort}{\underline{\hspace{1cm}}}
\newcommand{\blankshort}{\underline{\hspace{2cm}}}

\newcommand{\coursecode}{MATH 202}
\newcommand{\coursename}{Linear Algebra}

% Now we can extract the lecture number from the file name
\title{Lecture \lecturenum{}}

\newcommand{\exercisepic}[1]{%
\begin{figure}[htpb]%
    \centering{}%
    \includegraphics[width=0.9\textwidth]{exercise-#1.jpg}%
\end{figure}%
}

\newcommand{\lectureoutline}{\begin{frame}%
\frametitle{Summary}%
\tableofcontents{}%
\end{frame}}

\date{Jan 2023}
