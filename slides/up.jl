# A script to handle vmtex update on 2022-10-12

function predicate(line)
    return startswith(line, "\\input{../meta.tex}")
end

function update(file)
    (tmppath, tmpio) = mktemp()
    open(file) do io
        for line in eachline(io, keep=true) # keep so the new line isn't chomped
            if predicate(line)
                line = raw"""
                \input{../options.tex}
                \documentclass{beamer}
                \input{../meta.tex}
                """
            end
            write(tmpio, rstrip(line) .* "\n")
        end
    end
    close(tmpio)
    mv(tmppath, file, force=true)
end

function main()
    for (root, dirs, files) in walkdir(".")
        for f in files
            base, ext = splitext(f)
            if ext == ".tex"
                file = joinpath(root, f)
                update(file)
            end
        end
    end
end

main()
