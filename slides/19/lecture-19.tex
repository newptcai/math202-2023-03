\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Digital Signal Processing, Application to Difference Equations}

\input{../tikz.tex}

\begin{document}

\maketitle{}

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{4.7 Digital Signal Processing}

\subsection{Discrete-Time Signals}

\begin{frame}
    \frametitle{Music signal}

    The sounds produced from a $CD$ are produced from music that has been sampled
    $44,100$ times per second.
    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.8\linewidth]{cd.jpg}
        \includegraphics<2>[width=0.8\linewidth]{discrete-time-signal-music.png}
        \caption*{\only<1>{CD (Compact Disc)}\only<2>{Sampled data from a music signal}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \alert{Digital Signal Processing} (DSP) involves manipulating and analyzing digital signals for various applications.

    Discrete-time signals, sampled versions of continuous signals, are central
    to DSP.

    Vector spaces of discrete-time signals provide a framework for DSP concepts.

    Linear transformations, like \alert{Linear Time Invariant}  
    and Fourier analysis, operate within this vector space.
\end{frame}

\begin{frame}
    \frametitle{Discrete-Time Signals}

    The vector space $\dsS$ (double-indexed sequences) of discrete-time signals contains 
    all doubly-infinite sequences $\{y_{k}\}$.

    For example,
    \only<1>{
        \begin{equation*}
            \{y_k\} = (\dots, 0.7^{-2}, 0.7^{-1}, 0.7^{0}, 0.7^{1}, 0.7^{2}, \dots)
        \end{equation*}
        \begin{center}
            \begin{tikzpicture}[scale=0.6]
                \begin{axis}[
                    axis lines = middle,
                    xlabel = {$k$},
                    ylabel = {$y_k$},
                    xtick = {-2, -1, 0, 1, 2},
                    ytick = {0.2, 0.49, 1, 1.428, 2.04},
                    yticklabels = {0.2, 0.49, 1, 1.428, 2.04},
                    ymin = 0,
                    ymax = 2.5,
                    xmin = -2.5,
                    xmax = 2.5,
                    grid = both,
                    minor tick num = 1,
                    grid style = {line width=.1pt, draw=gray!10},
                    major grid style = {line width=.2pt, draw=gray!50},
                    width = 10cm,
                    height = 7cm,
                    scale only axis,
                    tick label style={font=\small},
                    label style={font=\small},
                    legend style={font=\small},
                    legend pos = north east,
                    ]

                    \addplot+[only marks, mark=*, mark options={fill=blue}, mark size=3pt] coordinates {
                            (-2, 2.04)
                            (-1, 1.428)
                            (0, 1)
                            (1, 0.7)
                            (2, 0.49)
                        };

                    \legend{$y_k = 0.7^k$}
                \end{axis}
            \end{tikzpicture}   
        \end{center}
    }
    \only<2>{
        \begin{equation*}
            \delta = \{y_k\} =  (\dots, 0, 0, {\color{red} 1}, 0, 0, \dots)
        \end{equation*}
        \begin{center}
        \begin{tikzpicture}[scale=0.5]
            \begin{axis}[
                axis lines = middle,
                xlabel = {$k$},
                ylabel = {$y_k$},
                xtick = {-2, -1, 0, 1, 2},
                ytick = {0, 1},
                ymin = -0.5,
                ymax = 1.5,
                xmin = -2.5,
                xmax = 2.5,
                grid = both,
                minor tick num = 1,
                grid style = {line width=.1pt, draw=gray!10},
                major grid style = {line width=.2pt, draw=gray!50},
                width = 10cm,
                height = 7cm,
                scale only axis,
                tick label style={font=\small},
                label style={font=\small},
                legend style={font=\small},
                legend pos = north east,
                ]

                \addplot+[only marks, mark=*, mark options={fill=blue}, mark size=3pt] coordinates {
                        (-2, 0)
                        (-1, 0)
                        (0, 1)
                        (1, 0)
                        (2, 0)
                    };
            \end{axis}
        \end{tikzpicture}
        \end{center}
    }
    \only<3>{
        \begin{equation*}
            \bfzero = \{y_k\} =  (\dots, 0, 0, {\color{red} 0}, 0, 0, \dots)
        \end{equation*}
        \begin{center}
        \begin{tikzpicture}[scale=0.5]
            \begin{axis}[
                axis lines = middle,
                xlabel = {$k$},
                ylabel = {$y_k$},
                xtick = {-2, -1, 0, 1, 2},
                ytick = {0, 1},
                ymin = -0.5,
                ymax = 1.5,
                xmin = -2.5,
                xmax = 2.5,
                grid = both,
                minor tick num = 1,
                grid style = {line width=.1pt, draw=gray!10},
                major grid style = {line width=.2pt, draw=gray!50},
                width = 10cm,
                height = 7cm,
                scale only axis,
                tick label style={font=\small},
                label style={font=\small},
                legend style={font=\small},
                legend pos = north east,
                ]

                \addplot+[only marks, mark=*, mark options={fill=blue}, mark size=3pt] coordinates {
                        (-2, 0)
                        (-1, 0)
                        (0, 0)
                        (1, 0)
                        (2, 0)
                    };
            \end{axis}
        \end{tikzpicture}
        \end{center}
    }
\end{frame}

\subsection{Linear Time Invariant}

\begin{frame}
    \frametitle{Example 1}

    Define a transformation of signal $S$ by
    letting $S(\{x_{k}\}) = \{y_{k}\}$ where $y_{k} = x_{k-1}$.

    In other words, $S$ shifts the signal to the right.

    \pause{}

    For example, we have
    \begin{equation*}
        \begin{aligned}
            \delta & = (\dots, 0, 0, 1, 0, 0, \dots) \\
            S(\delta) & = (\dots, 0, 0, 0, 1, 0, \dots) \\
            S^{2}(\delta) & = (\dots, 0, 0, 0, 0, 1, \dots)
        \end{aligned}
    \end{equation*}

    \pause{}

    \cake{} What does $S^{-1}$, the inverse of $S$, do?
    \begin{equation*}
        \begin{aligned}
            \delta & = (\dots, 0, 0, 1, 0, 0, \dots) \\
            S^{-1}(\delta) & = \\
            S^{-2}(\delta) & = 
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Linear Time Invariant}
    
    A transformation $T: \dsS \mapsto \dsS$ is a \alert{Linear Time Invariant (LTI)} 
    if 
    \begin{itemize}
        \item[(i)] $T(\{x_{k} + y_{k}\}) = T(\{x_{k}\}) + T(\{y_{k}\})$ 
            for all $\{x_{k}\}, \{y_{k}\} \in \dsS$;
        \item[(ii)] $T(c \{x_{k}\}) = c T(\{x_{k}\})$
            for all $\{x_{k}\} \in \dsS$ and $c \in \dsR$,
        \item[(iii)] 
            if $T(\{x_{k}\}) = \{y_{k}\}$, 
            then $T(\{x_{k + q}\}) = \{y_{k + q}\}$ for all $q \in \dsZ$
            for all $\{x_{k}\} \in \dsS$ and $c \in \dsR$.
    \end{itemize}

    \pause{}

    \begin{block}{Theorem 16}
        A Linear Time Invariant (LTI) is a special type of linear
        transformation.
    \end{block}
\end{frame}

\subsection{Digital Signal Processing}

\begin{frame}
    \frametitle{Example 2 --- Smoothing}
    For any positive integer $m$, the \alert{moving average LTI} transformation
    with period $m$ is defined by
    \begin{equation*}
        M_{m}(\{x_{k}\})
        =
        \{y_{k}\}
        \quad
        \text{where}
        \quad
        y_{k}
        =
        \frac{1}{m}
        \sum_{j=k-m+1}^{k} x_{j}
    \end{equation*}

    \pause{}

    \cake{}
    In the following picture, which one is the original? Which one is the moving
    average.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{4.7-fig-3-audio.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 3 --- Auralization}

    \alert{Auralization}  in DSP refers to the process of 
    improving acoustic properties of a generated sounds.

    Let $T = 44,100$. The following picture shows how combining sounds
    enhances $\{\cos(440 \pi k /T)\}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=\textwidth]{4.7-fig-4-1.pdf}
        \includegraphics<2>[width=\textwidth]{4.7-fig-4-2.pdf}
        \includegraphics<3>[width=\textwidth]{4.7-fig-4-3.pdf}
        \includegraphics<4>[width=\textwidth]{4.7-fig-4-4.pdf}
    \end{figure}
\end{frame}


\subsection{Generating the Bases for Subspaces of \texorpdfstring{$\dsS$}{S}}

\begin{frame}
    \frametitle{Signals of length $n$}
    Let $\dsS_{n}$ be the set of signals of length $n$.

    In other words, $\{y_{k}\} \in \dsS_{n}$ if and only if
    $y_{k} = 0$ when $k < 0$ or $k > n$.

\end{frame}

\begin{frame}
    \frametitle{Theorem 17}

    The set $\dsS_{n}$ is a subspace of $\dsS$.

    It has a basis
    \begin{equation*}
        \scB_{n} = 
        \{\delta, S(\delta), \dots, S^{n}(\delta)\}
    \end{equation*}
    \cake{} What is the dimension of $\dsS_{n}$?
\end{frame}

\begin{frame}
    \frametitle{Example 4}
    Consider the following signal in $\dsS_{2}$ ---
    \begin{equation*}
        \{y_{k}\}
        =
        (\dots, 0, 0, 0, {\color{red}2}, 3, -1, 0, 0, 0, \dots)
    \end{equation*}
    What is its coordinates with respect to
    $\scB_{2} = \{\delta, S(\delta), S^{2}(\delta)\}$?
\end{frame}

\begin{frame}
    \frametitle{Finitely supported signals}
    If a signal $\{y_{k}\}$ contains only finitely many non-zero entries,
    then it is called a \alert{finitely supported} signal.

    Let $\dsS_{f}$ be the set of \alert{finitely supported} signals.

    \dizzy{} Is the signal $x_{k} = e^{-k}$ in $\dsS_{f}$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 18}

    The set $\dsS_{f}$ is a subspace of $\dsS$.

    It has a basis
    \begin{equation*}
        \scB_{f} = 
        \{S^{j}(\delta) : j \in \dsZ\}
    \end{equation*}

    \pause{}

    \cake{} What is the dimension of $\dsS_{f}$?

    \zany{} Does $\dsS$ itself have a basis?
\end{frame}

\section{4.8 Applications to Linear Difference Equations}

\subsection{Linear Independence in the Space \texorpdfstring{$\dsS$}{S} of Signals}

\begin{frame}
    \frametitle{Linear Independence in $\dsS$}

    Three signals $\bfu = \{u_{k}\}$, $\bfv = \{v_{k}\}$, $\bfw = \{w_{k}\}$
    are \alert{linearly independent} if
    \begin{equation}
        \label{eq:1}
        c_{1} \bfu
        +
        c_{2} \bfv
        +
        c_{3} \bfw
        =
        \bfzero
        ,
    \end{equation}
    implies $c_{1} = c_{2} = c_{3} = 0$.

    \cake{} Do you remember what is $\bfzero$ in $\dsS$?

    \pause{}
    Note that \eqref{eq:1} is the same as
    \begin{equation*}
        c_{1} u_{k}
        +
        c_{2} v_{k}
        +
        c_{3} w_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Casorati matrix}

    In other words, \eqref{eq:1} is equivalent that for all $k \in \dsZ$,
    \begin{equation*}
        \begin{bmatrix}
            u_{k} & v_{k} & w_{k} \\
            u_{k+1} & v_{k+1} & w_{k+1} \\
            u_{k+2} & v_{k+2} & w_{k+2} \\
        \end{bmatrix}
        \begin{bmatrix}
            c_{1} \\ c_{2} \\ c_{3}
        \end{bmatrix}
        =
        \begin{bmatrix}
            0 \\ 0 \\ 0
        \end{bmatrix}
    \end{equation*}
    The matrix is call \alert{Casorati matrix} of signals, $C(k)$.

    Its determinant $\det C(k)$ is called the \alert{Casoratian}.

    \pause{}

    If the matrix is \emph{invertible} for some $k$, then we must have
    $c_{1} = c_{2} = c_{3} = 0$. (\cake{} Why?)
    And $\bfu, \bfv$ and $\bfz$ are linearly independent.

    \pause{}

    \dizzy{}
    What can we say if the matrix is \emph{not invertible} for some $k$?
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Are $1^{k}$, $(-2)^{k}$ and $3^{k}$ linearly independent?

    Let's check $k=0$.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Are $\{k^2\}$ and $\{2 k \abs{k}\}$ linearly independent?

    \hint{} Try $k=\pm 1$.
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.7}{9, 23, 25, 27, 29, 31}
        \end{column}
    \end{columns}
\end{frame}
\end{document}
