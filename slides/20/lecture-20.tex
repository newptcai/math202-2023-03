\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Difference Equations}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{4.8 Applications to Linear Difference Equations}

\subsection{Linear Difference Equations}

\begin{frame}
    \frametitle{A puzzle of \emoji{rabbit-face}}

    You have a pair of \emoji{rabbit-face}, one male and one female, on an island.

    Rabbits mate at the age of one month and produce one pair of offspring (one male, one female) every month.

    Each pair of \emoji{rabbit-face} takes one month to mature and be able to mate.

    Assume that \emoji{rabbit-face} never die.

    After $n$ months, how many pairs of \emoji{rabbit-face} will there be on the island?
\end{frame}

\begin{frame}
    \frametitle{A guessing game}

    \begin{equation*}
        (\dots, 0, 0, {\color{red}0} , 1, 1, 2, 3, 5, 8, 13, 21, \dots)
    \end{equation*}
    \cake{} Can you guess the next number in the signal $\{f_k\}$

    The is the famous
    \href{https://math.oxford.emory.edu/site/math125/fibonacciRabbits/}{Fibonacci sequence}.
\end{frame}

\begin{frame}
    \frametitle{Linear Difference Equations}

    Let $a_{0}, \dots, a_{n}$ be scalars with $a_{0}$ and $a_{n}$ being nonzero,
    and let $\{y_{k}\}, \{z_{k}\}$ be signals.

    The equation
    \begin{equation*}
        a_{0} y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        a_{n} y_{k}
        =
        z_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ.
    \end{equation*}
    is called a \alert{linear difference equation} of \alert{order $n$}.

    If $z_{k} =0$ for all $k$, then it is call \alert{homogeneous}.
    Otherwise it is \alert{nonhomogeneous}.
\end{frame}

\begin{frame}
    \frametitle{The Fibonacci sequence}
    
    The Fibonacci \emoji{rabbit-face} sequence satisfies the following linear
    difference equation
    \begin{equation*}
        f_{k+2} - f_{k+1} - f_{k} = 0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}

    \cake{} What is the \emph{order} of the equation? Is it \emph{homogeneous}?
\end{frame}

\begin{frame}
    \frametitle{Review of $S$}
    
    Recall that 4.7 defined the Linear Time Invariant $S$ which
    shifts a signal to the right.

    \cake{} 
    If $\{x_{k}\} = S^{-1}(\{y_k\})$, then
    \begin{flalign*}
        x_{k} = &&
    \end{flalign*}

    \cake{} 
    If $\{x_{k}\} = S^{-2}(\{y_k\})$, then
    \begin{flalign*}
        x_{k} = &&
    \end{flalign*}

    \cake{} 
    If $\{x_{k}\} = S^{-n}(\{y_k\})$, then
    \begin{flalign*}
        x_{k} = &&
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Linear Difference Equations with LTI}

    Define the transformation $T$ of signals by
    \begin{equation*}
        T 
        = 
        a_{0} S^{-n}
        +
        a_{1} S^{-n+1}
        +
        \dots
        +
        a_{n-1} S^{-1}
        +
        a_{n} S^{0}
    \end{equation*}
    Then the equation
    \begin{equation*}
        a_{0} y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        a_{n} y_{k}
        =
        z_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    is simply
    \begin{equation*}
        T(\{y_{k}\}) = \{z_{k}\}.
    \end{equation*}
    A transformation like $T$ is called a \alert{filter}.
\end{frame}

\begin{frame}
    \frametitle{Example 3}
    Given that $y_{k} = \cos(\pi k /4)$ and
    \begin{equation*}
        z_{k} = 
        \frac{\sqrt{2}}{4} y_{k+2}
        +
        \frac{1}{2} y_{k+1}
        +
        \frac{\sqrt{2}}{4} y_{k}
    \end{equation*}
    we have
    \only<1>{
    \begin{equation*}
        z_{k}
        =
        \frac{\cos \left(\frac{\pi  k}{4}\right)-\sin \left(\frac{\pi  k}{4}\right)}{\sqrt{2}}
    \end{equation*}}
    \begin{figure}[htpb]
        \centering
        \includegraphics<2>[width=0.8\textwidth]{4.8-tab-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Given the equation
    \begin{equation*}
        y_{k+3} - 2 y_{k+2} - 5 y_{k+1} + 6 y_{k} = 0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    how to solve it? 

    In other words, how to find some $\{y_{k}\}$ which satisfies it?

    \emoji{bulb} Try $r^{k}$.
\end{frame}

\begin{frame}
    \frametitle{Some solutions of homogeneous equations}

    In general, $r^{k}$ is a solution of the equation
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    if $r$ is a \alert{root (solution)} of
    \begin{equation*}
        x^{n}
        +
        a_{1} x^{n-1}
        +
        \dots
        +
        a_{n}
        =
        0
        ,
    \end{equation*}
    where $x$ is unknown variable.

    \think{} Does this method give all the solutions?
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \think{} Find \emph{all} solutions of
%    \begin{equation*}
%        f_{k+2} - f_{k+1} - f_{k} = 0
%        ,
%        \qquad
%        \text{for all }
%        k \in \dsZ,
%    \end{equation*}
%\end{frame}

\subsection{Solution Sets of Linear Difference Equations}

\begin{frame}
    \frametitle{A vector space}

    Let $T:\dsS \mapsto \dsS$ be the mapping that maps $\bfy = \{y_{k}\}$ to
    $\{w_{k}\}$ where
    \begin{equation*}
        w_{k}
        =
        a_{0} y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        a_{n} y_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        .
    \end{equation*}
    Then $T$ is a \emph{linear transform}.

    \pause{}

    The solution of
    $T(\bfy) = \bfzero$, i.e.,
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    is the \emph{kernel} of $T$.

    \emoji{star-struck} So the solution set is \emph{subspace} of $\dsS$. (See
    4.2)
\end{frame}

\begin{frame}
    \frametitle{Theorem 16 --- Unique solutions}

    If $a_n \ne 0$, 
    and $\{z_{k}\}$ is given, 
    the equation
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        z_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    has a unique solution,
    whenever $y_{0}, \dots, y_{n-1}$ are specified.
\end{frame}

\begin{frame}
    \frametitle{Theorem 17 --- Dimensions}

    The set $H$ of all solutions of the $n$-th order homogeneous linear difference equation
    \begin{equation*}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}
    is an $n$-dimensional vector space.

    \begin{block}{Exercise 52 (4.5)}
        If there is one-to-one and onto
        linear mapping $T$ from vector space $V$ to vector space $W$,
        then $V$ and $W$ are called isomorphic.

        Moreover, $\dim V = \dim W$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Find a basis of the solution set of
    \begin{equation*}
        y_{k+3} - 2 y_{k+2} - 5 y_{k+1} + 6 y_{k} = 0
        ,
        \qquad
        \text{for all }
        k \in \dsZ,
    \end{equation*}

    Answer: $\{3^{k}, 1^k, (-2)^k\}$.
\end{frame}


\subsection{Nonhomogeneous equations}

\begin{frame}
    \frametitle{Nonhomogeneous equations}

    The general solution of
    \begin{equation}
        \label{eq:nonhomogeneous}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        z_{k}
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        ,
    \end{equation}
    can be written as a particular solution of \eqref{eq:nonhomogeneous}
    plus an arbitrary solution of
    \begin{equation}
        \label{eq:homogeneous}
        y_{k+n}
        +
        a_{1} y_{k+n-1}
        +
        \dots
        +
        a_{n} y_{k}
        =
        0
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        .
    \end{equation}

    This is analogous to nonhomogeneous linear systems $A \bfx = \bfb$.

    \hint{} Here a \emph{particular solution} means a
    \emph{specific/concrete/fixed} signal $\{y_{k}\}$ that satisfies \eqref{eq:nonhomogeneous}.
\end{frame}

\begin{frame}
    \frametitle{Example 6}

    Verify that $k^2$ is a solution
    \begin{equation}
        \label{eq:nonhomogeneous:1}
        y_{k+2}
        -
        4 y_{k+1}
        +
        3 y_{k}
        =
        -4 k
        ,
        \qquad
        \text{for all }
        k \in \dsZ
        ,
    \end{equation}
    Then find all solutions of to \eqref{eq:nonhomogeneous:1}.
\end{frame}

%\subsection{Reduction to Systems of First-Order Equations}
%
%\begin{frame}
%    \frametitle{Systems of first-order equations}
%
%    Write the following equation
%    \begin{equation}
%        y_{k+3}
%        -
%        2 y_{k+2}
%        -
%        5 y_{k+1}
%        +
%        6 y_{k}
%        =
%        0
%        ,
%        \qquad
%        \text{for all }
%        k \in \dsZ
%        ,
%    \end{equation}
%    as
%    \begin{equation*}
%        \bfx_{k+1} = A \bfx_{k}
%        ,
%        \qquad
%        \text{for all }
%        k \in \dsZ
%    \end{equation*}
%    where $\bfx_{k} = \begin{bmatrix} y_k \\ y_{k+1} \\ y_{k+2} \end{bmatrix}$.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Generalization}
%
%    In general
%    \begin{equation}
%        y_{k+n}
%        +
%        a_{1} y_{k+n-1}
%        +
%        \dots
%        +
%        a_{n} y_{k}
%        =
%        0
%        ,
%        \qquad
%        \text{for all }
%        k \in \dsZ
%        ,
%    \end{equation}
%    can be written as
%    \begin{equation*}
%        \bfx_{k+1} = A \bfx_{k}
%    \end{equation*}
%    where
%    \begin{equation*}
%        \bfx_{k} =
%        \begin{bmatrix}
%            y_{k}     \\
%            y_{k+1}   \\
%            \vdots    \\
%            y_{k+n-1} \\
%        \end{bmatrix}
%        ,
%        \qquad
%        A
%        =
%        \begin{bmatrix}
%            0 & 1 & 0 & \dots & 0 \\
%            0 & 0 & 1 & \dots & 0 \\
%            \vdots &   & \ddots  &  & \vdots \\
%            0 & 0 & 0 & \dots & 1 \\
%            -a_{n} & -a_{n-1} & -a_{n-2} & \dots & -a_{1} \\
%        \end{bmatrix}
%    \end{equation*}
%\end{frame}

% This has been removed from Chapter 4 in the 6th edition
%\section{4.9 Applications to Markov Chains}
%
%\begin{frame}[c]
%    \frametitle{Moving around}
%
%    Assume that the annual percentage of  migration between city and suburbs is as
%    follows
%
%    \vspace{1em}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{city-suburb.png}
%    \end{figure}
%
%\end{frame}
%
%\begin{frame}
%    \frametitle{How the population changes}
%
%    If the first year there are $600$ citizens and $400$ suburbans,
%    the numbers of the second year is
%    \begin{equation*}
%        \begin{bmatrix}
%            .95 & .03 \\
%            .05 & .97 \\
%        \end{bmatrix}
%        \begin{bmatrix}
%            600 \\
%            400
%        \end{bmatrix}
%        =
%        \begin{bmatrix}
%            582 \\
%            418
%        \end{bmatrix}
%        .
%    \end{equation*}
%
%    \pause{}
%
%    This can be written as in percentage as
%    \begin{equation*}
%        \begin{bmatrix}
%            .95 & .03 \\
%            .05 & .97 \\
%        \end{bmatrix}
%        \begin{bmatrix}
%            .6 \\
%            .4
%        \end{bmatrix}
%        =
%        \begin{bmatrix}
%            .582 \\
%            .418
%        \end{bmatrix}
%        .
%    \end{equation*}
%
%    \pause{}
%
%    And for the third year
%
%    \begin{equation*}
%        \begin{bmatrix}
%            .95 & .03 \\
%            .05 & .97 \\
%        \end{bmatrix}^{2}
%        \begin{bmatrix}
%            .6 \\
%            .4
%        \end{bmatrix}
%        =
%        \begin{bmatrix}
%            .56544 \\
%            .43456 \\
%        \end{bmatrix}
%        .
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Markov Chain}
%
%    A vector with nonnegative entries that add up to $1$ is a \alert{probability vector}.
%
%    A square matrix whose columns are probability vectors is a \alert{stochastic matrix}.
%
%    A \emph{Markov chain} is sequence of probability vectors $\bfx_{0}, \bfx_{1}, \dots$
%    together with a stochastic matrix $P$ such that
%    \begin{equation*}
%        \bfx_{k + 1} = P \bfx_{k}, \qquad \text{for all } k \ge 0.
%    \end{equation*}
%
%    \pause{}
%    In the population example
%    \begin{equation*}
%        P =
%        \begin{bmatrix}
%            .95 & .03 \\
%            .05 & .97 \\
%        \end{bmatrix}
%        ,
%        \quad
%        \bfx_{0} =
%        \begin{bmatrix}
%            .6 \\
%            .4
%        \end{bmatrix}
%        ,
%        \quad
%        \bfx_{1} =
%        \begin{bmatrix}
%            .582 \\
%            .418
%        \end{bmatrix}
%        ,
%        \quad
%        \bfx_{2} =
%        \begin{bmatrix}
%            .56544 \\
%            .43456 \\
%        \end{bmatrix}
%        \dots
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{The distant future}
%
%    In $100$ years, the population stabilizes
%    \begin{equation*}
%        \begin{bmatrix}
%            \bfx_{97} &
%            \bfx_{98} &
%            \bfx_{99} &
%            \bfx_{100}
%        \end{bmatrix}
%        =
%        \begin{bmatrix}
%            .37508 & .37507 & .37506 & .37506 \\
%            .62492 & .62493 & .62494 & .62494 \\
%        \end{bmatrix}
%    \end{equation*}
%    \pause{}
%    In other words, $\bfx_{k} \to \bfq$ as $k \to \infty$,
%    where $\bfq$ satisfies
%    \begin{equation*}
%        P
%        \bfq
%        =
%        \begin{bmatrix}
%            .95 & .03 \\
%            .05 & .97 \\
%        \end{bmatrix}
%        \begin{bmatrix}
%            q_1 \\ q_2
%        \end{bmatrix}
%        =
%        \begin{bmatrix}
%            q_1 \\ q_2
%        \end{bmatrix}
%        =
%        \bfq
%    \end{equation*}
%    and that $q_{1} + q_{2} = 1$.
%    Such $\bfq$ is called a \alert{steady-state} vector for $P$.
%
%    \pause{}
%
%    Solving this gives
%    \begin{equation*}
%        \bfq
%        =
%        \left[
%            \begin{array}{c}
%                \frac{3}{8} \\
%                \frac{5}{8} \\
%            \end{array}
%        \right]
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Regular stochastic matrix}
%
%    A stochastic matrix is \emph{regular} if all entries of $P^{k}$ contains strictly
%    positive entries for some $k$. For example
%    \begin{equation*}
%        P =
%        \left[
%            \begin{array}{ccc}
%                0.5 & 0.5 & 0.3 \\
%                0.3 & 0.8 & 0.3 \\
%                0.2 & 0 & 0.4 \\
%            \end{array}
%        \right]
%    \end{equation*}
%    is regular because
%    \begin{equation*}
%        P^2 =
%        \left[
%            \begin{array}{ccc}
%                0.46 & 0.65 & 0.42 \\
%                0.45 & 0.79 & 0.45 \\
%                0.18 & 0.1 & 0.22 \\
%            \end{array}
%        \right]
%    \end{equation*}
%    \cake{} Can you think of a \emph{stochastic} matrix which is not regular?
%\end{frame}
%
%\begin{frame}
%    \frametitle{Theorem 18 --- Convergence to steady-state vector}
%
%    If $P$ is a regular stochastic matrix,
%    then $P$ has a \emph{unique} steady-state vector $\bfq$.
%
%    Moreover, the Markov chain $\{\bfx_{k}\}$ of $P$ converges to $\bfq$
%    regardless of the choice of $\bfx_0$.
%
%    \pause{}
%
%    \begin{example}
%        In the population example
%        \begin{equation*}
%            P =
%            \begin{bmatrix}
%                .95 & .03 \\
%                .05 & .97 \\
%            \end{bmatrix}
%            ,
%            \quad
%            \bfq
%            =
%            \left[
%                \begin{array}{c}
%                    \frac{3}{8} \\
%                    \frac{5}{8} \\
%                \end{array}
%            \right]
%            .
%        \end{equation*}
%    \end{example}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example}
%
%    The convergence to $\bfq$ does not depend on $\bfx_0$, e.g.,
%    \begin{equation*}
%        \begin{aligned}
%            \bfx_{0} & =
%            \begin{bmatrix}
%                .6 \\
%                .4
%            \end{bmatrix}
%            ,
%            \dots
%            ,
%            \bfx_{100}
%            =
%            \begin{bmatrix}
%                .37506 \\
%                .62494 \\
%            \end{bmatrix}
%            \approx
%            \left[
%                \begin{array}{c}
%                    3/8 \\
%                    5/8 \\
%                \end{array}
%            \right]
%            \\
%            \bfx_{0} & =
%            \begin{bmatrix}
%                .3 \\
%                .7
%            \end{bmatrix}
%            ,
%            \dots
%            ,
%            \bfx_{100}
%            =
%            \begin{bmatrix}
%                0.37498 \\
%                0.62502 \\
%            \end{bmatrix}
%            \approx
%            \left[
%                \begin{array}{c}
%                    3/8 \\
%                    5/8 \\
%                \end{array}
%            \right]
%            \\
%            \bfx_{0} & =
%            \begin{bmatrix}
%                .2 \\
%                .3
%            \end{bmatrix}
%            ,
%            \dots
%            ,
%            \bfx_{100}
%            =
%            \begin{bmatrix}
%                0.1875 \\
%                0.3125 \\
%            \end{bmatrix}
%            \ne
%            \left[
%                \begin{array}{c}
%                    3/8 \\
%                    5/8 \\
%                \end{array}
%            \right]
%        \end{aligned}
%    \end{equation*}
%
%    \bomb{} Why does the last one look wrong?
%\end{frame}
%
%%\begin{frame}
%%    \frametitle{\sweat{} Think-Pair-Share}
%%
%%    Show that every $2 \times 2$ stochastic matrix has at least one steady-state
%%    vector.
%%
%%    \pause{}
%%
%%    \hint{} Such a matrix can be written as
%%    \begin{equation*}
%%        A=
%%        \begin{bmatrix}
%%            1 - \alpha & \beta \\
%%            \alpha & 1-\beta
%%        \end{bmatrix}
%%        ,
%%    \end{equation*}
%%    where $0 \le \alpha, \beta \le 1$.
%%
%%    \pause{}
%%
%%    When $\alpha = \beta = 0$, we have
%%    \begin{equation*}
%%        A
%%        \begin{bmatrix}
%%            q \\ 1-q
%%        \end{bmatrix}
%%        =
%%        \begin{bmatrix}
%%            q \\ 1-q
%%        \end{bmatrix}
%%    \end{equation*}
%%    by taking $q = \text{\question}$.
%%
%%    \pause{}
%%
%%    What $q$ should we take when $\alpha \ne 0$ or $\beta \ne 0$?
%%\end{frame}


\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{4.8}{13, 19, 21, 27, 33, 35}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
