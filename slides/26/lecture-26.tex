\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Orthogonal Sets,  Orthogonal Projections}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{6.2 Orthogonal Sets}

\begin{frame}
    \frametitle{Review --- Orthogonal vectors in $\dsR^{n}$}

    In $\dsR^{n}$, we say two vectors $\bfu$ and $\bfv$ are \alert{orthogonal} 
    if $\bfu \cdot \bfv = 0$.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.5, 
            every label/.style={black, fill=white, inner sep=1pt},
            every node/.style={black, fill=white, inner sep=1pt}
            ]
            \fill[white] (-6, -1) rectangle (6, 6);

            % Grid
            \draw[step=1, lightgray, very thin] (-5, 0) grid (5, 5);

            % Axes
            \draw[thick, ->] (-5, 0) -- (5, 0) node[right] {$x_1$};
            \draw[thick, ->] (0, 0) -- (0, 5) node[above] {$x_2$};

            % Vector u
            \draw[vector] (0, 0) -- (3, 4) node[right] {$\mathbf{u}$};

            % Vector v
            \draw[vector] (0, 0) -- (-2, 1.5) node[left] {$\mathbf{v}$};
        \end{tikzpicture}
        \caption*{Vectors $\mathbf{u}$ and $\mathbf{v}$ in $\mathbb{R}^{2}$}
    \end{figure}

    \think{} How to generalize this to more than two vectors?
\end{frame}


\begin{frame}[c]
    \frametitle{Orthogonal sets}

    \only<1>{
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.5\textwidth]{6.2-01.png}
        \end{figure}
    }
    \only<2>{
        A set of vectors $\{\bfu_1, \dots, \bfu_p\}$ in $\dsR^{n}$
        is an \alert{orthogonal set} if $\bfu_i \cdot \bfu_j = 0$
        for all $i \ne j$.

        For example,
        \begin{equation*}
            \bfu_{1} =
            \begin{bmatrix}
                3 \\ 1 \\ 1
            \end{bmatrix}
            ,
            \quad
            \bfu_{2} =
            \begin{bmatrix}
                -1 \\ 2 \\ 1
            \end{bmatrix}
            ,
            \quad
            \bfu_{3} =
            \begin{bmatrix}
                \frac{-1}{2} \\ -2 \\ \frac{7}{2}
            \end{bmatrix}
        \end{equation*}
        form an orthogonal set.

        \cake{} If $\bfv_1 \ne \bfzero$, is the set $\{\bfv_1\}$ an orthogonal set?

        \dizzy{} If $\bfv_1 = \bfzero$, is the set $\{\bfv_1, \dots, \bfv_p\}$ an orthogonal set?
    }
\end{frame}

\begin{frame}
    \frametitle{Theorem 4 --- Bases}

    If $S = \{\bfu_1, \dots, \bfu_p\}$ is an orthogonal set of \emph{non-zero}
    vectors in $\dsR^{n}$,
    then the vectors in $S$ are linearly independent and hence forms a basis for
    the subspace spanned by $S$.
\end{frame}

\begin{frame}
    \frametitle{Orthogonal basis}

    An \alert{orthogonal basis} for a subspace $W$ of $\dsR^n$ is a basis
    that is also an \emph{orthogonal set}.

    \only<1>{
    \cake{} In the following picture, which sets of vectors form \emph{orthogonal bases} for
    $\dsR^{2}$?

    \cake{} Which sets of vectors form bases of $\dsR^{2}$?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.5, 
            every label/.style={black, fill=white, inner sep=1pt},
            every node/.style={black, fill=white, inner sep=1pt}
            ]
            \fill[white] (-6, -1) rectangle (6, 6);

            % Grid
            \draw[step=1, lightgray, very thin] (-5, 0) grid (5, 5);

            % Axes
            \draw[thick, ->] (-5, 0) -- (5, 0) node[right] {$x_1$};
            \draw[thick, ->] (0, 0) -- (0, 5) node[above] {$x_2$};

            % Vector u
            \draw[vector] (0, 0) -- (3, 4) node[right] {$\mathbf{u}$};

            % Vector v
            \draw[vector] (0, 0) -- (-2, 1.5) node[left] {$\mathbf{v}$};

            % Vector u'
            \draw[vector, red] (0, 0) -- (0, 4) node[right] {$\mathbf{u'}$};

            % Vector v
            \draw[vector, red] (0, 0) -- (-2, 0) node[left] {$\mathbf{v'}$};
        \end{tikzpicture}
    \end{figure}
    }
    \only<2>{
    \cake{} What is an orthogonal basis of a subspace of $\Span{\bfu}$?
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.5, 
            every label/.style={black, fill=white, inner sep=1pt},
            every node/.style={black, fill=white, inner sep=1pt}
            ]
            \fill[white] (-6, -1) rectangle (6, 6);

            % Grid
            \draw[step=1, lightgray, very thin] (-5, 0) grid (5, 5);

            % Axes
            \draw[thick, ->] (-5, 0) -- (5, 0) node[right] {$x_1$};
            \draw[thick, ->] (0, 0) -- (0, 5) node[above] {$x_2$};

            % Vector u
            \draw[red, thin] (-1/2, -4/6) -- (3, 4) node[right] {$\Span{\mathbf{u}}$};
            \draw[vector] (0, 0) -- (3/2, 2) node[right] {$\mathbf{u}$};
        \end{tikzpicture}
    \end{figure}
    }
\end{frame}

\begin{frame}{Theorem 5}
    Let $\scB = \{\bfu_1, \dots, \bfu_p\}$ be an \emph{orthogonal basis} of $W$,
    then for all $\bfy \in W$, the we can write
    \begin{equation*}
        \bfy = c_{1} \bfu_1 + \dots + c_p \bfu_p
    \end{equation*}
    where
    \begin{equation*}
        c_{j} = \frac{\bfy \cdot \bfu_{j}}{\bfu_{j}\cdot \bfu_j},
        \qquad
        (j \in \{1, \dots, p\})
    \end{equation*}

    \cake{} Why do we have $\bfu_j \cdot \bfu_j \ne 0$ for all $j \in \{1, \dots, p\}$?

    \cake{} What do we call the vector $\begin{bmatrix} c_1 \\ \vdots \\ c_p
    \end{bmatrix}$?
\end{frame}

\begin{frame}
    \frametitle{Example 2}

    Express
    \begin{equation*}
        \bfy =
        \begin{bmatrix}
            6 \\ 1 \\ -8
        \end{bmatrix}
    \end{equation*}
    as a linear combination of the orthogonal basis
    \begin{equation*}
        \bfu_{1} =
        \begin{bmatrix}
            3 \\ 1 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfu_{2} =
        \begin{bmatrix}
            -1 \\ 2 \\ 1
        \end{bmatrix}
        ,
        \quad
        \bfu_{3} =
        \begin{bmatrix}
            \frac{-1}{2} \\ -2 \\ \frac{7}{2}
        \end{bmatrix}
    \end{equation*}
    forms an orthogonal set.
\end{frame}

\subsection{An Orthogonal Projection}

\begin{frame}
    \frametitle{Projections to a vector}

    \only<1>{
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{projection-2d.png}
    \end{figure}
    }
    \only<2>{
    Give $\bfu, \bfy \in \dsR^{n}$, we want to write
    \begin{equation*}
        \bfy = \bfz + \hat{\bfy}
    \end{equation*}
    such that $\bfz$ is orthogonal to $\bfu$
    and $\hat{\bfy} = \alpha \bfu$ for some $\alpha \in \dsR$.

    The vector $\hat{\bfy}$ is called the \alert{orthogonal
    projection/projection} of $\bfy$ onto $\bfu$.

    The vector $\bfz$ is called the \alert{component of $\bfy$ orthogonal to $\bfu$}.
    }
\end{frame}

\begin{frame}
    \frametitle{Compute the projection}

    We can compute the projection of $\bfy$ to $\bfu$ by
    \begin{equation*}
        \hat{\bfy}
        =
        \frac{\bfy \cdot \bfu}{\bfu \cdot \bfu}
        \bfu
        .
    \end{equation*}

    Proof --- Write $\bfz = \bfy - \alpha \bfu$. Then we must have
    \begin{flalign*}
        \blankveryshort{}
        =
        \bfz \cdot \bfu
        =
        (\bfy - \alpha \bfu) \cdot \bfu
        =
        &&
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Projection to $c \bfu$}
    
    \cake{} Why for any $c \ne 0$, $\hat{\bfy}$, the projection of $\bfy$ to $c \bfu$ is the
    same?
\end{frame}

\begin{frame}
    \frametitle{Projections to a subspace}
    Thus for the subspace $L = \Span{\bfu}$, and we may define
    \begin{equation*}
        \proj_{L} \bfy =
        \frac{\bfy \cdot \bfu}{\bfu \cdot \bfu}
        \bfu
        .
    \end{equation*}

    \begin{figure}
        \centering
        \includegraphics[width=0.6\linewidth]{projection-subspace.png}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Projection}
%
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.8\linewidth]{projection.png}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Find the orthogonal projection of $\bfy$ to $\bfu$ where
    \begin{equation*}
        \bfy =
        \begin{bmatrix}
            7 \\ 6
        \end{bmatrix}
        ,
        \qquad
        \bfu =
        \begin{bmatrix}
            4 \\ 2
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{Theorem 5 --- A geometric interpretation}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Let $\{\bfu_1, \dots, \bfu_p\}$ be an orthogonal basis of $W$,
            then for all $\bfy \in W$, the weights in
            \begin{equation*}
                \bfy = \hat{\bfy}_1 + \dots + \hat{\bfy}_p
            \end{equation*}
            where for all $j$
            \begin{equation*}
                \hat{\bfy}_{j} = \frac{\bfy \cdot \bfu_{j}}{\bfu_{j}\cdot
                \bfu_j} \bfu_j.
            \end{equation*}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\linewidth]{projection-r2.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Orthonormal Sets}

    A set $\{\bfu_{1},\dots,\bfu_{p}\}$ is an \alert{orthonormal set} if it is an \alert{orthogonal set of unit vectors}.

    \begin{block}{\hint{} How to remember the word}
        \begin{center}
            Orthonormal = Orthogonal + Normal
        \end{center}
    \end{block}

    %\begin{columns}
    %    \begin{column}{0.3\textwidth}
    %        \begin{figure}[htpb]
    %            \centering
    %            \includegraphics[width=\linewidth]{projection-orthonormal.png}
    %        \end{figure}
    %    \end{column}
    %    \begin{column}{0.7\textwidth}
    %
    %    \end{column}
    %\end{columns}
\end{frame}

\subsection{Orthonormal Sets}

\begin{frame}
    \frametitle{Example 5}
    
    \think{} How to show the following vectors form an \alert{orthonormal set} ---
    \begin{equation*}
        \bfv_{1}
        =
        \frac{1}{\sqrt{11}}
        \begin{bmatrix}
            3 \\
            1 \\
            1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2}
        =
        \frac{1}{\sqrt{6}}
        \begin{bmatrix}
            -1 \\
            2 \\
            1 \\
        \end{bmatrix}
        ,
        \qquad
        \bfv_{3}
        =
        \frac{1}{\sqrt{66}}
        \begin{bmatrix}
            -1 \\
            -4 \\
            7 \\
        \end{bmatrix}
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Example 6}

    Let
    \begin{equation*}
        U
        =
        \left[
            \begin{array}{cc}
                \frac{\sqrt{2}}{2} & \frac{2}{3} \\
                \frac{\sqrt{2}}{2} & \frac{-2}{3} \\
                0 & \frac{1}{3} \\
            \end{array}
        \right]
        ,
        \qquad
        \bfx
        =
        \left[
            \begin{array}{c}
                \sqrt{2} \\
                3 \\
            \end{array}
        \right]
        .
    \end{equation*}
    Note that $U$ has \alert{orthonormal columns}, i.e., its columns form a
    \emph{orthonormal set}.

    \cake{} What are $U^T U$? 

    \cake{} What are $\norm{U \bfx}$ and $\norm{\bfx}$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 6 --- Orthonormal columns}

    An $m \times n$ matrix $U$ has orthonormal columns if and only if $U^T U =
    \blankshort{}$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 7 --- Orthonormal columns}

    Let $U$ be an $m \times n$ matrix with orthonormal columns,
    and let $x$ and $y$ be in $\dsR^n$.
    Then
    \begin{enumerate}[a.]
        \item $\norm{U \bfx} = \blankshort{}$.
        \item $(U \bfx) \cdot (U \bfy) = \bfx \cdot \bfy$.
        \item $(U \bfx) \cdot (U \bfy) = 0$ if and only if $\bfx \cdot \bfy = 0$.
    \end{enumerate}
    \cake{} Why does (b) imply (a)?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Prove the following theorem ---

    \begin{block}{Theorem 7}
    Let $U$ be an $m \times n$ matrix with orthonormal columns,
    and let $x$ and $y$ be in $\dsR^n$.
    Then
    \begin{enumerate}
        \item[b.] $(U \bfx) \cdot (U \bfy) = \bfx \cdot \bfy$.
    \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Orthogonal matrix}

    An \alert{orthogonal matrix} is a \emph{square} matrix $U$ with $U^{-1} = U^{T}$.

    \only<1>{
        For example, the following are all \emph{orthogonal matrices} ---
        \begin{equation*}
            \begin{bmatrix}
                1 & 0 \\
                0 & 1
            \end{bmatrix}
            ,
            \qquad
            \begin{bmatrix}
                \cos(\theta) & \sin(\theta) \\
                -\sin(\theta) & \cos(\theta)
            \end{bmatrix}
        \end{equation*}

        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[
                scale=1.5, 
                every label/.style={black, fill=white, inner sep=1pt},
                every node/.style={black, fill=white, inner sep=1pt}
                ]
                \fill[white] (-1.5, -0.5) rectangle (1.5, 1.5);

                % Grid
                \draw[step=1, lightgray, very thin] (-1.5, -0.5) grid (1.5, 1.5);

                % Axes
                \draw[thick] (-1.5, 0) -- (1.5, 0) node[right] {$x_1$};
                \draw[thick] (0, -0.5) -- (0, 1.5) node[above] {$x_2$};

                % Vector e1
                \draw[->, green, very thick] (0, 0) -- (1, 0) node[right] {$\mathbf{e_1}$};

                % Vector e2
                \draw[->, green, very thick] (0, 0) -- (0, 1) node[above] {$\mathbf{e_2}$};

                % Vector u1
                \draw[->, blue, very thick] (0, 0) -- ([rotate around={45:(0,0)}]1,0) node[above right] {$\mathbf{u_1}$};

                % Vector u2
                \draw[->, blue, very thick] (0, 0) -- ([rotate around={45:(0,0)}]0,1) node[above left] {$\mathbf{u_2}$};
            \end{tikzpicture}
            \caption*{Vectors $\mathbf{e_1}$, $\mathbf{e_2}$, $\mathbf{u_1}$, and $\mathbf{u_2}$ in $\mathbb{R}^{2}$}
        \end{figure}
    }
    \only<2->{
        Note that an \emph{orthogonal matrix} has \emph{orthonormal columns} by
        theorem 6.

        \begin{block}{Theorem 6}
            An $m \times n$ matrix $U$ has orthonormal columns if and only if $U^T U = I$.
        \end{block}
    }
    \only<3>{
        \begin{block}{\tps}
            \cake{} Is every matrix with \emph{orthonormal columns}
            an \emph{orthogonal matrix}?
        \end{block}
    }
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{6.2}{33, 35, 37, 39, 41}
        \end{column}
    \end{columns}
\end{frame}
\end{document}
