\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Eigenvectors and Linear Transformations, Discrete Dynamical System}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{5.5 Complex Eigenvalues}

\begin{frame}
    \frametitle{Complex eigenvalues and eigenvectors}
    
    The matrix eigenvalue--eigenvector theory already developed for
    $\mathbb{R}^n$ applies equally well to $\mathbb{C}^n$. 

    So a complex scalar $\lambda$ satisfies $\det(A-\lambda I)=0$ 
    if and only if there is a nonzero vector $\bfx$ in $\mathbb{C}$ such that $A
    \bfx=\lambda \bfx$.

    We call $\lambda$ a \alert{(complex) eigenvalue}
    and $x$ a \alert{(complex) eigenvector}  corresponding to $\lambda$.
\end{frame}

\begin{frame}
    \frametitle{Example 1}
    
    Let $A = \begin{bmatrix} 0 & -1 \\ 1 & 0 \end{bmatrix}$.
    Find the eigenvalues of $A$, and find a basis for each eigenspace.
\end{frame}

\begin{frame}
    \frametitle{Example 2}
    
    Let $A = \begin{bmatrix} \frac{1}{2} & - \frac{3}{5} \\ \frac{3}{4} &
    \frac{11}{10} \end{bmatrix}$.
    Find the eigenvalues of $A$, and find a basis for each eigenspace.
\end{frame}

\begin{frame}
    \frametitle{Example 3}
    Let $A$ be as in Example 2 and let $\bfx_{0} = \begin{bmatrix} 2 \\ 0
    \end{bmatrix}$. 

    Let 
    \begin{equation*}
        \bfx_{k+1} = A \bfx_{k}\qquad(k \in \{0, 1, \dots\}).
    \end{equation*}

    \think{} If we plot $\{x_{k}\}$, we get an elliptical orbit!
    Why?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{5-5-fig-1.png}
    \end{figure}
\end{frame}

\subsection{Real and Imaginary Parts of Vectors}

\begin{frame}
    \frametitle{Complex conjugates of complex vectors}

    The \alert{complex conjugate}  of a complex vector $x$ in $\mathbb{C}^n$ is the
    vector $\overline{\bfx}$ in $\mathbb{C}^n$ whose entries are the complex conjugates
    of the entries in $x$. 

    \cake{} 
    Let $\bfx = \begin{bmatrix} 1 + 5 i \\ 3 - 2i  \end{bmatrix}$.
    What is
    \begin{equation*}
        \overline{\bfx} = \blankshort{}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Properties of complex conjugates}

    If $B$ is an $m \times n$ matrix with possibly complex entries, then
    $\overline{B}$ denotes the matrix
    whose entries are the complex conjugates of the entries in $B$. 

    Properties of conjugates
    for complex numbers carry over to complex matrix algebra:
    \begin{align*}
        \overline{r \times \bfx{}} &= \overline{r}\times \overline{\bfx{}}, \\
        \overline{B\times  \bfx{}} &= \overline{B}\times \overline{\bfx{}}, \\
        \overline{B\times C} &= \overline{B}\times \overline{C}, \\
        \overline{r\times B} &= \overline{r}\times \overline{B}.
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Real and imaginary parts of complex vectors}

    The \alert{real and imaginary}  parts of a complex vector $x$
    are the vectors $\operatorname{Re}(x)$ and $\operatorname{Im}(x)$ in
    $\mathbb{R}^n$ formed from the real and imaginary parts of the entries of
    $x$.

    Let $\bfx = \begin{bmatrix} 1 + 5 i \\ 3 - 2i  \end{bmatrix}$.
    What are
    \begin{equation*}
        \operatorname{Re}{\bfx} = \blankshort{}
    \end{equation*}
    and
    \begin{equation*}
        \operatorname{Im}{\bfx} = \blankshort{}
    \end{equation*}
\end{frame}

\subsection{Eigenvalues and Eigenvectors of a Real Matrix That Acts on
\texorpdfstring{$\dsC^n$}{C^n}}

\begin{frame}
    \frametitle{Conjugate of complex eigenvalues}

    Let $A$ be an $n \times n$ matrix whose entries are \emph{real}. 
    Then $\overline{A\bfx{}} = \overline{A}\overline{\bfx{}} = A\overline{\bfx{}}$. 

    If $\lambda$ is an eigenvalue of $A$ and $\bfx{}$ is a corresponding eigenvector in $\mathbb{C}^n$, then
    \begin{equation*}
        A\overline{\bfx{}} = 
        \overline{A} \overline{\bfx{}}
        = \overline{A \bfx{}}
        = \overline{\lambda\bfx{}}
        = \overline{\lambda}\overline{\bfx{}}
    \end{equation*}
    Hence $\overline{\lambda}$ is also an eigenvalue of $A$, with $\overline{\bfx{}}$ a corresponding eigenvector. 

    This shows that when $A$ is real, its complex eigenvalues occur in \emph{conjugate pairs}. 
\end{frame}

\begin{frame}
    \frametitle{Example 5}
    In Example 2, we have $A = \begin{bmatrix} \frac{1}{2} & - \frac{3}{5} \\ \frac{3}{4} &
    \frac{11}{10} \end{bmatrix}$.

    Recall that $\lambda_{1} = \frac{4}{5} - \frac{3}{5} i$ is an eigenvalue
    of $A$ 
    whose eigenspace has a basis
    \begin{equation*}
        \bfv_{1}
        =
        \begin{bmatrix} 
            -2 - 4i \\
            5
        \end{bmatrix} 
    \end{equation*}

    \cake{} Then $A$ has another eigenvalue
    \begin{equation*}
        \lambda_{2}
        =
        \blankshort{}
    \end{equation*}
    whose eigenspace has a basis
    \begin{equation*}
        \bfv_{2}
        =
        \begin{bmatrix} 
            \blankshort{} \\
            \blankshort{}
        \end{bmatrix} 
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 6}

    Let $C = \begin{bmatrix} a & -b \\ b & a \end{bmatrix}$ where $a, b \in
    \dsR$.

    \cake{} Then the eigenvalues of $C$ are
    \begin{equation*}
        \lambda = \blankshort{}
    \end{equation*}

    Let $r = \abs{\lambda} = \blankshort{}$.
    Then we have
    \begin{flalign*}
        C = 
        &&
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Example 6 in pictures}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{5-5-fig-2.png}
    \end{figure}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{5-5-fig-3.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 7}

    In Example 2, we have $A = \begin{bmatrix} \frac{1}{2} & - \frac{3}{5} \\ \frac{3}{4} &
    \frac{11}{10} \end{bmatrix}$.
    Recall $A$ has an eigenvector
    \begin{equation*}
        \bfv_{1}
        =
        \begin{bmatrix} 
            -2 - 4i \\
            5
        \end{bmatrix} 
    \end{equation*}
    corresponding to the eigenvalue
    $\lambda_{1} = \frac{4}{5} - \frac{3}{5} i$.

    Let $P = 
    \begin{bmatrix} 
        \operatorname{Re} \bfv_1 &  
        \operatorname{Im} \bfv_{1}
    \end{bmatrix}
    =
    \begin{bmatrix} 
        -2 & -4 \\
        5 & 0
    \end{bmatrix}
    $ 
    and
    \begin{equation*}
        C = P^{-1} A P
        =
        \begin{bmatrix}
            \frac{4}{5} & -\frac{3}{5} \\
            \frac{3}{5} & \frac{4}{5} \\
        \end{bmatrix}
    \end{equation*}
    Then $A = P C P^{-1}$.
\end{frame}

\begin{frame}
    \frametitle{Example 7}
    \think{} How does these explain the following elliptical orbit ---

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.6\textwidth]{5-5-fig-1.png}
        \includegraphics<2>[width=\textwidth]{5-5-fig-4.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 9}
    Let $A$ be a real $2 \times 2$ matrix with a complex eigenvalue $\lambda = a + bi$ ($b \neq 0$) and
    an associated eigenvector $\mathbf{v}$ in $\mathbb{C}^2$.

    Then
    \begin{equation*}
        A = P 
        \begin{bmatrix}
            a & -b \\
            b & a
        \end{bmatrix} P^{-1},
    \end{equation*}
    where
    \[
        P = \begin{bmatrix}
            \operatorname{Re} \mathbf{v} & \operatorname{Im} \mathbf{v}
        \end{bmatrix}
    \]
\end{frame}

\section{5.6 Discrete Dynamical System}

\subsection{A Predator-Prey System}

\begin{frame}
    \frametitle{A Predator-Prey System}

    Let $O_k$ and $R_k$ the population of \emoji{owl} and \emoji{mouse} in month
    $k$. Suppose
    \begin{equation*}
        \bfx_{k+1} =
        \begin{bmatrix}
            O_{k+1} \\ R_{k+1}
        \end{bmatrix}
        =
        \begin{bmatrix}
            .5 & .4 \\
            -.104 & 1.1 \\
        \end{bmatrix}
        \begin{bmatrix}
            O_{k} \\ R_{k}
        \end{bmatrix}
        =
        A \bfx_{k}
        \qquad
        (k \in \{0, 1, \dots\})
        .
    \end{equation*}
    \think{} How will the populations change over the time?

    \pause{}
    Using that $A$ has eigenvalues $1.02$ and $.58$, and eigenvectors
    \begin{equation*}
        \bfv_{1}
        =
        \begin{bmatrix}
            10 \\ 13
        \end{bmatrix}
        ,
        \qquad
        \bfv_{2}
        =
        \begin{bmatrix}
            5 \\ 1
        \end{bmatrix}
        ,
    \end{equation*}
    and $\bfx_{0} = c_{1} \bfv_{1} + c_{2} \bfv_{2}$, we have
    \pause{}
    \begin{equation*}
        \bfx_{k}
        =
        c_1
        (1.02)^{k}
        \bfv_{1}
        +
        c_2
        (.58)^{k}
        \bfv_{2}
        \approx
        c_1 (1.02)^{k}
        \bfv_{1}
        \approx
        1.02
        \bfx_{k-1}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The general case}
    Consider a dynamic system of the form $\bfx_{k+1} = A \bfx_{k}$.
    If $A$ has eigenvalues satisfying $\abs{\lambda_1}> 1$ and
    $1 > \abs{\lambda_j}$ for $j \ge 2$, then
    \begin{equation*}
        \bfx_{k+1}
        \approx
        \lambda_{1} \bfx_{k}
        ,
    \end{equation*}
    and
    \begin{equation*}
        \bfx_{k}
        \approx
        c_{1}
        \lambda_{1}^{k} \bfv_{1}
        .
    \end{equation*}
\end{frame}

\subsection{Graphic Descriptions of Solutions}

\begin{frame}
    \frametitle{Example 2}

    Let
    \begin{equation*}
        A =
        \begin{bmatrix}
            .8 & 0 \\
            0 & .64 \\
        \end{bmatrix}
        ,
        \qquad
        \bfx_{k+1} = A \bfx
        ,
        \qquad
        \bfx_{0} =
        \begin{bmatrix}
            c_1 \\
            c_2 \\
        \end{bmatrix}
        .
    \end{equation*}

    Then the eigenvalues of $A$ are $.8$ and $.64$, 
    with eigenvectors
    $\bfv_1 = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$
    and
    $\bfv_2 = \begin{bmatrix} 0 \\ 1 \end{bmatrix}$
    .

    \think{} What is
    \begin{equation*}
        \lim_{k \to 0} \bfx_{k} = \blankshort{}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2 in a picture}
    Thus we say $\bfzero$ is an \alert{attractor} of the system.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{attractor.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 3}

    Let
    \begin{equation*}
        A =
        \begin{bmatrix}
            1.44 & 0 \\
            0 & 1.2 \\
        \end{bmatrix}
        ,
        \qquad
        \bfx_{k+1} = A \bfx
        ,
        \qquad
        \bfx_{0} =
        \begin{bmatrix}
            c_1 \\
            c_2 \\
        \end{bmatrix}
        .
    \end{equation*}

    Then
    \begin{equation*}
        \bfx_{k}
        =
        c_1 (1.44)^k
        \begin{bmatrix}
            1 \\0
        \end{bmatrix}
        +
        c_2 (1.2)^k
        \begin{bmatrix}
            0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}

    \cake{} What is
    \begin{equation*}
        \lim_{k \to 0} \bfx_{k} = \blankshort{}
    \end{equation*}
    if $c_{1} > 0, c_{2} = 0$?

    \cake{} What is
    \begin{equation*}
        \lim_{k \to 0} \bfx_{k} = \blankshort{}
    \end{equation*}
    if $c_{1} = 0, c_{2} < 0$?
\end{frame}

\begin{frame}
    \frametitle{Example 3 in a picture}
    So $\bfzero$ is an \alert{repeller} of the system.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{repeller.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 4}

    Let
    \begin{equation*}
        A =
        \begin{bmatrix}
            2.0 & 0 \\
            0 & 0.5 \\
        \end{bmatrix}
        ,
        \qquad
        \bfx_{0} =
        \begin{bmatrix}
            c_1 \\
            c_2 \\
        \end{bmatrix}
        .
    \end{equation*}
    Then
    \begin{equation*}
        \bfx_{k}
        =
        c_1 (2.0)^k
        \begin{bmatrix}
            1 \\0
        \end{bmatrix}
        +
        c_2 (.5)^k
        \begin{bmatrix}
            0 \\ 1
        \end{bmatrix}
        .
    \end{equation*}

    \cake{} What is
    \begin{equation*}
        \lim_{k \to 0} \bfx_{k} = \blankshort{}
    \end{equation*}
    if $c_{1} > 0, c_{2} > 0$?

    \cake{} What is
    \begin{equation*}
        \lim_{k \to 0} \bfx_{k} = \blankshort{}
    \end{equation*}
    if $c_{1} > 0, c_{2} < 0$?
\end{frame}

\begin{frame}
    \frametitle{Example 4 in a picture}

    So $\bfzero$ is a \alert{saddle point} of the system.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{saddle-point.png}
    \end{figure}
\end{frame}

\subsection{Change of variables}

\begin{frame}
    \frametitle{Change of variables}

    Suppose that $A = P D P^{-1}$ and $\bfx_{k+1} = A \bfx_{k}$.

    Letting $\bfy_{k} = P^{-1} \bfx_{k}$, we have
    \begin{equation*}
        \bfy_{k+1} = D \bfy_{k}.
    \end{equation*}

    \pause{}
    Thus,
    \begin{equation*}
        \bfy_{k+1}
        =
        \begin{bmatrix}
            y_{1}(k+1) \\
            y_{2}(k+1) \\
            \vdots \\
            y_{n}(k+1) \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            \lambda_1 & 0 & \dots & 0 \\
            0 & \lambda_2 &  & \vdots \\
            \vdots & &  \ddots & 0 \\
            0 & \dots &  0 & \lambda_{n} \\
        \end{bmatrix}
        \begin{bmatrix}
            y_{1}(k) \\
            y_{2}(k) \\
            \vdots \\
            y_{n}(k) \\
        \end{bmatrix}
        =
        \begin{bmatrix}
            \lambda_{1} y_{1}(k) \\
            \lambda_{2} y_{2}(k) \\
            \vdots \\
            \lambda_{n} y_{n}(k) \\
        \end{bmatrix}
        .
    \end{equation*}
    So changing variables from $\bfx_{k}$ to $\bfy_{k}$ has \emph{decoupled} the
    system,

    In other words, how $y_{1}(k)$ evolves is not affected by $y_{2}(k), \dots,
    y_{n}(k)$.
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    Let
    \begin{equation*}
        A =
        \begin{bmatrix}
            1.25 & -.75 \\
            .75 & 1.25 \\
        \end{bmatrix}
        ,
        \qquad
        \bfx_{k+1} = A \bfx.
    \end{equation*}
    Then $A$ has eigenvalues $2$ and $.5$,
    with corresponding eigenvectors 
    $\bfv_{1} = \begin{bmatrix} 1 \\ -1 \end{bmatrix}$
    and
    $\bfv_{2} = \begin{bmatrix} 1 \\ 1 \end{bmatrix}$.

    Thus $A = P D P^{-1}$ where
    \begin{equation*}
        P = \begin{bmatrix} \bfv_{1} & \bfv_{2} \end{bmatrix},
        \qquad
        D
        =
        \begin{bmatrix}
            2 & 0 \\
            0 & .5
        \end{bmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 5 --- Continue}
    Thus, changing variable with
    $
    \bfx_{k} =
    \begin{bmatrix}
        \bfv_{1} & \bfv_{2}
    \end{bmatrix}
    \bfy_{k}
    ,
    $
    we have
    \begin{equation*}
        \bfy_{k}
        =
        \begin{bmatrix}
            2^{k} c_{1} \\
            (.5)^{k} c_{2}
        \end{bmatrix}
    \end{equation*}
    for any
    $
    \bfy_{0}
    =
    \begin{bmatrix}
        c_{1} \\
        c_{2}
    \end{bmatrix}
    $

    Thus
    \begin{equation*}
        \bfx_{k}
        =
        c_1 (2.0)^k
        \begin{bmatrix}
            1 \\ -1
        \end{bmatrix}
        +
        c_2 (.5)^k
        \begin{bmatrix}
            1 \\ 1
        \end{bmatrix}
        =
        c_1 (2.0)^k
        \bfv_{1}
        +
        c_2 (.5)^k
        \bfv_{2}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 5 in a picture}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{change-of-variable.png}
    \end{figure}
\end{frame}
%
%\begin{frame}
%    \frametitle{Complex eigenvalues}
%
%    A real matrix can have complex eigenvalues.
%    For example, let
%    \begin{equation*}
%        A =
%        \begin{bmatrix}
%             .8 & .5 \\
%            -.1 & 1.0 \\
%        \end{bmatrix}
%        ,
%        \qquad
%        \bfx_{k+1} = A \bfx_{k}.
%    \end{equation*}
%    Then
%    \begin{equation*}
%        \bfx_{k}
%        =
%        c_1 (.9+.2i)^k
%        \begin{bmatrix}
%            1 - 2i \\ 1
%        \end{bmatrix}
%        +
%        c_2 (.9-.2i)^k
%        \begin{bmatrix}
%            1 + 2i \\ 1
%        \end{bmatrix}
%        .
%    \end{equation*}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.4\linewidth]{complex-system.png}
%    \end{figure}
%\end{frame}

%\section{Applications to Differential equations}
%
%\begin{frame}
%    \frametitle{Systems of differential equations}
%
%    Let $A$ be a $n \times n$ matrix.
%    Consider a matrix differential equation
%    \begin{equation*}
%        \bfx'(t) = A \bfx(t),
%    \end{equation*}
%    where
%    \begin{equation*}
%        \bfx(t)
%        =
%        \begin{bmatrix}
%            x_1(t) \\ \vdots \\ x_n(t)
%        \end{bmatrix}
%        ,
%        \qquad
%        \bfx'(t)
%        =
%        \begin{bmatrix}
%            x_1'(t) \\ \vdots \\ x_n'(t)
%        \end{bmatrix}
%        \qquad
%    \end{equation*}
%    The solution set is a \emph{subspace} of all continuous functions with values in
%    $\dsR^{n}$.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Decoupled system}
%
%    If $A$ is diagonal, then
%    \begin{equation*}
%        \bfx'(t) = A \bfx(t)
%    \end{equation*}
%    is \emph{decoupled}.
%
%    For example, the system
%    \begin{equation*}
%        \begin{bmatrix}
%            x_1'(t) \\
%            x_2'(t) \\
%        \end{bmatrix}
%        =
%        \begin{bmatrix}
%            3 & 0 \\
%            0 & -5 \\
%        \end{bmatrix}
%        \begin{bmatrix}
%            x_1(t) \\
%            x_2(t) \\
%        \end{bmatrix}
%    \end{equation*}
%    is decoupled.
%
%    \pause{}
%
%    It is easy to verify
%    \begin{equation*}
%        \begin{bmatrix}
%            x_1'(t) \\
%            x_2'(t) \\
%        \end{bmatrix}
%        =
%        c_1
%        \begin{bmatrix}
%            1 \\ 0
%        \end{bmatrix}
%        e^{3t}
%        +
%        c_2
%        \begin{bmatrix}
%            0 \\ 1
%        \end{bmatrix}
%         e^{-5t}
%         .
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{The general case}
%
%    Assume that $\lambda$ is an eigenvalue of $A$ and $\bfv$ is a corresponding
%    eigenvector.
%    Then $\bfx(t) = \bfv e^{\lambda t}$ is a solution since
%    \begin{equation*}
%        A \bfx(t) = A \bfv e^{\lambda t} = \lambda \bfv e^{\lambda t} = \bfx'(t).
%    \end{equation*}
%
%    The function $\bfv e^{\lambda t}$ is called an eigenfunction.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example}
%
%    Let
%    $
%        A =
%        \begin{bmatrix}
%            -1.5 & .5 \\
%            1 & -1
%        \end{bmatrix}
%        .
%    $
%    Then
%    $
%        \bfx'(t) = A \bfx(t)
%    $
%    has solutions of the form
%    \begin{equation*}
%        \bfx(t) =
%        c_{1} \bfv_{1} e^{\lambda_{1}t }
%        +
%        c_{2} \bfv_{2} e^{\lambda_{2}t }
%        ,
%    \end{equation*}
%    where
%    \begin{equation*}
%        \lambda_1 = -.5, \quad
%        \lambda_2 = -2, \quad
%        \bfv_1 =
%        \begin{bmatrix}
%            1 \\ 2
%        \end{bmatrix}
%        ,
%        \quad
%        \bfv_2
%        \begin{bmatrix}
%            -1 \\ 1
%        \end{bmatrix}
%        .
%    \end{equation*}
%
%    \pause{}
%
%    When $t = 0$,
%    \begin{equation*}
%        \bfx(0) = c_{1} \bfv_{1} + c_{2} \bfv_{2}
%        =
%        \begin{bmatrix}
%            \bfv_1 & \bfv_2
%        \end{bmatrix}
%        \begin{bmatrix}
%            c_1 \\ c_2
%        \end{bmatrix}
%        .
%    \end{equation*}
%    Thus,
%    $
%        \begin{bmatrix}
%            c_1 \\ c_2
%        \end{bmatrix}
%        =
%        \begin{bmatrix}
%            \bfv_1 & \bfv_2
%        \end{bmatrix}^{-1}
%        \bfx(0)
%        .
%    $
%\end{frame}
%
%\begin{frame}
%    \frametitle{The picture}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\linewidth]{differential-equations.png}
%    \end{figure}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Decoupling}
%
%    If $A = P D P^{-1}$ where $D$ is diagonal, then we can change variable by letting
%    \begin{equation*}
%        \bfy(t) = P^{-1} \bfx(t).
%    \end{equation*}
%    This implies the easily solvable
%    \begin{equation*}
%        \bfy' = D \bfy.
%    \end{equation*}
%    \pause{}
%    And the solution of the original equations is of the form
%    \begin{equation*}
%        \bfx(t) = P \bfy(t).
%    \end{equation*}
%\end{frame}
%
%\appendix{}
%
%\begin{frame}[c]
%    \frametitle{Reference}
%
%    \begin{itemize}
%        \item \emoji{books}
%            The lecture covers Chapter 5.6-5.7 of
%            \href{https://www.pearson.com/us/higher-education/program/Lay-My-Lab-Math-with-Pearson-e-Text-Access-Card-for-Linear-Algebra-and-its-Applications-18-Weeks-6th-Edition/PGM2788183.html}{Linear Algebra And Its Applications}
%            by Lay, Lay and McDonald.
%    \end{itemize}
%\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{5.5}{11, 19, 21, 27, 29}
            \sectionhomework{5.6}{7, 9, 11, 15, 17}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
