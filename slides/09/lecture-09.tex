\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Matrix Factorizations}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{2.5 Matrix Factorizations}

\subsection{The LU Factorization}

\begin{frame}[c]
    \frametitle{The LU factorization}
    A \alert{factorization}  of a matrix $A$ is an equation that expresses 
    $A$ as a product of two or more matrices. 

    %If $A$ can be row reduced to echelon form \emph{without row interchanges},
    %then it has an \alert{LU factorization}.

    For example, the following is called an \alert{LU factorization} of $A$.
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            1 & 0 & 0 & 0 \\
            \ast & 1 & 0 & 0 \\
            \ast & \ast & 1 & 0 \\
            \ast & \ast & \ast & 1
        \end{bmatrix}
        \begin{bmatrix}
            \blacksquare & \ast & \ast & \ast & \ast & \ast \\
            0 & \blacksquare & \ast & \ast & \ast & \ast \\
            0 & 0 & 0 & \blacksquare & \ast & \ast \\
            0 & 0 & 0 & 0 & 0 & 0
        \end{bmatrix}
        =
        L U
    \end{equation*}
    %\begin{figure}[htpb]
    %    \centering
    %    \includegraphics[width=0.8\linewidth]{LU-01.png}
    %\end{figure}
    where the matrix $L$ is said to be a \alert{unit lower triangular matrix},
    and $U$ is a matrix in echelon form.
\end{frame}

\begin{frame}
    \frametitle{A few questions about LU factorization}
    Assume that
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            1 & 0 & 0 & 0 \\
            \ast & 1 & 0 & 0 \\
            \ast & \ast & 1 & 0 \\
            \ast & \ast & \ast & 1
        \end{bmatrix}
        \begin{bmatrix}
            \blacksquare & \ast & \ast & \ast & \ast & \ast \\
            0 & \blacksquare & \ast & \ast & \ast & \ast \\
            0 & 0 & 0 & \blacksquare & \ast & \ast \\
            0 & 0 & 0 & 0 & 0 & 0
        \end{bmatrix}
        =
        L U
    \end{equation*}

    \cake{} If $A$ is of size $m \times n$, what are the sizes of $L$ and $U$?

    \cake{} The matrix $L$ is invertible. Why?
\end{frame}

\begin{frame}
    \frametitle{\emoji{fire} Motivations}
    
    In industry applications, it is common to have to solve
    \begin{equation*}
        A \bfx = \bfb_1, \, A \bfx = \bfb_2, \, \dots, \, A \bfx = \bfb_p
    \end{equation*}
    where $A$ is fixed but $\bfb_{1}, \dots, \bfb_{p}$ are different.

    \think{} How does LU factorization help?
\end{frame}

\begin{frame}
    \frametitle{Example 1}

    We are given
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            3 & -7 & -2 & 2\\
            -3 & 5 & 1 & 0\\
            6 & -4 & 0 & -5\\
            -9 & 5 & -5 & 12\\
        \end{bmatrix}
        =
        \begin{bmatrix}
            1 & 0 & 0 & 0\\
            -1 & 1 & 0 & 0\\
            2 & -5 & 1 & 0\\
            -3 & 8 & 3 & 1\\
        \end{bmatrix}
        \begin{bmatrix}
            3 & -7 & -2 & 2\\
            0 & -2 & -1 & 2\\
            0 & 0 & -1 & 1\\
            0 & 0 & 0 & -1\\
        \end{bmatrix}
        =
        L U
    \end{equation*}

    How does this simplify solving $A \bfx = \bfb$ where $\bfb = \begin{bmatrix}
    -9 \\ 5 \\ 7 \\ 11 \end{bmatrix}$?
\end{frame}

\subsection{An LU Factorization Algorithm}

\begin{frame}
    \frametitle{Unit lower triangular elementary matrices}

    \cake{} How to get the following elementary matrices
    \begin{equation*}
        E_{1}
        =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                -2 & 1 & 0 \\
                0 & 0 & 1 \\
            \end{array}
        \right]
        ,
        \quad
        E_{2} =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                0 & 1 & 0 \\
                0 & 5 & 1 \\
            \end{array}
        \right]
        ,
        \quad
        E_{3} =
        \left[
            \begin{array}{ccc}
                0 & 1 & 0 \\
                1 & 0 & 0 \\
                0 & 0 & 1 \\
            \end{array}
        \right]
    \end{equation*}
    from
    \begin{equation*}
        I_{3} =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                0 & 1 & 0 \\
                0 & 0 & 1 \\
            \end{array}
        \right]
    \end{equation*}
    \cake{} Which of them are also \emph{unit lower triangular}?

    \cake{} What makes an elementary matrix \emph{unit lower triangular}?
\end{frame}

\begin{frame}
    \frametitle{Properties of unit lower triangular elementary matrices}
    Let
    \begin{equation*}
        E_{1}
        =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                -2 & 1 & 0 \\
                0 & 0 & 1 \\
            \end{array}
        \right]
        \only<2>{
        ,
        \quad
        E_{2} =
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                0 & 1 & 0 \\
                0 & 5 & 1 \\
            \end{array}
        \right]
        }
    \end{equation*}
    \cake{} Then what is
    \begin{equation*}
        \only<1>{
        E_{1}^{-1}
        =
        \left[
            \phantom{
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    -2 & 1 & 0 \\
                    0 & 0 & 1 \\
                \end{array}
            }
        \right]
        }
        \only<2>{
        E_2 E_1 
        =
        \left[
            \phantom{
                \begin{array}{ccc}
                    1 & 0 & 0 \\
                    -2 & 1 & 0 \\
                    0 & 0 & 1 \\
                \end{array}
            }
        \right]
        }
    \end{equation*}
    \only<1>{\cake{} What does this say about the inverse of an elementary
        matrix that is also \emph{unit lower triangular}?}
    \only<2>{\cake{} What does this say about the product of elementary
        matrices that are also \emph{unit lower triangular}?}
\end{frame}

\begin{frame}
    \frametitle{The idea of the algorithm}

    Assume that there exist \emph{unit lower triangular elementary} matrices
    $E_1, \dots, E_p$ such that
    \begin{equation*}
        E_p \dots E_1 A = U
    \end{equation*}
    where $U$ is in echelon form.

    \think{} How can we get a LU factorization of $A$ from this?
\end{frame}

\begin{frame}[c]
    \frametitle{The algorithm}

    To find a LU factorization of $A$ ---
    \begin{enumerate}
        \item First reduce $A$ to an echelon form $U$ using \emph{only} row
            \emph{replacements} which add the multiple of row to a row lower
            than it.
        \item Then find $L$ which is reduced to $I$ using the same operations.
    \end{enumerate}

    \bomb{} Step 1 is not always possible.
\end{frame}

\begin{frame}[c]
    \frametitle{Example 2}

    \only<1>{
        \begin{equation*}
            A
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                -4 & -5 & 3 & -8 & 1 \\
                2 & -5 & -4 & 1 & 8 \\
                -6 & 0 & 7 & -3 & 1 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L
            =
            \begin{bmatrix}
                1 &   &   &   \\
                \underline{\phantom{-2}} & 1 &   &      \\
                \underline{\phantom{+1}} &   & 1 &      \\
                \underline{\phantom{-3}} &   &   & 1    \\
            \end{bmatrix}
        \end{equation*}
    }

    \only<2>{
        \begin{equation*}
            A_{1}
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                0 & 3 & 1 & 2 & -3 \\
                0 & -9 & -3 & -4 & 10 \\
                0 & 12 & 4 & 12 & -5 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L
            =
            \begin{bmatrix}
                1 &   &   &   \\
                -2&  1 &   &   \\
                1 & \underline{\phantom{-3}} & 1 &   \\
                -3& \underline{\phantom{+4}} &   & 1 \\
            \end{bmatrix}
        \end{equation*}
    }

    \only<3>{
        \begin{equation*}
            A_{2}
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                0 & 3 & 1 & 2 & -3 \\
                0 & 0 & 0 & 2 & 1 \\
                0 & 0 & 0 & 4 & 7 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L
            =
            \begin{bmatrix}
                1 &   &   &   \\
                -2& 1 &   &   \\
                1 &-3 & 1 &   \\
                -3& 4 & \underline{\phantom{+2}} & 1 \\
            \end{bmatrix}
        \end{equation*}
    }

    \only<4>{
        \begin{equation*}
            U
            =
            \begin{bmatrix}
                2 & 4 & -1 & 5 & -2 \\
                0 & 3 & 1 & 2 & -3 \\
                0 & 0 & 0 & 2 & 1 \\
                0 & 0 & 0 & 0 & 5 \\
            \end{bmatrix}
        \end{equation*}
        \begin{equation*}
            L
            =
            \begin{bmatrix}
                1 &   &   &   \\
                -2& 1 &   &   \\
                1 &-3 & 1 &   \\
                -3& 4 & 2 & 1 \\
            \end{bmatrix}
        \end{equation*}
    }
\end{frame}


\begin{frame}[c]
    \frametitle{Example 2 recap}

    A simple trick to carry out the algorithm ---
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{LU-02.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Fine the LU factorization of
    \begin{equation*}
        A
        =
        \left[
            \begin{array}{cc}
                6 & 9 \\
                4 & 5 \\
            \end{array}
        \right]
        =
        \left[
            \phantom{
                \begin{array}{cc}
                    6 & 9 \\
                    4 & 5 \\
                \end{array}
            }
        \right]
        \left[
            \phantom{
                \begin{array}{cc}
                    6 & 9 \\
                    4 & 5 \\
                \end{array}
            }
        \right]
        =
        L U
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{When you cannot LU factorized}
    
    \bomb{} Not every matrix can be LU factorized. 

    However, we can always factorize a matrix like
    \begin{equation*}
        A = 
        \left[
            \begin{array}{ccc}
                -1 & 3 & 3 \\
                0 & -1 & 2 \\
                -2 & -1 & 0 \\
            \end{array}
        \right]
        =
        \left[
            \begin{array}{ccc}
                \frac{1}{2} & 1 & 0 \\
                0 & \frac{-2}{7} & 1 \\
                1 & 0 & 0 \\
            \end{array}
        \right]
        \left[
            \begin{array}{ccc}
                -2 & -1 & 0 \\
                0 & \frac{7}{2} & 3 \\
                0 & 0 & \frac{20}{7} \\
            \end{array}
        \right]
        =
        L U
    \end{equation*}
    where $L$ is a unit lower triangular array
    \begin{equation*}
        \left[
            \begin{array}{ccc}
                1 & 0 & 0 \\
                \frac{1}{2} & 1 & 0 \\
                0 & \frac{-2}{7} & 1 \\
            \end{array}
        \right]
    \end{equation*}
    with its rows permuted.

    \emoji{smile} This still helps solving linear systems.
\end{frame}

%\begin{frame}
%    \frametitle{\sweat{} Think-Pair-Share}
%
%    Let $A$ be a lower triangular $n \times n$ matrix with nonzero entries
%    on the diagonal.
%    Show that $A$ is invertible and $A^{-1}$ is also lower triangular.
%
%    \begin{enumerate}
%        \item Why $A$ can be reduced to $I$ using only row replacements and scaling?
%        \item Why applying these operations to $I$ gives a lower triangular array?
%        \item Do you remember the algorithm to find $A^{-1}$?
%    \end{enumerate}
%\end{frame}

\section{2.6 The Leontief Input-Output Model}%

\begin{frame}
    \frametitle{How much do we need?}

    Your country has three industries, \emoji{tv}, \emoji{beer} and
    \emoji{carrot}.
    \begin{table}[]
        \begin{tabular}{@{}c*{3}{w{c}{5em}}@{}}
            \toprule
            \multirow{2}{*}{Purchased from} & \multicolumn{3}{c}{Inputs Consumed per Unit of Output}                                                 \\ \cmidrule(l){2-4}
                               & \multicolumn{1}{c}{\emoji{tv}}  & \multicolumn{1}{c}{\emoji{beer}} & \multicolumn{1}{c}{\emoji{carrot}} \\ \midrule
            \emoji{tv}     & 0.5   & 0.4   & 0.2   \\
            \emoji{beer}  & 0.2   & 0.3   & 0.1   \\
            \emoji{carrot}& 0.1   & 0.1   & 0.3   \\ \bottomrule
        \end{tabular}
    \end{table}
\end{frame}

\begin{frame}[c]
    \frametitle{The question}

    Let
    $
    \bfx
        =
        \begin{bmatrix}
            x_t \\ x_b \\ x_c
        \end{bmatrix}
    $
    be the \alert{production vector} that lists the output of each sector next
    year.

    \pause{}

    Let
    $
    \bfd
        =
        \begin{bmatrix}
            d_t \\ d_b \\ d_c
        \end{bmatrix}
    $
    be the \alert{final demand vector} that lists the demand of products from
    your people next year.

    \pause{}

    Let \alert{intermediate demand} be the need of each sector to produce $\bfd$.

    \pause{}

    Leontief asked if we can find $\bfx$ such that
    \begin{equation*}
        \bfx = \text{intermediate demand} + \bfd.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Consumption vectors}

    Given the following table,
    how much \emoji{tv}, \emoji{beer}, and \emph{carrot} 
    are needed to produce $x_t$ \emoji{tv}?

    \begin{table}[]
        \begin{tabular}{@{}c*{3}{w{c}{5em}}@{}}
            \toprule
            \multirow{2}{*}{Purchased from} & \multicolumn{3}{c}{Inputs Consumed per Unit of Output}                                                 \\ \cmidrule(l){2-4}
                               & \multicolumn{1}{c}{\emoji{tv}}  & \multicolumn{1}{c}{\emoji{beer}} & \multicolumn{1}{c}{\emoji{carrot}} \\ \midrule
            \emoji{tv}     & 0.5   & 0.4   & 0.2   \\
            \emoji{beer}  & 0.2   & 0.3   & 0.1   \\
            \emoji{carrot}& 0.1   & 0.1   & 0.3   \\ \bottomrule
                               & $\bfc_t$  & $\bfc_b$  & $\bfc_c$  \\
        \end{tabular}
    \end{table}

    \cake{} What about $x_b$ \emoji{beer}, and $x_c$ \emoji{carrot}?
\end{frame}

\begin{frame}
    \frametitle{The intermediate demand}
    Then
    \begin{equation*}
        \begin{aligned}
        \text{intermediate demand}
        &
        = x_t \bfc_t + x_b \bfc_b + x_c \bfc_c
        \\
        &
        =
        \begin{bmatrix}
            \bfc_t & \bfc_b & \bfc_c
        \end{bmatrix}
        \begin{bmatrix}
            x_t \\ x_b \\ x_c
        \end{bmatrix}
        = C \bfx,
        \end{aligned}
    \end{equation*}
    where $C$ is called the \alert{consumption} matrix.

\end{frame}

\begin{frame}
    \frametitle{The solution}
    
    Thus, the equation
    \begin{equation*}
        \bfx = \text{intermediate demand} + \bfd.
    \end{equation*}
    is equivalent to
    \begin{equation*}
        \bfx = C \bfx + \bfd
        .
    \end{equation*}
    \think{} Why the solution is
    \begin{flalign*}
        &
        \bfx = (I- C)^{-1} \bfd.
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Back to the example}

    In the example
    \begin{equation*}
        C =
        \begin{bmatrix}
            0.5   & 0.4   & 0.2   \\
            0.2   & 0.3   & 0.1   \\
            0.1   & 0.1   & 0.3   \\
        \end{bmatrix}
        ,
        \qquad
        \bfd
        =
        \begin{bmatrix}
            50 \\ 30 \\ 20
        \end{bmatrix}
        .
    \end{equation*}
    So the solution is
    \begin{equation*}
        \bfx
        =
        (I-C)^{-1} \bfd
        \approx
        \left[
            \begin{array}{c}
                225.93 \\
                118.52 \\
                77.78 \\
            \end{array}
        \right]
    \end{equation*}

    \emoji{wink} Now you should ask the professor one natural question. What is
    that?
\end{frame}

\begin{frame}
    \frametitle{Theorem 11 --- The existence of the solution}

    If the sum of each column in $C$ is \emph{strictly} less than $1$,
    then $(I-C)^{-1}$ exists.

    \laughing{} If it costs more than one unit worth to produce one unit of worth,
    probably we shouldn't do it.
\end{frame}

\begin{frame}
    \frametitle{Theorem 11 --- Idea of the proof}
    \cake{} When is the sum
    \begin{equation*}
        1 + x + x^{2} + x^{3} \dots
        =
        \sum_{i=0}^{\infty} x^{i}
        =
        \frac{1}{1-x}
    \end{equation*}

    The idea for proving Theorem 11 is similar.
    %\begin{equation*}
    %    \begin{aligned}
    %        \bfx & = \bfd + C\bfd + C^{2} \bfd + \dots \\
    %             & = (I + C + C^{2} + \dots ) \bfd \\
    %             & = (I - C)^{-1} \bfd
    %    \end{aligned}
    %\end{equation*}

    %When $C$ satisfies the condition in Theorem 11, we have
    %\begin{equation*}
    %    I + C + C^{2} + \dots
    %    =
    %    (I - C)^{-1}.
    %\end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Approximate solution}

    So we can take a large $m$ and approximate by
    \begin{equation*}
        (I - C)^{-1}
        \approx
        I + C + C^{2} + \dots C^{m}
        .
    \end{equation*}

    Let
    \begin{equation*}
        \dot{\bfx}.
        =
        (I + C + C^{2} + \dots C^{32})\bfd
    \end{equation*}
    Then 
    \begin{equation*}
        \dot{\bfx} - \bfx
        =
        \left[
            \begin{array}{c}
                -0.033 \\
                -0.016 \\
                -0.011 \\
            \end{array}
        \right]
    \end{equation*}

    \hint{} Good presentation topic --- \href{https://www.quantamagazine.org/mathematicians-inch-closer-to-matrix-multiplication-goal-20210323/}{Matrix Multiplication Inches Closer to Mythic Goal}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{2.5}{19, 21, 23}
            \sectionhomework{2.6}{5, 7, 11}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
