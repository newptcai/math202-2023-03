\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture \lecturenum{} --- Linear Transformation}

\begin{document}

\maketitle{}

\lectureoutline{}

\section{1.8 Introduction to Linear Transformations}

\begin{frame}
    \frametitle{How to get such a funny picture?}

    \vspace{2em}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{me-2.jpg}
    \end{figure}
\end{frame}

\subsection{Transformations}

\begin{frame}
    \frametitle{Transformations}

    A \alert{transformation/function/mapping} $T:\dsR^{n}\mapsto\dsR^{m}$
    is a rule that assigns to each $\bfx \in \dsR^{n}$
    a vector $T(\bfx)$ in $\dsR^{m}$.

    $\dsR^{n}$ is the \alert{domain}, and $\dsR^{m}$ is the \alert{codomain}.

    $T(\bfx)$ is the \alert{image} of $\bfx$.

    The set of all images is the \alert{range} of $T$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{linear-transformation-03.png}
    \end{figure}
\end{frame}

%\begin{frame}[t]
%    \frametitle{Matrix transformations}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{linear-transformation-04.png}
%    \end{figure}
%\end{frame}

%\begin{frame}[t]
%    \frametitle{Projection}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\linewidth]{linear-transformation-05.png}
%        \includegraphics[width=0.4\linewidth]{linear-transformation-06.png}
%    \end{figure}
%
%    \astonished{} An $X$-ray photo is projection of human body!
%\end{frame}

\begin{frame}
    \frametitle{Transformation by multiplication}

    The product $A \bfx$ is a transformation from $\bfx$
    to $A \bfx$.

    \begin{exampleblock}{$\bfx \mapsto A \bfx$}
        \begin{equation*}
            \begin{bmatrix}
                4 & -3 & 1 & 3 \\
                2 &  0 & 5 & 1 \\
            \end{bmatrix}
            \begin{bmatrix}
                1 \\
                1 \\
                1 \\
                1 \\
            \end{bmatrix}
            =
            \begin{bmatrix}
                5 \\ 
                8
            \end{bmatrix}
        \end{equation*}
    \end{exampleblock}

    \cake{} Let $T(\bfx)=A \bfx$.
    What are the domain, codomain?
    What do we call $\bfb = T(\bfx)$?

    \think{} What is the range?
\end{frame}

\begin{frame}
    \frametitle{Transformation by multiplication}

    Since $A$ is of size $2 \times 4$, it is a transformation 
    from $\dsR^{4}$ to $\dsR^{2}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{linear-transformation-02.png}%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3 --- Shear transformation}

    Let $A = \begin{bmatrix} 1 & 3 \\ 0 & 1 \end{bmatrix}$.
    The transformation $T(\bfx) = A \bfx$ is called a \alert{shear
    transformation}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-08.png}
    \end{figure}

    \laughing{} \emph{Shearing sheep} refers to the process of cutting off the
    woolly fleece of \emoji{sheep}.
\end{frame}

\subsection{Linear Transformations}%

\begin{frame}
    \frametitle{The definition of linear transformations}

    A transformation $T$ is \alert{linear} if
    for all $\bfu$ $\bfv$
    \begin{enumerate}[(i)]
        \item  $T(\bfu + \bfv) = T(\bfu) + T(\bfv)$,
        \item  $T(c \bfu) = c T(\bfu)$.
    \end{enumerate}

    \cake{} Why the matrix transformation $T(\bfx) = A \bfx$ is \emph{linear}.
\end{frame}

\begin{frame}
    \frametitle{Properties of linear transformations}

    If $T$ is a \emph{linear transformation}, then
    for all vectors $\bfu, \bfv$ and scalars $c, d$,
    \begin{enumerate}
        \item[(iii)] $T(\bfzero) = \bfzero$,
        \item[(iv)] $T(c \bfu + d \bfv) = c T (\bfu) + d T(\bfv)$,
    \end{enumerate}

    \hint{} These follow from (i) and (ii).
\end{frame}

\begin{frame}
    \frametitle{One more property}
    If $T$ is a \emph{linear transformation},
    then for all vectors $\bfv_1, \cdots, \bfv_{p}$
    and scalars $c_1, \cdots, c_p$.
    \begin{enumerate}
        \item[(v)] $T(c_1 \bfv_1 + \cdots c_{p} \bfv_p)
            = c_1 T(\bfv_1) + \cdots + c_{p} T(\bfv_p)$,
    \end{enumerate}

    \hint{} Linear combinations in, linear combinations out.
\end{frame}

\begin{frame}
    \frametitle{Example 4 --- Contraction and dilation}

    Let $T(\bfx) = r \bfx$. Show that $T$ is linear.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-10.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 5 --- Rotation}

    Let
    \begin{equation*}
        T(\bfx) =
        \begin{bmatrix}
            0 & -1 \\
            1 & 0 \\
        \end{bmatrix}
        \begin{bmatrix}
            x_1 \\ x_2
        \end{bmatrix}
    \end{equation*}
    What are $T(\bfu), T(\bfv)$ and $T(\bfu + \bfv)$ with
    \begin{equation*}
        \bfu =
        \begin{bmatrix}
            4 \\ 1
        \end{bmatrix}
        ,
        \qquad
        \bfv =
        \begin{bmatrix}
            2 \\ 3
        \end{bmatrix}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{linear-transformation-12.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Summary of 1.8}

    \begin{itemize}
        \item[\emoji{arrow-right}] What are transformations, domains, codomains,
            etc.?
        \item[\emoji{straight-ruler}] What are linear transformations and their properties?
        \item[\emoji{multiply}] $\bfx \to A \bfx$ is a linear transformation.
        \item[\emoji{sheep}] What shear transformations,
            dilation, contractions, and rotations?
    \end{itemize}
\end{frame}

\section{1.9 The Matrix of a Linear Transformation}

\subsection{Standard Matrix}

\begin{frame}
    \frametitle{Example 1 --- Identity matrix}
    Let $\bfe_{1} = \begin{bmatrix} 1 \\ 0 \end{bmatrix}$
    and $\bfe_{2} = \begin{bmatrix} 0 \\ 1 \end{bmatrix}$.
    If $T$ is a \emph{linear transformation} with
    \begin{equation*}
        T(\bfe_{1}) =
        \begin{bmatrix}
            5 \\ -7 \\ 2
        \end{bmatrix}
        ,
        \qquad
        T(\bfe_{2}) =
        \begin{bmatrix}
            -3 \\ 8 \\ 0
        \end{bmatrix}
    \end{equation*}
    then what is
    \begin{flalign*}
        & T\left( \begin{bmatrix} x_1 \\ x_2 \end{bmatrix} \right) = &
    \end{flalign*}

    \hint{} In other words, $T:\dsR^{2}$ can be written as $T(x) = A \bfx$.

    \hint{} Note that $[\bfe_{1} \, \bfe_{2}] = I_{2}$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 10 --- Standard matrix}

    Let $T : \dsR^{n} \mapsto \dsR^{n}$ be a linear transformation.

    Let $\veclist[n]{e}$ be the columns of $I_{n}$.

    Let
    \begin{equation*}
        A
        =
        \begin{bmatrix}
            T(\bfe_1) &
            T(\bfe_2) &
            \cdots &
            T(\bfe_n)
        \end{bmatrix}
    \end{equation*}
    Then $T(\bfx) = A \bfx$ for every $\bfx \in \dsR^{n}$

    The matrix $A$ is called the \alert{standard matrix} of $T$.
\end{frame}

\begin{frame}
    \frametitle{Example 3 --- Rotation}

    Find the standard matrix $A$ for $T:\dsR^{2} \mapsto \dsR^{2}$
    which rotates a vector by degree $\phi$.

    \hint{} First check it is a linear transformation.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{linear-transformation-16.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Reflection}

    What is the \emph{standard matrix} for reflection over $x_{1}$ axis?

    \includegraphics[width=0.4\linewidth]{linear-transformation-17.png}

    See the textbook for more examples.

    Try this \href{https://observablehq.com/@yurivish/example-of-2d-linear-transforms}{live demonstration}.
\end{frame}

\begin{frame}
    \frametitle{Think-Pair-Share \think{}}

    %(a) Are these two mappings linear? Why or why not?
    %\begin{equation*}
    %    T_{1}
    %    \left(
    %        \begin{bmatrix}
    %            x_1 \\ x_2  \\ x_3
    %        \end{bmatrix}
    %    \right) =
    %    \begin{bmatrix}
    %            x_1 \\ x_2  \\ -x_3
    %    \end{bmatrix}
    %    ,
    %    \qquad
    %    T_{2}
    %    \left(
    %        \begin{bmatrix}
    %            x_1 \\ x_2
    %        \end{bmatrix}
    %    \right) =
    %    \begin{bmatrix}
    %        4 x_1 -2 x_2 \\ 3 \abs{x_2}
    %    \end{bmatrix}
    %    ,
    %\end{equation*}

    %\pause{}

    %(b) Find the standard matrix $A$ for $T(\bfx) = r \bfx$.

    %\pause{}

    %(c) 
    What is the standard matrix from the linear transformation 
    which maps the left to the right?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.43\linewidth]{me.jpg}
        \includegraphics[width=0.43\linewidth]{me-1.jpg}
    \end{figure}
\end{frame}

\subsection{Existence and Uniqueness Questions}

\begin{frame}
    \frametitle{Onto/surjective mapping}

    $T : \dsR^{n} \mapsto \dsR^{m}$ is \alert{onto} if there is \emph{at least one}
    $\bfx$ such that $T(\bfx) = \bfb$ for all $\bfb \in \dsR^{m}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=\linewidth]{linear-transformation-20.png}
    \end{figure}

    \laughing{} Why are onto mappings never late? They have surjective time management.

    %\cake{} Which of the following are \emph{onto} mappings?
    %\begin{equation*}
    %    \begin{bmatrix}
    %        1 & 0 & 0 \\
    %        0 & 1 & 0 \\
    %        0 & 0 & 1 \\
    %    \end{bmatrix}
    %    \hspace{3cm}
    %    \begin{bmatrix}
    %        1 & 0 \\
    %        0 & 1 \\
    %        0 & 0 \\
    %    \end{bmatrix}
    %\end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Onto mapping and the existence of solutions}

    Let $A$ be the \emph{standard matrix} of an onto mapping $T$.

    \cake{} Is it true linear system $A \bfx = \bfb$ 
    is consistent for all $\bfb \in \dsR^{m}$.
\end{frame}

\begin{frame}
    \frametitle{One-to-one/injective mapping}

    $T : \dsR^{n} \mapsto \dsR^{m}$ is \alert{one-to-one} if there is
    \alert{at most one} $\bfx$ such that
    $T(\bfx) = \bfb$ for all $\bfb \in \dsR^{m}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{linear-transformation-22.png}
    \end{figure}

    %\cake{} Which of the following are \emph{onto} mappings?
    %\begin{equation*}
    %    \begin{bmatrix}
    %        1 & 0 & 0 \\
    %        0 & 1 & 0 \\
    %        0 & 0 & 1 \\
    %    \end{bmatrix}
    %    \hspace{3cm}
    %    \begin{bmatrix}
    %        1 & 0 \\
    %        0 & 1 \\
    %        0 & 0 \\
    %    \end{bmatrix}
    %\end{equation*}
\end{frame}

\begin{frame}
    \frametitle{One-to-one and the uniqueness of solutions}

    Let $A$ be the \emph{standard matrix} of a one-to-one mapping $T$.

    Then for every $\bfb \in \dsR^{m}$, 
    the linear system $A \bfx = \bfb$ has at most one solution.

    \laughing{} Why is the following joke inaccurate mathematically?

    \begin{quote}
        Why did the one-to-one function get a job as a detective?
        Because it always found a unique solution to the case!
    \end{quote}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise}
%
%    \sweat{} Is the following a one-to-one mapping?
%    \begin{equation*}
%        \begin{bmatrix}
%            1 & -4 & 8 & 1 \\
%            0 & 2 & -1 & 3 \\
%            0 & 0 & 0 & 5\\
%        \end{bmatrix}
%    \end{equation*}
%
%    \hint{} Use the following
%    \begin{block}{Theorem 4}
%        The followings are equivalent
%        \begin{enumerate}
%        \item[a.] For each $\bfb$, the equation $A \bfx = \bfb$ has a solution, i.e., it is
%            \emph{consistent}.
%        \item[d.] $A$ has pivot position in every row.
%        \end{enumerate}
%    \end{block}
%\end{frame}

\begin{frame}
    \frametitle{Theorem 11 --- When is $T$ one-to-one?}

    Let $T: \dsR^{n} \mapsto \dsR^{m}$ be a linear transformation.
    $T$ is \emph{one-to-one} if and only if $T(\bfx) = \bfzero$
    has only the trivial solution.

    \hint{} Use 
    \begin{equation*}
        P \iff Q \sim (P \imp Q) \wedge (Q \imp P), 
        \qquad
        Q \imp P \sim \neg P \imp \neg Q
        .
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{When is $T$ one-to-one?}
%
%    %\think{} Can you prove it using the following?
%
%    %\begin{block}{Theorem 11}
%    %    $T$ is \emph{one-to-one} if and only if $T(\bfzero) = \bfzero$
%    %    has only the trivial solution.
%    %\end{block}
%\end{frame}

\begin{frame}
    \frametitle{Theorem 12 --- When is $T$ onto?}

    Let $T: \dsR^{m} \mapsto\dsR^{n}$ be a linear transformation,
    and let $A = [\bfa_{1} \, \bfa_{2} \, \dots \, \bfa_{n}]$ be its standard matrix.

    \only<1>{
    (a) Then $T$ is onto if and only if $\Span{\bfa_{1}, \dots, \bfa_{n}} = \dsR^{m}$.

    Proof of (a) by Theorem 4 (1.4).
    }

    \only<2>{
    (b) Then $T$ is one-to-one if and only if $\bfa_{1}, \dots, \bfa_{n}$ are \emph{linearly
    independent}.

    Proof of (b) by Theorem 11.
    }
\end{frame}

\begin{frame}
    \frametitle{Example 5}

    Is $T$ one-to-one?

    \begin{equation*}
        T\left(
            \begin{bmatrix}
                x_1 \\ x_2
            \end{bmatrix}
        \right)
        =
        \begin{bmatrix}
            3 x_1 + x_2 \\
            5 x_1 + 7 x_2 \\
            x_1 + 3 x_2
        \end{bmatrix}
    \end{equation*}

    \pause{}

    Is $T$ onto?
    \begin{block}{\hint{} Theorem 4 (1.4)}
        If $A$ is a $m \times n$ matrix, with columns
        $\bfa_1,\dots, \bfa_n$, and $\bfx \in \dsR^n$, then
        the followings are equivalent
        \begin{enumerate}
        \item[c.] $\dsR^{m} = \Span{\bfa_1, \dots, \bfa_n}$.
        \item[d.] $A$ has a pivot position in every row.
        \end{enumerate}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Summary of 1.9}
    Let $T$ be a linear transformation.
    \begin{itemize}
        \item[\emoji{orange-square}] What is the standard matrix of $T$?
        \item[\emoji{down-arrow}] When is $T$ an onto mappings?
        \item[\emoji{left-right-arrow}] When is $T$ an one-to-one mappings?
        \item[\question{}] When do solutions of $T(\bfx) = \bfb$ exist?
        \item[\emoji{unicorn}] When are solutions of $T(\bfx) = \bfb$ unique?
    \end{itemize}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Recommended exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \sectionhomework{1.8}{33, 35, 39, 41}
            \sectionhomework{1.9}{37, 39, 41, 43}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
